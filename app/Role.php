<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = ['name', 'permissions'];

    public function getPermissionsAttribute($value){
        return json_decode($value);
    }

	public static function criar($input){

		 DB::table('roles')->insert([
			  [
					'name' => $input['name'],
					'slug' => $input['slug']
			  ]
		 ]);

		 $id_role = DB::getPdo()->lastInsertId();


		 return $id_role;
	}

	public static function editar($input, $id){

		 $updateArray = [
			  'name' => $input['name'],
			  'slug' => $input['slug'],
		 ];

		 DB::table('roles')->where('id', $id)
		 ->update($updateArray);

		 return $id;
	}

	public function permissionsRole(){
		return $this->hasMany('App\Permission', 'id_role');
	}

	public function permissionsList(){
		return $this->permissionsRole()->lists('id_modulo')->toArray();
    }
    
    public function users(){
        return $this->belongsToMany('App\User', 'role_users', 'role_id', 'user_id')->withTimestamps();
    }
}
