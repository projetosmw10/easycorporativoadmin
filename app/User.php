<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use DB;
use Storage;

class User extends Authenticatable {
    use Notifiable;
    // use \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table = 'users';
    protected $dates = ['deleted_at', 'premium_expire_at'];

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = ['first_name', 'permissions', 'last_name', 'avatar', 'email', 'password', 'telefone', 'premium_expire_at', 'premium_expiring_notification_at', 'objetivo_descricao', 'genero', 'data_nascimento', 'altura', 'atual_kg', 'inicio_kg', 'objetivo_kg', 'objetivo_kcal', 'cpf'];
    protected $appends = ['premium'];
    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getPremiumAttribute() {
        return $this->isPremium();
    }

    public function getAvatarAttribute($value) {
        https://laravel.com/docs/5.6/filesystem#file-visibility
        return $value ? asset($value) : '';
    }

    public function getPermissionsAttribute($value) {
        return json_decode($value);
    }

    public function roleUser() {
        return $this->hasOne('App\RoleUser', 'user_id');
    }

    public function roles() {
        return $this->belongsToMany('App\Role', 'role_users', 'user_id', 'role_id')->withTimestamps();
    }

    // Sistema duo

    public function notifications() {
        return $this->belongsToMany('\App\Modules\Notifications\Models\Notifications', 'notification_user', 'user_id', 'notification_id');
    }

    // Relação com a mesma tabela usuário(cliente) tem 1 responsável
    public function conteudos() {
        return $this->belongsToMany('\App\Modules\Conteudos\Models\Conteudos', 'conteudo_user', 'user_id', 'conteudo_id')->withPivot('favorited_at', 'watched_at');
        ;
    }

    public function onesignal() {
        return $this->hasMany('\App\Modules\Onesignal\Models\Onesignal', 'user_id');
    }

    public function codes() {
        return $this->hasMany('\App\Modules\Codes\Models\Codes', 'user_id');
    }

    public function pessoa() {
        return $this->hasOne('\App\Modules\Pessoas\Models\Pessoas', 'user_id');
    }

    public function empresa() {
        return $this->hasOne('\App\Modules\Empresas\Models\Empresas', 'user_id');
    }

    public function isPremium() {
        $this->loadMissing('pessoa');
        // caso nao tenha code_id não é
        if(!$this->pessoa) {
            return false;
        }
        if(!$this->pessoa->code_id) {
            return false;
        }
        $this->pessoa->loadMissing('code');

        // caso n tenha code nao é
        if(!$this->pessoa->code) {
            return false;
        }

        return !$this->pessoa->code->isExpired();
    }

    public function jwtTokens() {
        return $this->hasMany('App\JWTTokens', 'user_id');
    }

    /** SPORTS */
    public function pdfs() {
        return $this->belongsToMany('\App\Modules\Pdfs\Models\Pdfs', 'pdf_user', 'user_id', 'pdf_id')->withPivot('favorited_at', 'read_at');
    }

    /** NUTRI */
    public function alimentos() {
        return $this->belongsToMany('\App\Modules\Alimentos\Models\Alimentos', 'alimento_user', 'user_id', 'alimento_id')->withPivot('quantidade', 'tipo', 'data', 'favorited_at');
    }

    public function exercicios() {
        return $this->belongsToMany('\App\Modules\Exercicios\Models\Exercicios', 'exercicio_user', 'user_id', 'exercicio_id')->withPivot('quantidade', 'data');
    }

    public function consultas() {
        return $this->hasMany('\App\Modules\Medicos\Models\Medicos', 'user_id');
    }

    public function agua() {
        return $this->hasMany('\App\Agua', 'user_id');
    }
}
