<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(){

        $this->app->singleton('\App\Services\ConteudosService', function(){
            return new \App\Services\ConteudosService;
        });
        $this->app->singleton('\App\Services\NotificationService', function(){
            return new \App\Services\NotificationService;
        });
    }
}
