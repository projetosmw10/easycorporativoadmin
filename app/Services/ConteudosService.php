<?php

namespace App\Services;

use App\Modules\Conteudos\Models\Conteudos;
use App\Modules\Tags\Models\Tags;
use App\Modules\Categorias\Models\Categorias;
use App\Modules\Topicos\Models\Topicos;
use App\User;
use App\JWTUser;
use DB;
use App\Services\NotificationService;

class ConteudosService {
    public function queryBuilder($queryString = [], $relations = []) {
        if (!empty($relations['tag_id'])) {
            $items = Tags::select('id')->findOrFail($relations['tag_id'])->conteudos();
        } elseif (!empty($relations['categoria_id'])) {
            $items = Categorias::select('id')->findOrFail($relations['categoria_id'])->conteudos();
        } elseif (!empty($relations['topico_id'])) {
            $items = Topicos::select('id')->findOrFail($relations['topico_id'])->conteudos();
        } elseif (!empty($relations['user_id'])) {
            $items = User::select('id')->findOrFail($relations['user_id'])->conteudos();
        } else {
            $items = Conteudos::select('conteudos.*');
        }

        if (!empty($queryString)) {
            $items->where(function ($q) use ($queryString) {
                $fillable = (new Conteudos())->getFillable();
                foreach ($queryString as $qryStr => $val) {
                    foreach($fillable as $f) {
                        if($qryStr === $f) {
                            $q->where($f, $val);
                        }
                    }
                    if ($qryStr === 'search') {
                        $q->orWhere('titulo', 'like', '%' . $val . '%');
                        $q->orWhere('id', $val);
                    } elseif (in_array($qryStr, $fillable)) {
                        $q->orWhere($qryStr, $val);
                    }
                }
            });
        }

        // Order by deve ser passado como query string da seguinte maneira orderBy=created_at.asc,titulo.desc, por padrão é asc, ou seja se não for informado ele vai assumir asc
        if (isset($queryString['orderBy'])) {
            $orderBy = explode(',', $queryString['orderBy']);
            foreach ($orderBy as $oBy) {
                $oBy = explode('.', $oBy);
                $oBy[1] = $oBy[1] ?? 'asc';
                $items->orderBy($oBy[0], $oBy[1]);
            }
        }
        return $items;
    }
}
