<?php

namespace App\Services;
use Validator;
use Illuminate\Validation\Rule;


class CieloService {
    // Brands that cielo accepts
    public static $brands = ['Visa', 'Master', 'Diners', 'Amex', 'Discover' ,'Hipercard', 'Elo', 'JCB', 'Aura'];
    public static $rulesBoleto = [
        'MerchantOrderId' => 'required',
        'Customer.Name' => 'required|max:34',
        'Customer.Identity' => 'required',
        'Customer.Address.ZipCode' => 'required',
        'Customer.Address.Country' => 'required',
        'Customer.Address.State' => 'required',
        'Customer.Address.City' => 'required',
        'Customer.Address.District' => 'required',
        'Customer.Address.Street' => 'required',
        'Customer.Address.Number' => 'required|numeric',
        'Payment.Amount' => 'required|numeric'
    ];


    // Validation rulesCreditCard necessary before reaching cielo's endpoint
    public static $rulesCreditCard = [];
    public static $rulesCreditCardMessages = [];
    public static $rulesCreditCardAttributes = [
        'MerchantOrderId' => 'Nº do pedido',
        'Customer.Name' => 'nome',
        'Payment.Amount' => 'valor',
        'Payment.Installments' => 'parcelas',
        'Payment.SoftDescriptor' => 'descrição cartao de crédito',
        'Payment.CreditCard.CardNumber' => 'número do cartao de crédito',
        'Payment.CreditCard.Holder' => 'nome do proprietário do cartão de crédito',
        'Payment.CreditCard.ExpirationDate' => 'data de validade do cartão de crédito',
        'Payment.CreditCard.SecurityCode' => 'código de segurança do cartão de crédito',
        'Payment.CreditCard.Brand' => 'bandeira do cartão de crédito'
    ];

    // cielo status transacional https://developercielo.github.io/manual/cielo-ecommerce#status-transacional
    public static $status = [
        0 => [
            'en' => 'NotFinished',
            'pt' => 'Aguardando pagamento',
            'payment_method' => 'ALL',
            'description' => 'Aguardando atualização de status'
        ],
        1 => [
            'en' => 'Authorized',
            'pt' => 'Pagamento autorizado',
            'payment_method' => 'ALL',
            'description' => 'Pagamento apto a ser capturado ou definido como pago'
        ],
        2 => [
            'en' => 'PaymentConfirmed',
            'pt' => 'Pagamento aprovado',
            'payment_method' => 'ALL',
            'description' => 'Pagamento confirmado e finalizado'
        ],
        3 => [
            'en' => 'Denied',
            'pt' => 'Pagamento negado',
            'payment_method' => 'CC + CD + TF',
            'description' => 'Pagamento negado por Autorizador'
        ],
        10 => [
            'en' => 'Voided',
            'pt' => 'Pagamento cancelado',
            'payment_method' => 'ALL',
            'description' => 'Pagamento cancelado'
        ],
        11 => [
            'en' => 'Refunded',
            'pt' => 'Pagamento estornado',
            'payment_method' => 'CC + CD',
            'description' => 'Pagamento cancelado após 23:59 do dia de autorização'
        ],
        12 => [
            'en' => 'Pending',
            'pt' => 'Aguardando instituição financeira',
            'payment_method' => 'ALL',
            'description' => 'Aguardando Status de instituição financeira'
        ],
        13 => [
            'en' => 'Aborted',
            'pt' => 'Pagamento abortado',
            'payment_method' => 'ALL',
            'description' => 'Pagamento cancelado por falha no processamento ou por ação do AF'
        ],
        20 => [
            'en' => 'Scheduled',
            'pt' => 'Pagamento agendado',
            'payment_method' => 'CC',
            'description' => 'Recorrência agendada'
        ],
    ];
    

    public static function boot() {
        CieloService::$rulesCreditCard = [
            'MerchantOrderId' => 'required',
            'Customer.Name' => 'required',
            'Payment.Amount' => 'required|numeric',
            'Payment.Installments' => 'required|integer',
            'Payment.SoftDescriptor' => 'max:13',
            'Payment.CreditCard.CardNumber' => 'required|min:16|max:19',
            'Payment.CreditCard.Holder' => 'required',
            'Payment.CreditCard.ExpirationDate' => 'required|date_format:m/Y',
            'Payment.CreditCard.SecurityCode' => 'required|numeric',
            'Payment.CreditCard.Brand' => [
                'required',
                'in' => Rule::in(CieloService::$brands)
            ]
        ];
    }

    public static function creditCard($data) {
        $validator = Validator::make($data, CieloService::$rulesCreditCard, CieloService::$rulesCreditCardMessages, CieloService::$rulesCreditCardAttributes);
        if ($validator->fails()) {
            throw new \Exception($validator->messages()->first());
       }

        $body = [
            'MerchantOrderId' => $data['MerchantOrderId'],
            'Customer' => [
                'Name' => $data['Customer']['Name']
            ],
            'Payment' => [
                'Type' => 'CreditCard',
                'Amount' => (int) (((float) $data['Payment']['Amount']) * 100),
                'Installments' => (int) $data['Payment']['Installments'],
                // "Installments" => '1',
                'Capture' => true,
                // 'ReturnUrl' => url('cielo/sucesso'),
                'SoftDescriptor' => $data['Payment']->SoftDescriptor ?? config('cielo.SoftDescriptor'), //Texto que aparece na fatura do cartao
                'CreditCard' => [
                    'CardNumber' => preg_replace('/\D/', '', $data['Payment']['CreditCard']['CardNumber']),
                    'Holder' => $data['Payment']['CreditCard']['Holder'],
                    'ExpirationDate' => $data['Payment']['CreditCard']['ExpirationDate'],
                    'SecurityCode' => $data['Payment']['CreditCard']['SecurityCode'],
                    'Brand' => $data['Payment']['CreditCard']['Brand']
                ]
            ]
        ];

        $json_body = json_encode($body);

        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', config('cielo.RequestUrl').'/1/sales/', [
            'headers' => [
                'MerchantId' => config('cielo.MerchantId'),
                'MerchantKey' => config('cielo.MerchantKey'),
                'Content-Length' => strlen($json_body)
            ],
            'json' => $body
        ]);
        

        $response = json_decode($response->getBody()->getContents(), true);

        // faz a captura no painel da cielo
        try {
            $client->request('PUT', config('cielo.RequestUrl') . '/1/sales/' . $response['Payment']['PaymentId']  .'/capture', [
                'headers' => [
                    'MerchantId' => config('cielo.MerchantId'),
                    'MerchantKey' => config('cielo.MerchantKey')
                ],
                'json' => [
                    'PaymentId' => $response['Payment']['PaymentId']
                ]
            ]);
        } catch (\Exception $e) {
            // throw $e;
        }
        
        return $response;

    
    }


    public static function getStatus($code) {
        return CieloService::$status[$code] ?? null;
    } 


    public static function getCCBrand($number) {
        $brands = array(
            'Visa'       => '/^4\d{12}(\d{3})?$/',
            'Master' => '/^(5[1-5]\d{4}|677189)\d{10}$/',
            'Diners'     => '/^3(0[0-5]|[68]\d)\d{11}$/',
            'Discover'   => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
            'Elo'        => '/^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})$/',
            'Amex'       => '/^3[47]\d{13}$/',
            'JCB'        => '/^(?:2131|1800|35\d{3})\d{11}$/',
            'Aura'       => '/^(5078\d{2})(\d{2})(\d{11})$/',
            'Hipercard'  => '/^(606282\d{10}(\d{3})?)|(3841\d{15})$/',
            'Maestro'    => '/^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/',
        );
        foreach ($brands as $_brand => $regex ) {
            if ( preg_match( $regex, $number ) ) {
                return $brand = $_brand;
            }
        }
    }
}

CieloService::boot();