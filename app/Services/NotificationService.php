<?php
namespace App\Services;
use App\Modules\Notifications\Models\Notifications;
use App\Modules\Onesignal\Models\Onesignal as OnesignalModel;
use App\User;
use OneSignal;

use Illuminate\Support\Facades\Log;

class NotificationService {   


    public function sendToUsers(Array $extra, Array $ids, $headings, $subtitle, $contents = null) {
        // encontra o player_ids dos Users
        $player_ids = $this->getUsersPlayerIds($ids);
        $this->send($extra, $player_ids, $headings, $subtitle);
    }


    public function sendToUsersAndInsert(Array $extra, Array $ids, $headings, $subtitle, $contents = null) {
        
        $player_ids = $this->getUsersPlayerIds($ids);
        $this->sendAndInsert($extra, $player_ids, $headings, $subtitle, $contents);
    }

    public function sendAndInsert(Array $extra, Array $ids, $headings, $subtitle, $contents = null){
        $contents = $contents ?? $subtitle;
        $this->send($extra, $ids, $headings, $subtitle); 
        $notification = new Notifications([
            'params' => json_encode([
                // 'template_id' => '73f35af7-5697-46a0-9a98-fab7da83c57a',
                'include_player_ids' => $ids,
                'headings' => $headings,
                'subtitle' => $subtitle,
                'contents' => $contents
            ]),
            'titulo' => $headings,
            'descricao' => $contents
        ]);

        $notification->save();
        $user_ids = OnesignalModel::select('user_id')->whereIn('player_id', $ids)->get()->toArray();
        $user_ids = array_column($user_ids, 'user_id');
        $notification->users()->sync($user_ids);   

             
    }

    public function getUsersPlayerIds(Array $ids) {
        $player_ids = OnesignalModel::select('player_id')
                        ->whereIn('user_id', $ids)
                        ->get()
                        ->toArray();
        return array_column($player_ids, 'player_id');
    }

    public function send(Array $extra, Array $ids, $headings, $subtitle, $contents = null){
        $contents = $contents ?? $subtitle;
        $ids = array_values(array_unique(array_filter($ids)));
        OneSignal::setParam('data', $extra)
                   ->setParam('include_player_ids', $ids)
                   ->setParam('headings', ['en' => $headings])
                   ->setParam('subtitle', ['en' => $subtitle])
                   ->setParam('contents', ['en' => $contents])
                   ->sendNotificationCustom(['include_player_ids' => $ids]);
    }
    
}

