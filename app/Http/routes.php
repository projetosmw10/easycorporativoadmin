<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Redireciona para a pasta com o front /app
Route::get('', function () { return redirect('/admin'); });

// Registration

Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::get('register', 'RegistrationController@create');
    Route::post('register', ['as' => 'registration.store', 'uses' => 'RegistrationController@store']);
});

Route::group(['middleware' => ['auth']], function () {
    //Route::resource('admin/dashboard', ['uses' => 'Admin\AdminController@getHome']);
    // Route::resource('admin/dashboard', 'Admin\AdminController', ['only' => ['getHome']]);
});

Route::group(['middleware' => ['auth']], function () {
    Route::get('admin', ['as' => 'admin_dashboard', 'uses' => 'Admin\AdminController@getHome']);
    Route::get('admin/dashboard', ['as' => 'admin_dashboard', 'uses' => 'Admin\AdminController@getDashboard']);
});
Route::post('admin/getcep', ['uses' => 'Admin\AdminController@getCep']);

// Authentication
    Route::get('admin/login', ['as' => 'admin/login', 'middleware' => 'guest', 'uses' => 'Auth\AuthController@create']);
    Route::get('admin/logout', ['as' => 'admin/logout', 'uses' => 'Auth\AuthController@destroy']);
    Route::resource('admin/login', 'Auth\AuthController', ['only' => ['create', 'store', 'destroy']]);

// Forgotten Password
    Route::get('admin/forgot_password', 'Auth\PasswordController@getEmail');
    Route::post('admin/forgot_password', 'Auth\PasswordController@postEmail');
    Route::get('admin/reset_password/{token}', 'Auth\PasswordController@getReset');
    Route::post('admin/reset_password/{token}', 'Auth\PasswordController@postReset');

// Standard User Routes
Route::group(['middleware' => ['auth', 'standardUser']], function () {
    Route::get('userProtected', 'StandardUser\StandardUserController@getUserProtected');
    Route::resource('profiles', 'StandardUser\UsersController', ['only' => ['show', 'edit', 'update']]);
});
// Admin Routes
Route::group(['middleware' => ['auth', 'admin']], function () {
    Route::resource('admin/profiles', 'Admin\AdminUsersController', ['only' => ['index', 'show', 'edit', 'update', 'destroy']]);
});

// ==============================
// Configurações (Admin) routes
// ==============================
Route::get('admin/informacoes-basicas', 'Admin\BasicInfoController@index');
Route::post('admin/informacoes-basicas/save', 'Admin\BasicInfoController@save');
Route::get('admin/gerador', 'Admin\GeradorController@index');
Route::get('admin/gerador/add', 'Admin\GeradorController@add');
Route::get('admin/gerador/edit/{id}', 'Admin\GeradorController@edit');
Route::get('admin/gerador/delete/{id}', 'Admin\GeradorController@delete');
Route::post('admin/gerador/save', 'Admin\GeradorController@save');
Route::get('admin/roles', 'Admin\RoleController@index');
Route::get('admin/roles/add', 'Admin\RoleController@add');
Route::get('admin/roles/edit/{id}', 'Admin\RoleController@edit');
Route::get('admin/roles/delete/{id}', 'Admin\RoleController@delete');
Route::post('admin/roles/save', 'Admin\RoleController@save');
Route::post('admin/campo-modulo/delete/{id}', 'Admin\CampoModuloController@delete');

// ==============================
// Tipo Módulo routes
// ==============================
Route::get('admin/tipo-modulo', 'Admin\TipoModuloController@index');
Route::get('admin/tipo-modulo/add', 'Admin\TipoModuloController@add');
Route::get('admin/tipo-modulo/edit/{id}', 'Admin\TipoModuloController@edit');
Route::get('admin/tipo-modulo/delete/{id}', 'Admin\TipoModuloController@delete');
Route::post('admin/tipo-modulo/save', 'Admin\TipoModuloController@save');

// ================================
// UserGroup routes BEGINING
// ================================

Route::get('/admin/busca', 'Admin\AdminController@processBuscar');

// ================================
// User routes BEGINING
// ================================
Route::get('admin/users', 'Admin\UserController@index');
Route::get('admin/users/add', 'Admin\UserController@add');
Route::get('admin/users/edit/{id}', 'Admin\UserController@edit');
Route::post('admin/users/save', 'Admin\UserController@save');
Route::get('admin/users/delete/{id}', 'Admin\UserController@delete');
Route::post('admin/users/upload', 'Admin\UserController@upload_image');
Route::post('admin/users/crop', 'Admin\UserController@crop_image');

Route::get('admin/bad_permissions', 'Admin\AdminController@bad_permissions');

// ================================
// Auth routes BEGINING
// ================================
//Route::auth();

// ================================
// Home routes BEGINING
// ================================

// ================================
// Telegram routes BEGINING
// ================================
Route::get('telegram/get-updates', 'TelegramController@getUpdates');
Route::post('telegram/send-message', 'TelegramController@postSendMessage');
//Route::get('send-message','TelegramController@getSendMessage');

// API API API
// API API API
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('login', 'App\Http\Controllers\JWTAuthController@authenticate');
    $api->post('register', 'App\Http\Controllers\JWTAuthController@register');
    $api->post('forgot-password', 'App\Http\Controllers\JWTAuthController@forgotPassword');

    $api->get('teste', 'App\Http\Controllers\Api\CategoriasController@teste');
    // NOTIFICAÇÕES NOTIFICAÇÕES NOTIFICAÇÕES
    $api->group(['prefix' => 'notificacoes'], function ($api) {
        $api->get('premium-expirando', 'App\Http\Controllers\Api\ApiController@premiumExpirando');
    });

    $api->get('certificados/gerar/{id}', 'App\Http\Controllers\Api\CertificadosController@gerar');

    // ADMIN ADMIN ADMIN
    $api->group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'App\Http\Controllers\Api'], function ($api) {
        $api->get('conteudos', 'ConteudosController@adminIndex');
        // CATEGORIAS CATEGORIAS CATEGORIAS
        $api->group(['prefix' => 'categorias'], function ($api) {
            $api->post('ordem', 'CategoriasController@adminOrdem');
            $api->group(['prefix' => '{categoria_id}/topicos'], function ($api) {
                $api->post('ordem', 'TopicosController@adminOrdem');
            });
        });

        // CONTEUDOS CONTEUDOS CONTEUDOS
        $api->group(['prefix' => 'conteudos'], function ($api) {
            $api->post('ordem', 'ConteudosController@adminOrdem');
        });

        // DICAS DICAS DICAS
        $api->group(['prefix' => 'dicas'], function ($api) {
            $api->post('ordem', 'DicasController@adminOrdem');
        });

        // PDFS PDFS PDFS
        $api->group(['prefix' => 'pdfs'], function ($api) {
            $api->post('ordem', 'PdfsController@adminOrdem');
        });
    });

    // CODES CODES CODES
    $api->group(['prefix' => 'codigos', 'namespace' => 'App\Http\Controllers\Api'], function ($api) {
        $api->get('', 'CodesController@index');
        $api->post('', 'CodesController@add');
        $api->get('{id}', 'CodesController@detail');
        // $api->delete('{id}', 'CodesController@delete');
    });

    $api->group(['namespace' => 'App\Http\Controllers\Api', 'middleware' => 'checkIsPremium'], function ($api) {

        
        // FRASES FRASES FRASES
        $api->group(['prefix' => 'frases'], function ($api) {
            $api->get('', 'FrasesController@index');
        });
        // APLICATIVOS APLICATIVOS APLICATIVOS
        $api->get('aplicativos', 'AplicativosController@index');
        $api->get('aplicativos2', 'AplicativosController@index2');

        // CONHEÇA CONHECA CONHEÇA CONHECA CONHEÇA CONHECA
        $api->group(['prefix' => 'conheca'], function ($api) {
            $api->get('', 'ConhecaController@index');
        });
        // INFORMAÇÕES INFORMAÇÕES INFORMAÇÕES
        $api->group(['prefix' => 'informacoes'], function ($api) {
            $api->get('', 'InformacoesController@index');
        });
        
    });


    $api->group(['middleware' => ['jwtauth', 'checkIsPremium'], 'namespace' => 'App\Http\Controllers\Api'], function ($api) {

        // CONTATO CONTATO CONTATO
        $api->group(['prefix' => 'contato'], function ($api) {
            $api->post('', 'ContatoController@save');
        });


        // CERTIFICADO CERTIFICADO CERTIFICADO
        $api->post('certificados/pagar', 'CertificadosController@pagar');
        $api->get('certificados', 'CertificadosController@index');
        
        /** SPORTS */
        // PDFS PDFS PDFS
        $api->group(['prefix' => 'pdfs'], function ($api) {
            $api->get('', 'PdfsController@index');
            $api->patch('{id}/ler', ['middleware' => 'jwtauth', 'uses' => 'PdfsController@ler']);
            $api->patch('{id}/favoritar', ['middleware' => 'jwtauth', 'uses' => 'PdfsController@patchFavoritar']);
            $api->delete('{id}/favoritar', ['middleware' => 'jwtauth', 'uses' => 'PdfsController@deleteFavoritar']);
        });
        // DICAS DICAS DICAS
        $api->group(['prefix' => 'dicas'], function ($api) {
            $api->get('', 'DicasController@index');
        });;

        /** NUTRI */
         // ALIMENTOS ALIMENTOS ALIMENTOS
         $api->group(['prefix' => 'alimentos'], function ($api) {
            $api->get('', 'AlimentosController@index');
            $api->patch('{id}/favoritar', ['middleware' => 'jwtauth', 'uses' => 'AlimentosController@patchFavoritar']);
            $api->delete('{id}/favoritar', ['middleware' => 'jwtauth', 'uses' => 'AlimentosController@deleteFavoritar']);
        });
        // EXERCICIOS EXERCICIOS EXERCICIOS
        $api->group(['prefix' => 'exercicios'], function ($api) {
            $api->get('', 'ExerciciosController@index');
        });

        // CATEGORIAS CATEGORIAS CATEGORIAS
        $api->group(['prefix' => 'categorias'], function ($api) {
            $api->get('', 'CategoriasController@index');
            $api->get('{id}', 'CategoriasController@show');
            $api->get('{categoria_id}/topicos', 'CategoriasController@categoriasTopicos');
        });
        
        // TAGS TAGS TAGS
        $api->group(['prefix' => 'tags'], function ($api) {
            $api->get('', 'TagsController@index');
            $api->get('{id}', 'TagsController@detail');
        });

        // TAGS TAGS TAGS
        $api->group(['prefix' => 'area'], function ($api) {
            $api->get('', 'AreaController@index');
        });

        $api->group(['prefix' => 'noticias'], function ($api) {
            $api->get('', 'NoticiasController@index');
        });


        // CONTEUDOS CONTEUDOS CONTEUDOS
        $api->group(['prefix' => 'conteudos'], function ($api) {
            $api->get('', 'ConteudosController@index');
            $api->get('/search', 'ConteudosController@search');
            $api->get('{id}', 'ConteudosController@detail');
            $api->patch('{id}/visualizar', ['middleware' => 'jwtauth', 'uses' => 'ConteudosController@visualizar']);
            $api->patch('{id}/favoritar', ['middleware' => 'jwtauth', 'uses' => 'ConteudosController@patchFavoritar']);
            $api->delete('{id}/favoritar', ['middleware' => 'jwtauth', 'uses' => 'ConteudosController@deleteFavoritar']);
        });

        // USERS USERS USERS
        $api->group(['prefix' => 'users'], function ($api) {


            /** NUTRI */
            // ALIMENTOS ALIMENTOS ALIMENTOS
            $api->get('{user_id}/alimentos', 'UsersController@alimentos');
            $api->post('{user_id}/alimentos', 'UsersController@alimentosStore');
            $api->patch('{user_id}/alimentos', 'UsersController@alimentosUpdate');
            /**
             * @todo lembrar de verificar se o negocio ta com favorited_at se esta apenas seta a data como null
             */
            $api->delete('{user_id}/alimentos/{id}', 'UsersController@alimentosDelete');

            // EXERCICIOS EXERCICIOS EXERCICIOS
            $api->get('{user_id}/exercicios', 'UsersController@exercicios');
            $api->post('{user_id}/exercicios', 'UsersController@exerciciosStore');
            $api->patch('{user_id}/exercicios', 'UsersController@exerciciosUpdate');
            /**
             * @todo lembrar de verificar se o negocio ta com favorited_at se esta apenas seta a data como null
             */
            $api->delete('{user_id}/exercicios/{id}', 'UsersController@exerciciosDelete');

            // AGUA AGUA AGUA
            $api->get('{user_id}/agua', 'UsersController@agua');
            $api->post('{user_id}/agua', 'UsersController@aguaStore');

            $api->post('{id}', 'UsersController@save');
            $api->post('{id}/premium', 'UsersController@premium');
            $api->get('{id}/premium', 'UsersController@getPremium');
            $api->get('{id}/favoritos', 'UsersController@favoritos');

            $api->post('{id}/avatar', 'UsersController@uploadAvatar');
            $api->post('{user_id}/onesignal', 'OnesignalController@save');

            // NOTIFICATION NOTIFICATION NOTIFICATION NOTIFICATION
            $api->get('{id}/notifications', 'UsersController@notifications');

            // CONSULTAS CONSULTAS CONSULTAS
            
            $api->get('{user_id}/consultas', 'UsersController@consultas');
            $api->delete('{user_id}/consultas/{id}', 'UsersController@consultasDelete');
            $api->get('{user_id}/consultasImagens', 'UsersController@consultasImagens');
            $api->post('{user_id}/consultas', 'UsersController@consultasStore');
            $api->post('{user_id}/imagemConsulta', 'UsersController@imagemConsulta');
        });
    });
});
