<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\BaseController;
use App\Http\Requests;
use App\User;
use Sentinel;
use Activation;

class UserController extends BaseController {
    public function __construct() {
        parent::__construct();
        $this->middleware('auth');
    }

    public function index() {
        $data['listaUser'] = \App\User::with(['pessoa', 'pessoa.empresa'])->get();

        return view('admin/users', $data);
    }

    public function add() {
        $data = [];
        $data['roles'] = \App\Role::get();
        $data['userLogado'] = \App\User::find(Sentinel::getUser()->id);
        return view('admin/form-users', $data);
    }

    public function edit($id) {
		$data['user'] = \App\User::with(['conteudos' => function($q){
			$q->orderBy('created_at');
		}])->find($id);

        $data['roles'] = \App\Role::get();
        $data['userLogado'] = \App\User::find(Sentinel::getUser()->id);
        return view('admin/form-users', $data);
    }

    public function save(Request $request) {
        try {
            $post = $request->input();
            if(\App\User::where('email', $post['email'])->where('id', '!=', $request->input('id', 0))->exists()) {
                throw new \Exception('E-mail escolhido já está sendo utilizado');
            }
            if ($post['password']) {
                $post['password'] = bcrypt($post['password']);
            } else {
                unset($post['password']);
            }

            $role = Sentinel::findRoleById($post['id_role']);

            

            if ($request->input('id')) {
                $user = \App\User::find($request->input('id'));
                $user->update($post);
                $user->roles()->detach();
            } else {
                $user = \App\User::create($post);
                $user_sentinel = Sentinel::findById($user->id);
                $activation = Activation::create($user_sentinel);
                Activation::complete($user_sentinel, $activation->code);
            }

            $user->roles()->attach([$post['id_role']]);
            \Session::flash('type', 'success');
            \Session::flash('message', 'Alteracoes salvas com sucesso!');
            return redirect('admin/users');
        } catch (\Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    public function upload_image(Request $request) {
        if ($request->hasFile('file')) {
            //upload an image to the /img/tmp directory and return the filepath.
            $file = $request->file('file');
            $tmpFilePath = '/uploads/users/';
            $tmpFileName = time() . '-' . $file->getClientOriginalName();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path = $tmpFilePath . $tmpFileName;
            return response()->json(['path' => $path, 'file_name' => $tmpFileName], 200);
        } else {
            return response()->json(false, 200);
        }
    }

    public function crop_image(Request $request) {
        $img = \Image::make('uploads/users/' . $request->input('file_name'));
        $dataCrop = json_decode($request->input('data_crop'));
        if ($img->crop(intval($dataCrop->width), intval($dataCrop->height), intval($dataCrop->x), intval($dataCrop->y))->save('uploads/users/thumb_' . $request->input('file_name'))) {
            @unlink('uploads/users/' . $request->input('file_name'));
            echo json_encode([
                'status' => true,
                'path' => '/uploads/users/thumb_' . $request->input('file_name'),
                'file_name' => 'thumb_' . $request->input('file_name'),
            ]);
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'Não foi possível alterar a imagem.'
            ]);
        }
    }

    public function delete($id) {
        try {
            \App\User::destroy($id);
            @unlink("uploads/users/$user->thumbnail_principal");
            \Session::flash('type', 'success');
            \Session::flash('message', 'Registro removido com sucesso!');
            return redirect('admin/users');
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Nao foi possivel remover o registro!');
            return redirect()->back();
        }
    }
}
