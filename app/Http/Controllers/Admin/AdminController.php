<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use DB;
use App\Modules\Aplicativos\Models\Aplicativos;
use App\Modules\Frases\Models\Frases;
use App\User;
use Sentinel;

class AdminController extends BaseController
{
	public function __construct(){
		parent::__construct();
	}
    public function getHome()
    {
		if(Sentinel::getUser()->inRole('empresa')) {
			return redirect('/admin/empresas/dashboard');
		} else if(Sentinel::getUser()->inRole('gerencia')) {
			return redirect('/admin/codes/dashboard');
		}

		$data = array();

		$data['aplicativos'] = Aplicativos::orderBy('ordem')->get();

        return view('admin.index', $data);
	}
	
	public function getDashboard() {
		return view('admin.dashboard');
	}

    public function processBuscar(Request $request){
		$keyword = $request->get('q');
		$data = array();
		$modulos = \App\Gerador::where('id_tipo_modulo',1)->get();
		foreach ($modulos as $modulo) {
			$listagem = array();
			$query = DB::table($modulo->nome_tabela)->select('*');
			foreach ($modulo->camposTexto as $campo) {
				$query->orWhere($campo->nome, 'LIKE', "%$keyword%");
				if($campo->listagem){
					$listagem[] = $campo;
				}
			}
			$results = $query->get();

			if(count($results)){
				$data['modulos'][$modulo->id]['modulo'] = $modulo;
				$data['modulos'][$modulo->id]['campos_listagem'] = $listagem;
				$data['modulos'][$modulo->id]['registros'] = $results;
			}

		}

		//print_r($data);die();

		return view('admin/busca', $data);
	}

	public function getCep() {
	  	$cep = $_POST['cep'];
	  	$url = 'http://republicavirtual.com.br/web_cep.php?cep='.$cep.'&formato=jsonp';
	  	$resultado = file_get_contents($url);
	  	if(!$resultado){
	  		$resultado = "&resultado=0&resultado_txt=erro+ao+buscar+cep";
	  	}
	  	print_r($resultado);
	  	exit;
	}

	public function bad_permissions(){
		return view('admin/bad_permissions');
	}

}
