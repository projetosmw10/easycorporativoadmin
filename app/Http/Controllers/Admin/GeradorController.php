<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use DB;
use Schema;
use App\Http\Requests;
use App\Permission;
use App\Http\Controllers\BaseController;
use Sentinel;

class GeradorController extends BaseController
{
    public function __construct(){
		parent::__construct();
		$this->middleware('auth');
	}

	public function index(){
		$data['modulos'] = \App\Gerador::get();
		return view('admin/gerador',$data);
	}

	public function add(){
		$data = array();
		$data['tipos'] = \App\TipoModulo::get();
		return view('admin/form-gerador', $data);
	}

	public function edit($id){
		$data['modulo'] = \App\Gerador::find($id);
		$data['tipos'] = \App\TipoModulo::get();
		$data['campos'] = \App\CampoModulo::where('id_modulo', $data['modulo']->id)->get();
		return view('admin/form-gerador',$data);
	}

	public function save(Request $request){
		try{
			$post = $request->input();
			if($request->input('id')){
				$modulo = \App\Gerador::find($request->input('id'));
				\App\Gerador::editar($post, $request->input('id'));

				$this->updateTable($request->input(), $modulo);
			}else{
				if(!(\Schema::hasTable($post['nome_tabela']))){
					if(!(\App\Gerador::where('nome', $post['nome'])->count())){
						$id_modulo = \App\Gerador::criar($post);
						$modulo = \App\Gerador::find($id_modulo);
						$this->createTable($request->input(), $modulo);
						$this->generateFiles($modulo);
						$this->createPermissionsAdmin($modulo);
					}else{
						die('Já existe um módulo com esse nome, seu infeliz !');
					}
				}else{
					die('Já existe uma tabela com esse nome, seu infeliz !');
				}
			}
			\Session::flash('type', 'success');
         \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/gerador');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
         \Session::flash('message', $e->getMessage());
         return redirect()->back();
		}


	}

	public function delete($id){
		try{
			$modulo = \App\Gerador::find($id);

			

			/* Apaga a pasta do módulo recursivamente */
			$this->rrmdir('../app/Modules/'.$modulo->nome);
			$this->rrmdir('../public/uploads/'.$modulo->rota);

			/* Remove do config/module.php */
			$modules = config("module.modules");
			$str = "<?php
			# config/module.php
			return  [
			    'modules' => [
			";
			foreach($modules as $module) {
				if($module != $modulo->nome){
					$str .= "'$module',
					";
				}
			}
			$str .= "
				]
			];";
			file_put_contents('../config/module.php',$str);

			\App\CampoModulo::where('id_modulo',$modulo->id)->delete();
			// DB::statement('DROP TABLE '.$modulo->nome_tabela);
            Schema::dropIfExists($modulo->nome_tabela.'_imagens');
            Schema::dropIfExists($modulo->nome_tabela.'_translations');
            Schema::dropIfExists($modulo->nome_tabela);
			

            \App\Gerador::deletar($id);

			\Session::flash('type', 'success');
         \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/gerador');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possível remover o registro!");
            return redirect()->back();
		}


	}

	private function generateFiles($modulo){
        $tipo_modulo = \App\TipoModulo::find($modulo->id_tipo_modulo);


        $colunas_modulo = \App\CampoModulo::where('id_modulo', $modulo->id)->get()->toArray();
        
        $more_fillable = array();
        if($modulo->id_tipo_modulo != 3){
            $more_fillable[] = 'thumbnail_principal';
		}

		if($modulo->id_tipo_modulo == 1){
            $more_fillable[] = 'meta_keywords';
            $more_fillable[] = 'meta_descricao';
            $more_fillable[] = 'slug';
		}
        
        $colunas_fillable = array_merge(array_column($colunas_modulo, 'nome'), $more_fillable);

        $tabela_html = str_replace('<ROTA_MODULO>', $modulo->rota, $this->generateHtmlTable($colunas_modulo));
        $formulario_html = str_replace('<ROTA_MODULO>', $modulo->rota, $this->generateHtmlForm($colunas_modulo, $modulo->imagem));
        // string das colunas
        $colunas_fillable = preg_replace("/[0-9]+ \=\>/i", '', var_export($colunas_fillable, true));
        $colunas_fillable = preg_replace('/\s+/', '', $colunas_fillable);
        $colunas_fillable = str_replace(',', ', ', $colunas_fillable);



		$replaces = array('<NOME_MODULO>','<ID_MODULO>','<ROTA_MODULO>','<ITEM_MODULO>','<ITEMS_MODULO>','<NOME_TABELA>','<LABEL_MODULO>', '<COLUNAS_FILLABLE>', '<HTML_TABELA>', '<HTML_FORMULARIO>');
		$by = array($modulo->nome,$modulo->id,$modulo->rota,$modulo->item_modulo,$modulo->items_modulo,$modulo->nome_tabela,$modulo->label, $colunas_fillable, $tabela_html, $formulario_html);

		if(file_exists('../app/Modules/'.$modulo->nome)){
			die('Ja existe um módulo com esse nome, seu idiota !');
		}

		/* Cria as pastas */
		mkdir(__DIR__.'/../../../../public/uploads/'.$modulo->rota, 0777, true);
		mkdir('../app/Modules/'.$modulo->nome, 0777, true);
		mkdir('../app/Modules/'.$modulo->nome.'/Models', 0777, true);
		if($tipo_modulo->id != 3){
			mkdir('../app/Modules/'.$modulo->nome.'/Views', 0777, true);
			mkdir('../app/Modules/'.$modulo->nome.'/Views/admin', 0777, true);
			mkdir('../app/Modules/'.$modulo->nome.'/Controllers', 0777, true);
			mkdir('../app/Modules/'.$modulo->nome.'/Controllers/Admin', 0777, true);
		}


		/* Gera o Model */
		$text = str_replace($replaces,$by,file_get_contents("../resources/views/templates_tipo_modulo/".$tipo_modulo->model));
		file_put_contents('../app/Modules/'.$modulo->nome.'/Models/'.$modulo->nome.'.php',$text);

		if($tipo_modulo->id != 3){
			/* Gera o Controller */
			$text = str_replace($replaces,$by,file_get_contents("../resources/views/templates_tipo_modulo/".$tipo_modulo->controller_admin));
			file_put_contents('../app/Modules/'.$modulo->nome.'/Controllers/Admin/Admin'.$modulo->nome.'Controller.php',$text);

			/* Gera o Controller do Site */
			$text = str_replace($replaces,$by,file_get_contents("../resources/views/templates_tipo_modulo/controller_basico.php"));
			file_put_contents('../app/Modules/'.$modulo->nome.'/Controllers/'.$modulo->nome.'Controller.php',$text);

			/* Gera a View Index */
			$text = str_replace($replaces,$by,file_get_contents("../resources/views/templates_tipo_modulo/".$tipo_modulo->view_admin_index));
			file_put_contents('../app/Modules/'.$modulo->nome.'/Views/admin/'.$modulo->rota.'.blade.php',$text);

			if($tipo_modulo->id == 1){ // Com Detalhe
				/* Gera a View Form */
				$text = str_replace($replaces,$by,file_get_contents("../resources/views/templates_tipo_modulo/".$tipo_modulo->view_admin_form));
				file_put_contents('../app/Modules/'.$modulo->nome.'/Views/admin/form-'.$modulo->rota.'.blade.php',$text);
			}

			/* Gera a view index do site */
			file_put_contents('../app/Modules/'.$modulo->nome.'/Views/'.$modulo->rota.'.blade.php','');

			/* Gera as rotas */
			$text = str_replace($replaces,$by,file_get_contents("../resources/views/templates_tipo_modulo/".$tipo_modulo->rotas));
			file_put_contents('../app/Modules/'.$modulo->nome.'/routes.php',$text);
		}

		/* Adiciona o módulo ao config/module.php */
		$modules = config("module.modules");

		$str = "<?php
		# config/module.php

		return  [
		    'modules' => [
		";
		foreach($modules as $module) {
			$str .= "'$module',
			";
		}
		$str .= "'$modulo->nome'
		";
		$str .= "
			]
		];";

		file_put_contents('../config/module.php',$str);

		return true;
	}

	private function createTable($input, $modulo){
		if($modulo->id_tipo_modulo != 3){
			$sqlColumns = '( id INT NOT NULL AUTO_INCREMENT, thumbnail_principal VARCHAR(255) DEFAULT NULL';
		}else{
			$sqlColumns = '( id INT NOT NULL AUTO_INCREMENT';
		}

		if($modulo->id_tipo_modulo == 1){
			$sqlColumns .= ', meta_keywords TEXT DEFAULT NULL, meta_descricao TEXT DEFAULT NULL, slug VARCHAR(255) NOT NULL';
        }
        
        

		foreach ($input['campo-nome'] as $key => $nome_campo) {
			if(!$nome_campo) continue;
			$sqlColumns .= ', ';
			switch ($input['campo-tipo-campo'][$key]) {
				case 'INT':
					$tipo = 'INT';
					$valor_tipo = '(11)';
					break;
				case 'I':
					$tipo = 'VARCHAR';
					$valor_tipo = '(255)';
					break;
				case 'N':
					$tipo = 'DECIMAL';
					$valor_tipo = '(15,2)';
					break;
				case 'T':
					$tipo = 'TEXT';
					$valor_tipo = '';
					break;
				case 'D':
					$tipo = 'DATE';
					$valor_tipo = '';
					break;
				case 'DT':
					$tipo = 'DATETIME';
					$valor_tipo = '';
					break;
				case 'S':
					$tipo = 'TINYINT';
					$valor_tipo = '';
					break;
				case 'SI':
					$tipo = 'VARCHAR';
					$valor_tipo = '(255)';
					break;
			}
			$sqlColumns .= $nome_campo.' '.$tipo.' '.$valor_tipo.' DEFAULT NULL';
            

			$campoInfo = array(
				'nome' => $nome_campo,
				'valor_padrao' => $input['campo-valor-padrao'][$key],
				'listagem' => $input['campo-listagem'][$key],
				'required' => $input['campo-required'][$key],
				'label' => $input['campo-label'][$key],
				'required' => $input['campo-required'][$key],
				'tipo_campo' => $input['campo-tipo-campo'][$key],
				'ordem' => $input['campo-ordem'][$key],
				'id_modulo' => $modulo->id,
			);
			\App\CampoModulo::criar($campoInfo);
        }
        $sqlColumns  .= ', created_at DATETIME NULL, updated_at DATETIME NULL ';
		$sqlColumns .= ', PRIMARY KEY (id))';
		DB::statement('CREATE TABLE '.$input['nome_tabela'].' '.$sqlColumns);

		if($modulo->id_tipo_modulo != 3){
			DB::statement('CREATE TABLE '.$input['nome_tabela'].'_imagens (id INT NOT NULL AUTO_INCREMENT, thumbnail_principal VARCHAR (255) DEFAULT NULL, id_'.$modulo->item_modulo.' INT(11) NOT NULL, created_at DATETIME NULL, updated_at DATETIME NULL , PRIMARY KEY (id))');
		}

		if($modulo->id_tipo_modulo == 2){
			DB::statement('INSERT INTO '.$input['nome_tabela'].' (id) VALUES (1)');
		}

		return true;
	}

	private function updateTable($input, $modulo){
		if(isset($input['edit-campo-nome'])){
			foreach ($input['edit-campo-nome'] as $key => $nome_campo) {
				if($input['old-campo-nome'][$key] != $nome_campo){
					$new_name = $nome_campo;
				}else{
					$new_name = '';
				}
				switch ($input['edit-campo-tipo-campo'][$key]) {
					case 'INT':
						$tipo = 'INT';
						$valor_tipo = '(11)';
						break;
					case 'I':
						$tipo = 'VARCHAR';
						$valor_tipo = '(255)';
						break;
					case 'N':
						$tipo = 'DECIMAL';
						$valor_tipo = '(15,2)';
						break;
					case 'T':
						$tipo = 'TEXT';
						$valor_tipo = '';
						break;
					case 'D':
						$tipo = 'DATE';
						$valor_tipo = '';
						break;
					case 'DT':
						$tipo = 'DATETIME';
						$valor_tipo = '';
						break;
					case 'S':
						$tipo = 'TINYINT';
						$valor_tipo = '';
						break;
					case 'SI':
						$tipo = 'VARCHAR';
						$valor_tipo = '(255)';
						break;
				}
				DB::statement('ALTER TABLE '.$modulo->nome_tabela.' CHANGE COLUMN '.$input['old-campo-nome'][$key].' '.$nome_campo.' '.$tipo.' '.$valor_tipo.' DEFAULT NULL');

				$campoObject = \App\CampoModulo::find($input['edit-campo-id'][$key]);

				$campoInfo = array(
					'nome' => $nome_campo,
					'valor_padrao' => $input['edit-campo-valor-padrao'][$key],
					'listagem' => $input['edit-campo-listagem'][$key],
					'required' => $input['edit-campo-required'][$key],
					'label' => $input['edit-campo-label'][$key],
					'tipo_campo' => $input['edit-campo-tipo-campo'][$key],
					'ordem' => $input['edit-campo-ordem'][$key],
					'id_modulo' => $modulo->id,
				);
				\App\CampoModulo::editar($campoInfo, $campoObject->id);
			}

		}

		if(isset($input['campo-nome'])){
			foreach ($input['campo-nome'] as $key => $nome_campo) {
				switch ($input['campo-tipo-campo'][$key]) {
					case 'INT':
						$tipo = 'INT';
						$valor_tipo = '(11)';
						break;
					case 'I':
						$tipo = 'VARCHAR';
						$valor_tipo = '(255)';
						break;
					case 'N':
						$tipo = 'DECIMAL';
						$valor_tipo = '(15,2)';
						break;
					case 'T':
						$tipo = 'TEXT';
						$valor_tipo = '';
						break;
					case 'D':
						$tipo = 'DATE';
						$valor_tipo = '';
						break;
					case 'DT':
						$tipo = 'DATETIME';
						$valor_tipo = '';
						break;
					case 'S':
						$tipo = 'TINYINT';
						$valor_tipo = '';
						break;
					case 'SI':
						$tipo = 'VARCHAR';
						$valor_tipo = '(255)';
						break;
				}
				DB::statement('ALTER TABLE '.$modulo->nome_tabela.' ADD '.$nome_campo.' '.$tipo.' '.$valor_tipo.' DEFAULT NULL');

				$campoInfo = array(
					'nome' => $nome_campo,
					'valor_padrao' => $input['campo-valor-padrao'][$key],
					'listagem' => $input['campo-listagem'][$key],
					'required' => $input['campo-required'][$key],
					'label' => $input['campo-label'][$key],
					'tipo_campo' => $input['campo-tipo-campo'][$key],
					'ordem' => $input['campo-ordem'][$key],
					'id_modulo' => $modulo->id,
				);
				\App\CampoModulo::criar($campoInfo);
			}
		}


		return true;

	}

	public function createPermissionsAdmin($modulo){
		$role = Sentinel::findRoleBySlug('admins');
		$newPermissions = array(
			$modulo->nome_tabela.'.view' => true,
			$modulo->nome_tabela.'.create' => true,
			$modulo->nome_tabela.'.update' => true,
			$modulo->nome_tabela.'.delete' => true,
		);
		$role->permissions = array_merge($role->permissions, $newPermissions);
		$role->save();
		return true;
	}

	public function rrmdir($dir) {
	  if (is_dir($dir)) {
		 $objects = scandir($dir);
		 foreach ($objects as $object) {
			if ($object != "." && $object != "..") {
			  if (is_dir($dir."/".$object))
				 $this->rrmdir($dir."/".$object);
			  else
				 unlink($dir."/".$object);
			}
		 }
		 rmdir($dir);
	  }
    }
    
    public function generateHtmlTable($fields){
$tabela = '';
$tabela .='
                            <thead>
                                 <tr>
                                     <th>ID</th>'.PHP_EOL;
                                    foreach ($fields as $field){
$tabela .='                                     <th>'.$field['label'].'</th>'.PHP_EOL;
                                    
                                    }
                                    
$tabela .='                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody>'.PHP_EOL;
$tabela .='                            <?php foreach ($itens as $item){ ?>
									<tr>
										<td><?php echo $item->id; ?></td>'.PHP_EOL;
										foreach ($fields as $field):
                                            $campo = $field['nome'];
                                            switch ($field['tipo_campo']) {
                                                case 'S':
                                                    $valor = '($item->'.$campo.') ? "Sim" : "Não"';
                                                    break;
                                                case 'N':
                                                    $valor = 'number_format($item->'.$campo.',2,",",".")';
                                                    break;
                                                case 'D':
                                                    $valor = 'date("d/m/Y",strtotime($item->'.$campo.'))';
                                                    break;
                                                case 'DT':
                                                    $valor = 'date("d/m/Y H:i:s",strtotime($item->'.$campo.'))';
                                                    break;
                                                case 'SI':
                                                    $valor = '\'<i class="fa \'.$item->'.$campo.'.\'"></i>\'';
                                                    break;
                                                default:
                                                    $valor = '$item->'.$campo;
                                                    break;
                                             }
$tabela .='                                            <td> <?php echo '.$valor.'?> </td>'.PHP_EOL;
                                        endforeach;
$tabela .='                                            <td>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<a href="/admin/<ROTA_MODULO>/edit/<?php echo $item->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/<ROTA_MODULO>/delete/<?php echo $item->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>'.PHP_EOL;
                                    foreach ($fields as $field){
$tabela .='                                     <th>'.$field['label'].'</th>'.PHP_EOL;
                                    
                                    }
$tabela .='                                     <th>Ação</th>
								</tr>
                            </tfoot>';
        return $tabela;
    }


    public function generateHtmlForm($fields, $imagem = false){
$form = '';
if($imagem){
$form .= '
									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Imagem</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<img src="<?php echo url($item->thumbnail_principal); ?>" height="150px">
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
									</div>'.PHP_EOL;
}
        foreach($fields as $field){
        $campo = $field["nome"];
$form .='                                    <div class="form-group">
                                        <label for="'.$field["nome"].'" class="col-md-3 control-label">'.$field["label"].' '.( ($field["required"]) ? "*" : "").'</label>'.PHP_EOL;
                if($field["tipo_campo"] == 'INT'){
$form .='                                        <div class="col-md-7">
                                            <input id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' type="number" step="1" class="form-control" value="<?php echo (isset($item)) ? $item->'.$campo.' : "'.$field["valor_padrao"].'"; ?>" name="'.$field["nome"].'" />
                                         </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'I'){
$form .='                                        <div class="col-md-7">
                                        <input id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' type="text" class="form-control" value="<?php echo (isset($item)) ? $item->'.$campo.' : "'.$field["valor_padrao"].'"; ?>" name="'.$field["nome"].'" />
                                    </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'N'){
$form .='                                        <div class="col-md-7">
                                        <input id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' type="number" class="form-control" value="<?php echo (isset($item)) ? $item->'.$campo.' : "'.$field["valor_padrao"].'"; ?>" name="'.$field["nome"].'" />
                                    </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'T'){
$form .='                                        <div class="col-md-7">
                                        <textarea id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' class="form-control tinymce" name="'.$field["nome"].'"><?php echo (isset($item)) ? $item->'.$campo.' : "'.$field["valor_padrao"].'"; ?></textarea>
                                    </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'D'){
$form .='                                        <div class="col-md-7">
                                        <input id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' type="date" class="form-control" value="<?php echo (isset($item)) ? $item->'.$campo.' : "'.$field["valor_padrao"].'"; ?>" name="'.$field["nome"].'" />
                                    </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'DT'){
$form .='                                        <div class="col-md-7">
                                        <input id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' type="datetime" class="form-control" value="<?php echo (isset($item)) ? $item->'.$campo.' : "'.$field["valor_padrao"].'"; ?>" name="'.$field["nome"].'" />
                                    </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'S'){
$form .='                                        <div class="col-md-7">
                                        <select id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' class="form-control" name="'.$field["nome"].'">
                                            <option <?php echo (isset($item) && $item->'.$campo.' == 1) ? "selected" : ""; ?> value="1">Sim</option>
                                            <option <?php echo (isset($item) && $item->'.$campo.' == 0) ? "selected" : ""; ?> value="0">Não</option>
                                        </select>
                                    </div>'.PHP_EOL;
                }else
                if($field["tipo_campo"] == 'SI'){
$form .='                                        <div class="col-md-5">
                                        <?php $icons = explode(",",file_get_contents("fonts/admin/icons-font-awesome.txt")); ?>
                                        <select id="'.$field["nome"].'" '. (($field["required"]) ? "required" : "") .' class="form-control select2 select-icone" name="'.$field["nome"].'">
                                        <?php foreach ($icons as $icone): ?>
                                            <option <?php echo (isset($item) && $icone == $item->'.$campo.') ? "selected" : ""; ?> value="<?php echo $icone; ?>"><?php echo $icone; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="icone-viewer">
                                    <i class="fa fa-3x"></i>
                                </div>'.PHP_EOL;
                }
$form.= '             </div>'.PHP_EOL;
        }


        return $form;
    }

}
