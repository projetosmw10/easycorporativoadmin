<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Sentinel;
use App\User;
use App\JWTUser;    
use App\JWTTokens;
use Activation;
Use Storage;
use Mail;
use DB;


class JWTAuthController extends Controller {
    public function authenticate(Request $request) {
        $email = $request->input('email');
        $password = $request->input('password');

        try {
            if (empty($email) || empty($password)) {
                throw new \InvalidArgumentException('You must inform a email and a password');
            }

            if ($user = Sentinel::stateless(['email' => $email, 'password' => $password])) {
                $user = User::with(['roles', 'pessoa', 'pessoa.code'])->find($user->id);
            } else {
                return response(null, 401);
            }
            $user->last_login = date('Y-m-d H:i:s');
            $user->save();
            

            return response()->json(['token' => JWTUser::generateToken($user)]);
        } catch (\InvalidArgumentException $e) {
            return response()->json([
                'message_dev' => $e->getMessage()
            ], 400);
        }
    }

    public function register(Request $request, $id = null) {
        $post = $request->input();


        if (empty($post['email'])) {
            return response()->json(['message' => 'Você deve informar um e-mail'], 400);
        }
        if (!filter_var($post['email'], FILTER_VALIDATE_EMAIL)) {
            return response()->json(['message' => 'Você deve informar um e-mail válido'], 400);
        }

        $duplicateEmail = User::where('email', $post['email']);
        if ($id) {
            $duplicateEmail->where('id', '!=', $id);
        }
        if ($duplicateEmail->exists()) {
            return response()->json(['message' => 'O e-mail informado já está sendo utilizado'], 400);
        }

        if (!empty($post['password'])) {
            $post['password'] = bcrypt($post['password']);
        } else {
            if($id){
                unset($post['password']);
            }else{
                return response()->json(['message' => 'Você deve informar uma senha'], 400);
            }
        }

        if(empty($post['first_name'])){
            return response()->json(['message' => 'Você deve informar seu nome'], 400);
        }

        if (isset($post['premium_expire_at'])) {
            unset($post['premium_expire_at']);
        }

        if (isset($post['permissions'])) {
            unset($post['permissions']);
        }

        try {
            if ($id) {
                $user = User::findOrFail($id);
                $user->update($post);
            } else {
                $user = User::create($post);
                $user_sentinel = Sentinel::findById($user->id);
                $activation = Activation::create($user_sentinel);
                Activation::complete($user_sentinel, $activation->code);
            }

            if ($request->hasFile('avatar')) {
                // https://laravel.com/docs/5.6/filesystem#file-uploads
                if ($request->file('avatar')->isValid()) {
                    $avatar = $request->avatar->store('users/' . $user->id . '/avatar', 'public');
                    if ($avatar && $user->avatar) {
                        // Get original por que temos um mutator no model User para enviar para api a url completa
                        Storage::disk('public')->delete($user->getOriginal('avatar'));
                    }
                    $user->update(['avatar' => $avatar]);
                }
            }

            $user->roles()->sync(2);
            $user->roles;
            $token = [
                'iat' => time(),
                'exp' => time() + (604800 * 104), // 604800 - 1 semana
                'user' => $user->toArray()
            ];

            $token = JWT::encode($token, env('JWT_SECRET'));

            return response()->json(['token' => $token]);
        } catch (ModelNotFoundException $e) {
            return response(null, 404);
        }
    }


    public function forgotPassword(Request $request){
        $post = $request->input();
        if(empty($post['email'])){
            return response()->json(['message' => 'Por favor informe seu e-mail'], 400);
        }

        $credentials = ['email' => $post['email']];

        $user = Sentinel::findByCredentials($credentials);
        if(!$user){
            return response('', 404);
        }
        
        $tempPassword = bin2hex(openssl_random_pseudo_bytes(4));
        Mail::send('emails.forgot_password', [
            'user' => $user,
            'tempPassword' => $tempPassword
        ], function ($m) use ($user) {
            $m->from(env('MAIL_USERNAME'), env('MAIL_USERNAME'))
              ->to($user->email, $user->first_name)
              ->subject('Easylife APPS - Esqueci minha senha');
        });
        $user = Sentinel::update($user, ['password' => $tempPassword]);
        return response('');
    }

}
