<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Dicas\Models\Dicas;

class DicasController extends Controller {

    public function index(Request $request){
        $items = Dicas::orderBy('ordem', 'asc')->where('aplicativo_id', $request->aplicativo_id)->get();
        return response()->json($items);
    }
    
    public function adminOrdem(Request $request) {
        $body = $request->input();
        foreach ($body['dicas'] as $key => $t) {
            Dicas::where('id', $key)->update($t);
        }
    }
}