<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Codes\Models\Codes;

class CodesController extends Controller {
    private $tokens = ['e560946ea255831db0a79f1ea4d098700256f9f8'];

    public function index(Request $request) {
        $token = $request->query('token', null);
        if (empty($token)) {
            return response()->json(['message' => 'Você deve informar seu token. Por exemplo: /api/codigos?token=4f2fd1a4c56d72f2bd4cbb080de8a975c76f6d54'], 400);
        }
        if (!in_array($token, $this->tokens)) {
            return response()->json(['message' => 'Seu token não é válido'], 401);
        }
        $codigos = Codes::withTrashed()->get()->toArray();
        foreach ($codigos as $key => $c) {
            $codigos[$key]['codigo'] = $codigos[$key]['code'];
            unset($codigos[$key]['deleted_at'], $codigos[$key]['created_at'], $codigos[$key]['updated_at'], $codigos[$key]['user_id'], $codigos[$key]['id'], $codigos[$key]['code']);
        }
        return response()->json($codigos);
    }

    public function add(Request $request) {
        $token = $request->query('token', null);
        if (!$request->isJson()) {
            return response()->json(['message' => 'Utilize o header Content-Type: application/json, corpo do request também deve vir em json'], 400);
        }
        if (empty($token)) {
            return response()->json(['message' => 'Você deve informar seu token. Por exemplo: /api/codigos?token=4f2fd1a4c56d72f2bd4cbb080de8a975c76f6d54'], 400);
        }
        if (!in_array($token, $this->tokens)) {
            return response()->json(['message' => 'Seu token não é válido'], 401);
        }

        $post = $request->input();
        if (!is_array($post)) {
            return response()->json(['message' => 'Os códigos devem ser enviados em formato de array por exemplo: [{"codigo": "ASD123","codigo": "ALJ253"}]'], 400);
        }

        // echo json_encode($request->codigos);
        $response = [
            'ok' => [],
            'existente' => [],
        ];
        foreach ($post as $key => $c) {
            if (empty($c['codigo'])) {
                continue;
            }
            if (Codes::withTrashed()->where('code', $c['codigo'])->exists()) {
                $response['existente'][] = $c;
            } else {
                Codes::create(['code' => $c['codigo']]);
                $response['ok'][] = $c;
            }
        }

        return response()->json($response);
    }

    // public function detail(Request $request, $id) {
    //     $token = $request->query('token', null);
    //     if (empty($token)) {
    //         return response()->json(['message' => 'Você deve informar seu token. Por exemplo: /api/codigos?token=4f2fd1a4c56d72f2bd4cbb080de8a975c76f6d54'], 400);
    //     }
    //     if (!in_array($token, $this->tokens)) {
    //         return response()->json(['message' => 'Seu token não é válido'], 401);
    //     }
    //     try {
    //         $item = Codes::withTrashed()->orWhere('id', $id)->orWhere('code', $id)->firstOrFail();
    //         $item['codigo'] = $item['code'];
    //         unset($item['deleted_at'], $item['created_at'], $item['updated_at'], $item['user_id'], $item['id'], $item['code']);
    //         return response()->json($item);
    //     } catch (ModelNotFoundException $e) {
    //         return response('', 404);
    //     }
    // }

    // public function delete(Request $request, $id) {
    //     $token = $request->query('token', null);
    //     if (empty($token)) {
    //         return response()->json(['message' => 'Você deve informar seu token. Por exemplo: /api/codigos?token=4f2fd1a4c56d72f2bd4cbb080de8a975c76f6d54'], 400);
    //     }
    //     if (!in_array($token, $this->tokens)) {
    //         return response()->json(['message' => 'Seu token não é válido'], 401);
    //     }
    //     $force = $request->query('force', false);
    //     $force = $force === 'true' ? true : $force;

    //     $code = Codes::withTrashed()->orWhere('id', $id)->orWhere('code', $id)->first();
    //     if (!$code) {
    //         return response(null, 200);
    //     }

    //     if ($force === true) {
    //         $code->forceDelete();
    //     } else {
    //         $code->delete();
    //     }
    //     return response(null, 200);
    // }
}
