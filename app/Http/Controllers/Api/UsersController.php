<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Firebase\JWT\JWT;
use App\JWTUser;
use App\User;
use Sentinel;
use Mail;
use Activation;
use Storage;
use DB;
use DateTime;
use DateInterval;
use App\Modules\Empresas\Models\Empresas;
use App\Modules\Pessoas\Models\Pessoas;
use App\Modules\Codes\Models\Codes;

class UsersController extends Controller {
    public function premium(Request $request, $id) {
        if (!$request->code) {
            return response()->json(['message' => 'Você deve informar o código', 'message_dev' => 'code is empty'], 400);
        }
        
        // verifica se o code existe
        $code = Codes::where('code', $request->code)->first();
        if (!$code) {
            return response()->json(['message' => 'O código informado é inválido'], 400);
        }

        
        // caso a empresa esteja sem identificador, cria um novo para cada pessoa que se cadastrou
        if($code->empresa->sem_identificador) {
            $pessoa = Pessoas::create([
                'nome' => JWTUser::getUser()->first_name,
                'identificador' => mt_rand(0, 999999999),
                'empresa_id' => $code->empresa_id
            ]);
            $request->identificador = $pessoa->identificador;
            
        }

        if (!$request->identificador) {
            return response()->json(['message' => 'Você deve informar sua identificação', 'message_dev' => 'identificador is empty'], 400);
        }

        // verifica se A identificação existe e é daquela empresa do codigo
        $identificador = Pessoas::with('code')->where('empresa_id', $code->empresa_id)->where('identificador', $request->identificador)->first();
        if (!$identificador) {
            return response()->json(['message' => 'A identificação informada não foi encontrado'], 400);
        }

        // Verificamos a válidade do código, para não ativar um codigo que ja esteja expirado
        if ($code->isExpired()) {
            return response()->json(['message' => 'O código informado é inválido #3'], 400);
        }

        // Verifica se A identificação tme um usuario e se tem verifica se é diferente do que a pessoa que esta tentando utilizar, cpf só é invalido caso o codigo daquele cpf esteja expirado
        if ($identificador->user_id && $identificador->user_id != JWTUser::getUser()->id) {
            if($identificador->code && !$identificador->code->isExpired()) {
                return response()->json(['message' => 'A identificação ' . $identificador->identificador . ' já está sendo utilizado'], 400);
            }
        }

        // verifica se a quantidade não chegou no limite
        if($code->quantity <= $code->pessoas()->count()) {
            return response()->json(['message' => 'O código informado atingiu o limite de usuários.'], 400);
        }

        // Finalmente, atrela o code e o usuário naquele PESSOA
        $identificador->user_id = JWTUser::getUser()->id;
        $identificador->code_id = $code->id;
        $identificador->save();

        // retorna um novo token
        $user = User::with(['roles', 'pessoa', 'pessoa.code'])->find(JWTUser::getUser()->id);

        return response()->json(['token' => JWTUser::generateToken($user)]);
    }

    public function getPremium(Request $request, $id) {
        if (JWTUser::getUser()->id != $id && !JWTUser::inAnyRole(['admins'])) {
            return response()->json(['message' => 'Você só pode visualizar se seu usuário é premium'], 403);
        }
        return response()->json(['premium' => JWTUser::isPremium()]);
    }

    public function notifications(Request $request, $id) {
        if (JWTUser::getUser()->id != $id && !JWTUser::inAnyRole(['admins'])) {
            return response()->json(['message' => 'Você só pode visualizar suas notificações'], 403);
        }
        $items = User::select('id')->find($id)->notifications()->orderBy('created_at', 'desc')->get();
        // $items->setCurrentPage($request->query('page', 1));
        return response()->json($items);
    }

    public function uploadAvatar(Request $request, $id) {
        if (!($request->hasFile('avatar') || $request->hasFile('thumbnail_principal'))) {
            return response()->json(['message' => 'Você deve enviar uma foto'], 400);
        }
        if (JWTUser::getUser()->id != $id) {
            return response('', 403);
        }

        try {
            $user = User::find($id);

            if (is_file(public_path() . $user->avatar)) {
                @unlink(public_path() . $user->avatar);
            }

            $file = $request->file('avatar') ?? $request->file('thumbnail_principal');
            if (is_array($file)) {
                $file = $file[0];
            }

            $tmpFilePath = '/uploads/users/' . $id . '/avatar/';
            if (!is_dir($tmpFilePath)) {
                @mkdir($tmpFilePath, 0777, true);
            }

            $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->extension();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path = $tmpFilePath . $tmpFileName;

            $user->update(['avatar' => $path]);

            return response()->json(['token' => $this->generateToken()], 200);
        } catch (ModelNotFoundException $e) {
            return response('', 400);
        }
    }

    public function save(Request $request) {
        $post = $request->input();
        if (isset($post['password'])) {
            $post['password'] = bcrypt($post['password']);
        } else {
            unset($post['password']);
        }
        unset($post['email'], $post['avatar'], $post['roles'], $post['permissions']);

        User::find(JWTUser::getUser()->id)->update($post);

        return response()->json(['token' => $this->generateToken()]);
    }

    public function generateToken() {
        $token = [
            'iat' => time(),
            'exp' => time() + (604800 * 104), // 604800 = 1 semana
            'user' => User::find(JWTUser::getUser()->id)->toArray()
        ];
        $token = JWT::encode($token, env('JWT_SECRET'));
        return $token;
    }

    public function favoritos(Request $request, $id) {
        $user = User::findOrFail(JWTUser::getUser()->id);
        $conteudos = $user->conteudos()
        ->select('conteudos.*', 'topicos.titulo as topico')
        ->leftJoin('topicos', 'topicos.id', 'conteudos.topico_id')
        ->wherePivot('favorited_at', '!=', null);
        if ($request->aplicativo_id) {
            $conteudos->where('aplicativo_id', $request->aplicativo_id);
        }
        $conteudos = $conteudos->get();
        foreach($conteudos as $key => $conteudo) {
            $conteudos[$key]->titulo = $conteudo->topico . ' - ' . $conteudo->titulo;
        }
        return response()->json($conteudos, 200);
    }

    public function esqueciSenha(Request $request) {
        $post = $request->input();
        if (empty($post['email'])) {
            return response()->json(['message' => 'Por favor informe seu e-mail'], 400);
        }

        $credentials = ['email' => $post['email']];

        $user = Sentinel::findByCredentials($credentials);
        if (!$user) {
            return response('', 404);
        }

        $tempPassword = bin2hex(openssl_random_pseudo_bytes(4));
        Mail::send('emails.esqueci_senha', [
            'user' => $user,
            'tempPassword' => $tempPassword
        ], function ($m) use ($user) {
            $m->from(env('MAIL_USERNAME'), env('MAIL_USERNAME'))
              ->to($user->email, $user->first_name)
              ->subject('Easylife APPS - Esqueci minha senha');
        });
        $user = Sentinel::update($user, ['password' => $tempPassword]);
        return response('');
    }

    /** NUTRI */
    public function agua(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode cadastrar sua água'], 403);
        }

        $user = User::find($user_id);
        $agua = $user->agua()->where('data', $request->data)->first();
        if (!$agua) {
            $agua = $user->agua()->create([
                'data' => $request->data,
                'quantidade' => 2000,
                'consumido' => 0
            ]);
        }
        return response()->json($agua);
    }

    public function aguaStore(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode cadastrar sua água'], 403);
        }
        $post = $request->input();
        $post['user_id'] = $user_id;
        $user = User::find($user_id);
        $agua = $user->agua()->where('data', $request->data)->first();

        if (!$agua) {
            $agua = $user->agua()->create($post);
        } else {
            $agua->update($post);
        }

        return response()->json($agua);
    }

    public function alimentos(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode ver seus alimentos'], 403);
        }

        $items = User::find($user_id)->alimentos()->wherePivot('data', $request->query('data'))->get();
        return response()->json($items);
    }

    public function alimentosStore(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode adicionar alimentos para si mesmo'], 403);
        }
        $post = array_only($request->input(), ['alimento_id', 'data', 'tipo', 'quantidade']);
        $user = User::find($user_id);

        // se o alimento existe vamos acrescentar a quantidade invez de adicionar uma nova linha
        if ($user->alimentos()->wherePivot('data', $post['data'])->wherePivot('tipo', $post['tipo'])->wherePivot('alimento_id', $post['alimento_id'])->exists()) {
            $alimento = $user->alimentos()
                             ->wherePivot('data', $post['data'])
                             ->wherePivot('tipo', $post['tipo'])
                             ->wherePivot('alimento_id', $post['alimento_id'])
                             ->first();
            $alimento->pivot->quantidade += $post['quantidade'];
            $alimento->pivot->save();
        } else {
            $user->alimentos()->attach([$post['alimento_id'] => $post]);
        }
    }

    public function alimentosUpdate(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode adicionar alimentos para si mesmo'], 403);
        }

        $post = $request->input();
        $alimento = DB::table('alimento_user')
        ->where('user_id', $user_id)
        ->where('data', $post['data'])
        ->where('tipo', $post['tipo'])
        ->where('alimento_id', $post['alimento_id'])
        ->update(['quantidade' => $post['quantidade']]);
    }

    public function alimentosDelete(Request $request, $user_id, $id) {
        $post = $request->input();
        DB::table('alimento_user')
          ->where('user_id', $user_id)
          ->where('alimento_id', $id)
          ->where('data', $post['data'])
          ->where('tipo', $post['tipo'])
          ->delete();
    }

    public function exercicios(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode ver seus exercicios'], 403);
        }

        $items = User::find($user_id)->exercicios()->wherePivot('data', $request->query('data'))->get();
        return response()->json($items);
    }

    public function exerciciosStore(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode adicionar exercicios para si mesmo'], 403);
        }
        $post = $request->input();
        $user = User::find($user_id);

        // se o exercicio existe vamos acrescentar a quantidade invez de adicionar uma nova linha
        if ($user->exercicios()->wherePivot('data', $post['data'])->wherePivot('exercicio_id', $post['exercicio_id'])->exists()) {
            $exercicio = $user->exercicios()
                             ->wherePivot('data', $post['data'])
                             ->wherePivot('exercicio_id', $post['exercicio_id'])
                             ->first();
            $exercicio->pivot->quantidade = $post['quantidade'];
            $exercicio->pivot->save();
        } else {
            $user->exercicios()->attach([$post['exercicio_id'] => $post]);
        }
    }

    public function exerciciosUpdate(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode adicionar exercicios para si mesmo'], 403);
        }
        $post = $request->input();
        $user = User::find($user_id);

        $exercicio = $user->exercicios()
                         ->wherePivot('data', $post['data'])
                         ->wherePivot('exercicio_id', $post['exercicio_id'])
                         ->first();
        $exercicio->pivot->quantidade += $post['quantidade'];
        $exercicio->pivot->save();
    }

    public function exerciciosDelete(Request $request, $user_id, $id) {
        $post = $request->input();
        DB::table('exercicio_user')
        ->where('user_id', $user_id)
        ->where('exercicio_id', $id)
        ->where('data', $post['data'])
        ->delete();
    }

    public function consultasDelete(Request $request, $user_id, $id) {
        DB::table('medicos')
          ->where('user_id', $user_id)
          ->where('id', $id)
          ->delete();
    }

    public function consultas(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode ver seus consultas'], 403);
        }


        if ($request->query('id'))
        {
            $items = User::find($user_id)->consultas()->where('id', $request->query('id'))->first();
        }
        else if ($request->query('area_id'))
        {
            $items_novos = User::find($user_id)->consultas()->where('data_consulta', '>=', date('Y-m-d'))->where('area_id', $request->query('area_id'))->orderBy('data_consulta','asc')->get()->toArray();
            $items_passados = User::find($user_id)->consultas()->where('data_consulta', '<', date('Y-m-d'))->where('area_id', $request->query('area_id'))->orderBy('data_consulta','desc')->get()->toArray();

            foreach ($items_novos as $key => $i) {
                $items_novos[$key]['novo'] = true;
            }
            foreach ($items_passados as $key =>  $i) {
                $items_passados[$key]['novo'] = false;
            }

            $items = array_merge($items_novos, $items_passados);

        }
        else
        {
            $items_novos = User::find($user_id)->consultas()->where('data_consulta', '>=', date('Y-m-d'))->orderBy('data_consulta','asc')->get()->toArray();
            $items_passados = User::find($user_id)->consultas()->where('data_consulta', '<', date('Y-m-d'))->orderBy('data_consulta','desc')->get()->toArray();

            foreach ($items_novos as $key => $i) {
                $items_novos[$key]['novo'] = true;
            }
            foreach ($items_passados as $key => $i) {
                $items_passados[$key]['novo'] = false;
            }

            $items = array_merge($items_novos, $items_passados);
        }
        
        return response()->json($items);
    }

    public function consultasImagens(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode ver seus consultas'], 403);
        }

        if ($request->query('area_id'))
        {
        
            $items = User::find($user_id)->consultas()->where('area_id', $request->query('area_id'))->whereNotNull("thumbnail_principal")->orderBy('data_consulta','desc')->get();    
        }
        else
        {
         $items = User::find($user_id)->consultas()->whereNotNull("thumbnail_principal")->orderBy('data_consulta','desc')->get();       
        }
        return response()->json($items);
    }

    public function consultasStore(Request $request, $user_id) {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode adicionar consultas para si mesmo'], 403);
        }
        $post = $request->input();
        $post['user_id'] = $user_id;
        $user = User::find($user_id);
       

        if (isset($post['id']))
        {
            $consultas = $user->consultas()->where('id', $post['id'])->first()->update($post);    
        }
        else
        {
            $consultas = $user->consultas()->create($post);

            if ($post["tem_revisao"] == 1)
            {
                   $nova_consultas = $user->consultas()->create(array("user_id" => $user_id,
                                                                 "area_id" => $post["area_id"],
                                                                 "subarea_id" => $post["subarea_id"],
                                                                 "nome" => $post["nome"],
                                                                "telefone" => $post["telefone"],
                                                                "data_consulta" => $post["data_revisao"],
                                                                "tem_Revisao" => 0));
            }
        }
    }


    public function imagemConsulta(Request $request, $user_id)
    {
        if ($user_id != JWTUser::getUser()->id) {
            return response()->json(['message' => 'Você só pode adicionar consultas para si mesmo'], 403);
        }

        if ($request->hasFile('file')) {
          
            $file = $request->file('file');
            if (is_array($file)) {
                $file = $file[0];
            }

            $tmpFilePath = '/uploads/users/' . $user_id . '/consultas/';
            if (!is_dir($tmpFilePath)) {
                @mkdir($tmpFilePath, 0777, true);
            }

            $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->extension();
            $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
            $path = $tmpFilePath . $tmpFileName;

            return response()->json(['thumbnail_principal' => $path, 'preview' => url($path)]);
        }
    }

}
