<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ConteudosService;
use App\Modules\Tags\Models\Tags;
use App\JWTUser;

use DB;

class TagsController extends Controller {
    /**
     * @var App\Services\ConteudosService
     */
    protected $_conteudos;

    public function __construct(ConteudosService $conteudosService) {
        $this->_conteudos = $conteudosService;
    }
    public function index(Request $request){
        $items = Tags::select('*');
        $queryString = $request->query();
        foreach($queryString as $qryStr => $val){
            if($qryStr === 'search' || $qryStr === 'busca'){
                $items->where(function($q) use ($val){
                    $q->orWhere('titulo', 'like', '%' . $val . '%');
                });
            }else if($qryStr === 'orderBy'){
                // Order by deve ser passado como query string da seguinte maneira orderBy=created_at.asc,titulo.desc, por padrão é asc, ou seja se não for informado ele vai assumir asc
                $orderBy = explode(',', $val);
                foreach($orderBy as $oBy){                 
                    $oBy = explode('.', $oBy);
                    $oBy[1] = $oBy[1] ?? 'asc';
                    $items->orderBy($oBy[0], $oBy[1]);
                }
            }
        }
        return response()->json($items->get());
    }

    public function detail($id) {
        try {
            $item = Tags::findOrFail($id);
            return response()->json($item);
        } catch (ModelNotFoundException $e) {
            return response('', 404);
        }
    }

    
}
