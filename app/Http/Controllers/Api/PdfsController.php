<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Pdfs\Models\Pdfs;
use DB;
use App\JWTUser;

class PdfsController extends Controller {

    public function index(Request $request){
        $items = Pdfs::orderBy('ordem', 'asc')->where('aplicativo_id', $request->aplicativo_id)->get();
        return response()->json($items);
    }

    public function ler(Request $request, $id) {
        if(!Pdfs::where('id', $id)->exists()){
            return response('', 404);
        }
        $user = JWTUser::getUser();
        $user_id = $user ? $user->id : 2; // Usuario 2 eé um usuario visitante

        if (!DB::table('pdf_user')->where('pdf_id', $id)->where('user_id', $user_id)->exists()) {
            DB::table('pdf_user')->insert(['pdf_id' => $id, 'user_id' => $user_id, 'read_at' => date('Y-m-d H:i:s')]);
        } else {
            DB::table('pdf_user')
              ->where('pdf_id', $id)
              ->where('user_id', $user_id)
              ->update(['read_at' => date('Y-m-d H:i:s')]);
        }
        return response('', 200);
    }

    public function patchFavoritar(Request $request, $id) {
        if(!Pdfs::where('id', $id)->exists()){
            return response('', 404);
        }
        $user_id = JWTUser::getUser()->id;

        if (!DB::table('pdf_user')->where('pdf_id', $id)->where('user_id', $user_id)->exists()) {
            DB::table('pdf_user')->insert(['pdf_id' => $id, 'user_id' => $user_id, 'favorited_at' => date('Y-m-d H:i:s')]);
        } else {
            DB::table('pdf_user')
              ->where('pdf_id', $id)
              ->where('user_id', $user_id)
              ->update(['updated_at' => date('Y-m-d H:i:s'), 'favorited_at' => date('Y-m-d H:i:s')]);
        }
        return response('', 200);
    }

    public function deleteFavoritar(Request $request, $id) {
        if(!Pdfs::where('id', $id)->exists()){
            return response('', 404);
        }
        $user_id = JWTUser::getUser()->id;

        DB::table('pdf_user')
          ->where('pdf_id', $id)
          ->where('user_id', $user_id)
          ->update(['updated_at' => date('Y-m-d H:i:s'), 'favorited_at' => null]);

        return response('', 200);
    }

    public function adminOrdem(Request $request) {
        $body = $request->input();
        foreach ($body['pdfs'] as $key => $t) {
            Pdfs::where('id', $key)->update($t);
        }
    }
}