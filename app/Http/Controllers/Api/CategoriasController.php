<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ConteudosService;
use App\Modules\Tags\Models\Tags;
use App\Modules\Categorias\Models\Categorias;
use App\Modules\Conteudos\Models\Conteudos;
use App\Modules\Topicos\Models\Topicos;
use App\JWTUser;
use DB;

class CategoriasController extends Controller {
    /**
     * @var App\Services\ConteudosService
     */
    protected $_conteudos;

    public function __construct(ConteudosService $conteudosService) {
        $this->_conteudos = $conteudosService;
    }

    public function adminOrdem(Request $request) {
        $body = $request->input();
        foreach ($body['categorias'] as $key => $t) {
            Categorias::where('id', $key)->update($t);
        }
    }

    public function index(Request $request) {
        $categorias = Categorias::with(['topicos' => function($q) {
            $q->orderBy('ordem', 'asc');
        }])->orderBy('ordem', 'asc');
        
        if($request->aplicativo_id) {
            $categorias->where('aplicativo_id',  $request->aplicativo_id);
        }
        if($request->completo) {
            $categorias->with([ 'topicos.topicos' => function ($q) {
                     $q->orderBy('ordem', 'asc');
                 }, 'topicos.conteudos' => function ($q) {
                     $q->orderBy('ordem', 'asc');
                 }, 'topicos.topicos.conteudos' => function($q) {
                     $q->orderBy('ordem', 'asc');
                 }]);
        }


        // $queryString = $request->query();
        // foreach ($queryString as $qryStr => $val) {
        //     if ($qryStr === 'search' || $qryStr === 'busca') {
        //         $categorias->where(function ($q) use ($val) {
        //             $q->orWhere('titulo', 'like', '%' . $val . '%');
        //         });
        //     } elseif ($qryStr === 'orderBy') {
        //         // Order by deve ser passado como query string da seguinte maneira orderBy=created_at.asc,titulo.desc, por padrão é asc, ou seja se não for informado ele vai assumir asc
        //         $orderBy = explode(',', $val);
        //         foreach ($orderBy as $oBy) {
        //             $oBy = explode('.', $oBy);
        //             $oBy[1] = $oBy[1] ?? 'asc';
        //             $categorias->orderBy($oBy[0], $oBy[1]);
        //         }
        //     }
        // }

        $categorias = $categorias->get();
        // $user = JWTUser::getUser();

        
        return response()->json($categorias);
    }

    public function show(Request $request, $id) {
        return Categorias::findOrFail($id);
    }

    public function categoriasTopicos(Request $request, $categoria_id) {
        $topicos = Topicos::with(['topicos' => function ($q) {
            $q->orderBy('ordem', 'asc');
        }, 'conteudos' => function ($q) {
            $q->orderBy('ordem', 'asc');
        }, 'topicos.conteudos' => function($q) {
            $q->orderBy('ordem', 'asc');
        }])
        ->orderBy('ordem', 'asc')
        ->where('categoria_id', $categoria_id)
        ->whereNull('topico_id')->get();

        return response()->json($topicos);
    }


    public function topicos(Request $request, $categoria_id) {
        try {
            $items = Categorias::select('id')->findOrFail($categoria_id)->topicos()->orderBy('ordem', 'asc');
            return response()->json($items->get());
        } catch (ModelNotFoundException $e) {
            return response('', 404);
        }
    }
}
