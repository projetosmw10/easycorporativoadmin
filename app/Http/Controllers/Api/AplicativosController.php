<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Aplicativos\Models\Aplicativos;

class AplicativosController extends Controller {

    public function index(Request $request){
        return response()->json(Aplicativos::where('id', '!=', 7)->get());
    }
    public function index2(Request $request) {
        return response()->json(Aplicativos::orderBy('ordem')->get());
    }
}