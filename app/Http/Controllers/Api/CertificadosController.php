<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use PDF;
use DB;
use Mail;
use App\Modules\Categorias\Models\Categorias;
use App\Modules\Certificados\Models\Certificados;
use App\Modules\Informacoes\Models\Informacoes;
use App\JWTUser;
use App\User;

use App\Services\CieloService;


class CertificadosController extends Controller {

    public function index() {
        return Certificados::all();
    }

    public function gerar($id) {
        try {
            $id = decrypt($id);

            $categoria_certificado = DB::table('categoria_certificado')->where('id', $id)->first();
            if(!$categoria_certificado) {
                return 'Certificado inválido';
            }
            
            $certificado = (Certificados::find($categoria_certificado->certificado_id)->getOriginal('thumbnail_principal'));

            $pdf = PDF::loadView('certificados.certificados', ['nome' => $categoria_certificado->nome, 'categoria' => $categoria_certificado->categoria, 'data' => $categoria_certificado->created_at, 'certificado' => $certificado]);

            $pdf->setOptions(['dpi' => 300]);
            $pdf->setPaper('a4', 'landscape');

        } catch(\Exception $e) {
            return 'Certificado inválido';
        }

        return $pdf->stream();
    }

    public function pagar(Request $request) {
        $certificado = Certificados::findOrFail($request->certificado_id);
        $categoria = Categorias::findOrFail($request->categoria_id);
        $user = JWTUser::getUser();

        // CieloService


        

        // Gera registro que o certificado foi gerado 
        $categoria_certificado = DB::table('categoria_certificado')->insertGetId([
            'categoria_id' => $categoria->id,
            'categoria' => $categoria->titulo,
            'user_id' => $user->id,
            'certificado_id' => $certificado->id,
            'nome' => $request->nome,
            'emails' => $request->emails
        ]);

        try {
            $cielo = CieloService::creditCard([
                'MerchantOrderId' => $categoria_certificado,
                'Customer' => [
                    'Name' => $request->cc['name']
                ],
                'Payment' => [
                    'Amount' => $certificado->preco,
                    'Installments' => 1,
                    'CreditCard' => [
                        'CardNumber' => str_replace(' ', '', $request->cc['number']),
                        'Holder' => $request->cc['name'],
                        'ExpirationDate' => str_pad($request->cc['month'], 2, '0', STR_PAD_LEFT).'/20'.$request->cc['year'],
                        'SecurityCode' => $request->cc['cvv'],
                        'Brand' => CieloService::getCCBrand(str_replace(' ', '', $request->cc['number']))
                    ]
                ]
            ]);
            DB::table('categoria_certificado')->where('id', $categoria_certificado)->update(['payment' => json_encode($cielo)]);
            $cieloStatus = $cielo && isset($cielo['Payment']['Status']) ? $cielo['Payment']['Status'] : null;
            if(!($cieloStatus == 2 || $cieloStatus == 1)) return response(['message' => 'Não foi possível processar o pagamento, verifique seus dados e tente novamente #1'], 400);

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            DB::table('categoria_certificado')->where('id', $categoria_certificado)->update(['payment' => json_encode($e->getResponse()->getBody()->getContents())]);
            return response([
                'message' => 'Não foi possível processar o pagamento, verifique seus dados e tente novamente #2',
                'error' => $e->getResponse()->getBody()->getContents()
            ], 400);
        }
        

        $countCertificados = DB::table('categoria_certificado')
        ->where('user_id', $user->id)
        ->where('categoria_id', $categoria->id)
        ->count();

        // envia alerta de muitos certificados para os admins easylife
        if($countCertificados >= 5) {
            $emailsEasy = Informacoes::first();
            $emailsEasy = explode(',',$emailsEasy->contato_emails);

            Mail::send('certificados.email_atencao', [
                'user' => $user,
                'categoria' => $categoria,
                'countCertificados' => $countCertificados
            ], function ($m) use ($user, $emailsEasy, $countCertificados) {
                $m->from(env('MAIL_USERNAME'), env('MAIL_USERNAME'))
                ->subject($user->first_name.' gerou '.$countCertificados .' certificados!');
                foreach($emailsEasy as $email) {
                    // $m->to($email);
                }
                $m->to('pedronidegodois@hotmail.com');
            });

        }

        Mail::send('certificados.email', [
            'user' => $user,
            'nome' => $request->nome,
            'categoria' => $categoria,
            'categoria_certificado' => $categoria_certificado,
            'countCertificados' => $countCertificados
        ], function ($m) use ($user, $countCertificados, $categoria_certificado, $request) {
            $m->from(env('MAIL_USERNAME'), env('MAIL_USERNAME'))
            ->subject('EasyLife - Imprima seu certificado!');
            $emails = explode(',', $request->emails);
            $m->to($user->email);
            foreach($emails as $email) {
                $m->to($email);
            }
        });

        return response([
            'link' => url('api/certificados/gerar/'.encrypt($categoria_certificado)),
            'message' => 'Parabéns! O pagamento foi efetuado com sucesso, agora você pode imprimir seu certificado'
        ]);

        

    }

}
