<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Onesignal\Models\Onesignal;
use App\JWTUser;

use DB;

class OnesignalController extends Controller {
    public function save(Request $request, $user_id){
        $body = $request->input();
        if($user_id != JWTUser::getUser()->id){
            return response()->json(['message_dev' => 'O usuário autenticado é diferente da url especificada'], 400);
        }
        if(!isset($body['player_id'])){
            return response()->json(['message_dev' => 'Você deve informar o player id do usuário'], 400);
        }
        $first = Onesignal::where('player_id', $body['player_id'])->first();
        if($first){
            $first->user_id = $user_id;
            $first->save();
        }else{
            $first = new Onesignal();
            $first->user_id = $user_id;
            $first->player_id = $body['player_id'];
            $first->save();
        }

        return response()->json($first, 200);
    }
}
