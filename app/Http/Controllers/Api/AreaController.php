<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Area\Models\Area;
use App\JWTUser;
use DB;

class AreaController extends Controller {
    /**
     * @var App\Services\ConteudosService
     */
    protected $_conteudos;

    public function index(Request $request) {
     
        $areas = Area::orderBy('descricao')->get();
        // $user = JWTUser::getUser();

        foreach ($areas as $key => $a) {
        	$areas[$key]["subareas"] = $a->subareas()->orderBy('descricao')->get()->toArray();
        }

        
        
        return response()->json($areas);
    }

}
