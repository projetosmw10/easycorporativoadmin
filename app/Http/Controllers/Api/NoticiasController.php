<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Noticias\Models\Noticias;
use App\JWTUser;
use DB;

class NoticiasController extends Controller {
    /**
     * @var App\Services\ConteudosService
     */
    protected $_conteudos;

    public function index(Request $request) {
     
        $noticias = Noticias::orderBy('data', 'desc')->get();
        // $user = JWTUser::getUser();

        
        return response()->json($noticias);
    }

}
