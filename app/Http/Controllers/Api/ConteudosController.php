<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ConteudosService;
use App\Modules\Conteudos\Models\Conteudos;
use App\Modules\Tags\Models\Tags;
use App\Modules\Categorias\Models\Categorias;
use App\JWTUser;
use DB;
use Sentinel;

class ConteudosController extends Controller {
    /**
     * @var App\Services\ConteudosService
     */
    protected $_conteudos;

    public function __construct(ConteudosService $conteudosService) {
        $this->_conteudos = $conteudosService;
    }

    public function adminIndex(Request $request) {
        $items = $this->_conteudos->queryBuilder($request->query());
        return response()->json($items->get(), 200);
    }

    public function index(Request $request) {
        $items = $this->_conteudos->queryBuilder($request->query());
        $items->with(['tags' => function ($q) {
            $q->select('tags.id', 'tags.titulo');
        }, 'categoria' => function ($q) {
            $q->select('categorias.id', 'titulo');
        }]);

        /**
         * NOVA REGRA DE NEGOCIO AGORA QUANDO É CONTEUDO É PAGO SO NAO MOSTRA O LINK E NO APP FICA  
         */
        // if (!JWTUser::isPremium()) {
        //     $items->where('gratuito', 1);
        // }
        $items = $items->orderBy('ordem', 'asc')->get();
 
        return response()->json($items, 200);
    }
    
    public function search(Request $request) {
        $search = explode(' ', $request->query('search', ''));
        $conteudos = Conteudos::select('conteudos.*', 'categorias.titulo AS categoria', 'topicos.titulo AS topico')->distinct()
                                ->leftJoin('conteudo_tag', 'conteudo_tag.conteudo_id', 'conteudos.id')
                                ->leftJoin('topicos', 'topicos.id', 'conteudos.topico_id')
                                ->leftJoin('categorias', 'categorias.id', 'topicos.categoria_id')
                                ->leftJoin('tags', 'tags.id', 'conteudo_tag.tag_id')
                                ->orderBy('categorias.ordem')
                                ->orderBy('topicos.ordem')
                                ->orderBy('conteudos.ordem');

        foreach($search as $s) {
            $conteudos->orWhere('tags.titulo', 'like', '%'.$s.'%')
                      ->orWhere('conteudos.titulo', 'like', '%'.$s.'%')
                      ->orWhere('categorias.titulo', 'like', '%'.$s.'%')
                      ->orWhere('topicos.titulo', 'like', '%'.$s.'%');
        }
        $conteudos = $conteudos->get();
        foreach($conteudos as $key => $conteudo) {
            $conteudos[$key]->titulo = $conteudo->topico . ' - ' . $conteudo->titulo;
        }
        return response()->json($conteudos, 200);
    }

    public function detail($id) {
        try {
            $item = Conteudos::with(['tags' => function ($q) {
                $q->select('tags.id', 'titulo');
            }, 'categoria' => function ($q) {
                $q->select('categorias.id', 'titulo');
            }])->findOrFail($id);
            if (!JWTUser::isPremium() && $item->gratuito != 1) {
                return response()->json(['message' => 'O conteudo que você está tentando acessar não é gratuito'], 403);
            }
            return response()->json($item);
        } catch (ModelNotFoundException $e) {
            return response('', 404);
        }
    }

    public function visualizar(Request $request, $id) {
        if(!Conteudos::where('id', $id)->exists()){
            return response('', 404);
        }
        $user = JWTUser::getUser();
        $user_id = $user ? $user->id : 2; // Usuario 2 eé um usuario visitante

        if (!DB::table('conteudo_user')->where('conteudo_id', $id)->where('user_id', $user_id)->exists()) {
            DB::table('conteudo_user')->insert(['conteudo_id' => $id, 'user_id' => $user_id, 'watched_at' => date('Y-m-d H:i:s')]);
        } else {
            DB::table('conteudo_user')
              ->where('conteudo_id', $id)
              ->where('user_id', $user_id)
              ->update(['watched_at' => date('Y-m-d H:i:s')]);
        }
        return response('', 200);
    }

    public function patchFavoritar(Request $request, $id) {
        if(!Conteudos::where('id', $id)->exists()){
            return response('', 404);
        }
        $user_id = JWTUser::getUser()->id;

        if (!DB::table('conteudo_user')->where('conteudo_id', $id)->where('user_id', $user_id)->exists()) {
            DB::table('conteudo_user')->insert(['conteudo_id' => $id, 'user_id' => $user_id, 'favorited_at' => date('Y-m-d H:i:s')]);
        } else {
            DB::table('conteudo_user')
              ->where('conteudo_id', $id)
              ->where('user_id', $user_id)
              ->update(['updated_at' => date('Y-m-d H:i:s'), 'favorited_at' => date('Y-m-d H:i:s')]);
        }
        return response('', 200);
    }

    public function deleteFavoritar(Request $request, $id) {
        if(!Conteudos::where('id', $id)->exists()){
            return response('', 404);
        }
        $user_id = JWTUser::getUser()->id;

        DB::table('conteudo_user')
          ->where('conteudo_id', $id)
          ->where('user_id', $user_id)
          ->update(['updated_at' => date('Y-m-d H:i:s'), 'favorited_at' => null]);

        return response('', 200);
    }

    public function adminOrdem(Request $request) {
        $body = $request->input();
        foreach ($body['conteudos'] as $key => $t) {
            Conteudos::where('id', $key)->update($t);
        }
    }
}
