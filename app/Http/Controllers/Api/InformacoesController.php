<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Informacoes\Models\Informacoes;

class InformacoesController extends Controller {

    public function index(Request $request){
        $item = Informacoes::first();
        $item->thumbnail_principal = $item->thumbnail_principal ? url($item->thumbnail_principal) : null;
        // se 0 NAO mostra o menu de codigo no app
        // se 1 mostra o menu de codigo no app

        // agora ta no banco de dados o c1
        $item->c = 1;
        // $item->c1 = 1;
        return response()->json($item);
    }
}