<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Exercicios\Models\Exercicios;
use App\Modules\Tags\Models\Tags;
use App\Modules\Categorias\Models\Categorias;
use App\Modules\Roteiros\Models\Roteiros;
use App\JWTUser;
use App\User;
use DB;
use Sentinel;

class ExerciciosController extends Controller {

    public function index(Request $request) {
        $page = $request->query('page', 0);
        $search = $request->query('search', null);

        $items = Exercicios::orderBy('titulo');
    
        
        if(!empty($search)) {
            $search = explode(' ', $search);
            $items->where(function($q) use ($search) {
                $fillable = (new Exercicios())->getFillable();
                foreach($search as $s) {
                    foreach($fillable as $f) {
                        $q->orWhere($f, 'like', '%'.$s.'%');
                    }
                }
            });
        }

        if($page) {
            $items = $items->paginate(10);
        }else {
            $items = $items->get();
        }
        return response()->json($items, 200);
    }


}
