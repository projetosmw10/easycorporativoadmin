<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Alimentos\Models\Alimentos;
use App\Modules\Tags\Models\Tags;
use App\Modules\Categorias\Models\Categorias;
use App\Modules\Roteiros\Models\Roteiros;
use App\JWTUser;
use App\User;
use DB;
use Sentinel;

class AlimentosController extends Controller {

    public function index(Request $request) {
        $page = $request->query('page', 0);
        $search = $request->query('search', null);
        $favoritado = $request->query('favoritado', null);

        if($favoritado == 1) {
            $ids = DB::table('alimento_user')->where('user_id', JWTUser::getUser()->id)->whereNotNull('favorited_at')->pluck('alimento_id');    
            $items = Alimentos::orderBy('titulo')->whereIn('id', $ids);
        }else{
            $items = Alimentos::orderBy('titulo');
        }
 
        $items->where('destaque', $request->query('destaque', 0));
        
        if(!empty($search)) {
            $search = explode(' ', $search);
            $items->where(function($q) use ($search) {
                $fillable = (new Alimentos())->getFillable();
                foreach($search as $s) {
                    foreach($fillable as $f) {
                        $q->orWhere($f, 'like', '%'.$s.'%');
                    }
                }
            });
        }

        if($page) {
            $items = $items->paginate(10);
        }else {
            $items = $items->get();
        }
        return response()->json($items, 200);
    }


    public function patchFavoritar(Request $request, $id) {
        if (!Alimentos::where('id', $id)->exists()) {
            return response('', 404);
        }
        $user_id = JWTUser::getUser()->id;

        if (!DB::table('alimento_user')->where('alimento_id', $id)->where('user_id', $user_id)->exists()) {
            DB::table('alimento_user')->insert(['alimento_id' => $id, 'user_id' => $user_id, 'favorited_at' => date('Y-m-d H:i:s')]);
        } else {
            DB::table('alimento_user')
              ->where('alimento_id', $id)
              ->where('user_id', $user_id)
              ->update(['favorited_at' => date('Y-m-d H:i:s')]);
        }
        return response('', 200);
    }

    public function deleteFavoritar(Request $request, $id) {
        if (!Alimentos::where('id', $id)->exists()) {
            return response('', 404);
        }
        $user_id = JWTUser::getUser()->id;

        DB::table('alimento_user')
          ->where('alimento_id', $id)
          ->where('user_id', $user_id)
          ->update(['favorited_at' => null]);

        return response('', 200);
    }

}
