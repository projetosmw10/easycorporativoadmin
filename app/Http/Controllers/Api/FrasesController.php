<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Frases\Models\Frases;

class FrasesController extends Controller {

    public function index(Request $request){
        $item = Frases::where('data', date('Y-m-d'))->first();
        if(!$item) {
            $item = Frases::latest()->first();
        }
        return response()->json($item);
    }
}