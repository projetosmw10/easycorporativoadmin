<?php

namespace App\Http\Controllers\Api;

use Validator;
use App\Http\Requests;
//use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\LoginFormRequest;
use Sentinel;
use Dingo\Api\Routing\Helpers;
use Illuminate\Routing\Controller;
use App\User;
use Activation;
use Image;
use DB;
use Mail;
use OneSignal;
use App\Services\NotificationService;

class ApiController extends Controller {
    use Helpers;

	/**
     * @var \App\Services\NotificationService
     */
    private $_notification;

    public function __construct(NotificationService $notificationService) {
        $this->_notification = $notificationService;
	}

    public function premiumExpirando(Request $request) {
		
		$users = User::whereRaw('(premium_expire_at BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY))')
		->whereRaw('(premium_expiring_notification_at IS NULL OR premium_expiring_notification_at NOT BETWEEN DATE_SUB(NOW(), INTERVAL 3 DAY) AND NOW())')->get();

		$response = [];
        foreach ($users as $user) {
            User::where('id', $user->id)->update(['premium_expiring_notification_at' => date('Y-m-d H:i:s')]);
            $this->_notification->sendToUsers(['table' => 'codes'], [$user->id], 'O seu código está prestes a expirar', 'Seu código irá expirar em '.date('d/m/Y', strtotime($user->premium_expire_at)));
            $response[] = 'Notificação de premium expirando enviada para "' . $user->email . '" com sucesso';
        }
        return response()->json($response);

    }
}
