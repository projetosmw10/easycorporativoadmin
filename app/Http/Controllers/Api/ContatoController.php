<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Contato\Models\Contato;
use App\Modules\Informacoes\Models\Informacoes;
use Mail;

class ContatoController extends Controller {

    public function save(Request $request){
        $item = Contato::create($request->input());
        $emails = Informacoes::first();
        $emails = explode(',',$emails->contato_emails);
        if(count($emails)) {
            Mail::send('emails.contato', [
                'item' => $item
            ], function ($m) use ($item, $emails) {
                $m->from(env('MAIL_USERNAME'), env('MAIL_USERNAME'))
                ->replyTo($item->email)
                ->subject('EasyFit Sport - CONTATO');
                foreach($emails as $email) {
                    $m->to($email);
                }
            });
        }
        return response('', 200);
    }
}