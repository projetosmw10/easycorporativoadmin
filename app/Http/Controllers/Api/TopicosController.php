<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ConteudosService;
use App\Modules\Topicos\Models\Topicos;
use App\Modules\Conteudos\Models\Conteudos;
use App\Modules\Tags\Models\Tags;
use App\Modules\Categorias\Models\Categorias;
use App\JWTUser;
use DB;
use Sentinel;

class TopicosController extends Controller {
        /**
     * @var \App\Services\ConteudosService
     */
    protected $_conteudos;

    public function __construct(ConteudosService $conteudosService) {
        $this->_conteudos = $conteudosService;
    }
    public function adminOrdem(Request $request, $categoria_id) {
        $body = $request->input();
        foreach ($body['topicos'] as $key => $t) {
            Topicos::where('id', $key)->update($t);
        }
    }




}
