<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Conheca\Models\Conheca;

class ConhecaController extends Controller {

    public function index(Request $request){
        return response()->json(Conheca::first());
    }
}