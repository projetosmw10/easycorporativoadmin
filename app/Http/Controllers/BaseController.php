<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Sentinel;
use App\Modules\Aplicativos\Models\Aplicativos;

class BaseController extends Controller
{

	protected $current_user;
	protected $current_app;
	protected $current_module;
	protected $current_role;
	protected $current_template;

	public function __construct()
	{

		$this->current_app = Aplicativos::find(session('aplicativo_id'));
		$user = Sentinel::getUser();
		if($user && $user->inRole('empresa')) {
			$this->current_app = new \stdClass();
			$this->current_app->id = 0;
			$this->current_app->thumbnail_principal = 'img/logo-brand.svg';
			$this->current_app->cor = '#0e0e0e'; 
			$this->current_app->titulo = 'Easylife';
			$this->current_app->descricao = 'Easylife';
		} else if ($user && $user->inRole('gerencia')) {
			$this->current_app = new \stdClass();
			$this->current_app->id = 0;
			$this->current_app->thumbnail_principal = 'img/logo-brand.svg';
			$this->current_app->cor = '#d4af37'; 
			$this->current_app->titulo = 'Easylife';
			$this->current_app->descricao = 'Easylife';
		}
		
		if(!$this->current_app && request()->segment(1) == 'admin' && !empty(request()->segment(2)) && request()->segment('2') != 'aplicativos') {
			die('<script>window.location.href = "/admin"</script>');
		}
		session(['current_app' => $this->current_app]);
		\View::share('current_app', $this->current_app);
		$this->current_user = Sentinel::getUser();
		if($this->current_user){
			\View::share('current_user', $this->current_user);
			$this->current_role = Sentinel::findRoleById($this->current_user->roles()->first()->id);
			\View::share('current_role', $this->current_role);

			switch (json_decode($this->current_role)->slug) {
				default:
					$this->current_template = 'layouts.app';
					break;
			}

			\View::share('current_template', $this->current_template);

		}


		$currentPath = \Request::path();
		$pieces = explode('/', $currentPath);
		if(!empty($pieces) && isset($pieces[1])){
			$this->current_module = \App\Gerador::where('rota',$pieces[1])->first();
		}else{
			$this->current_module = null;
		}
		\View::share('current_module', $this->current_module);
	}

}
