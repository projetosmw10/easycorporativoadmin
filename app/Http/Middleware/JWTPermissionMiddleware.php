<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use App\JWTUser;
use \Exception;
use Closure;

// the first argument must be the table's, the rest can be create, view, update or delete.
// the middleware JWTMiddleware must be always called before JWTRoleMiddleware...
class JWTPermissionMiddleware {

    public function handle(Request $request, Closure $next, $table, ...$params){
        
        if(!JWTUser::getUser()){
            throw new \UnexpectedValueException('The user hasn\t been defined yet. Check whether you have called jwtauth middleware before calling jwtpermission');
        }
        $check = [];
        foreach($params as $p){
            $check[] = $table.'.'.$p;
        }
        if(JWTUser::hasAnyAccess($check)){
            return $next($request);
        }

        return response('', 403);

    }
}
