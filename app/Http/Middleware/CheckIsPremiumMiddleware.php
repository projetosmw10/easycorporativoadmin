<?php

namespace App\Http\Middleware;

use Closure;
use App\JWTUser;

class CheckIsPremiumMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        JWTUser::isPremium();
        return $next($request);
    }
}
