<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use App\JWTUser;
use \Exception;
use Closure;
use Sentinel;
use App\Http\Controllers\BaseController;

class InRoleMiddleware extends BaseController {
    public function handle(Request $request, Closure $next, $role) {
        if (Sentinel::getUser()->inRole($role)) {
            return $next($request);
        }

        if ($request->ajax()) {
            return response('Bad Permissions.', 403);
        } else {
            return redirect('admin/bad_permissions', 302);
        }
    }
}
