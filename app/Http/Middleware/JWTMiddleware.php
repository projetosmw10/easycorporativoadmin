<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use App\JWTUser;
use App\JWTTokens;
use \Exception;
use \Closure;

class JWTMiddleware {
    public function handle(Request $request, Closure $next) {
        $token = $request->header('Authorization');
        if (empty($token)) {
            $token = $request->server('HTTP_AUTHORIZATION');
        }
        if (empty($token)) {
            return response()->json(['message_dev' => 'You must provide an Authorization header containing your jwt access token'], 401);
        }
        
        $token = str_replace('Bearer ', '', $token);
        $token = str_replace('Bearer', '', $token);

        if(JWTTokens::isBlacklisted($token)) {
            return response()->json(['message_dev' => 'The token has been blacklisted, generate a new one'], 401);
        }
        try {
            $request->auth = JWTUser::setUser(JWT::decode($token, env('JWT_SECRET'), ['HS256'])->user);
            return $next($request);
        } catch (SignatureInvalidException $e) {
            return response()->json([
                'message_dev' => $e->getMessage()
            ], 401);
        } catch (BeforeValidException $e) {
            return response()->json([
                'message_dev' => $e->getMessage()
            ], 401);
        } catch (ExpiredException $e) {
            return response()->json([
                'message_dev' => $e->getMessage()
            ], 401);
        } catch (Exception $e) {
            return response()->json([
                'message_dev' => $e->getMessage()
            ], 401);
        }
    }

    public function terminate($request, $response) {
        Log::info('app.requests', [
            'request' => [
                'ips' => $request->ips(),
                'user_id' => !empty(JWTUser::getUser()) ? JWTUser::getUser()->id : null,
                'method' => $request->method(),
                'path' => $request->path(),
                'query' => $request->query(),
                'input' => $request->input()
            ],
            'response' => [
                'status' => $response->status(),
                'content' => $request->method() == 'GET' && $response->status() < 400 ? 'No content for successful GET' : $response->getOriginalContent()
            ]
        ]);
    }
}
