<?php

namespace App\Http\Middleware;

use Closure;
use App\JWTUser;

class IsPremiumMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!JWTUser::isPremium()) {
            return response()->json(['message' => 'O conteudo que você está tentando acessar não é gratuito'], 403);
        }
        return $next($request);
    }
}
