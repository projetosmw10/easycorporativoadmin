<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use App\JWTUser;
use \Exception;
use Closure;

// verifies if the user is in any of the specified role, separete the roles using commas ","
// the middleware JWTMiddleware must be always called before JWTRoleMiddleware...
class JWTRoleMiddleware {

    public function handle(Request $request, Closure $next, ...$roles){
        
        if(!JWTUser::getUser()){
            throw new \UnexpectedValueException('The user hasn\'t been defined yet. Check whether you have called jwtauth middleware before calling jwtrole');
        }
        if(JWTUser::inAnyRole($roles)){
            return $next($request);
        }

        return response('', 403);

    }
}
