<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class JWTTokens extends Model {
    protected $table = 'jwt_tokens';
    protected $fillable = ['user_id', 'token', 'token_sha256', 'blacklist', 'expire_at'];

    public function setTokenAttribute($val) {
        $this->attributes['token'] = $val;
        $this->attributes['token_sha256'] = hash('sha256', $val); // salva o token em md5 para poder comparar depois
    }

    public function topicos() {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function scopeExpired($query) {
        return $query->where('expire_at', '<=', date('Y-m-d H:i:s'));
    }
    public function scopeIsBlacklisted($query, $val) {

        $val = hash('sha256', $val);
        return $query->where('token_sha256', $val)->where('blacklist', 1)->exists();
    }

}