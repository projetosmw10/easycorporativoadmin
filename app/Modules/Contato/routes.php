<?php

// ================================
// Contato routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Contato\Controllers'), function() {
	Route::get('contato', ['uses' => 'ContatoController@index']);
	Route::get('contato/detalhe/{slug}', ['uses' => 'ContatoController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Contato\Controllers\Admin'), function() {
	Route::get('admin/contato', ['uses' => 'AdminContatoController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Contato\Controllers\Admin'), function() {
	Route::get('admin/contato/add', ['uses' => 'AdminContatoController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Contato\Controllers\Admin'), function() {
	Route::get('admin/contato/edit/{id}', ['uses' => 'AdminContatoController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Contato\Controllers\Admin'), function() {
	Route::get('admin/contato/delete/{id}', ['uses' => 'AdminContatoController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Contato\Controllers\Admin'), function() {
	Route::post('admin/contato/save', ['uses' => 'AdminContatoController@save']);
	Route::post('admin/contato/upload_galeria/{id}', ['uses' => 'AdminContatoController@upload_galeria']);
    Route::post('admin/contato/delete_thumbnail_principal', ['uses' => 'AdminContatoController@delete_thumbnail_principal']);
    Route::post('admin/contato/delete_imagem/{id}', ['uses' => 'AdminContatoController@delete_imagem']);
});
