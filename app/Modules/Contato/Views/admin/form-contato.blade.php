@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Contato</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/contato') }}">Contato</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
							<?php if($modulo->galeria){ ?>
								<li><a data-toggle="pill" href="#imagens-tab">Galeria</a></li>
							<?php } ?>
							<li><a data-toggle="pill" href="#seo-tab">SEO</a></li>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/contato/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
                                    <div class="form-group">
                                        <label for="nome" class="col-md-3 control-label">nome *</label>
                                        <div class="col-md-7">
                                        <input id="nome" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->nome : ""; ?>" name="nome" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="telefone" class="col-md-3 control-label">telefone *</label>
                                        <div class="col-md-7">
                                        <input id="telefone" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->telefone : ""; ?>" name="telefone" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="email" class="col-md-3 control-label">email *</label>
                                        <div class="col-md-7">
                                        <input id="email" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->email : ""; ?>" name="email" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="mensagem" class="col-md-3 control-label">mensagem *</label>
                                        <div class="col-md-7">
                                        <textarea id="mensagem" required class="form-control tinymce" name="mensagem"><?php echo (isset($item)) ? $item->mensagem : ""; ?></textarea>
                                    </div>
             </div>

								</div>

								<div id="seo-tab" class="tab-pane fade">
									<div class="form-group">
										<label for="meta_keywords" class="col-md-3 control-label">URL Amigável</label>

										<div class="col-md-7">
											<input type="text" class="form-control" name="slug" value="<?php echo isset($item) ? $item->slug : ''; ?>">
										</div>
									</div>
									<div class="form-group">
										<label for="meta_keywords" class="col-md-3 control-label">Palavras Chave</label>

										<div class="col-md-7">
											<div id="meta_keywords"></div>
										</div>
									</div>
									<div class="form-group">
										<label for="meta_descricao" class="col-md-3 control-label">Meta Descrição</label>

										<div class="col-md-7">
											<textarea id="meta_descricao" type="text" class="form-control" name="meta_descricao"><?php echo (isset($item)) ? $item->meta_descricao : ''; ?></textarea>
										</div>
									</div>
									<script>
										new Taggle('meta_keywords', {
											<?php if(isset($item) && $item->meta_keywords != ''){ ?>
												tags: [
													<?php $tags = explode(',',$item->meta_keywords); ?>

													<?php foreach($tags as $tag){ ?>
												    	'<?php echo $tag; ?>',
												   <?php } ?>
												],
											<?php }else{ ?>
												tags: [
													'item'
												],
											<?php } ?>
										    duplicateTagClass: 'bounce',
											 hiddenInputName: 'meta_keywords[]'
										});
									</script>
								</div>
							</form>

							<?php if($modulo->galeria){ ?>

								<div id="imagens-tab" class="tab-pane fade">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lista-galeria">
										<?php if(isset($item) && count($item->imagens)){?>
											<?php foreach ($item->imagens as $image){?>
												<div id="item_<?php echo $image->id; ?>" class="item imagem-galeria-<?php echo $image->id; ?>">
													<div style="background-image: url(<?php echo "/uploads/contato/$image->thumbnail_principal";?>);" class="thumb"></div>
													<span data="<?php echo $image->id; ?>" data-modulo="contato" class="icon delete-image" aria-hidden="true"><i class="fa fa-trash"></i></span>
												</div>
											<?php }?>
										<?php }?>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<form class="dropzone" id="galeria-dropzone" method="POST" action="<?php echo (isset($item)) ? url('/admin/contato/upload_galeria/'.$item->id) : url('/admin/contato/upload_galeria/'.$nextId); ?> " enctype="multipart/form-data">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" />
											<div class="fallback">
												<input name="file" type="file" multiple />
											</div>
										<form>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/contato/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
