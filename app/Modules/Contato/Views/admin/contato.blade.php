@extends('layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Contato
			<small>Listagem</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Contato</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<div class="box">
					<div class="box-header">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<!-- <a href="{{ url('admin/contato/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a> -->
						<?php } ?>
						<hr>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<table id="list-data-table" class="table table-bordered table-striped">

                            <thead>
                                 <tr>
                                     <th>Enviado em</th>
                                     <th>Aplicativo</th>
                                     <th>Nome</th>
                                     <th>Telefone</th>
                                     <th>Email</th>
                                     <th>mensagem</th>
                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($itens as $item){ ?>
									<tr>
										<td><span hidden><?php echo strtotime($item->created_at)?></span>
										<?php echo date('d/m/Y H:i', strtotime($item->created_at))?>
										</td>
                                            <td> <?php echo $item->aplicativo ? $item->aplicativo->titulo : ''?> </td>
                                            <td> <?php echo $item->nome?> </td>
                                            <td> <?php echo $item->telefone?> </td>
                                            <td> <a href="mailto:<?php echo $item->email?>"><?php echo $item->email?></a> </td>
                                            <td> <?php echo $item->mensagem?> </td>
                                            <td>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<!-- <a href="/admin/contato/edit/<?php echo $item->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/contato/delete/<?php echo $item->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/contato/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
