<?php

namespace App\Modules\Contato\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table = 'contato';
    protected $fillable = array('nome', 'telefone', 'email', 'mensagem', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', 'aplicativo_id' );

	public function aplicativo() {
		return $this->belongsTo('App\Modules\Aplicativos\Models\Aplicativos', 'aplicativo_id');
	}

	public static function getImagens($id){
		return DB::table('contato_imagens')->where('id_contato', $id)->get();
    }
    public static function getImagem($id){
		return DB::table('contato_imagens')->find($id);
    }
    
	public static function criar_imagem($input){
		return DB::table('contato_imagens')->insert([
			[
				'id_contato' => $input['id_contato'],
				'thumbnail_principal' => $input['thumbnail_principal'],
			]
		]);
    }
    public static function deletar_imagem($id){
		return DB::table('contato_imagens')
				->where('id', $id)
				->delete();
	}

	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'contato' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
