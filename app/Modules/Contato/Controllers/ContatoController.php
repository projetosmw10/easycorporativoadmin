<?php

namespace App\Modules\Contato\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ContatoController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(28);
	}

	public function index(){
		$data = array();
		return view('contato',$data);
	}
}
