<?php

namespace App\Modules\Medicos\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Medicos extends Model
{
    protected $table = 'medicos';
    protected $fillable = array('area_id', 'nome', 'telefone', 'data_consulta', 'observacoes', 'tem_revisao', 'data_revisao', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', 'subarea_id', 'user_id');

    public function user() {
        return $this->belongsTo('\App\User', 'user_id');
    }

    public function area() {
        return $this->belongsTo('\App\Modules\Area\Models\Area', 'area_id');
    }

    public function getThumbnailPrincipalAttribute($val) {
        return $val ? url($val) : '';
    }

	public static function getImagens($id){
		return DB::table('medicos_imagens')->where('id_medicos', $id)->get();
    }
    public static function getImagem($id){
		return DB::table('medicos_imagens')->find($id);
    }
    
	public static function criar_imagem($input){
		return DB::table('medicos_imagens')->insert([
			[
				'id_medicos' => $input['id_medicos'],
				'thumbnail_principal' => $input['thumbnail_principal'],
			]
		]);
    }
    public static function deletar_imagem($id){
		return DB::table('medicos_imagens')
				->where('id', $id)
				->delete();
	}

	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'medicos' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
