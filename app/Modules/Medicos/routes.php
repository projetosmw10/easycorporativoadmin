<?php

// ================================
// Médicos routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Medicos\Controllers'), function() {
	Route::get('medicos', ['uses' => 'MedicosController@index']);
	Route::get('medicos/detalhe/{slug}', ['uses' => 'MedicosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Medicos\Controllers\Admin'), function() {
	Route::get('admin/medicos', ['uses' => 'AdminMedicosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Medicos\Controllers\Admin'), function() {
	Route::get('admin/medicos/add', ['uses' => 'AdminMedicosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Medicos\Controllers\Admin'), function() {
	Route::get('admin/medicos/edit/{id}', ['uses' => 'AdminMedicosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Medicos\Controllers\Admin'), function() {
	Route::get('admin/medicos/delete/{id}', ['uses' => 'AdminMedicosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Medicos\Controllers\Admin'), function() {
	Route::post('admin/medicos/save', ['uses' => 'AdminMedicosController@save']);
	Route::post('admin/medicos/upload_galeria/{id}', ['uses' => 'AdminMedicosController@upload_galeria']);
    Route::post('admin/medicos/delete_thumbnail_principal', ['uses' => 'AdminMedicosController@delete_thumbnail_principal']);
    Route::post('admin/medicos/delete_imagem/{id}', ['uses' => 'AdminMedicosController@delete_imagem']);
});
