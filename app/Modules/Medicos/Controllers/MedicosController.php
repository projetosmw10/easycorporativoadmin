<?php

namespace App\Modules\Medicos\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class MedicosController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(40);
	}

	public function index(){
		$data = array();
		return view('medicos',$data);
	}
}
