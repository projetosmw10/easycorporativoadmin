@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Médicos</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/medicos') }}">Médicos</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
							<?php if($modulo->galeria){ ?>
								<li><a data-toggle="pill" href="#imagens-tab">Galeria</a></li>
							<?php } ?>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/medicos/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>

									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Anexo</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<a href="<?php echo url($item->thumbnail_principal); ?>"> Download <i class="fa fa-download"></i> </a>
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="area_id" class="col-md-3 control-label">Área *</label>
                                        <div class="col-md-7">
                                            <select id="area_id" required  class="form-control" name="area_id" >
                                            		<option value="" selected></option>
                                            	@foreach ($area as $a)
                                            		<option value="{{$a['id']}}" {{(isset($item)) && $item->area_id == $a['id'] ? "selected" : "" }}>{{$a['descricao']}}</option>
                                            	@endforeach
                                            </select>
                                         </div>
             </div>
                                    <div class="form-group">
                                        <label for="nome" class="col-md-3 control-label">Nome *</label>
                                        <div class="col-md-7">
                                        <input id="nome" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->nome : ""; ?>" name="nome" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="telefone" class="col-md-3 control-label">Telefone </label>
                                        <div class="col-md-7">
                                        <input id="telefone"  type="text" class="telefone  form-control" value="<?php echo (isset($item)) ? $item->telefone : ""; ?>" name="telefone" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="data_consulta" class="col-md-3 control-label">Data Consulta *</label>
                                        <div class="col-md-7">
                                        <input id="data_consulta" required type="date" class="form-control" value="<?php echo (isset($item)) ? $item->data_consulta : ""; ?>" name="data_consulta" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="observacoes" class="col-md-3 control-label">Observações </label>
                                        <div class="col-md-7">
                                        <textarea id="observacoes"  class="form-control" name="observacoes"><?php echo (isset($item)) ? $item->observacoes : ""; ?></textarea>
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="tem_revisao" class="col-md-3 control-label">Tem Revisão *</label>
                                        <div class="col-md-7">
                                        <select id="tem_revisao" required class="form-control" name="tem_revisao">
                                            <option <?php echo (isset($item) && $item->tem_revisao == 1) ? "selected" : ""; ?> value="1">Sim</option>
                                            <option <?php echo (isset($item) && $item->tem_revisao == 0) ? "selected" : ""; ?> value="0">Não</option>
                                        </select>
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="data_revisao" class="col-md-3 control-label">Data Revisão </label>
                                        <div class="col-md-7">
                                        <input id="data_revisao"  type="date" class="form-control" value="<?php echo (isset($item)) && ($item->tem_revisao == 1) ? $item->data_revisao : ""; ?>" name="data_revisao" />
                                    </div>
             </div>

								</div>

							</form>

							<?php if($modulo->galeria){ ?>

								<div id="imagens-tab" class="tab-pane fade">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lista-galeria">
										<?php if(isset($item) && count($item->imagens)){?>
											<?php foreach ($item->imagens as $image){?>
												<div id="item_<?php echo $image->id; ?>" class="item imagem-galeria-<?php echo $image->id; ?>">
													<div style="background-image: url(<?php echo "/uploads/medicos/$image->thumbnail_principal";?>);" class="thumb"></div>
													<span data="<?php echo $image->id; ?>" data-modulo="medicos" class="icon delete-image" aria-hidden="true"><i class="fa fa-trash"></i></span>
												</div>
											<?php }?>
										<?php }?>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<form class="dropzone" id="galeria-dropzone" method="POST" action="<?php echo (isset($item)) ? url('/admin/medicos/upload_galeria/'.$item->id) : url('/admin/medicos/upload_galeria/'.$nextId); ?> " enctype="multipart/form-data">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" />
											<div class="fallback">
												<input name="file" type="file" multiple />
											</div>
										<form>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/medicos/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
