@extends('layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Médicos
			<small>Listagem</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Médicos</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<div class="box">
					<div class="box-header">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/medicos/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
						<hr>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<table id="list-data-table" class="table table-bordered table-striped">

                            <thead>
                                 <tr>
                                     <th>Usuário</th>
                                     <th>Área</th>
                                     <th>Nome</th>
                                     <th>Telefone</th>
                                     <th>Data Consulta</th>
                                     <th>Observações</th>
                                     <th>Tem Revisão</th>
                                     <th>Data Revisão</th>
                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($itens as $item){ ?>
									<tr>
										<td><?php echo $item->user->first_name. ' ' . $item->user->last_name; ?></td>
                                            <td> <?php echo $item->area->descricao?> </td>
                                            <td> <?php echo $item->nome?> </td>
                                            <td> <?php echo $item->telefone?> </td>
                                            <td> <?php echo date("d/m/Y",strtotime($item->data_consulta))?> </td>
                                            <td> <?php echo $item->observacoes?> </td>
                                            <td> <?php echo ($item->tem_revisao) ? "Sim" : "Não"?> </td>
                                            <td> <?php echo ($item->tem_revisao) ? date("d/m/Y",strtotime($item->data_revisao)) : ''?> </td>
                                            <td>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<a href="/admin/medicos/edit/<?php echo $item->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/medicos/delete/<?php echo $item->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<th>ID</th>
                                     <th>Área</th>
                                     <th>Nome</th>
                                     <th>Telefone</th>
                                     <th>Data Consulta</th>
                                     <th>Observações</th>
                                     <th>Tem Revisão</th>
                                     <th>Data Revisão</th>
                                     <th>Ação</th>
								</tr>
                            </tfoot>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/medicos/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
