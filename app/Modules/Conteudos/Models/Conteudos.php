<?php

namespace App\Modules\Conteudos\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\JWTUser;
use Sentinel;

class Conteudos extends Model {
    protected $table = 'conteudos';
    protected $fillable = ['titulo', 'imagem', 'descricao', 'vimeo', 'vimeo_thumbnail', 'gratuito', 'thumbnail_principal', 'tipo', 'topico_id', 'ordem', 'dificuldade', 'aplicativo_id'];
    protected $appends = ['assistido', 'favoritado'];
    // se eu soubesse que ia ter varios tipos de arquivos teria feito um modulo so para os tipos, mas ja tavam na pressa entao fiz assim, me desculpe
    public static $tipos = ['video', 'audio', 'pdf'];
    public static $tiposNome = [
        '' => 'Conteúdos',
        'video' => 'Vídeos',
        'audio' => 'Áudios',
        'pdf' => 'PDFs'
    ];
    public static function boot()
    {
        parent::boot();

        self::retrieved(function($model){
            // $model = Conteudos::fetchVimeoThumbnail($model, true);
        });

        self::creating(function($model){
            // $model = Conteudos::fetchVimeoThumbnail($model);
        });

        self::updating(function($model){
            // $model = Conteudos::fetchVimeoThumbnail($model);
        });

    }

    public static function fetchVimeoThumbnail($model, $save = false) {
        if($vimeo = Conteudos::getVimeoId($model->vimeo) && !$model->vimeo_thumbnail) {
            try {
                $client = new \GuzzleHttp\Client();
                $response = $client->request('GET', 'http://vimeo.com/api/v2/video/' . $vimeo . '.json');
                $body = $response->getBody();
                if($body) {
                    $body = $body->getContents();
                    if($body) {
                        $response = json_decode($body, true);
                        $model->vimeo_thumbnail = isset($response[0]) ? $response[0]['thumbnail_large'] : '';
                    }
                }
                if($save) {
                    $model->save();
                }
            } catch (\Exception $e) {
            }
        }

        return $model;
    }
    

    public function getAssistidoAttribute($value) {
        // verifica se o usuario atual ja assistiu aquele conteudo
        if(!JWTUser::getUser()) {
            return 0;
        }
        $rel = DB::table('conteudo_user')
                 ->where('conteudo_id',$this->id)
                 ->where('user_id', JWTUser::getUser()->id)
                 ->first();

        return $rel && $rel->watched_at ? 1 : 0;
    }
    
    public function getFavoritadoAttribute($value) {
        // verifica se o usuario atual ja favoritou aquele conteudo
        if(!JWTUser::getUser()) {
            return 0;
        }
        $rel = DB::table('conteudo_user')
                 ->where('conteudo_id',$this->id)
                 ->where('user_id', JWTUser::getUser()->id)
                 ->first();

        return $rel && $rel->favorited_at ? 1 : 0;
    }

    public function getThumbnailPrincipalAttribute($value) {
        // Nao retorna link das coisas caso usuario nao tenha acessa
        if(Sentinel::getUser() && Sentinel::inRole('admins')) {
            return $value ? url($value) : '';
        }
        if(JWTUser::isPremium() || $this->gratuito == 1) {
            return $value ? url($value) : '';
        }
        return null;
    }

    public function getImagemAttribute($value) {
        if((!$value || !file_exists(public_path($value))) && JWTUser::getUser()) {
            if($this->tipo == 'pdf') {
                return url('img/placeholder-pdf.jpg');
            } else {
                return url('img/placeholder.jpg');
            }
        }
        return $value ? url($value) : '';
    }

    public function getVimeoAttribute($value) {
        // Nao retorna link das coisas caso usuario nao tenha acessa
        if(Sentinel::getUser() && Sentinel::inRole('admins')) {
            return $value;
        }
        if(JWTUser::isPremium() || $this->gratuito == 1) {
            return $value;
        }
        // var_dump(Sentinel::getUser());die();
        return null;
    }
    
    public function getDescricaoAttribute($value) {
        return strip_tags($value);
    }

    public function getTipoNomeAttribute($value) {
        return self::$tiposNome[$value] ?? '';
    }

    public function getTituloCompletoAttribute($val = '') {
        $this->loadMissing(['topico', 'topico.topico', 'topico.categoria']);
        $titulo = [];
        if(!empty($this->topico)) {
            if(!empty($this->topico->categoria)) {
                $titulo[] = $this->topico->categoria->titulo;
            }
            if(!empty($this->topico->topico)) {
                $titulo[] = $this->topico->topico->titulo; 
            }
            $titulo[] = $this->topico->titulo;
        }

        $titulo[] = $this->titulo;

        return implode(' - ', array_unique($titulo));
    }

    public static function getVimeoId($vimeo) {
        $vimeo = trim($vimeo, '/');
        $vimeo = explode('/', $vimeo);
        $partes = count($vimeo);
        return ($partes ? $vimeo[$partes - 1] : '');
    }

    public function tags() {
        return $this->belongsToMany('App\Modules\Tags\Models\Tags', 'conteudo_tag', 'conteudo_id', 'tag_id');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'conteudo_user', 'conteudo_id', 'user_id')->withPivot('favorited_at', 'watched_at');
    }

    public function topico() {
        return $this->belongsTo('App\Modules\Topicos\Models\Topicos', 'topico_id');
    }
}
