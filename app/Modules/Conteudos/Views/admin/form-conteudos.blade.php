@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Conteúdos</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/conteudos?topico_id=' . request()->query('topico_id')) }}">Conteúdos</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>

						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/conteudos/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
                                    <?php } ?>
                                    <div class="form-group" >
										<label for="imagem" class="col-md-3 control-label">Thumbnail</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->imagem != "") ){ ?>
												<div class="imagem">
                                                    <a href="<?php echo $item->imagem; ?>" target="blank">
                                                    <img src="<?php echo $item->imagem; ?>" width="300" alt="">
                                                    </a>
													<div class="clear"></div>
													<div class="delete-imagem btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="imagem" class="form-control" style="display: none;">
											<?php } else { ?>
												<input type="file" name="imagem" class="form-control">
                                            <?php } ?>
                                            <small>Tamanho sugerido 640x360px</small>
										</div>
                                    </div>

                                    <div class="form-group">
                                        <label for="tipo" class="col-md-3 control-label">Tipo</label>
                                        <div class="col-md-7">
                                            <select id="tipo" name="tipo" class="form-control select2">
                                                <?php
                                                foreach(\App\Modules\Conteudos\Models\Conteudos::$tiposNome as $tipo => $tipoNome){
                                                    if(!$tipo) continue;
                                                ?>
                                                <option value="<?php echo $tipo?>" <?=(isset($item) && $item->tipo == $tipo ? 'selected' : (app('request')->query('tipo') == $tipo ? 'selected' : ''))?>><?php echo $tipoNome?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                         </div>
                                    </div>
                                    <div class="form-group hidden">
                                        <label for="topico_id" class="col-md-3 control-label">Tópico</label>
                                        <div class="col-md-7">
                                            <select id="topico_id" name="topico_id" class="form-control select2">

                                            <option value="">Nenhum</option>
                                                <?php
                                                foreach($topicos as $topico){
                                                ?>
                                                <option value="<?php echo $topico->id?>" <?=(isset($item) && $topico->id == $item->topico_id ? 'selected' : (app('request')->query('topico_id', 0) == $topico->id ? 'selected' : ''))?>><?php echo $topico->titulo_completo?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                         </div>
                                    </div>

                                    <div class="form-group pdf" style="<?=isset($item) && $item->tipo == 'pdf' ? '' : 'display:none'?>">
										<label for="thumbnail_principal" class="col-md-3 control-label">Arquivo</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
                                                    <a href="<?php echo $item->thumbnail_principal; ?>" target="blank">
                                                    Ver arquivo
                                                    </a>
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
                                    </div>

                                    <div class="form-group vimeo" style="<?=isset($item) && $item->tipo != 'pdf' ? '' : 'display:none'?>" >
                                        <label for="vimeo" class="col-md-3 control-label">Vimeo </label>
                                        <div class="col-md-7">
                                        <input id="vimeo"  type="text" class="form-control" value="<?php echo (isset($item)) ? $item->vimeo : ''; ?>" name="vimeo" />
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="form-group">
                                        <label for="titulo" class="col-md-3 control-label">Título *</label>
                                        <div class="col-md-7">
                                        <input id="titulo" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->titulo : ""; ?>" name="titulo" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="descricao" class="col-md-3 control-label">Descrição </label>
                                        <div class="col-md-7">
                                        <textarea id="descricao"  class="form-control" name="descricao"><?php echo (isset($item)) ? $item->descricao : ""; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group hidden">
                                    <label for="tags" class="col-md-3 control-label">Tags</label>
										<div class="col-md-6">
											<input type="text" class="form-control" name="busca_tags" />
										</div>
                                        <div class="col-md-3"><a target="_blank" href="<?=url('admin/tags/add')?>" class="btn btn-success">Adicionar</a> <!-- /.btn btn-success -->
                                        </div>
                                    </div>

                                    
                                    <div class="form-group hidden">
										<label for="tags" class="col-md-3 control-label">Tags Selecionadas</label>
										<div class="col-md-7">
												<div class="scrollbox" id="conteudo_tag">
													<?php if(isset($item->tags)){?>
															<?php foreach ($item->tags as $tag) { ?>
																<div id="conteudo_tag<?php echo $tag->id; ?>"><?php echo $tag->titulo; ?><img src="img/delete.png" alt="" /><input type="hidden" name="tag_id[]" value="<?php echo $tag->id; ?>" /></div>
															<?php } ?>
													 <?php } ?>
												</div>
										</div>
                                    </div>

                                    <div class="form-group">
                                        <label for="dificuldade" class="col-md-3 control-label">Dificuldade *</label>
                                        <div class="col-md-7">
                                        <select id="dificuldade" required class="form-control" name="dificuldade">
                                            <option <?php echo (isset($item) && $item->dificuldade == 0) ? "selected" : ""; ?> value="0">Nenhuma</option>
                                            <option <?php echo (isset($item) && $item->dificuldade == 1) ? "selected" : ""; ?> value="1">1</option>
                                            <option <?php echo (isset($item) && $item->dificuldade == 2) ? "selected" : ""; ?> value="2">2</option>
                                            <option <?php echo (isset($item) && $item->dificuldade == 3) ? "selected" : ""; ?> value="3">3</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="form-group hidden">
                                        <label for="gratuito" class="col-md-3 control-label">Gratuito *</label>
                                        <div class="col-md-7">
                                        <select id="gratuito" required class="form-control" name="gratuito">
                                            <option <?php echo (isset($item) && $item->gratuito == 0) ? "selected" : ""; ?> value="0">Não</option>
                                            <option <?php echo (isset($item) && $item->gratuito == 1) ? "selected" : ""; ?> value="1">Sim</option>
                                        </select>
                                        </div>
                                    </div>



								</div>

								
							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">

$('#tipo.select2').on('select2:select', function(e) {
    if($(this).val() == 'pdf') {
        $('.pdf').show();
        $('.vimeo').hide();
    } else {
        $('.vimeo').show();
        $('.pdf').hide();
    }
}).trigger('select2:select')

$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/conteudos/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

$('.delete-imagem').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/conteudos/delete_imagem",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.imagem').hide();

                    $('input[name="imagem"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});


$('input[name="busca_tags"]').autocomplete({
    delay: 500,
    source: function(request, response) {
        $.ajax({
            url: 'api/tags',
            method: 'GET',
            dataType: 'json',
            data: {
                    'search': request.term
            },
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.titulo,
                        value: item.id
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        $('#conteudo_tag' + ui.item.value).remove();

        $('#conteudo_tag').append('<div id="conteudo_tag' + ui.item.value + '">' + ui.item.label + '<img src="img/delete.png" alt="" /><input type="hidden" name="tag_id[]" value="' + ui.item.value + '" /></div>');

        $('#conteudo_tag div:odd').attr('class', 'odd');
        $('#conteudo_tag div:even').attr('class', 'even');

        return false;
    },
    focus: function(event, ui) {
            return false;
        }
});

$(document).on('click', '#conteudo_tag div img', function(){
    $(this).parent().remove();

    $('#conteudo_tag div:odd').attr('class', 'odd');
    $('#conteudo_tag div:even').attr('class', 'even');
});

//relacional
//adiciona topico_id em todos os links
$('a').each(function () {
	var href = $(this).attr('href');
	link = 'topico_id=' + <?=app('request')->query('topico_id', 0)?>;
	if(href.indexOf('?') === -1) {
		link = '?' + link;
	} else {
		link = '&' + link;
	}
	$(this).attr('href', href + link);
});
//cola o input


</script>
@endsection
