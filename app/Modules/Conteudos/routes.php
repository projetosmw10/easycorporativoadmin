<?php

// ================================
// Conteudos routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Conteudos\Controllers'), function() {
	Route::get('conteudos', ['uses' => 'ConteudosController@index']);
	Route::get('conteudos/detalhe/{slug}', ['uses' => 'ConteudosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Conteudos\Controllers\Admin'), function() {
	Route::get('admin/conteudos', ['uses' => 'AdminConteudosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Conteudos\Controllers\Admin'), function() {
	Route::get('admin/conteudos/add', ['uses' => 'AdminConteudosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Conteudos\Controllers\Admin'), function() {
	Route::get('admin/conteudos/edit/{id}', ['uses' => 'AdminConteudosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Conteudos\Controllers\Admin'), function() {
	Route::get('admin/conteudos/delete/{id}', ['uses' => 'AdminConteudosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Conteudos\Controllers\Admin'), function() {
	Route::post('admin/conteudos/save', ['uses' => 'AdminConteudosController@save']);
    Route::post('admin/conteudos/delete_thumbnail_principal', ['uses' => 'AdminConteudosController@delete_thumbnail_principal']);
    Route::post('admin/conteudos/delete_imagem', ['uses' => 'AdminConteudosController@delete_imagem']);
});
