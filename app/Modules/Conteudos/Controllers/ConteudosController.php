<?php

namespace App\Modules\Conteudos\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ConteudosController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(18);
	}

	public function index(){
		$data = array();
		return view('conteudos',$data);
	}
}
