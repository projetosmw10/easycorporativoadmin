<?php

namespace App\Modules\Conteudos\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Conteudos\Models\Conteudos;
use App\Modules\Categorias\Models\Categorias;
use App\Modules\Topicos\Models\Topicos;
use App\Modules\Tags\Models\Tags;

class AdminConteudosController extends Controller {
    private $modulo;
    private $lastInsertId;

    public function __construct() {
        $this->middleware('auth');
        $this->modulo = \App\Gerador::find(18);
    }

    public function index(Request $request) {
        $data['itens'] = Conteudos::with('topico');

        if ($request->query('topico_id')) {
            $data['itens']->where('topico_id', $request->query('topico_id'))->orderBy('ordem');
            $data['topico'] = Topicos::find($request->topico_id);
        }


        $data['itens'] = $data['itens']->get();
        return view('Conteudos::admin/conteudos', $data);
    }

    public function add() {
        $data = [];
        $data['modulo'] = $this->modulo;
        $data['topicos'] = Topicos::orderBy('titulo')->get();
        return view('Conteudos::admin/form-conteudos', $data);
    }

    public function edit(Request $request, $id) {
        $data['modulo'] = $this->modulo;
        $data['topicos'] = Topicos::orderBy('titulo')->get();
        $data['item'] = Conteudos::find($id);
        $data['item']->tags;
        
        $request->query('topico_id', $data['item']->topico_id);

        return view('Conteudos::admin/form-conteudos', $data);
    }

    public function save(Request $request) {
        try {
            $post = $request->input();

            if ($request->hasFile('thumbnail_principal')) {
                $file = $request->file('thumbnail_principal');

                $tmpFilePath = '/uploads/conteudos/';
                if (!is_dir($tmpFilePath)) {
                    @mkdir($tmpFilePath, 0777, true);
                }

                $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->extension();
                $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
                $path = $tmpFilePath . $tmpFileName;

                $post['thumbnail_principal'] = $path;
            }

            if ($request->hasFile('imagem')) {
                $file = $request->file('imagem');

                $tmpFilePath = '/uploads/conteudos/';
                if (!is_dir($tmpFilePath)) {
                    @mkdir($tmpFilePath, 0777, true);
                }

                $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->extension();
                $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
                $path = $tmpFilePath . $tmpFileName;

                $post['imagem'] = $path;
            }

            $post['topico_id'] = empty($post['topico_id']) ? null : $post['topico_id'];
            $post['aplicativo_id'] = session('aplicativo_id');
            if ($request->input('id')) {
                $item = Conteudos::find($request->input('id'));
                $item->update($post);
            } else {
                $item = Conteudos::create($post);
            }
            if (isset($post['tag_id']) && is_array($post['tag_id'])) {
                $item->tags()->sync($post['tag_id']);
            } else {
                $item->tags()->sync([]);
            }

            \Session::flash('type', 'success');
            return redirect('admin/conteudos?topico_id='.$post['topico_id']);
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    public function delete(Request $request, $id) {
        try {
            Conteudos::destroy($id);

            \Session::flash('type', 'success');
            \Session::flash('message', 'Registro removido com sucesso!');
            return redirect()->back();
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Nao foi possivel remover o registro!');
            return redirect()->back();
        }
    }

    public function delete_thumbnail_principal(Request $request) {
        $post = $request->input();

        try {
            $imagem = Conteudos::find($post['id']);

            if (is_file(public_path() . $imagem->getOriginal('thumbnail_principal'))) {
                @unlink(public_path() . $imagem->getOriginal('thumbnail_principal'));
                Conteudos::where('id', $post['id'])->update(['thumbnail_principal' => '']);
            }

            return response()->json(['status' => true, 'message' => 'Registro removido com sucesso!']);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
    public function delete_imagem(Request $request) {
        $post = $request->input();

        try {
            $imagem = Conteudos::find($post['id']);

            if (is_file(public_path() . $imagem->getOriginal('imagem'))) {
                @unlink(public_path() . $imagem->getOriginal('imagem'));
                Conteudos::where('id', $post['id'])->update(['imagem' => '']);
            }

            return response()->json(['status' => true, 'message' => 'Registro removido com sucesso!']);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
