<?php

namespace App\Modules\Pdfs\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use Sentinel;
use App\JWTUser;

class Pdfs extends Model
{
    protected $table = 'pdfs';
	protected $fillable = array('aplicativo_id', 'titulo', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', 'dicas');
	protected $appends = ['lido', 'favoritado'];

	public function getLidoAttribute($value) {
        // verifica se o usuario atual ja assistiu aquele conteudo
        if(!JWTUser::getUser()) {
            return 0;
        }
        $rel = DB::table('pdf_user')
                 ->where('pdf_id', $this->id)
                 ->where('user_id', JWTUser::getUser()->id)
                 ->first();

        return $rel && $rel->read_at ? 1 : 0;
    }
    public function getFavoritadoAttribute($value) {
        // verifica se o usuario atual ja favoritou aquele conteudo
        if(!JWTUser::getUser()) {
            return 0;
        }
        $rel = DB::table('pdf_user')
                 ->where('pdf_id', $this->id)
                 ->where('user_id', JWTUser::getUser()->id)
                 ->first();

        return $rel && $rel->favorited_at ? 1 : 0;
    }
	

	public function getThumbnailPrincipalAttribute($value) {
		// Nao retorna link das coisas caso usuario nao tenha acessa
		return $value ? url($value) : '';
	}


	public function users() {
        return $this->belongsToMany('App\User', 'pdf_user', 'pdf_id', 'user_id')->withPivot('favorited_at', 'read_at');
    }
}
