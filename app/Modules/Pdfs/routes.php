<?php

// ================================
// PDFs routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Pdfs\Controllers'), function() {
	Route::get('pdfs', ['uses' => 'PdfsController@index']);
	Route::get('pdfs/detalhe/{slug}', ['uses' => 'PdfsController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Pdfs\Controllers\Admin'), function() {
	Route::get('admin/pdfs', ['uses' => 'AdminPdfsController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Pdfs\Controllers\Admin'), function() {
	Route::get('admin/pdfs/add', ['uses' => 'AdminPdfsController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Pdfs\Controllers\Admin'), function() {
	Route::get('admin/pdfs/edit/{id}', ['uses' => 'AdminPdfsController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Pdfs\Controllers\Admin'), function() {
	Route::get('admin/pdfs/delete/{id}', ['uses' => 'AdminPdfsController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Pdfs\Controllers\Admin'), function() {
	Route::post('admin/pdfs/save', ['uses' => 'AdminPdfsController@save']);
	Route::post('admin/pdfs/upload_galeria/{id}', ['uses' => 'AdminPdfsController@upload_galeria']);
    Route::post('admin/pdfs/delete_thumbnail_principal', ['uses' => 'AdminPdfsController@delete_thumbnail_principal']);
    Route::post('admin/pdfs/delete_imagem/{id}', ['uses' => 'AdminPdfsController@delete_imagem']);
});
