<?php

namespace App\Modules\Pdfs\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PdfsController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(30);
	}

	public function index(){
		$data = array();
		return view('pdfs',$data);
	}
}
