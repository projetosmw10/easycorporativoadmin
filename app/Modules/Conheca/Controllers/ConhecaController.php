<?php

namespace App\Modules\Conheca\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ConhecaController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(29);
	}

	public function index(){
		$data = array();
		return view('conheca',$data);
	}
}
