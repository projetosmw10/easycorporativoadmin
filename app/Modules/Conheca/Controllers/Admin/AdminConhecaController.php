<?php

namespace App\Modules\Conheca\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Conheca\Models\Conheca;

class AdminConhecaController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(29);
	}

	public function index(){
		$data['modulo'] = $this->modulo;
		$data['item'] = Conheca::find(1);
		if($this->modulo->galeria){
			$data['item']->imagens = Conheca::getImagens(1);
		}
		return view('Conheca::admin/conheca',$data);
	}


	public function save(Request $request){
		try{
            $post = $request->input();
            
            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/conheca/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }

			Conheca::find($request->input('id'))->update($post);

			\Session::flash('type', 'success');
         \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/conheca');
		}catch(\Exception $e){
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}


    }
    
    
	public function upload_galeria(Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/conheca/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Conheca::criar_imagem(array('id_conheca' => 1, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
    }
    public function delete_imagem($id){
		try{
			$imagem = Conheca::getImagem($id);
            
			unlink('uploads/conheca/'.$imagem->thumbnail_principal);
			Conheca::deletar_imagem($id);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


	public function delete($id){
		try{
			Conheca::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/conheca');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}


	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Conheca::find($post['id']);

			if(is_file(public_path() . $imagem->getOriginal('thumbnail_principal'))){
				unlink(public_path() . $imagem->getOriginal('thumbnail_principal'));
				Conheca::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}

}
