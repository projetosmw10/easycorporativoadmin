@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Conheça</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/conheca') }}">Conheça</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">

					<!-- /.box-header -->
					<div class="box-body">

                            <form id="mainForm" class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{ url('/admin/conheca/save') }}">
								
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
									<div class="text-danger text-center">Ao alterar aqui, estará alterando em todos apps</div>
									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Imagem</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<img src="<?php echo ($item->thumbnail_principal); ?>" height="150px">
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="titulo" class="col-md-3 control-label">Título *</label>
                                        <div class="col-md-7">
                                        <input id="titulo" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->titulo : ""; ?>" name="titulo" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="descricao" class="col-md-3 control-label">Descrição *</label>
                                        <div class="col-md-7">
                                        <textarea id="descricao" required class="form-control" name="descricao"><?php echo (isset($item)) ? $item->descricao : ""; ?></textarea>
                                    </div>
             </div>

							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<?php if($current_role->hasAccess($current_module->nome_tabela.'.update')){ ?>
						<div class="box-footer">
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						</div>
					<?php } ?>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/conheca/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
