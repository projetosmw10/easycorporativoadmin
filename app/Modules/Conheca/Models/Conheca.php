<?php

namespace App\Modules\Conheca\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Conheca extends Model
{
    protected $table = 'conheca';
    protected $fillable = array('titulo', 'descricao', 'thumbnail_principal', );

	public function getThumbnailPrincipalAttribute($value) {
        https://laravel.com/docs/5.6/filesystem#file-visibility
        return $value ? url($value) : '';
    }

}
