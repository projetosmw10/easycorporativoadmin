<?php

// ================================
// Conheça routes BEGGINING
// ================================


Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Conheca\Controllers\Admin'), function() {
	Route::get('admin/conheca', ['uses' => 'AdminConhecaController@index']);
});

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Conheca\Controllers'), function() {
	Route::get('conheca', ['uses' => 'ConhecaController@index']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Conheca\Controllers\Admin'), function() {
	Route::get('admin/conheca', ['uses' => 'AdminConhecaController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create,update'], 'namespace' => 'App\Modules\Conheca\Controllers\Admin'), function() {
	Route::post('admin/conheca/save', ['uses' => 'AdminConhecaController@save']);
	Route::post('admin/conheca/upload_galeria', ['uses' => 'AdminConhecaController@upload_galeria']);
    Route::post('admin/conheca/delete_thumbnail_principal', ['uses' => 'AdminConhecaController@delete_thumbnail_principal']);
    Route::post('admin/conheca/delete_imagem/{id}', ['uses' => 'AdminConhecaController@delete_imagem']);
});
