<?php

namespace App\Modules\Tags\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TagsController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(15);
	}

	public function index(){
		$data = array();
		return view('tags',$data);
	}
}
