<?php

namespace App\Modules\Tags\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'tags';
    protected $hidden = ['pivot'];
    protected $fillable = array('titulo', 'thumbnail_principal');

    public function conteudos(){
        return $this->belongsToMany('App\Modules\Conteudos\Models\Conteudos', 'conteudo_tag', 'tag_id', 'conteudo_id');
    }

}
