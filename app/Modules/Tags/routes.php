<?php

// ================================
// Tags routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Tags\Controllers'), function() {
	Route::get('tags', ['uses' => 'TagsController@index']);
	Route::get('tags/detalhe/{slug}', ['uses' => 'TagsController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Tags\Controllers\Admin'), function() {
	Route::get('admin/tags', ['uses' => 'AdminTagsController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Tags\Controllers\Admin'), function() {
	Route::get('admin/tags/add', ['uses' => 'AdminTagsController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Tags\Controllers\Admin'), function() {
	Route::get('admin/tags/edit/{id}', ['uses' => 'AdminTagsController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Tags\Controllers\Admin'), function() {
	Route::get('admin/tags/delete/{id}', ['uses' => 'AdminTagsController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Tags\Controllers\Admin'), function() {
	Route::post('admin/tags/save', ['uses' => 'AdminTagsController@save']);
    Route::post('admin/tags/delete_thumbnail_principal', ['uses' => 'AdminTagsController@delete_thumbnail_principal']);
});
