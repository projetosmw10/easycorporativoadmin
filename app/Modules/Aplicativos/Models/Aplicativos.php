<?php

namespace App\Modules\Aplicativos\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Aplicativos extends Model
{
    protected $table = 'aplicativos';
    protected $fillable = array('titulo', 'descricao', 'cor', 'thumbnail_principal', 'fundo', 'meta_keywords', 'meta_descricao', 'slug', 'ordem');


    public function getThumbnailPrincipalAttribute($val = null) {
        return $val ? asset($val) : ''; 
    }
    public function getFundoAttribute($val = null) {
        return $val ? asset($val) : ''; 
    }

}
