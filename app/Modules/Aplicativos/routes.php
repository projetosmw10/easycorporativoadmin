<?php

// ================================
// Aplicativos routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Aplicativos\Controllers'), function() {
	Route::get('aplicativos', ['uses' => 'AplicativosController@index']);
	Route::get('aplicativos/detalhe/{slug}', ['uses' => 'AplicativosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Aplicativos\Controllers\Admin'), function() {
	Route::get('admin/aplicativos/session/{id}', ['uses' => 'AdminAplicativosController@session']);
	Route::get('admin/aplicativos', ['uses' => 'AdminAplicativosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Aplicativos\Controllers\Admin'), function() {
	Route::get('admin/aplicativos/add', ['uses' => 'AdminAplicativosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Aplicativos\Controllers\Admin'), function() {
	Route::get('admin/aplicativos/edit/{id}', ['uses' => 'AdminAplicativosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Aplicativos\Controllers\Admin'), function() {
	Route::get('admin/aplicativos/delete/{id}', ['uses' => 'AdminAplicativosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Aplicativos\Controllers\Admin'), function() {
	Route::post('admin/aplicativos/save', ['uses' => 'AdminAplicativosController@save']);
	Route::post('admin/aplicativos/upload_galeria/{id}', ['uses' => 'AdminAplicativosController@upload_galeria']);
    Route::post('admin/aplicativos/delete_thumbnail_principal', ['uses' => 'AdminAplicativosController@delete_thumbnail_principal']);
    Route::post('admin/aplicativos/delete_fundo', ['uses' => 'AdminAplicativosController@delete_fundo']);
    Route::post('admin/aplicativos/delete_imagem/{id}', ['uses' => 'AdminAplicativosController@delete_imagem']);
});
