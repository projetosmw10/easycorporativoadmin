<?php

namespace App\Modules\Aplicativos\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Aplicativos\Models\Aplicativos;

class AdminAplicativosController extends Controller
{
	private $modulo;
	private $lastInsertId;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(34);
	}

	public function session($id) {
		session(['aplicativo_id' => $id]);
		return redirect('/admin/dashboard');
	}


	public function index(){
		$data['itens'] = Aplicativos::all();
		return view('Aplicativos::admin/aplicativos',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		return view('Aplicativos::admin/form-aplicativos', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		$data['item'] = Aplicativos::find($id);
		return view('Aplicativos::admin/form-aplicativos',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();

			$post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',',$post['meta_keywords']) : null;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/aplicativos/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
            if($request->hasFile('fundo')) {

				$file = $request->file('fundo');

				$tmpFilePath = '/uploads/aplicativos/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['fundo'] = $path;
            }
            
			if($request->input('id')){
				Aplicativos::find($request->input('id'))->update($post);
			}else{
				Aplicativos::create($post);
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/aplicativos');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    
	public function upload_galeria($id, Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/aplicativos/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Aplicativos::criar_imagem(array('id_aplicativos' => $id, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
	}

    public function delete($id){
		try{
			Aplicativos::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/aplicativos');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}
	public function delete_imagem($id){
		try{
			$imagem = Aplicativos::getImagem($id);
			Aplicativos::deletar_imagem($id);

			unlink('uploads/aplicativos/'.$imagem->thumbnail_principal);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}


	}

	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Aplicativos::find($post['id']);

			if(is_file(public_path() . $imagem->getOriginal('thumbnail_principal'))){
				unlink(public_path() . $imagem->getOriginal('thumbnail_principal'));
				Aplicativos::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}
	public function delete_fundo(Request $request){

		$post = $request->input();

		try{

			$imagem = Aplicativos::find($post['id']);

			if(is_file(public_path() . $imagem->getOriginal('fundo'))){
				unlink(public_path() . $imagem->getOriginal('fundo'));
				Aplicativos::where('id', $post['id'])->update(array("fundo" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


}
