<?php

namespace App\Modules\Codes\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CodesController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(24);
	}

	public function index(){
		$data = array();
		return view('codes',$data);
	}
}
