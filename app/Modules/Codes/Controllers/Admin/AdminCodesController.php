<?php

namespace App\Modules\Codes\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Codes\Models\Codes;
use App\Modules\Empresas\Models\Empresas;

class AdminCodesController extends Controller {
    private $modulo;
    private $lastInsertId;

    public function __construct() {
        $this->middleware('auth');
        $this->modulo = \App\Gerador::find(24);
    }

    public function index() {
        $data['itens'] = Codes::withTrashed()->with(['user', 'empresa'])->withTrashed()->get();
        return view('Codes::admin/codes', $data);
    }

    public function add() {
        $data = [];
        $data['modulo'] = $this->modulo;
        $data['empresas'] = Empresas::orderBy('titulo')->get();
        return view('Codes::admin/form-codes', $data);
    }

    public function edit($id) {
        $data['modulo'] = $this->modulo;
        $data['item'] = Codes::withTrashed()->with(['user', 'empresa', 'pessoas', 'pessoas.user'])->find($id);
        $data['empresas'] = Empresas::orderBy('titulo')->get();
        return view('Codes::admin/form-codes', $data);
    }

    public function save(Request $request) {
        try {
            $post = $request->input();
            if(Codes::where('code', $post['code'])->where('id', '!=', $request->input('id', 0))->exists()) {
                throw new \Exception('O código '.$post['code'].' já está sendo utilizado');
            }

            if ($request->input('id')) {
                Codes::withTrashed()->find($request->input('id'))->update($post);
            } else {
                Codes::create($post);
            }
            \Session::flash('type', 'success');
            \Session::flash('message', 'Alteracoes salvas com sucesso!');
            return redirect('admin/codes');
        } catch (\Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    public function delete($id) {
        try {
            Codes::withTrashed()->find($id)->forceDelete();

            \Session::flash('type', 'success');
            \Session::flash('message', 'Registro removido com sucesso!');
            return redirect('admin/codes');
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Nao foi possivel remover o registro!');
            return redirect()->back();
        }
    }

    public function dashboard(Request $request) {
        $week = Codes::withTrashed()
            ->with('empresa')
            ->whereRaw('expire_at BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 7 DAY)')
            ->orderBy('expire_at')
            ->get();

        $expired = Codes::withTrashed()
            ->with('empresa')
            ->whereRaw('expire_at BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()')
            ->orderBy('expire_at')
            ->get();
        return view('Codes::admin/dashboard-codes', [
            'week' => $week,
            'expired' => $expired,
        ]);
    }
}
