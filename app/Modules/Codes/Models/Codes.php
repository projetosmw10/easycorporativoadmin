<?php

namespace App\Modules\Codes\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use DateTime;

class Codes extends Model {
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'codes';
    protected $dates = ['deleted_at'];
    protected $fillable = ['user_id', 'empresa_id', 'code', 'quantity', 'expire_at'];
    protected $appends = ['utilizado'];

    public function user(){
        return $this->belongsTo('\App\User', 'user_id');
    }
    public function empresa(){
        return $this->belongsTo('\App\Modules\Empresas\Models\Empresas', 'empresa_id');
    }
    public function pessoas(){
        return $this->hasMany('\App\Modules\Pessoas\Models\Pessoas', 'code_id');
    }

    public function getUtilizadoAttribute() {
        if($this->deleted_at === null){
            return false;
        }
        return true;
    }

    public function scopeExpired($query) {
        return $query->whereRaw('NOW() > codes.expire_at');
    }

    public function isExpired() {
        // caso agora seja maior q expire at, quer dizer q ele é expirado
        if(time() > strtotime($this->getOriginal('expire_at'))) {
            return true;
        }

        return false;
    }

    public function getExpireAtAttribute($val) {
        if(!$val) return $val;
        return date('d/m/Y', strtotime($val));
    }

    public function setExpireAtAttribute($val) {
        $expire_at = DateTime::createFromFormat('d/m/Y', $val);
        $this->attributes['expire_at'] = $expire_at->format('Y-m-d') . ' 00:00:00';
    }

}
