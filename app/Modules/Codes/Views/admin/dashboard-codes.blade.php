@extends($current_template)


@section('content')

<div class="content-wrapper">
    
    <div class="content">
        <h3>
        Irão expirar em breve
        </h3>
        <hr>
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Código</th>
                    <th>Quantidade</th>
                    <th>Expira Em</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @forelse($week as $code)
                <tr class="{{ $code->isExpired() ? 'text-danger' : '' }}">
                    <td>
                    @if(!empty($code->empresa))
                    <a href="{{ url('admin/empresas/edit/'.$code->empresa->id) }}">{{ $code->empresa->titulo }}</a>
                    @endif
                    </td>
                    <td>{{ $code->code }}</td>
                    <td>{{ $code->pessoas()->count() }} / {{ $code->quantity }}</td>
                    <td>{{ $code->expire_at }}</td>
                    <td>
                        <a href="/admin/codes/edit/{{ $code->id }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <a href="/admin/codes/delete/{{ $code->id }}" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5"><br>Nenhum registro a ser mostrado<br><br></td>
                </tr>
                @endforelse
            </tbody>
        </table>

        <h3>
        Expiraram recentemente
        </h3>
        <hr>
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Empresa</th>
                    <th>Código</th>
                    <th>Quantidade</th>
                    <th>Expirou Em</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @forelse($expired as $code)
                <tr class="{{ $code->isExpired() ? 'text-danger' : '' }}">
                    <td>
                    @if(!empty($code->empresa))
                    <a href="{{ url('admin/empresas/edit/'.$code->empresa->id) }}">{{ $code->empresa->titulo }}</a>
                    @endif
                    </td>
                    <td>{{ $code->code }}</td>
                    <td>{{ $code->pessoas()->count() }} / {{ $code->quantity }}</td>
                    <td>{{ $code->expire_at }}</td>
                    <td>
                        <a href="/admin/codes/edit/{{ $code->id }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <a href="/admin/codes/delete/{{ $code->id }}" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5"><br>Nenhum registro a ser mostrado<br><br></td>
                </tr>
                @endforelse
            </tbody>
        </table>

        




        
    </div>

</div>


@endsection