@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Codes</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/codes') }}">Codes</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>

						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/codes/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
									
                                    <div class="form-group">
                                        <label for="code" class="col-md-3 control-label">Código *</label>
                                        <div class="col-md-7">
											<input id="code" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->code : ""; ?>" name="code" />
										</div>
									</div>

                                    <div class="form-group">
                                        <label for="quantity" class="col-md-3 control-label">Quantidade *</label>
                                        <div class="col-md-7">
											<input id="quantity" min="0" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->quantity : ""; ?>" name="quantity" />
											<script>
											$('#quantity').mask('000000000000')
											</script>
										</div>
									</div>

                                    <div class="form-group">
                                        <label for="expire_at" class="col-md-3 control-label">Expira em *</label>
                                        <div class="col-md-7">
											<input id="expire_at" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->expire_at : ""; ?>" name="expire_at" />
										</div>
									</div>
									<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
									<script>
									var date = moment($('#expire_at').val(), 'DD/MM/YYYY').toDate();
									$('#expire_at').datepicker({
  dateFormat: 'dd/mm/yy',
  dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
  dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
  dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
  monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
  monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
  nextText: 'Proximo',
  prevText: 'Anterior',
//   minDate: 0
});
									$('#expire_at').datepicker('setDate', date)
									</script>

									<div class="form-group">
										<label for="empresa_id" class="col-md-3 control-label">Empresa</label>
										<div class="col-md-7">
											<select required  <?=isset($item) && $item->empresa_id ? 'disabled' : ''?> name="empresa_id" id="empresa_id" class="select2">
											<option value="">Selecione</option>
											<?php
											foreach($empresas as $empresa) {
											?>
											<option value="<?=$empresa->id?>" <?=isset($item) && $item->empresa_id == $empresa->id ? 'selected' : ''?>><?=$empresa->titulo?></option>
											<?php
											}
											?>
											</select>
										</div>
									</div>
									<?php
									if(isset($item)){
									?>
									<div class="form-group">
                                        <label for="expire_at" class="col-md-3 control-label">Pessoas </label>
                                        <div class="col-md-7">
										
										<table class="table">
											<thead>
												<tr>
													<th>Identificador</th>
													<th>Nome</th>
													<th>E-mail</th>
													<th>Usuário</th>
												</tr>
											</thead>
											<tbody>
											<?php
											foreach($item->pessoas as $pessoa) {
											?>
												<tr>
													<td><?=$pessoa->identificador?></td>
													<td><?=$pessoa->nome?></td>
													<td><?=isset($pessoa->user) ? $pessoa->user->email : ''?></td>
													<td>
													<?php
                                                    if (isset($pessoa->user)) {
                                                        ?>
													<a target="blank" href="/admin/users/edit/<?=$pessoa->user->id?>" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
													<?php
                                                    }
													?>
													</td>
												</tr>
											<?php
											}
											if(!count($item->pessoas)){

											?>
											<tr>
												<td colspan="4">Nenhum registro encontrado</td>
											</tr>
											<?php
											}
											?>
											</tbody>
										</table>
										
										</div>
									</div>
									<?php
									}
									?>

								</div>

								
							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
@endsection
