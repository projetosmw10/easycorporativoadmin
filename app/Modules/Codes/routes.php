<?php

// ================================
// Codes routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Codes\Controllers'), function() {
	Route::get('codes', ['uses' => 'CodesController@index']);
	Route::get('codes/detalhe/{slug}', ['uses' => 'CodesController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Codes\Controllers\Admin'), function() {
	Route::get('admin/codes', ['uses' => 'AdminCodesController@index']);
	Route::get('admin/codes/dashboard', ['uses' => 'AdminCodesController@dashboard']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Codes\Controllers\Admin'), function() {
	Route::get('admin/codes/add', ['uses' => 'AdminCodesController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Codes\Controllers\Admin'), function() {
	Route::get('admin/codes/edit/{id}', ['uses' => 'AdminCodesController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Codes\Controllers\Admin'), function() {
	Route::get('admin/codes/delete/{id}', ['uses' => 'AdminCodesController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Codes\Controllers\Admin'), function() {
	Route::post('admin/codes/save', ['uses' => 'AdminCodesController@save']);
	Route::post('admin/codes/upload_galeria/{id}', ['uses' => 'AdminCodesController@upload_galeria']);
    Route::post('admin/codes/delete_thumbnail_principal', ['uses' => 'AdminCodesController@delete_thumbnail_principal']);
    Route::post('admin/codes/delete_imagem/{id}', ['uses' => 'AdminCodesController@delete_imagem']);
});
