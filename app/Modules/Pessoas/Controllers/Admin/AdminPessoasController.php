<?php

namespace App\Modules\Pessoas\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Pessoas\Models\Pessoas;
use App\Modules\Empresas\Models\Empresas;

class AdminPessoasController extends Controller
{
	private $modulo;
	private $lastInsertId;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(36);
	}

	public function index(Request $request){
		$data['itens'] = Pessoas::where('empresa_id', $request->query('empresa_id'))->get();
		$data['empresa'] = Empresas::find($request->query('empresa_id'))->first();
		return view('Pessoas::admin/pessoas',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		return view('Pessoas::admin/form-pessoas', $data);
	}

	public function edit($id){

		$data['modulo'] = $this->modulo;
		$data['item'] = Pessoas::find($id);
		
		if(\Sentinel::getUser()->inRole('empresa')) {
			$empresa = Empresas::where('user_id', \Sentinel::getUser()->id)->first();
			if($empresa->id 
			!=
			$data['item']->empresa_id) {
				return redirect()->back();
			}
		}
	
		return view('Pessoas::admin/form-pessoas',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();

			$post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',',$post['meta_keywords']) : null;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/pessoas/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
			
			if(\Sentinel::getUser()->inRole('empresa')) {
				$empresa = Empresas::where('user_id', \Sentinel::getUser()->id)->first();
				$post['empresa_id'] = $empresa->id;
			}
			if($request->input('id')){
				Pessoas::find($request->input('id'))->update($post);
			}else{
				Pessoas::create($post);
			}
			\Session::flash('type', 'success');
			\Session::flash('message', "Alteracoes salvas com sucesso!");
			if(\Sentinel::getUser()->inRole('empresa')) {
				return redirect('admin');
			}
			return redirect('admin/pessoas?empresa_id='.$post['empresa_id']);
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    
	public function upload_galeria($id, Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/pessoas/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Pessoas::criar_imagem(array('id_pessoas' => $id, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
	}

    public function delete($id){
		try{
			Pessoas::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect()->back();
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}
	public function delete_imagem($id){
		try{
			$imagem = Pessoas::getImagem($id);
			Pessoas::deletar_imagem($id);

			unlink('uploads/pessoas/'.$imagem->thumbnail_principal);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}


	}

	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Pessoas::find($post['id']);

			if(is_file(public_path() . $imagem->thumbnail_principal)){
				unlink(public_path() . $imagem->thumbnail_principal);
				Pessoas::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


}
