<?php

namespace App\Modules\Pessoas\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PessoasController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(36);
	}

	public function index(){
		$data = array();
		return view('pessoas',$data);
	}
}
