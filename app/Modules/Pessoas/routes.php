<?php

// ================================
// Pessoas routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Pessoas\Controllers'), function() {
	Route::get('pessoas', ['uses' => 'PessoasController@index']);
	Route::get('pessoas/detalhe/{slug}', ['uses' => 'PessoasController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Pessoas\Controllers\Admin'), function() {
	Route::get('admin/pessoas', ['uses' => 'AdminPessoasController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Pessoas\Controllers\Admin'), function() {
	Route::get('admin/pessoas/add', ['uses' => 'AdminPessoasController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Pessoas\Controllers\Admin'), function() {
	Route::get('admin/pessoas/edit/{id}', ['uses' => 'AdminPessoasController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Pessoas\Controllers\Admin'), function() {
	Route::get('admin/pessoas/delete/{id}', ['uses' => 'AdminPessoasController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Pessoas\Controllers\Admin'), function() {
	Route::post('admin/pessoas/save', ['uses' => 'AdminPessoasController@save']);
	Route::post('admin/pessoas/upload_galeria/{id}', ['uses' => 'AdminPessoasController@upload_galeria']);
    Route::post('admin/pessoas/delete_thumbnail_principal', ['uses' => 'AdminPessoasController@delete_thumbnail_principal']);
    Route::post('admin/pessoas/delete_imagem/{id}', ['uses' => 'AdminPessoasController@delete_imagem']);
});
