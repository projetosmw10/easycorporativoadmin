@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Pessoa</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/pessoas?empresa_id='.app()->request->empresa_id) }}">Pessoa</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/pessoas/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
                                    <div class="form-group">
                                        <label for="nome" class="col-md-3 control-label">nome *</label>
                                        <div class="col-md-7">
                                        <input id="nome" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->nome : ""; ?>" name="nome" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="identificador" class="col-md-3 control-label">identificador *</label>
                                        <div class="col-md-7">
                                        <input id="identificador" required minlength="13" type="text" class="identificador form-control" value="<?php echo (isset($item)) ? $item->identificador : ""; ?>" name="identificador" />
                                    </div>
             </div>	

                                    <!-- <div class="form-group">
                                        <label for="ativo" class="col-md-3 control-label">ativo *</label>
                                        <div class="col-md-7">
                                            <input id="ativo" required type="number" step="1" class="form-control" value="<?php echo (isset($item)) ? $item->ativo : ""; ?>" name="ativo" />
                                         </div>
             </div> -->

								</div>

							
							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/pessoas/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

//relacional
//adiciona empresa_id em todos os links
$('a.except').each(function () {
	var href = $(this).attr('href');
	link = 'empresa_id=<?=app('request')->query('empresa_id')?>';
	if(href.indexOf('?') === -1) {
		link = '?' + link;
	} else {
		link = '&' + link;
	}
	$(this).attr('href', href + link);
});
//cola o input
$('#mainForm').append('<input id="empresa_id"  type="hidden" class="form-control" value="<?=app('request')->query('empresa_id'); ?>" name="empresa_id" />');

</script>
@endsection
