@extends('layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			@if(isset($empresa))
			{{ $empresa->titulo }}<br>
			@endif
			Pessoas
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Pessoas</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<div class="box">
					<div class="box-header">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/pessoas/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
						<hr>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<table id="list-data-table" class="table table-bordered table-striped">

                            <thead>
                                 <tr>
                                     <th>ID</th>
                                     <th>Nome</th>
                                     <th>Identificador</th>
                                     <th>Usuário</th>
                                     <th>Código</th>
                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($itens as $item){ ?>
									<tr>
										<td><?php echo $item->id; ?></td>
                                            <td> <?php echo $item->nome?> </td>
                                            <td> <?php echo $item->identificador?> </td>
                                            <td> <?php echo isset($item->user) ? '<a href="/admin/users/edit/'.$item->user->id. '">'.$item->user->email.'</a>' : ''?> </td>
                                            <td> 
												<?php if(isset($item->code)) { ?>
													<a href="/admin/codes/edit/<?=$item->code->id?>"><?=$item->code->code?></a>
												<?php
												}
												?>
											</td>
                                            <td>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<a href="/admin/pessoas/edit/<?php echo $item->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/pessoas/delete/<?php echo $item->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/pessoas/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
//relacional
//adiciona viagem_id em todos os links
$('a:not(.except)').each(function () {
	var href = $(this).attr('href');
	link = 'empresa_id=<?=app('request')->query('empresa_id', 0)?>';
	if(href.indexOf('?') === -1) {
		link = '?' + link;
	} else {
		link = '&' + link;
	}
	$(this).attr('href', href + link);
})
</script>
@endsection
