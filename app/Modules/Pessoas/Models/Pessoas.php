<?php
// Pesso desculpas pela nomeclatura, uma vez era só CPFS cadastrados para a empresa,
// agora o mais certo seriam EmpresasPessoas ou só Pessoas (algo do tipo)
namespace App\Modules\Pessoas\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Pessoas extends Model {
    protected $table = 'pessoas';
    protected $fillable = ['user_id', 'empresa_id', 'code_id', 'nome', 'identificador', 'ativo', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug'];

    public function empresa() {
        return $this->belongsTo('\App\Modules\Empresas\Models\Empresas', 'empresa_id');
    }
    public function user() {
        return $this->belongsTo('\App\User', 'user_id');
    }
    public function code() {
        return $this->belongsTo('\App\Modules\Codes\Models\Codes', 'code_id');
    }
}
