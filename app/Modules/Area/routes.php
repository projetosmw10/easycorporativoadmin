<?php

// ================================
// Área routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Area\Controllers'), function() {
	Route::get('area', ['uses' => 'AreaController@index']);
	Route::get('area/detalhe/{slug}', ['uses' => 'AreaController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Area\Controllers\Admin'), function() {
	Route::get('admin/area', ['uses' => 'AdminAreaController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Area\Controllers\Admin'), function() {
	Route::get('admin/area/add', ['uses' => 'AdminAreaController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Area\Controllers\Admin'), function() {
	Route::get('admin/area/edit/{id}', ['uses' => 'AdminAreaController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Area\Controllers\Admin'), function() {
	Route::get('admin/area/delete/{id}', ['uses' => 'AdminAreaController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Area\Controllers\Admin'), function() {
	Route::post('admin/area/save', ['uses' => 'AdminAreaController@save']);
	Route::post('admin/area/upload_galeria/{id}', ['uses' => 'AdminAreaController@upload_galeria']);
    Route::post('admin/area/delete_thumbnail_principal', ['uses' => 'AdminAreaController@delete_thumbnail_principal']);
    Route::post('admin/area/delete_imagem/{id}', ['uses' => 'AdminAreaController@delete_imagem']);
});
