<?php

namespace App\Modules\Area\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(38);
	}

	public function index(){
		$data = array();
		return view('area',$data);
	}
}
