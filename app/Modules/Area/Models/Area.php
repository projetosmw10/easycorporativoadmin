<?php

namespace App\Modules\Area\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $fillable = array('descricao', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', );

    public function subareas()
	{
		return $this->hasMany('App\Modules\Area\Models\Area_subarea', 'id_area');	
	}

	public static function getImagens($id){
		return DB::table('area_imagens')->where('id_area', $id)->get();
    }
    public static function getImagem($id){
		return DB::table('area_imagens')->find($id);
    }
    
	public static function criar_imagem($input){
		return DB::table('area_imagens')->insert([
			[
				'id_area' => $input['id_area'],
				'thumbnail_principal' => $input['thumbnail_principal'],
			]
		]);
    }
    public static function deletar_imagem($id){
		return DB::table('area_imagens')
				->where('id', $id)
				->delete();
	}

	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'area' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
