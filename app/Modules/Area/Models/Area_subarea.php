<?php

namespace App\Modules\Area\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Area_subarea extends Model
{
	protected $table = 'area_subarea';
	protected $fillable = array('id_area', 'descricao');


	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'area_subarea' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
