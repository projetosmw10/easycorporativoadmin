@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Área</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/area') }}">Área</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<li><a data-toggle="pill" href="#subareas-tab">Subáreas</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
							<?php if($modulo->galeria){ ?>
								<li><a data-toggle="pill" href="#imagens-tab">Galeria</a></li>
							<?php } ?>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/area/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
                                    <div class="form-group">
                                        <label for="descricao" class="col-md-3 control-label">Descrição *</label>
                                        <div class="col-md-7">
                                        <input id="descricao" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->descricao : ""; ?>" name="descricao" />
                                    </div>
             </div>

								</div>
							

							
							<div id="subareas-tab" class="tab-pane fade">
								<div class="col-lg-12 text-center">
  									<button type="button" class="btn btn-success" id="add_agencia">Adicionar Subárea</button>
  								</div>	
								<div class="spacer"></div>
								<table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Descrição</th>
                                            <th>Ação</th>
                                        </tr>

                                    </thead>
                                    <tbody id="tbody-subarea">
                                    	<?php if (isset($item->subareas))
                                    	{
                                    		foreach ($item->subareas as $a) {?>                                    		
                                    			<tr>
													<td><input name="subarea[]" type="text" class="form-control" value="<?php echo $a["descricao"]; ?>"/></td>
													<td><a class="delete_subarea btn btn-danger"><i class="fa fa-trash"></i></a></td>
												</tr>
                                    	<?php }
                                    	} ?>
                                    </tbody>

                                </table>
							  	
								<div class="spacer"></div>
                                <script type="text/javascript">
                            		$('.delete_subarea').click(function(){
                            			var tr = $(this).closest('tr');
                            			
                            			tr.remove();                            			
                            		});


                                	$('#add_agencia').click(function(){
										$('#tbody-subarea').append('<tr>'+
																		'<td><input name="subarea[]" type="text" class="form-control"/></td>'+
																		'<td><a class="delete_subarea btn btn-danger"><i class="fa fa-trash"></i></a></td>'+
																	'</tr>');

										$('.delete_subarea').click(function(){
	                            			var tr = $(this).closest('tr');
	                            			
	                            			tr.remove();                            			
	                            		});
											
                                	});
                                </script>
							</div>
							</form>
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/area/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
