<?php

namespace App\Modules\Onesignal\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Onesignal extends Model {
    protected $table = 'onesignal';
    protected $fillable = ['user_id', 'player_id'];

    public function user() {
        return $this->belongsTo('\App\User', 'user_id');
    }
}
