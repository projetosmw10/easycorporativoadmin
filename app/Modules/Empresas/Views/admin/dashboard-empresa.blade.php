@extends($current_template)


@section('content')

<div class="content-wrapper">
    
    <div class="content">
        <h3>
        Códigos
        </h3>
        <hr>
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Código</th>
                    <th>Quantidade</th>
                    <th>Expira Em</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if($empresa)
                @foreach($empresa->codes as $code)
                <tr class="{{ $code->isExpired() ? 'text-danger' : '' }}">
                    <td>{{ $code->code }}</td>
                    <td>{{ $code->pessoas()->count() }} / {{ $code->quantity }}</td>
                    <td>{{ $code->expire_at }}</td>
                    <td></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>




        <h3>
        Pessoas
        </h3>
        <hr>
        <a href="{{ url('admin/pessoas/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
        <form class="row" action="/admin/empresas/dashboard/importar" enctype="multipart/form-data" method="post"> 
        {{csrf_field()}}

            <div class="form-group col-md-8" >
                <label>Importar</label>
                <input type="file" class="form-control" name="arquivo">
                <small>
                <a href="{{ url('img/tutorial.jpg') }}" target="_blank">Clique aqui para ver como importar</a>
                </small>
            </div>
            <div class="col-md-4">
            <label>&ensp; </label><br>
            <button class="btn btn-primary btn-small">Enviar</button>
            </div>
        </form>
        <table class="table table-bordered table-striped">

            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Identificador</th>
                    <th>Usuário</th>
                    <th width="100">Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach($empresa->pessoas as $pessoa)
                <tr>
                    <td>{{ $pessoa->nome }}</td>
                    <td>{{ $pessoa->identificador }}</td>
                    <td>
                    @if(isset($pessoa->user))
                    <span title="{{ $pessoa->user->id }}">{{ $pessoa->user->first_name }} - {{ $pessoa->user->email }}</span>
                    @endif
                    </td>
                    <td>
                        <a href="/admin/pessoas/edit/{{ $pessoa->id }}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                        <a href="/admin/pessoas/delete/{{ $pessoa->id }}" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>


@endsection