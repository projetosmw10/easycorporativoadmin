@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Empresas</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/empresas') }}">Empresas</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<!-- /.box-header -->
					<div class="box-body">
					<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							@if(isset($item))
							<li class=""><a data-toggle="pill" href="#codigos-tab">Códigos</a></li>
							<li class=""><a  href="/admin/pessoas?empresa_id=<?php echo $item->id?>">Pessoas</a></li>
							@endif
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>

						</ul>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/empresas/save') }}">
						<div class="tab-content">
						<div class="text-danger text-center">Ao alterar aqui, estará alterando em todos apps</div>
								<div id="info-tab" class="tab-pane fade in active">
								
								<div class="text-center">
								</div>
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
									<br>
									<br>
									<div class="form-group">
										<label for="" class="col-md-3 control-label">Sem Identificador?</label>
										<div class="col-md-7">
											<select name="sem_identificador" class="form-control" id="">
												<option value="1" <?=isset($item) && $item->sem_identificador == 1 ? 'selected' : '' ?>>Sim</option>
												<option value="0" <?=isset($item) && $item->sem_identificador == 0 ? 'selected' : '' ?>>Não</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Imagem</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<img src="<?php echo url($item->thumbnail_principal); ?>" height="150px">
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="titulo" class="col-md-3 control-label">Título *</label>
                                        <div class="col-md-7">
                                        <input id="titulo" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->titulo : ""; ?>" name="titulo" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="descricao" class="col-md-3 control-label">Descrição </label>
                                        <div class="col-md-7">
                                        <textarea id="descricao"  class="form-control" name="descricao"><?php echo (isset($item)) ? $item->descricao : ""; ?></textarea>
                                    </div>
             </div>

								<div class="form-group">
										<label for="email" class="col-md-3 control-label">E-mail*</label>

										<div class="col-md-7">
											<input required <?=isset($user) ? 'disabled' : ''?> id="email" type="text" class="form-control" value="<?php echo (isset($user)) ? $user->email : ''; ?>"
											    name="user[email]" />
										</div>
									</div>

									<div class="form-group">
										<label for="password" class="col-md-3 control-label">Senha<?=empty($user) ? '*' : ''?></label>

										<div class="col-md-7">
											<input <?=empty($user) ? 'required' : ''?> id="password" type="password" class="form-control" value="" name="user[password]" />
											@if(isset($user))
											<small>Deixe em branco para não atualizar a senha</small>
											@endif
										</div>
									</div>
								
									

								</div>

								<div id="codigos-tab" class="tab-pane">
<table id="" class="table table-bordered table-striped">

<thead>
	 <tr>
		 <th>Código</th>
		 <th>Quantidade</th>
		 <th>Expira Em</th>
		 <th width="170">Ação</th>
	 </tr>
</thead>
<tbody>
	
<?php 
if(!empty($item->codes)){
foreach ($item->codes as $c){ ?>
		<tr>
				<td> <?php echo $c->code?> </td>
				<td>{{ $c->pessoas()->count() }} / {{ $c->quantity }}</td>
				<td> <?php echo $c->expire_at?> </td>
				<td>
				<?php if($current_role->hasAccess("codes.update")){ ?>
					<a href="/admin/codes/edit/<?php echo $c->id; ?>" class="btn btn-primary"><i class="fa fa-edit"></i></a>
				<?php } ?>
				<?php if($current_role->hasAccess("codes.delete")){ ?>
					<a href="/admin/codes/delete/<?php echo $c->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
				<?php } ?>
			</td>
		</tr>
	<?php }
	} ?>
</tbody>
</table>
								</div>
							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/empresas/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
