<?php

namespace App\Modules\Empresas\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class EmpresasController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(35);
	}

	public function index(){
		$data = array();
		return view('empresas',$data);
	}
}
