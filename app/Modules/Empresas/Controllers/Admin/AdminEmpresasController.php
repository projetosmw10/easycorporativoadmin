<?php

namespace App\Modules\Empresas\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Empresas\Models\Empresas;
use App\Modules\Codes\Models\Codes;
use App\Modules\Pessoas\Models\Pessoas;
use App\User;
use Sentinel;
use Activation;

class AdminEmpresasController extends Controller
{
	private $modulo;
	private $lastInsertId;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(35);
	}

	public function index(){
		$data['itens'] = Empresas::with('user')->get();
		return view('Empresas::admin/empresas',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		return view('Empresas::admin/form-empresas', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		$data['item'] = Empresas::with(['codes' => function($q) {
			$q->orderBy('expire_at', 'desc');
		}])->find($id);
		$data['user'] = User::find($data['item']->user_id);
		return view('Empresas::admin/form-empresas',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();

			$post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',',$post['meta_keywords']) : null;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/empresas/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
			$userPost = $post['user'];
			unset($post['user']);
			if ($userPost['password']) {
                $userPost['password'] = bcrypt($userPost['password']);
            } else {
                unset($userPost['password']);
			}
			$userPost['first_name'] = $post['titulo'];
			
			if($request->input('id')){
				$item = Empresas::find($request->input('id'));
				$item->update($post);

				$user = User::find($item->user_id);
				$user->update($userPost);

			}else{
				if(\App\User::where('email', $userPost['email'])->where('id', '!=', $request->input('id', 0))->exists()) {
					throw new \Exception('E-mail escolhido já está sendo utilizado');
				}
				$item = Empresas::create($post);

				$user = User::create($userPost);

				$item->user_id = $user->id;
				$item->save();

				$user = Sentinel::findById($user->id);
				$activation = Activation::create($user);
				Activation::complete($user, $activation->code);
				
				// 3 é role de empresa não MODIFICAR ESSE ROLE
				$user->roles()->attach([3]);
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/empresas');
		}catch(\Exception $e){
			\Session::flash('type', 'danger');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    
	public function upload_galeria($id, Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/empresas/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Empresas::criar_imagem(array('id_empresas' => $id, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
	}

    public function delete($id){
		try{
			Empresas::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/empresas');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}
	public function delete_imagem($id){
		try{
			$imagem = Empresas::getImagem($id);
			Empresas::deletar_imagem($id);

			unlink('uploads/empresas/'.$imagem->thumbnail_principal);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}


	}

	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Empresas::find($post['id']);

			if(is_file(public_path() . $imagem->thumbnail_principal)){
				unlink(public_path() . $imagem->thumbnail_principal);
				Empresas::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}



	// CUSTOM

	public function dashboard(Request $request) {
		$data['empresa'] = Empresas::with([
			'codes' => function($q) {
				$q->orderBy('expire_at', 'desc');
			},
			'pessoas' => function($q) {
				$q->orderBy('nome', 'asc');
			},
			'pessoas.user'
		])->where('user_id', Sentinel::getUser()->id)->first();

		return view('Empresas::admin/dashboard-empresa', $data);
	}

	public function dashboardImportar(Request $request) {

		$empresa = Empresas::where('user_id', \Sentinel::getUser()->id)->first();
		
		if (!$request->hasFile('arquivo')) {
			\Session::flash('type', 'error');
			\Session::flash('message', 'Faça o upload de um arquivo!');
			return redirect()->back();
		}
		
		
		$file = $request->file('arquivo');

		if($file->getClientOriginalExtension() != 'csv' && $file->getClientOriginalExtension() != '.csv' ) {
			\Session::flash('type', 'error');
			\Session::flash('message', 'Faça o upload de um arquivo .csv!');
			return redirect()->back();
		}

		
		$tmpFilePath = '/uploads/alimentos/importar/';
		if (!is_dir($tmpFilePath)) {
			@mkdir($tmpFilePath, 0777, true);
		}

		$tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->getClientOriginalExtension();
		$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
		$path = public_path().($tmpFilePath . $tmpFileName);

		$handle = fopen($path, 'r');

		while ($csvLine = fgetcsv($handle, 2000, ';')) {
			Pessoas::updateOrCreate([
				'identificador' => $csvLine[1]
			],[
				'nome' => $csvLine[0] ?? '',
				'identificador' => $csvLine[1] ?? '',
				'empresa_id' => $empresa->id
			]);
		}
		fclose($handle);
		@unlink($path);

		\Session::flash('type', 'success');
		\Session::flash('message', 'Pessoas importadas com sucesso!');
		return redirect()->back();
	
	}


}
