<?php

namespace App\Modules\Empresas\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Empresas extends Model
{
    protected $table = 'empresas';
    protected $fillable = array('titulo', 'descricao', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', 'user_id', 'sem_identificador');


		public function pessoas(){
			return $this->hasMany('\App\Modules\Pessoas\Models\Pessoas', 'empresa_id');
		}

		public function user() {
			return $this->belongsTo('\App\User', 'user_id');
		}

		public function codes() {
			return $this->hasMany('\App\Modules\Codes\Models\Codes', 'empresa_id');
		}
}
