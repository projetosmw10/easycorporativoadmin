<?php

// ================================
// Empresas routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Empresas\Controllers'), function() {
	Route::get('empresas', ['uses' => 'EmpresasController@index']);
	Route::get('empresas/detalhe/{slug}', ['uses' => 'EmpresasController@detalhe']);
});

Route::group(['middleware' => ['auth', 'inRole:empresa'], 'namespace' => 'App\Modules\Empresas\Controllers\Admin'], function() {
	Route::get('admin/empresas/dashboard', ['uses' => 'AdminEmpresasController@dashboard']);
	Route::post('admin/empresas/dashboard/importar', ['uses' => 'AdminEmpresasController@dashboardImportar']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Empresas\Controllers\Admin'), function() {
	Route::get('admin/empresas', ['uses' => 'AdminEmpresasController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Empresas\Controllers\Admin'), function() {
	Route::get('admin/empresas/add', ['uses' => 'AdminEmpresasController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Empresas\Controllers\Admin'), function() {
	Route::get('admin/empresas/edit/{id}', ['uses' => 'AdminEmpresasController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Empresas\Controllers\Admin'), function() {
	Route::get('admin/empresas/delete/{id}', ['uses' => 'AdminEmpresasController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Empresas\Controllers\Admin'), function() {
	Route::post('admin/empresas/save', ['uses' => 'AdminEmpresasController@save']);
	Route::post('admin/empresas/upload_galeria/{id}', ['uses' => 'AdminEmpresasController@upload_galeria']);
    Route::post('admin/empresas/delete_thumbnail_principal', ['uses' => 'AdminEmpresasController@delete_thumbnail_principal']);
    Route::post('admin/empresas/delete_imagem/{id}', ['uses' => 'AdminEmpresasController@delete_imagem']);
});
