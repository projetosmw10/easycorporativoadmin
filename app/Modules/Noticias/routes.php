<?php

// ================================
// Notícias routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Noticias\Controllers'), function() {
	Route::get('noticias', ['uses' => 'NoticiasController@index']);
	Route::get('noticias/detalhe/{slug}', ['uses' => 'NoticiasController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Noticias\Controllers\Admin'), function() {
	Route::get('admin/noticias', ['uses' => 'AdminNoticiasController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Noticias\Controllers\Admin'), function() {
	Route::get('admin/noticias/add', ['uses' => 'AdminNoticiasController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Noticias\Controllers\Admin'), function() {
	Route::get('admin/noticias/edit/{id}', ['uses' => 'AdminNoticiasController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Noticias\Controllers\Admin'), function() {
	Route::get('admin/noticias/delete/{id}', ['uses' => 'AdminNoticiasController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Noticias\Controllers\Admin'), function() {
	Route::post('admin/noticias/save', ['uses' => 'AdminNoticiasController@save']);
	Route::post('admin/noticias/upload_galeria/{id}', ['uses' => 'AdminNoticiasController@upload_galeria']);
    Route::post('admin/noticias/delete_thumbnail_principal', ['uses' => 'AdminNoticiasController@delete_thumbnail_principal']);
    Route::post('admin/noticias/delete_imagem/{id}', ['uses' => 'AdminNoticiasController@delete_imagem']);
});
