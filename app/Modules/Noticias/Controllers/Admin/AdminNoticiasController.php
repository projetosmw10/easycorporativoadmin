<?php

namespace App\Modules\Noticias\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Noticias\Models\Noticias;

class AdminNoticiasController extends Controller
{
	private $modulo;
	private $lastInsertId;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(41);
	}

	public function index(){
		$data['itens'] = Noticias::all();
		return view('Noticias::admin/noticias',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		$data['nextId'] = Noticias::getNextAutoIncrement();
		return view('Noticias::admin/form-noticias', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		$data['item'] = Noticias::find($id);
		return view('Noticias::admin/form-noticias',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();

			$post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',',$post['meta_keywords']) : null;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/noticias/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
            
			if($request->input('id')){
				Noticias::find($request->input('id'))->update($post);
			}else{
				Noticias::create($post);
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/noticias');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    
	public function upload_galeria($id, Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/noticias/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Noticias::criar_imagem(array('id_noticias' => $id, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
	}

    public function delete($id){
		try{
			Noticias::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/noticias');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}
	public function delete_imagem($id){
		try{
			$imagem = Noticias::getImagem($id);
			Noticias::deletar_imagem($id);

			unlink('uploads/noticias/'.$imagem->thumbnail_principal);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}


	}

	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Noticias::find($post['id']);

			if(is_file(public_path() . $imagem->thumbnail_principal)){
				unlink(public_path() . $imagem->thumbnail_principal);
				Noticias::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


}
