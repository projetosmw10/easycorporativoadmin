<?php

namespace App\Modules\Noticias\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Noticias extends Model
{
    protected $table = 'noticias';
    protected $fillable = array('titulo', 'data', 'texto', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', );

	public function getThumbnailPrincipalAttribute($value) {
        https://laravel.com/docs/5.6/filesystem#file-visibility
        return $value ? url($value) : '';
    }

	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'noticias' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
