<?php

namespace App\Modules\Dicas\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\JWTUser;
use Sentinel;

class Dicas extends Model {
	
    protected $table = 'dicas';
	protected $fillable = array('aplicativo_id', 'titulo', 'descricao', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', 'ordem');
		
	public function getThumbnailPrincipalAttribute($value) {
		return $value ? url($value) : '';
	}

}
