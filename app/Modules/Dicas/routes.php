<?php

// ================================
// Dicas routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Dicas\Controllers'), function() {
	Route::get('dicas', ['uses' => 'DicasController@index']);
	Route::get('dicas/detalhe/{slug}', ['uses' => 'DicasController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Dicas\Controllers\Admin'), function() {
	Route::get('admin/dicas', ['uses' => 'AdminDicasController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Dicas\Controllers\Admin'), function() {
	Route::get('admin/dicas/add', ['uses' => 'AdminDicasController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Dicas\Controllers\Admin'), function() {
	Route::get('admin/dicas/edit/{id}', ['uses' => 'AdminDicasController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Dicas\Controllers\Admin'), function() {
	Route::get('admin/dicas/delete/{id}', ['uses' => 'AdminDicasController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Dicas\Controllers\Admin'), function() {
	Route::post('admin/dicas/save', ['uses' => 'AdminDicasController@save']);
	Route::post('admin/dicas/upload_galeria/{id}', ['uses' => 'AdminDicasController@upload_galeria']);
    Route::post('admin/dicas/delete_thumbnail_principal', ['uses' => 'AdminDicasController@delete_thumbnail_principal']);
    Route::post('admin/dicas/delete_imagem/{id}', ['uses' => 'AdminDicasController@delete_imagem']);
});
