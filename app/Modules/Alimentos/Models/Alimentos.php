<?php

namespace App\Modules\Alimentos\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\JWTUser;

class Alimentos extends Model {
    protected $table = 'alimentos';
    protected $fillable = ['titulo', 'energia', 'proteina', 'carboidratos', 'gordura', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', 'destaque'];

    protected $appends = ['favoritado'];

    public function getFavoritadoAttribute($value) {
        // verifica se o usuario atual ja favoritou aquele conteudo
        if (!JWTUser::getUser()) {
            return 0;
        }
        $rel = DB::table('alimento_user')
               ->where('alimento_id', $this->id)
               ->where('user_id', JWTUser::getUser()->id)
               ->first();

        return $rel && $rel->favorited_at ? 1 : 0;
    }

    public function getThumbnailPrincipalAttribute($value) {
        return $value ? url($value) : '';
    }
}
