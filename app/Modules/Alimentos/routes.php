<?php

// ================================
// Alimentos routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Alimentos\Controllers'), function() {
	Route::get('alimentos', ['uses' => 'AlimentosController@index']);
	Route::get('alimentos/detalhe/{slug}', ['uses' => 'AlimentosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Alimentos\Controllers\Admin'), function() {
	Route::get('admin/alimentos', ['uses' => 'AdminAlimentosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Alimentos\Controllers\Admin'), function() {
	Route::get('admin/alimentos/add', ['uses' => 'AdminAlimentosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Alimentos\Controllers\Admin'), function() {
	Route::get('admin/alimentos/edit/{id}', ['uses' => 'AdminAlimentosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Alimentos\Controllers\Admin'), function() {
	Route::get('admin/alimentos/delete/{id}', ['uses' => 'AdminAlimentosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Alimentos\Controllers\Admin'), function() {
	Route::get('admin/alimentos/importar', ['uses' => 'AdminAlimentosController@importar_form']);
	Route::post('admin/alimentos/importar', ['uses' => 'AdminAlimentosController@importar_save']);
	Route::post('admin/alimentos/save', ['uses' => 'AdminAlimentosController@save']);
	Route::post('admin/alimentos/upload_galeria/{id}', ['uses' => 'AdminAlimentosController@upload_galeria']);
    Route::post('admin/alimentos/delete_thumbnail_principal', ['uses' => 'AdminAlimentosController@delete_thumbnail_principal']);
    Route::post('admin/alimentos/delete_imagem/{id}', ['uses' => 'AdminAlimentosController@delete_imagem']);
});
