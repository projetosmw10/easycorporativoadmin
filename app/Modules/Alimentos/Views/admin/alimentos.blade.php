@extends('layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Alimentos
			<small>Listagem</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Alimentos</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<div class="box">
					<div class="box-header">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="row">
								<div class="col-md-6"><a href="{{ url('admin/alimentos/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a></div>
								<div class="col-md-6"><a href="{{ url('admin/alimentos/importar') }}" class="table-add"><i class="fa fa-plus"></i> Importar</a></div>
							</div>
						<?php } ?>
						<hr>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<table id="list-data-table" class="table table-bordered table-striped">

                            <thead>
                                 <tr>
                                     <th>ID</th>
                                     <th>Descrição dos Alimentos</th>
                                     <th>Energia (kcal)</th>
                                     <th>Proteina (g)</th>
                                     <th>Carboidratos (g)</th>
                                     <th>Gordura (g)</th>
                                     <th>Destaque</th>
                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($itens as $item){ ?>
									<tr>
										<td><?php echo $item->id; ?></td>
                                            <td> <?php echo $item->titulo?> </td>
                                            <td> <?php echo number_format($item->energia,2,",",".")?> </td>
                                            <td> <?php echo number_format($item->proteina,2,",",".")?> </td>
                                            <td> <?php echo number_format($item->carboidratos,2,",",".")?> </td>
                                            <td> <?php echo number_format($item->gordura,2,",",".")?> </td>
											<td> <?php echo $item->destaque == 1 ? 'Sim' : 'Não'?> </td>
                                            <td>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<a href="/admin/alimentos/edit/<?php echo $item->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/alimentos/delete/<?php echo $item->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>

						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/alimentos/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->

@endsection
