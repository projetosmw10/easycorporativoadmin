@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<small>Importar Alimentos</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/alimentos') }}">Alimentos</a></li>
			<li class="active">Importar</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
	
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/alimentos/importar') }}">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}


									<div class="form-group">
										<label for="arquivo" class="col-md-3 control-label">Arquivo</label>
										<div class="col-md-7">
	
                                            <input class="form-control" type="file" accept=".csv" name="arquivo">
                                            <small>Arquivo em formato CSV<br>
											titulo,energia,proteina,carboidratos,gordura<br><br>
											Não é necessário informar os cabeçalhos
											</small>
										</div>
									</div>
								</div>

							</form>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
@endsection
