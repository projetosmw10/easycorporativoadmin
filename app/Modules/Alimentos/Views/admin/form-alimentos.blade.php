@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Alimentos</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/alimentos') }}">Alimentos</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/alimentos/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>

									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Imagem</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<img src="<?php echo ($item->thumbnail_principal); ?>" height="150px">
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="titulo" class="col-md-3 control-label">Descrição dos Alimentos *</label>
                                        <div class="col-md-7">
                                        <input id="titulo" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->titulo : ""; ?>" name="titulo" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="energia" class="col-md-3 control-label">Energia (kcal) </label>
                                        <div class="col-md-7">
                                        <input id="energia"  type="number" class="form-control" value="<?php echo (isset($item)) ? $item->energia : ""; ?>" name="energia" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="proteina" class="col-md-3 control-label">Proteina (g) </label>
                                        <div class="col-md-7">
                                        <input id="proteina"  type="number" class="form-control" value="<?php echo (isset($item)) ? $item->proteina : ""; ?>" name="proteina" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="carboidratos" class="col-md-3 control-label">Carboidratos (g) </label>
                                        <div class="col-md-7">
                                        <input id="carboidratos"  type="number" class="form-control" value="<?php echo (isset($item)) ? $item->carboidratos : ""; ?>" name="carboidratos" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="gordura" class="col-md-3 control-label">Gordura (g) *</label>
                                        <div class="col-md-7">
                                        <input id="gordura" required type="number" class="form-control" value="<?php echo (isset($item)) ? $item->gordura : ""; ?>" name="gordura" />
                                    </div>
             </div>

									<div class="form-group evento" style="">
										<label for="destaque" class="col-md-3 control-label">Destaque</label>
										<div class="col-md-7">
										<input type="hidden" name="destaque" value="0">
											<input type="checkbox" name="destaque" <?=isset($item) && $item->destaque == 1 ? 'checked' : ''?> value="1" style="height: 15px; width: 15px; border-color: rgb(210, 214, 222);">
										</div>
									</div>

								</div>

							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/alimentos/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
