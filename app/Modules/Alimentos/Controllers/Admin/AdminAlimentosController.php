<?php

namespace App\Modules\Alimentos\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Alimentos\Models\Alimentos;

class AdminAlimentosController extends Controller {
    private $modulo;
    private $lastInsertId;

    public function __construct() {
        $this->middleware('auth');
        $this->modulo = \App\Gerador::find(31);
    }

    public function index(Request $request) {
        $data['itens'] = Alimentos::select('*');
        if($request->ajax()) {
            if($request->query('search')) {
                $data['itens']->where('titulo', 'like', '%'.$request->query('search').'%');
            }
            $data['itens'] = $data['itens']->get();
            return response()->json($data['itens']);
        }
        $data['itens'] = $data['itens']->get();
        return view('Alimentos::admin/alimentos', $data);
    }

    public function add() {
        $data = [];
        $data['modulo'] = $this->modulo;
        return view('Alimentos::admin/form-alimentos', $data);
    }

    public function edit($id) {
        $data['modulo'] = $this->modulo;
        $data['item'] = Alimentos::find($id);
        return view('Alimentos::admin/form-alimentos', $data);
    }

    public function save(Request $request) {
        try {
            $post = $request->input();

            $post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',', $post['meta_keywords']) : null;

            if ($request->hasFile('thumbnail_principal')) {
                $file = $request->file('thumbnail_principal');

                $tmpFilePath = '/uploads/alimentos/';
                if (!is_dir($tmpFilePath)) {
                    @mkdir($tmpFilePath, 0777, true);
                }

                $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->extension();
                $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
                $path = $tmpFilePath . $tmpFileName;

                $post['thumbnail_principal'] = $path;
            }

            if ($request->input('id')) {
                $alimento = Alimentos::find($request->input('id'))->update($post);
            } else {
                $alimento = Alimentos::create($post);
            }
            \Session::flash('type', 'success');
            \Session::flash('message', 'Alteracoes salvas com sucesso!');
            return redirect('admin/alimentos');
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    public function delete($id) {
        try {
            Alimentos::destroy($id);

            \Session::flash('type', 'success');
            \Session::flash('message', 'Registro removido com sucesso!');
            return redirect('admin/alimentos');
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Nao foi possivel remover o registro!');
            return redirect()->back();
        }
    }

    public function delete_thumbnail_principal(Request $request) {
        $post = $request->input();

        try {
            $imagem = Alimentos::find($post['id']);

            if (is_file(public_path() . $imagem->getOriginal('thumbnail_principal'))) {
                @unlink(public_path() . $imagem->getOriginal('thumbnail_principal'));
                Alimentos::where('id', $post['id'])->update(['thumbnail_principal' => '']);
            }

            return response()->json(['status' => true, 'message' => 'Registro removido com sucesso!']);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }

    public function importar_form() {
        return view('Alimentos::admin/form-importar');
    }

    public function importar_save(Request $request) {
		
        if (!$request->hasFile('arquivo')) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Faça o upload de um arquivo!');
            return redirect()->back();
		}
		
		
		$file = $request->file('arquivo');

		if($file->getClientOriginalExtension() != 'csv' && $file->getClientOriginalExtension() != '.csv' ) {
			\Session::flash('type', 'error');
            \Session::flash('message', 'Faça o upload de um arquivo .csv!');
            return redirect()->back();
		}

		
        $tmpFilePath = '/uploads/alimentos/importar/';
        if (!is_dir($tmpFilePath)) {
            @mkdir($tmpFilePath, 0777, true);
        }

        $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->getClientOriginalExtension();
        $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
        $path = public_path().($tmpFilePath . $tmpFileName);

        $handle = fopen($path, 'r');
        $header = true;

        while ($csvLine = fgetcsv($handle, 2000, ',')) {
            if ($header) {
                $header = false;
            } else {
                Alimentos::create([
                    'titulo' => $csvLine[0],
                    'energia' => str_replace(',', '.', $csvLine[1]),
                    'proteina' => str_replace(',', '.', $csvLine[2]),
                    'carboidratos' => str_replace(',', '.', $csvLine[3]),
                    'gordura' => str_replace(',', '.', $csvLine[4]),
                ]);
            }
        }
        fclose($handle);
        unlink($path);
        \Session::flash('type', 'success');
        \Session::flash('message', 'Alimentos importados com sucesso!');
        return redirect()->back();
    }
}
