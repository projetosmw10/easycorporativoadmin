<?php

namespace App\Modules\Alimentos\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AlimentosController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(31);
	}

	public function index(){
		$data = array();
		return view('alimentos',$data);
	}
}
