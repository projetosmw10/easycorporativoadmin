<?php

namespace App\Modules\Certificados\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Certificados extends Model
{
    protected $table = 'certificados';
    protected $fillable = array('titulo', 'preco', 'descricao', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', );

	public static function getImagens($id){
		return DB::table('certificados_imagens')->where('id_certificados', $id)->get();
	}
	
    public static function getImagem($id){
		return DB::table('certificados_imagens')->find($id);
	}

	public function getThumbnailPrincipalAttribute($val = null) {
		return $val ? url($val) : ''; 
	}
    
	public static function criar_imagem($input){
		return DB::table('certificados_imagens')->insert([
			[
				'id_certificados' => $input['id_certificados'],
				'thumbnail_principal' => $input['thumbnail_principal'],
			]
		]);
	}
	
    public static function deletar_imagem($id){
		return DB::table('certificados_imagens')
				->where('id', $id)
				->delete();
	}

	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = 'certificados' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
