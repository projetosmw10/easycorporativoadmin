<?php

// ================================
// Certificados routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Certificados\Controllers'), function() {
	Route::get('certificados', ['uses' => 'CertificadosController@index']);
	Route::get('certificados/detalhe/{slug}', ['uses' => 'CertificadosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Certificados\Controllers\Admin'), function() {
	Route::get('admin/certificados', ['uses' => 'AdminCertificadosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Certificados\Controllers\Admin'), function() {
	Route::get('admin/certificados/add', ['uses' => 'AdminCertificadosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Certificados\Controllers\Admin'), function() {
	Route::get('admin/certificados/edit/{id}', ['uses' => 'AdminCertificadosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Certificados\Controllers\Admin'), function() {
	Route::get('admin/certificados/delete/{id}', ['uses' => 'AdminCertificadosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Certificados\Controllers\Admin'), function() {
	Route::post('admin/certificados/save', ['uses' => 'AdminCertificadosController@save']);
	Route::post('admin/certificados/upload_galeria/{id}', ['uses' => 'AdminCertificadosController@upload_galeria']);
    Route::post('admin/certificados/delete_thumbnail_principal', ['uses' => 'AdminCertificadosController@delete_thumbnail_principal']);
    Route::post('admin/certificados/delete_imagem/{id}', ['uses' => 'AdminCertificadosController@delete_imagem']);
});
