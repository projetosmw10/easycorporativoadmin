<?php

namespace App\Modules\Certificados\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CertificadosController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(42);
	}

	public function index(){
		$data = array();
		return view('certificados',$data);
	}
}
