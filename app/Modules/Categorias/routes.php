<?php

// ================================
// Categorias routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Categorias\Controllers'), function() {
	Route::get('categorias', ['uses' => 'CategoriasController@index']);
	Route::get('categorias/detalhe/{slug}', ['uses' => 'CategoriasController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Categorias\Controllers\Admin'), function() {
	Route::get('admin/categorias', ['uses' => 'AdminCategoriasController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Categorias\Controllers\Admin'), function() {
	Route::get('admin/categorias/add', ['uses' => 'AdminCategoriasController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Categorias\Controllers\Admin'), function() {
	Route::get('admin/categorias/edit/{id}', ['uses' => 'AdminCategoriasController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Categorias\Controllers\Admin'), function() {
	Route::get('admin/categorias/delete/{id}', ['uses' => 'AdminCategoriasController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Categorias\Controllers\Admin'), function() {
	Route::post('admin/categorias/save', ['uses' => 'AdminCategoriasController@save']);
    Route::post('admin/categorias/delete_thumbnail_principal', ['uses' => 'AdminCategoriasController@delete_thumbnail_principal']);
});
