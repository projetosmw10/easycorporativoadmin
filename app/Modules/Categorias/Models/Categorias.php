<?php

namespace App\Modules\Categorias\Models;

use DB;
use Illuminate\Database\Eloquent\Model;
use App\JWTUser;

class Categorias extends Model {
    protected $table = 'categorias';
    protected $fillable = ['aplicativo_id', 'titulo', 'descricao', 'gratuito', 'certificado', 'ordem', 'thumbnail_principal'];
    protected $appends = ['conteudos_count', 'conteudos_assistido_count','porcentagem_assistido'];

    public function topicos() {
        return $this->hasMany('App\Modules\Topicos\Models\Topicos', 'categoria_id')->whereNull('topico_id')->orderBy('ordem');
    }

    public function conteudos() {
        return $this->hasManyThrough('App\Modules\Conteudos\Models\Conteudos', 'App\Modules\Topicos\Models\Topicos', 'categoria_id', 'topico_id');
    }
    
    public function aplicativo() {
        return $this->belongsTo('App\Modules\Aplicativos\Models\Aplicativos', 'aplicativo_id');
    }

    public function getThumbnailPrincipalAttribute($val) {
        return $val ? url($val) : '';
    }

    public function getPorcentagemAssistidoAttribute() {
        if(!$this->conteudos_count) {
            return 0;
        }
        return ceil(($this->conteudos_assistido_count / $this->conteudos_count) * 100);
    }



    public function getConteudosCountAttribute() {
        return $this->conteudos()->count();
    }

    public function getConteudosAssistidoCountAttribute() {
        $conteudo_ids = $this->conteudos()->pluck('conteudos.id')->toArray();

        if(!JWTUser::getUser()) {
            return 0;
        }
        return DB::table('conteudo_user')
                 ->whereIn('conteudo_id', $conteudo_ids)
                 ->where('user_id', JWTUser::getUser()->id)
                 ->whereNotNull('watched_at')
                 ->count();
    }
}
