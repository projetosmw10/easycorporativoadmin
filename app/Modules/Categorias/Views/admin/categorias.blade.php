@extends('layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Categorias
			<small>Listagem</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Categorias</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<div class="box">
					<div class="box-header">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/categorias/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
						<hr>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<table class="table table-bordered table-striped">

                            <thead>
                                 <tr>
                                     <th></th>
                                     <th>Título</th>
                                     <!-- <th>Gratuito</th> -->
                                     <th>Tópicos</th>
                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody class="sortable">
                            <?php foreach ($itens as $item){ 
								
								?>
									<tr data-id="<?=$item->id?>">
									<td width="10" style="cursor: move;vertical-align:middle"><i class="fa fa-bars"></i></td>
											<td width="1000"> <?php echo $item->titulo?> </td>
											<!-- <td><?=$item->gratuito == 1 ? 'Sim' : 'Não'?></td> -->
                                            <td width="50"> <a href="/admin/topicos?categoria_id=<?=$item->id?>" class="btn btn-primary"><i class="fa fa-list"></i></a> </td>
                                            <td width="100">
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<a href="/admin/categorias/edit/<?php echo $item->id; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/categorias/delete/<?php echo $item->id; ?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>

					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/categorias/add') }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
					</div>
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
$( ".sortable" ).sortable({
	update: function(event, ui) {
		var body = new Object();
		body.categorias = new Object();
		$('.sortable tr').each(function (index) {
			// $(this).find('td')[1].innerText = index + 1;
			body.categorias[$(this).attr('data-id')] = {ordem: index + 1}; 
		});
		$.post('/api/admin/categorias/ordem', $.param(body))
	}
});
</script>
@endsection
