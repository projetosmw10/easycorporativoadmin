<?php

namespace App\Modules\Categorias\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoriasController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(22);
	}

	public function index(){
		$data = array();
		return view('categorias',$data);
	}
}
