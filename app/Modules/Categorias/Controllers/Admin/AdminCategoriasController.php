<?php

namespace App\Modules\Categorias\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Categorias\Models\Categorias;
use View;


class AdminCategoriasController extends Controller
{
	private $modulo;
	private $lastInsertId;
	public $current_app;
    public function __construct(){
		$this->middleware('auth');
		$this->current_app = session('current_app');
		$this->modulo = \App\Gerador::find(22);
		
	}

	public function index(){
		$data['itens'] = Categorias::orderBy('ordem', 'asc')->where('aplicativo_id', $this->current_app->id)->get();
		return view('Categorias::admin/categorias',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		return view('Categorias::admin/form-categorias', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		$data['item'] = Categorias::find($id);
		return view('Categorias::admin/form-categorias',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();
			$post['aplicativo_id'] = $this->current_app->id;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/categorias/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
            
			if($request->input('id')){
				Categorias::find($request->input('id'))->update($post);
			}else{
				$post['ordem'] = Categorias::select('ordem')
                                        ->orderBy('ordem', 'desc')
                                        ->first();
				$post['ordem'] = $post['ordem'] ? $post['ordem']->ordem + 1 : '1';
				
				Categorias::create($post);
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/categorias');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    

    public function delete($id){
		try{
			Categorias::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/categorias');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}


	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Categorias::find($post['id']);

			if(is_file(public_path() . $imagem->getOriginal('thumbnail_principal'))){

				unlink(public_path() . $imagem->getOriginal('thumbnail_principal'));

				Categorias::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


}
