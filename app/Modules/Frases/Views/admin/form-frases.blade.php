@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Frases do Dia</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/frases') }}">Frases do Dia</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/frases/save') }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Imagem</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<img src="<?php echo url($item->thumbnail_principal); ?>" height="150px">
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
											<small>Tamanho sugerido 1024x1024px</small>
										</div>
                                    </div>
									
                                    <div class="form-group">
                                        <label for="titulo" class="col-md-3 control-label">Frase *</label>
                                        <div class="col-md-7">
											<input id="titulo" required maxlength="255" type="text" class="form-control" value="<?php echo (isset($item)) ? $item->titulo : ""; ?>" name="titulo" />
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="cor" class="col-md-3 control-label">Cor da fonte *</label>
                                        <div class="col-md-7">
											<input id="cor" required maxlength="255" type="color" style="padding:0;" class="form-control" value="<?php echo (isset($item)) ? $item->cor : "#ffffff"; ?>" name="cor" />
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="data" class="col-md-3 control-label">Data *</label>
                                        <div class="col-md-7">
											<input id="data" required type="date" class="form-control" value="<?php echo (isset($item)) ? $item->data : ""; ?>" name="data" />
										</div>
             						</div>


                                    <div class="form-group hidden">
                                        <label for="descricao" class="col-md-3 control-label">Descrição </label>
                                        <div class="col-md-7">
                                        <input id="descricao"  type="text" class="form-control" value="<?php echo (isset($item)) ? $item->descricao : ""; ?>" name="descricao" />
                                    </div>
             </div>

								</div>

								
							</form>

							
						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/frases/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
