<?php

// ================================
// Frases do Dia routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Frases\Controllers'), function() {
	Route::get('frases', ['uses' => 'FrasesController@index']);
	Route::get('frases/detalhe/{slug}', ['uses' => 'FrasesController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Frases\Controllers\Admin'), function() {
	Route::get('admin/frases', ['uses' => 'AdminFrasesController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Frases\Controllers\Admin'), function() {
	Route::get('admin/frases/add', ['uses' => 'AdminFrasesController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Frases\Controllers\Admin'), function() {
	Route::get('admin/frases/edit/{id}', ['uses' => 'AdminFrasesController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Frases\Controllers\Admin'), function() {
	Route::get('admin/frases/delete/{id}', ['uses' => 'AdminFrasesController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Frases\Controllers\Admin'), function() {
	Route::post('admin/frases/save', ['uses' => 'AdminFrasesController@save']);
    Route::post('admin/frases/delete_thumbnail_principal', ['uses' => 'AdminFrasesController@delete_thumbnail_principal']);
});
