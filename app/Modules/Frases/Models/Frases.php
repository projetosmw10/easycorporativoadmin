<?php

namespace App\Modules\Frases\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Frases extends Model
{
    protected $table = 'frases';
    protected $fillable = array('titulo', 'data', 'cor', 'descricao', 'thumbnail_principal');


    public function getThumbnailPrincipalAttribute($value) {
        https://laravel.com/docs/5.6/filesystem#file-visibility
        return $value ? url($value) : '';
    }
}
