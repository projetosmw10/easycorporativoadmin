<?php

namespace App\Modules\Frases\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Frases\Models\Frases;

class AdminFrasesController extends Controller
{
	private $modulo;
	private $lastInsertId;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(21);
	}

	public function index(){
		$data['itens'] = Frases::all();
		return view('Frases::admin/frases',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		return view('Frases::admin/form-frases', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		$data['item'] = Frases::find($id);
		return view('Frases::admin/form-frases',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();


            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/frases/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
            
			if($request->input('id')){
				Frases::find($request->input('id'))->update($post);
			}else{
				Frases::create($post);
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/frases');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    


    public function delete($id){
		try{
			Frases::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/frases');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}


	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Frases::find($post['id']);

			if(is_file(public_path() . $imagem->thumbnail_principal)){
				@unlink(public_path() . $imagem->thumbnail_principal);
			}
			Frases::where('id', $post['id'])->update(array("thumbnail_principal" => ''));

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


}
