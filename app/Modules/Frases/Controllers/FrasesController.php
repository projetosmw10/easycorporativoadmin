<?php

namespace App\Modules\Frases\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FrasesController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(21);
	}

	public function index(){
		$data = array();
		return view('frases',$data);
	}
}
