<?php

namespace App\Modules\Exercicios\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Exercicios extends Model
{
    protected $table = 'exercicios';
		protected $fillable = array('titulo', 'energia', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug');


		public function getThumbnailPrincipalAttribute($value) {
			return $value ? url($value) : '';
		}

}
