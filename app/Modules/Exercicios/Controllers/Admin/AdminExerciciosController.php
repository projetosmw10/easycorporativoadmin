<?php

namespace App\Modules\Exercicios\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Exercicios\Models\Exercicios;

class AdminExerciciosController extends Controller
{
	private $modulo;
	private $lastInsertId;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(32);
	}

	public function index(){
		$data['itens'] = Exercicios::all();
		return view('Exercicios::admin/exercicios',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		return view('Exercicios::admin/form-exercicios', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		$data['item'] = Exercicios::find($id);
		return view('Exercicios::admin/form-exercicios',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();

			$post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',',$post['meta_keywords']) : null;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/exercicios/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
            
			if($request->input('id')){
				Exercicios::find($request->input('id'))->update($post);
			}else{
				Exercicios::create($post);
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/exercicios');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    

    public function delete($id){
		try{
			Exercicios::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/exercicios');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}
	public function delete_imagem($id){
		try{
			$imagem = Exercicios::getImagem($id);
			Exercicios::deletar_imagem($id);

			unlink('uploads/exercicios/'.$imagem->thumbnail_principal);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}


	}

	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Exercicios::find($post['id']);

			if(is_file(public_path() . $imagem->getOriginal('thumbnail_principal'))){
				unlink(public_path() . $imagem->getOriginal('thumbnail_principal'));
				Exercicios::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}

	public function importar_form() {
        return view('Exercicios::admin/form-importar');
    }

    public function importar_save(Request $request) {
		
        if (!$request->hasFile('arquivo')) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Faça o upload de um arquivo!');
            return redirect()->back();
		}
		
		
		$file = $request->file('arquivo');

		if($file->getClientOriginalExtension() != 'csv' && $file->getClientOriginalExtension() != '.csv' ) {
			\Session::flash('type', 'error');
            \Session::flash('message', 'Faça o upload de um arquivo .csv!');
            return redirect()->back();
		}

		
        $tmpFilePath = '/uploads/exercicios/importar/';
        if (!is_dir($tmpFilePath)) {
            @mkdir($tmpFilePath, 0777, true);
        }

        $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->getClientOriginalExtension();
        $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
        $path = public_path().($tmpFilePath . $tmpFileName);

        $handle = fopen($path, 'r');
        $header = true;

        while ($csvLine = fgetcsv($handle, 1000, ',')) {
            if ($header) {
                $header = false;
            } else {
                Exercicios::create([
                    'titulo' => $csvLine[0],
                    'energia' => is_numeric($csvLine[1]) ? $csvLine[1] : 0.0
                ]);
            }
        }
        fclose($handle);
        unlink($path);
        \Session::flash('type', 'success');
        \Session::flash('message', 'Exercicios importados com sucesso!');
        return redirect()->back();
    }


}
