<?php

namespace App\Modules\Exercicios\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ExerciciosController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(32);
	}

	public function index(){
		$data = array();
		return view('exercicios',$data);
	}
}
