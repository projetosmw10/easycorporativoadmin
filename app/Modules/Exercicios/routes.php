<?php

// ================================
// Exercícios routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Exercicios\Controllers'), function() {
	Route::get('exercicios', ['uses' => 'ExerciciosController@index']);
	Route::get('exercicios/detalhe/{slug}', ['uses' => 'ExerciciosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Exercicios\Controllers\Admin'), function() {
	Route::get('admin/exercicios', ['uses' => 'AdminExerciciosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Exercicios\Controllers\Admin'), function() {
	Route::get('admin/exercicios/add', ['uses' => 'AdminExerciciosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Exercicios\Controllers\Admin'), function() {
	Route::get('admin/exercicios/edit/{id}', ['uses' => 'AdminExerciciosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Exercicios\Controllers\Admin'), function() {
	Route::get('admin/exercicios/delete/{id}', ['uses' => 'AdminExerciciosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Exercicios\Controllers\Admin'), function() {
	Route::get('admin/exercicios/importar', ['uses' => 'AdminExerciciosController@importar_form']);
	Route::post('admin/exercicios/importar', ['uses' => 'AdminExerciciosController@importar_save']);
	Route::post('admin/exercicios/save', ['uses' => 'AdminExerciciosController@save']);
	Route::post('admin/exercicios/upload_galeria/{id}', ['uses' => 'AdminExerciciosController@upload_galeria']);
    Route::post('admin/exercicios/delete_thumbnail_principal', ['uses' => 'AdminExerciciosController@delete_thumbnail_principal']);
    Route::post('admin/exercicios/delete_imagem/{id}', ['uses' => 'AdminExerciciosController@delete_imagem']);
});
