<?php

namespace App\Modules\Informacoes\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class InformacoesController extends Controller
{
	private $modulo;
    public function __construct(){
		$this->modulo = \App\Gerador::find(25);
	}

	public function index(){
		$data = array();
		return view('informacoes',$data);
	}
}
