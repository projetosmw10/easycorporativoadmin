<?php

namespace App\Modules\Informacoes\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Informacoes\Models\Informacoes;

class AdminInformacoesController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(25);
	}

	public function index(){
		$data['modulo'] = $this->modulo;
		$data['item'] = Informacoes::find(1);
		if($this->modulo->galeria){
			$data['item']->imagens = Informacoes::getImagens(1);
		}
		return view('Informacoes::admin/informacoes',$data);
	}


	public function save(Request $request){
		try{
            $post = $request->input();
            
            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/informacoes/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }

			Informacoes::first()->update($post);

			\Session::flash('type', 'success');
         \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/informacoes');
		}catch(\Exception $e){
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}


    }
    
    
	public function upload_galeria(Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/informacoes/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Informacoes::criar_imagem(array('id_informacoes' => 1, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
    }
    public function delete_imagem($id){
		try{
			$imagem = Informacoes::getImagem($id);
            
			unlink('uploads/informacoes/'.$imagem->thumbnail_principal);
			Informacoes::deletar_imagem($id);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


	public function delete($id){
		try{
			Informacoes::destroy($id);
			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/informacoes');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}


	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Informacoes::find($post['id']);

			if(is_file(public_path() . $imagem->thumbnail_principal)){
				unlink(public_path() . $imagem->thumbnail_principal);
				Informacoes::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}

}
