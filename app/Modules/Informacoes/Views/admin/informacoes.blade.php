@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($item)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Informações</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/informacoes') }}">Informações</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<li><a class="btn btn-default" data-toggle="modal" data-target="#certificadoModal">Certificado</a></li>
							<?php /*<li><a data-toggle="pill" href="#image2-tab">Imagem Secundária</a></li><?php */ ?>
							<?php if($modulo->galeria){ ?>
								<li><a data-toggle="pill" href="#imagens-tab">Galeria</a></li>
							<?php } ?>
						</ul>
						<div class="spacer"></div>
						<div class="tab-content">
                            <form id="mainForm" class="form-horizontal" role="form" enctype="multipart/form-data" method="POST" action="{{ url('/admin/informacoes/save') }}">
							<div class="text-danger text-center">Ao alterar aqui, estará alterando em todos apps</div>
								
								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
									<div class="form-group">
										<label for="thumbnail_principal" class="col-md-3 control-label">Vídeo</label>
										<div class="col-md-7">
											<?php  if(isset($item) AND ($item->thumbnail_principal != "") ){ ?>
												<div class="thumbnail_principal">
													<a href="<?php echo url($item->thumbnail_principal); ?>">
													<?php echo ($item->thumbnail_principal); ?>
													</a>
													<div class="clear"></div>
													<div class="delete-thumbnail btn btn-danger" data-value="<?php echo $item->id; ?>">deletar</div>
												</div>
												<input type="file" name="thumbnail_principal" style="display: none;">
											<?php } else { ?>
												<input type="file" name="thumbnail_principal">
											<?php } ?>
										</div>
									</div>
                                    <div class="form-group">
                                        <label for="site" class="col-md-3 control-label">Site *</label>
                                        <div class="col-md-7">
                                        <input id="site" required type="url" class="form-control" value="<?php echo (isset($item)) ? $item->site : ""; ?>" name="site" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="facebook" class="col-md-3 control-label">Facebook </label>
                                        <div class="col-md-7">
                                        <input id="facebook"  type="url" class="form-control" value="<?php echo (isset($item)) ? $item->facebook : ""; ?>" name="facebook" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="twitter" class="col-md-3 control-label">Twitter </label>
                                        <div class="col-md-7">
                                        <input id="twitter"  type="url" class="form-control" value="<?php echo (isset($item)) ? $item->twitter : ""; ?>" name="twitter" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="instagram" class="col-md-3 control-label">Instagram </label>
                                        <div class="col-md-7">
                                        <input id="instagram"  type="url" class="form-control" value="<?php echo (isset($item)) ? $item->instagram : ""; ?>" name="instagram" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="youtube" class="col-md-3 control-label">Youtube </label>
                                        <div class="col-md-7">
                                        <input id="youtube"  type="url" class="form-control" value="<?php echo (isset($item)) ? $item->youtube : ""; ?>" name="youtube" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="contato_emails" class="col-md-3 control-label">E-mail </label>
                                        <div class="col-md-7">
                                        <input id="contato_emails"  type="email" class="form-control" value="<?php echo (isset($item)) ? $item->contato_emails : ""; ?>" name="contato_emails" />
										<small>E-mail que receberá os contatos vindo do aplicativo</small>
                                    </div>
             </div>
			 
			 <!-- nao tem no app empresarial -->
			 <div class="hidden">
			 <div class="form-group text-center">
			 <hr>
			 <b>Tela de Código</b>
			 </div>
                                    <div class="form-group">
                                        <label for="codigo_necessario" class="col-md-3 control-label"> Necessário código de acesso *</label>
                                        <div class="col-md-7">
                                        <input id="codigo_necessario" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->codigo_necessario : ""; ?>" name="codigo_necessario" />
                                    </div>
             </div>
                                    <div class="form-group">
                                        <label for="codigo_titulo" class="col-md-3 control-label"> Título *</label>
                                        <div class="col-md-7">
                                        <input id="codigo_titulo" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->codigo_titulo : ""; ?>" name="codigo_titulo" />
                                    </div>
             </div>
			 
                                    <div class="form-group">
                                        <label for="codigo_descricao" class="col-md-3 control-label"> Descrição </label>
                                        <div class="col-md-7">
                                        <textarea id="codigo_descricao"  class="form-control" name="codigo_descricao"><?php echo (isset($item)) ? $item->codigo_descricao : ""; ?></textarea>
                                    </div>
             </div>

			 
			 </div>
								</div>
								<div class="form-group text-center">
			 <hr>
			 <b>Termos de Uso</b>
			 </div>
                                    <div class="form-group">
                                        <label for="termos_de_uso" class="col-md-3 control-label"> Link *</label>
                                        <div class="col-md-7">
                                        <input id="termos_de_uso" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->termos_de_uso : ""; ?>" name="termos_de_uso" />
                                    </div>
             </div>
							</form>
                        </div>

							<?php if($modulo->galeria){ ?>


								<div id="imagens-tab" class="tab-pane fade">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 lista-galeria">
										<?php if(count($item->imagens)){?>
											<?php foreach ($item->imagens as $image){?>
												<div id="item_<?php echo $image->id; ?>" class="item imagem-galeria-<?php echo $image->id; ?>">
													<div style="background-image: url(<?php echo "/uploads/informacoes/$image->thumbnail_principal";?>);" class="thumb"></div>
													<span data="<?php echo $image->id; ?>" data-modulo="informacoes" class="icon delete-image" aria-hidden="true"><i class="fa fa-trash"></i></span>
												</div>
											<?php }?>
										<?php }?>
									</div>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<form class="dropzone" id="galeria-dropzone" method="POST" action="<?php echo url('/admin/informacoes/upload_galeria'); ?> " enctype="multipart/form-data">
											<input type="hidden" name="_token" value="{{ csrf_token() }}" />
											<div class="fallback">
												<input name="file" type="file" multiple />
											</div>
										<form>
									</div>
								</div>

							<?php } ?>
						</div>
					</div>
					<!-- /.box-body -->
					<?php if($current_role->hasAccess($current_module->nome_tabela.'.update')){ ?>
						<div class="box-footer">
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						</div>
					<?php } ?>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>


<div class="modal fade" id="certificadoModal" tabindex="-1" role="dialog" aria-labelledby="certificadoModalLabel">
	<form enctype="multipart/form-data" method="POST" action="{{ url('/admin/informacoes/save') }}" class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="certificadoModalLabel">Textos certificados</h4>
		</div>
		<div class="modal-body">
			{{ csrf_field() }}


				<div class="text-center text-bold">Textos ao decorrer do app relacionado a certificados</div>
				<div class="form-group">
					<label>Parabéns</label>
					<textarea name="certificado[parabens]" required class="form-control" id="" rows="4"><?=$item->certificado['parabens'] ?? ''?></textarea>
				</div>		
				<div class="form-group">
					<label>Parabéns botão</label>
					<input required type="text" class="form-control" value="<?=$item->certificado['parabens_btn'] ?? ''?>" name="certificado[parabens_btn]" />
				</div>
				<hr>
				<div class="form-group">
					<label>Ao concluir pagamento</label>
					<textarea name="certificado[concluir_pagamento]" required class="form-control" id="" rows="4"><?=$item->certificado['concluir_pagamento'] ?? ''?></textarea>
				</div>
				<hr>
				<div class="form-group">
					<label>Título: Dados do certificado</label>
					<input required type="text" class="form-control" value="<?=$item->certificado['titulo_dados_do_certificado'] ?? ''?>" name="certificado[titulo_dados_do_certificado]" />
				</div>
				<hr>
				<div class="form-group">
					<label>Campo: Nome no certificado</label>
					<input required type="text" class="form-control" value="<?=$item->certificado['label_dados_do_certificado'] ?? ''?>" name="certificado[label_dados_do_certificado]" />
				</div>
				<div class="form-group">
					<label>Auxilio: Nome no certificado (Digite seu nome)</label>
					<input required type="text" class="form-control" value="<?=$item->certificado['placeholder_nome_no_certificado'] ?? ''?>" name="certificado[placeholder_nome_no_certificado]" />
				</div>
				<hr>
				<div class="form-group">
					<label>Campo: Enviar certificado para</label>
					<input required type="text" class="form-control" value="<?=$item->certificado['campo_enviar_certificado_para'] ?? ''?>" name="certificado[campo_enviar_certificado_para]" />
				</div>
				<hr>
				<div class="form-group">
					<label>Títutlo: Dados do cartão</label>
					<input required type="text" class="form-control" value="<?=$item->certificado['titulo_dados_do_cartao'] ?? ''?>" name="certificado[titulo_dados_do_cartao]" />
				</div>
				<hr>




		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			<button type="submit" class="btn btn-primary">Salvar</button>
		</div>
		</div>
	</form>
</div>

<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/informacoes/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

</script>
@endsection
