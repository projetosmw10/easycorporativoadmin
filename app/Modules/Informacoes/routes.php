<?php

// ================================
// Informações routes BEGGINING
// ================================
Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Informacoes\Controllers\Admin'), function() {
	Route::get('admin/informacoes', ['uses' => 'AdminInformacoesController@index']);
});

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Informacoes\Controllers'), function() {
	Route::get('informacoes', ['uses' => 'InformacoesController@index']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Informacoes\Controllers\Admin'), function() {
	Route::get('admin/informacoes', ['uses' => 'AdminInformacoesController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create,update'], 'namespace' => 'App\Modules\Informacoes\Controllers\Admin'), function() {
	Route::post('admin/informacoes/save', ['uses' => 'AdminInformacoesController@save']);
	Route::post('admin/informacoes/upload_galeria', ['uses' => 'AdminInformacoesController@upload_galeria']);
	Route::post('admin/informacoes/delete_thumbnail_principal', ['uses' => 'AdminInformacoesController@delete_thumbnail_principal']);
    Route::post('admin/informacoes/delete_imagem/{id}', ['uses' => 'AdminInformacoesController@delete_imagem']);
});
