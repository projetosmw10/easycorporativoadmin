<?php

namespace App\Modules\Informacoes\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Informacoes extends Model
{
    protected $table = 'informacoes';
    protected $fillable = array('site', 'facebook', 'twitter', 'instagram', 'youtube', 'codigo_titulo', 'codigo_descricao', 'thumbnail_principal', 'codigo_necessario', 'termos_de_uso', 'contato_emails', 'c1', 'certificado' );

	public static function getImagens($id){
		return DB::table('informacoes_imagens')->where('id_informacoes', $id)->get();
	}
	public static function criar_imagem($input){
		return DB::table('informacoes_imagens')->insert([
			[
				'id_informacoes' => $input['id_informacoes'],
				'thumbnail_principal' => $input['thumbnail_principal'],
			]
		]);
    }
    public static function getImagem($id){
		return DB::table('informacoes_imagens')->find($id);
	}
    public static function deletar_imagem($id){
		return DB::table('informacoes_imagens')
				->where('id', $id)
				->delete();
	}
	public static function editar_imagem($input, $id){
		return DB::table('informacoes_imagens')->where('id', $id)
		->update([
			'id_informacoes' => $input['id_informacoes'],
			'thumbnail_principal' => $input['thumbnail_principal'],
		]);;
	}

	public function setCertificadoAttribute($val) {
		$this->attributes['certificado'] = json_encode($val);
	}
	
	public function getCertificadoAttribute($val) {
		$certificado = json_decode($val, true);
		return $certificado;
	}

}
