<?php

namespace App\Modules\Topicos\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TopicosController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(23);
	}

	public function index(){
		$data = array();
		return view('topicos',$data);
	}
}
