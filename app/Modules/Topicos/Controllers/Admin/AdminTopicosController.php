<?php

namespace App\Modules\Topicos\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Topicos\Models\Topicos;
use App\Modules\Categorias\Models\Categorias;

class AdminTopicosController extends Controller {
    private $modulo;
    private $lastInsertId;

    public function __construct() {
        $this->middleware('auth');

        $this->modulo = \App\Gerador::find(23);
    }

    public function index(Request $request) {
        $data['categoria'] = Categorias::find($request->query('categoria_id'));
        $data['topico_pai'] = Topicos::find($request->query('topico_id'));
        if($request->query('topico_id')) {
            $data['itens'] = $data['topico_pai']->topicos()->orderBy('ordem')->get();
        } else {
            $data['itens'] = $data['categoria']->topicos()->orderBy('ordem')->get();
        }
        return view('Topicos::admin/topicos', $data);
    }

    public function add(Request $request) {
        $data = [];
        $data['modulo'] = $this->modulo;
        $data['categoria'] = Categorias::find($request->query('categoria_id'));
        $data['topico_pai'] = Topicos::find($request->query('topico_id'));
        return view('Topicos::admin/form-topicos', $data);
    }

    public function edit(Request $request, $id) {
        $data['modulo'] = $this->modulo;
        $data['item'] = Topicos::find($id);
        $data['categoria'] = $data['item']->categoria()->first();
        $data['topico_pai'] = Topicos::find($request->query('topico_id'));
        return view('Topicos::admin/form-topicos', $data);
    }

    public function save(Request $request) {
        try {
            $post = $request->input();
            $post['topico_id'] = empty($post['topico_id']) ? null : $post['topico_id'];
            $post['descricao'] = empty($post['descricao']) ? '' : '';
            
            if ($request->hasFile('thumbnail_principal')) {
                $file = $request->file('thumbnail_principal');

                $tmpFilePath = '/uploads/topicos/';
                if (!is_dir($tmpFilePath)) {
                    @mkdir($tmpFilePath, 0777, true);
                }

                $tmpFileName = md5(uniqid(rand(), true)) . '.' . $file->extension();
                $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
                $path = $tmpFilePath . $tmpFileName;

                $post['thumbnail_principal'] = $path;
            }

            if ($request->input('id')) {
                $item = Topicos::find($request->input('id'));
                $item->update($post);
            } else {
                $post['ordem'] = Topicos::select('ordem')
                                        ->where('categoria_id', $request->query('categoria_id'))
                                        ->orderBy('ordem', 'desc')
                                        ->first();
                $post['ordem'] = $post['ordem'] ? $post['ordem']->ordem : '1';

                $item = Topicos::create($post);
            }

            if (isset($post['conteudo_id']) && is_array($post['conteudo_id'])) {
                $conteudos = [];
                foreach ($post['conteudo_id'] as $key => $c) {
                    $conteudos[$c] = ['ordem' => $key];
                }
                $item->conteudos()->sync($conteudos);
            }

            \Session::flash('type', 'success');
            \Session::flash('message', 'Alteracoes salvas com sucesso!');
            return redirect('admin/topicos?categoria_id=' . $request->query('categoria_id').'&topico_id='.$request->query('topico_id'));
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
        }
    }

    public function delete(Request $request, $id) {
        try {
            Topicos::destroy($id);

            \Session::flash('type', 'success');
            \Session::flash('message', 'Registro removido com sucesso!');
            return redirect('admin/topicos?categoria_id=' . $request->query('categoria_id').'&topico_id='.$request->query('topico_id'));
        } catch (Exception $e) {
            \Session::flash('type', 'error');
            \Session::flash('message', 'Nao foi possivel remover o registro!');
            return redirect()->back();
        }
    }

    public function delete_thumbnail_principal(Request $request) {
        $post = $request->input();
        try {
            $imagem = Topicos::find($post['id']);
            if (is_file(public_path() . $imagem->thumbnail_principal)) {
                unlink(public_path() . $imagem->thumbnail_principal);
                Topicos::where('id', $post['id'])->update(['thumbnail_principal' => '']);
            }
            return response()->json(['status' => true, 'message' => 'Registro removido com sucesso!']);
        } catch (Exception $e) {
            return response()->json(['status' => false, 'message' => $e->getMessage()]);
        }
    }
}
