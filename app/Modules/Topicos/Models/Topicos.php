<?php

namespace App\Modules\Topicos\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Topicos extends Model {
    protected $table = 'topicos';
    protected $fillable = ['categoria_id', 'topico_id', 'titulo', 'descricao', 'thumbnail_principal', 'ordem', 'gratuito'];

    public function categoria() {
        return $this->belongsTo('App\Modules\Categorias\Models\Categorias', 'categoria_id');
    }
    public function topico() {
        return $this->belongsTo('App\Modules\Topicos\Models\Topicos', 'topico_id');
    }
    public function topicos() {
        return $this->hasMany('App\Modules\Topicos\Models\Topicos', 'topico_id')->orderBy('ordem');
    }

    public function conteudos() {
        return $this->hasMany('App\Modules\Conteudos\Models\Conteudos', 'topico_id');
    }

    public function getTituloCompletoAttribute($val = '') {
        $this->loadMissing(['categoria', 'topico']);
        $categoria = $this->categoria->titulo ?? '';
        $categoria = $categoria ? $categoria . ' -> ' : '';
        $topico = $this->topico->titulo ?? '';
        $topico = $topico ? $topico .' -> ' : '';
        return $categoria.$topico.$this->titulo;
    }
}
