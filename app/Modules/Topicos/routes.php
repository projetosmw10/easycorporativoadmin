<?php

// ================================
// Tópicos routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Topicos\Controllers'), function() {
	Route::get('topicos', ['uses' => 'TopicosController@index']);
	Route::get('topicos/detalhe/{slug}', ['uses' => 'TopicosController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Topicos\Controllers\Admin'), function() {
	Route::get('admin/topicos', ['uses' => 'AdminTopicosController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Topicos\Controllers\Admin'), function() {
	Route::get('admin/topicos/add', ['uses' => 'AdminTopicosController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Topicos\Controllers\Admin'), function() {
	Route::get('admin/topicos/edit/{id}', ['uses' => 'AdminTopicosController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Topicos\Controllers\Admin'), function() {
	Route::get('admin/topicos/delete/{id}', ['uses' => 'AdminTopicosController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Topicos\Controllers\Admin'), function() {
	Route::post('admin/topicos/save', ['uses' => 'AdminTopicosController@save']);
    Route::post('admin/topicos/delete_thumbnail_principal', ['uses' => 'AdminTopicosController@delete_thumbnail_principal']);
});
