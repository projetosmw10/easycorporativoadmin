@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
            <?=$categoria->titulo?><br>
			<?=$topico_pai ? $topico_pai->titulo.'<br> ' : ''?>
			Tópicos
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/categorias/edit/'.app('request')->query('categoria_id')) }}"> <?=$categoria->titulo?></a></li>
			<li><a href="{{ url('/admin/topicos?categoria_id='.app('request')->query('categoria_id')) }}">Tópicos</a></li>
			<li class="active"><?php echo (isset($item)) ? 'Editar' : 'Criar'; ?></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('/admin/topicos/save?categoria_id='.app('request')->query('categoria_id').'&topico_id='.app('request')->query('topico_id')) }}">
						<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}
									<?php if(isset($item)){ ?>
										<input type="hidden" name="id" value="<?php echo $item->id; ?>"/>
									<?php } ?>
									<input type="hidden" name="categoria_id" value="<?=isset($item) ? $item->categoria_id : app('request')->query('categoria_id')?>">
                                    <div class="form-group">
                                        <label for="titulo" class="col-md-3 control-label">Título *</label>
                                        <div class="col-md-7">
                                        <input id="titulo" required type="text" class="form-control" value="<?php echo (isset($item)) ? $item->titulo : ""; ?>" name="titulo" />
                              	      </div>
           						    </div>
                                    <div class="form-group hidden">
                                        <label for="descricao" class="col-md-3 control-label">Descrição </label>
                                        <div class="col-md-7">
                                        <textarea id="descricao"  class="form-control tinymce" name="descricao"><?php echo (isset($item)) ? $item->descricao : ""; ?></textarea>
										</div>
									</div>

									
                                    
                                    <div class="form-group hidden">
                                        <label for="gratuito" class="col-md-3 control-label">Gratuito *</label>
                                        <div class="col-md-7">
                                        <select id="gratuito" required class="form-control" name="gratuito">
                                            <option <?php echo (isset($item) && $item->gratuito == 0) ? "selected" : ""; ?> value="0">Não</option>
                                            <option <?php echo (isset($item) && $item->gratuito == 1) ? "selected" : ""; ?> value="1">Sim</option>
                                        </select>
                                        </div>
                                    </div>

								</div>

							</form>

						</div>
					</div>
					<!-- /.box-body -->
					<div class="box-footer">
						<?php if(isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.update') || !isset($item) && $current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-btn fa-pencil"></i> Salvar
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
					<!-- /.box -->
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.delete-thumbnail').click(function(){
    var r = confirm("Você tem certeza que deseja deletar a imagem?");
    if (r == true) {

        var _self = this;
        $("#icon-loading").fadeIn();

        $.ajax({
            url: "/admin/topicos/delete_thumbnail_principal",
            dataType: 'json',
            type: 'POST',
            data: {
                    'id': $(this).data('value'),
                    '_token': $('[name="_token"]').val()
            },
            success: function(response) {

                if(response.status){
                    alertUtil.alertSuccess('Arquivo', 'Arquivo foi deletado com sucesso!');
                    $('.thumbnail_principal').hide();

                    $('input[name="thumbnail_principal"]').show();

                } else {
                    alertUtil.alertSuccess('Arquivo', 'Não foi possivel deletar o arquivo!');
                }
                $("#icon-loading").hide();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                    $("#icon-loading").hide();
                    alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }

});

$('input[name="busca_conteudo"]').autocomplete({
    delay: 500,
    source: function(request, response) {
        $.ajax({
            url: 'api/admin/conteudos',
            method: 'GET',
            dataType: 'json',
            data: {
                    'search': request.term,
                    'orderBy': 'titulo'
            },
            success: function(json) {
                response($.map(json, function(item) {
                    return {
                        label: item.titulo + ' (' + item.tipo + ')',
                        value: item.id,
                        link: item.tipo == 'pdf' ? item.thumbnail_principal : item.vimeo
                    }
                }));
            }
        });
    },
    select: function(event, ui) {
        
        $('#conteudo_topico' + ui.item.value).remove();

        $('#conteudo_topico').append('<li class="list-group-item id="conteudo_topico' + ui.item.value + '"><i class="fa fa-bars" style="cursor:move"></i>&ensp;&ensp;<img src="img/delete.png" alt="" />&ensp;<a href="'+ui.item.link+'" target="_blank">' + ui.item.label + '</a><input type="hidden" name="conteudo_id[]" value="' + ui.item.value + '" /></li>');

        $('input[name="busca_conteudo"]').val('');
        return false;
    },
    focus: function(event, ui) {
            return false;
        }
});

$(document).on('click', '#conteudo_topico img', function(){
    $(this).parent().remove();

    $('#conteudo_topico div:odd').attr('class', 'odd');
    $('#conteudo_topico div:even').attr('class', 'even');
});

$( ".sortable" ).sortable();
</script>
@endsection
