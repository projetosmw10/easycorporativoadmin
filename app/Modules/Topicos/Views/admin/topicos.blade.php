@extends('layouts.app')

@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?=$categoria->titulo?><br>
			<?=$topico_pai ? $topico_pai->titulo.'<br> ' : ''?>
			Tópicos
		</h1>
		<ol class="breadcrumb">
			<li>
			@if($topico_pai)
			<a style="font-size:16px;" href="{{ url('/admin/topicos?categoria_id=' . $categoria->id) }}">&lsaquo; Voltar</a>
			@else
			<a style="font-size:16px;" href="{{ url('/admin/categorias') }}">&lsaquo; Voltar</a>
			@endif
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">

				<div class="box">
					<div class="box-header">
						<?php if($current_role->hasAccess($current_module->nome_tabela.'.create')){ ?>
							<a href="{{ url('admin/topicos/add?categoria_id='.app('request')->query('categoria_id').'&topico_id='.app('request')->query('topico_id')) }}" class="table-add"><i class="fa fa-plus"></i> Adicionar</a>
						<?php } ?>
						<hr>
					</div>
					<!-- /.box-header -->
					<div class="box-body">

						<table class="table table-bordered table-striped">

                            <thead>
                                 <tr>
								 	<th></th>
                                     <!-- <th>#</th> -->
                                     <th>Título</th>
                                     <th width="50">Conteúdos</th>
									 <?php
									if(!app('request')->query('topico_id')){
									?>
									 <th>Tópicos</th>
									 <?php
									}
									 ?>
                                     <th width="170">Ação</th>
                                 </tr>
                            </thead>
                            <tbody class="sortable">
                            <?php foreach ($itens as $item){
								if(app('request')->query('topico_id') != $item->topico_id) {
									continue;
								}
								?>
									<tr data-id="<?=$item->id?>">
											<td width="10" style="cursor: move;vertical-align:middle"><i class="fa fa-bars"></i></td>
                                            <!-- <td width="25"> <?php echo $item->ordem?> </td> -->
                                            <td width="1000"> <?php echo $item->titulo?> </td>
											<td class="text-center" width="50"> <a href="/admin/conteudos?topico_id=<?=$item->id?>" class="btn btn-primary"><i class="fa fa-square"></i></a> </td>
											<?php
											if(!app('request')->query('topico_id')){
											?>
											<td width="50"> <a href="/admin/topicos?categoria_id=<?=$item->categoria_id?>&topico_id=<?=$item->id?>" class="btn btn-primary"><i class="fa fa-list"></i></a> </td>
											<?php
											}
											?>
                                            <td width="100">
											<?php if($current_role->hasAccess($current_module->nome_tabela.".update")){ ?>
												<a href="/admin/topicos/edit/<?php echo $item->id; ?>?categoria_id=<?=app('request')->query('categoria_id')?>&topico_id=<?=app('request')->query('topico_id')?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
											<?php } ?>
											<?php if($current_role->hasAccess($current_module->nome_tabela.".delete")){ ?>
												<a href="/admin/topicos/delete/<?php echo $item->id; ?>?categoria_id=<?=app('request')->query('categoria_id')?>&topico_id=<?=app('request')->query('topico_id')?>" class="btn btn-danger deletar"><i class="fa fa-trash"></i></a>
											<?php } ?>
										</td>
									</tr>
								<?php } ?>
							</tbody>

						</table>

					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
$( ".sortable" ).sortable({
	update: function(event, ui) {
		var body = new Object();
		body.topicos = new Object();
		$('.sortable tr').each(function (index) {
			body.topicos[$(this).attr('data-id')] = {ordem: index + 1}; 
		});
		$.post('/api/admin/categorias/<?=$categoria->id?>/topicos/ordem', $.param(body))
	}
});
</script>
@endsection
