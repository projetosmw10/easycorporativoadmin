<?php

// ================================
// Notificações routes BEGGINING
// ================================

Route::group(array('middleware' => 'web', 'namespace' => 'App\Modules\Notifications\Controllers'), function() {
	Route::get('notifications', ['uses' => 'notificationsController@index']);
	Route::get('notifications/detalhe/{slug}', ['uses' => 'notificationsController@detalhe']);
});

Route::group(array('middleware' => ['auth', 'role:view'], 'namespace' => 'App\Modules\Notifications\Controllers\Admin'), function() {
	Route::get('admin/notifications', ['uses' => 'AdminNotificationsController@index']);
});

Route::group(array('middleware' => ['auth', 'role:create'], 'namespace' => 'App\Modules\Notifications\Controllers\Admin'), function() {
	Route::get('admin/notifications/add', ['uses' => 'AdminNotificationsController@add']);
});

Route::group(array('middleware' => ['auth', 'role:update'], 'namespace' => 'App\Modules\Notifications\Controllers\Admin'), function() {
	Route::get('admin/notifications/edit/{id}', ['uses' => 'AdminNotificationsController@edit']);
});

Route::group(array('middleware' => ['auth', 'role:delete'], 'namespace' => 'App\Modules\Notifications\Controllers\Admin'), function() {
	Route::get('admin/notifications/delete/{id}', ['uses' => 'AdminNotificationsController@delete']);
});

Route::group(array('middleware' => ['auth', 'role:update,create'], 'namespace' => 'App\Modules\Notifications\Controllers\Admin'), function() {
	Route::post('admin/notifications/save', ['uses' => 'AdminNotificationsController@save']);
	Route::post('admin/notifications/upload_galeria/{id}', ['uses' => 'AdminNotificationsController@upload_galeria']);
    Route::post('admin/notifications/delete_thumbnail_principal', ['uses' => 'AdminNotificationsController@delete_thumbnail_principal']);
    Route::post('admin/notifications/delete_imagem/{id}', ['uses' => 'AdminNotificationsController@delete_imagem']);
});
