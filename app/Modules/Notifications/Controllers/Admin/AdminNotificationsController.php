<?php

namespace App\Modules\Notifications\Controllers\Admin;

use Illuminate\Http\Request;
use Mail;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Notifications\Models\Notifications;
use App\Services\NotificationService;
use App\User;

class AdminNotificationsController extends Controller
{
	private $modulo;
	private $lastInsertId;
	/**
     * @var \App\Services\NotificationService
     */
    private $_notification;

    public function __construct(NotificationService $notificationService) {
        $this->_notification = $notificationService;
		$this->middleware('auth');
		$this->modulo = \App\Gerador::find(27);
	}

	public function index(){
		$data['itens'] = Notifications::all();
		return view('Notifications::admin/notifications',$data);
	}

	public function add(){
		$data = array();
		$data['modulo'] = $this->modulo;
		
		$data['users'] = \App\User::orderBy('first_name')->get();

		return view('Notifications::admin/form-notifications', $data);
	}

	public function edit($id){
		$data['modulo'] = $this->modulo;
		
		$data['users'] = \App\User::orderBy('first_name')->get();

		$data['item'] = Notifications::find($id);
		return view('Notifications::admin/form-notifications',$data);
	}



	public function save(Request $request){
		try{
			$post = $request->input();

			$post['meta_keywords'] = (isset($post['meta_keywords'])) ? implode(',',$post['meta_keywords']) : null;

            if($request->hasFile('thumbnail_principal')) {

				$file = $request->file('thumbnail_principal');

				$tmpFilePath = '/uploads/notifications/';
				if(!is_dir($tmpFilePath)){
	  				@mkdir($tmpFilePath,0777,TRUE);
	  			}

				$tmpFileName = md5(uniqid(rand(), true)).'.'.$file->extension();
				$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
				$path = $tmpFilePath . $tmpFileName;

				$post['thumbnail_principal'] = $path;
            }
            
			if($request->input('id')){
				Notifications::find($request->input('id'))->update($post);
			}else{
				if(!isset($post['users'])) {
					$post['users'] = User::all()->toArray();
					$post['users'] = array_column($post['users'], 'id');
				}
				
				$this->_notification->sendToUsersAndInsert(['notifications' => 'default'], $post['users'], $post['titulo'], $post['descricao']);

				$last = Notifications::orderBy('id', 'desc')->first();
				
				foreach ($post['users'] as $u) {
					DB::table('notification_user')->insert(
					    ['notification_id' => $last->id, 'user_id' => $u]
					);
				}
			}
			\Session::flash('type', 'success');
            \Session::flash('message', "Alteracoes salvas com sucesso!");
			return redirect('admin/notifications');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', $e->getMessage());
            return redirect()->back();
		}

    }
    
	public function upload_galeria($id, Request $request) {
		if($request->hasFile('file')) {
			//upload an image to the /img/tmp directory and return the filepath.
			$file = $request->file('file');
			$tmpFilePath = '/uploads/notifications/';
			$tmpFileName = time() . '-' . $file->getClientOriginalName();
			$file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
			$path = $tmpFilePath . $tmpFileName;

			Notifications::criar_imagem(array('id_notifications' => $id, 'thumbnail_principal' => $tmpFileName));

			return response()->json(array('path'=> $path, 'file_name'=>$tmpFileName), 200);
		} else {
			return response()->json(false, 200);
		}
	}

    public function delete($id){
		try{
			Notifications::destroy($id);

			\Session::flash('type', 'success');
            \Session::flash('message', "Registro removido com sucesso!");
			return redirect('admin/notifications');
		}catch(\Exception $e){
			\Session::flash('type', 'error');
            \Session::flash('message', "Nao foi possivel remover o registro!");
            return redirect()->back();
		}
	}
	public function delete_imagem($id){
		try{
			$imagem = Notifications::getImagem($id);
			Notifications::deletar_imagem($id);

			unlink('uploads/notifications/'.$imagem->thumbnail_principal);

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));
		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}


	}

	public function delete_thumbnail_principal(Request $request){

		$post = $request->input();

		try{

			$imagem = Notifications::find($post['id']);

			if(is_file(public_path() . $imagem->thumbnail_principal)){
				unlink(public_path() . $imagem->thumbnail_principal);
				Notifications::where('id', $post['id'])->update(array("thumbnail_principal" => ''));
			}

			return response()->json(array('status' => true, 'message' => 'Registro removido com sucesso!'));

		}catch(\Exception $e){
			return response()->json(array('status' => false, 'message' => $e->getMessage()));
		}
	}


}
