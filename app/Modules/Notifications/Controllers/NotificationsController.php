<?php

namespace App\Modules\Notifications\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class notificationsController extends Controller
{
	private $modulo;

    public function __construct(){
		$this->modulo = \App\Gerador::find(27);
	}

	public function index(){
		$data = array();
		return view('notifications',$data);
	}
}
