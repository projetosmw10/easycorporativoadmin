<?php

namespace App\Modules\Notifications\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Notifications extends Model {
    protected $table = 'notifications';
    protected $fillable = ['params', 'titulo', 'descricao', 'thumbnail_principal', 'meta_keywords', 'meta_descricao', 'slug', ];

    public function users() {
        return $this->belongsToMany('\App\User', 'notification_user', 'notification_id', 'user_id');
    }
}
