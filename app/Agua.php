<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Storage;

class Agua extends Model {
    protected $table = 'agua';
    protected $fillable = ['user_id', 'data', 'quantidade', 'consumido'];

    public function users() {
        return $this->belongsTo('\App\User', 'user_id');
    }
}