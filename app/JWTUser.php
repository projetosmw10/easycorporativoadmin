<?php

namespace App;
use Firebase\JWT\JWT;
use App\User;

class JWTUser {
    protected static $user = null;
    private static $isPremium = null;

    private static $knownRoles = [];
    private static $knownPermissions = [];

    public static function inRole($role) {
        if (is_object($role)) {
            if (!isset($role->slug)) {
                throw new \UnexpectedValueException('Could not find the attribute "slug" on the object');
            }
            $role = $role->slug;
        } elseif (is_array($role)) {
            if (!isset($role['slug'])) {
                throw new \UnexpectedValueException('Could not find the attribute "slug" on the specified array');
            }
            $role = $role['slug'];
        }

        if (in_array($role, self::$knownRoles)) {
            return true;
        }

        foreach (self::$user->roles as $r) {
            if ($r->slug == $role) {
                self::$knownRoles[] = $role;
                return true;
            }
        }
        return false;
    }

    public static function inAnyRole(array $roles) {
        foreach ($roles as $role) {
            if (self::inRole($role)) {
                return true;
            }
        }
    }

    /**
     * Checks if the user has all acces that was passed on the parameter
     * usage:
     * array('blog.view', 'blog.delete')
     * will return true only if the user has both permissions
     */
    public static function hasAccess(array $check) {
        $checkCount = count($check);
        $hasAccessCount = 0;
        foreach ($check as $c) {
            if (isset(self::$knownPermissions[$c])) {
                $hasAccessCount++;
            }
        }
        // Se a contagem é maior ou igual a quantidade de checks então ele tem permissões suficicntes
        if ($hasAccessCount >= $checkCount) {
            return true;
        }

        foreach ($check as $c) {
            if (isset(self::$user->permissions->$c) && self::$user->permissions->$c) {
                self::$knownPermissions[$c] = true;
                $hasAccessCount++;
            }
        }
        if ($hasAccessCount >= $checkCount) {
            return true;
        }

        // check the permissions on the roles that the user has
        foreach (self::$user->roles as $r) {
            foreach ($check as $c) {
                if (isset($r->permissions->$c) && $r->permissions->$c) {
                    self::$knownPermissions[$c] = true;
                    $hasAccessCount++;
                }
            }
            if ($hasAccessCount >= $checkCount) {
                return true;
            }
        }
        return $hasAccessCount >= $checkCount;
    }

    /**
     * Checks if the user has any access that was passed on the parameter
     * usage:
     * array('blog.view', 'blog.delete')
     * will return true if the user has any of the permissions
     */
    public static function hasAnyAccess(array $check) {
        foreach ($check as $c) {
            if (self::hasAccess([$c])) {
                return true;
            }
        }
        return false;
    }

    public static function getRoles() {
        return self::$user->roles;
    }

    public static function getUser() {
        return self::$user;
    }

    public static function setUser($user) {
        if (!User::where('id', $user->id)->exists()) {
            throw new \Exception('User was deleted or doesn\'t exist');
        }
        self::$user = $user;
    }

    /** CUSTOM PARA APP DA MENTE */
    public static function isPremium() {
        
        if(self::$isPremium !== null){
            return self::$isPremium;
        }

        if (!self::$user) {
            $token = app('request')->header('Authorization');
            if(empty($token)){
                $token = app('request')->server('HTTP_AUTHORIZATION');
            }
            try{
                JWTUser::setUser(JWT::decode(str_replace('Bearer ', '', $token), env('JWT_SECRET'), array('HS256'))->user);
            } catch (\Exception $e) {
                return false;
            }
        }

        $user = User::find(self::$user->id);

        return self::$isPremium = $user->isPremium();
    }


    public static function generateToken($user) {
        $expireTimestamp = time() + (604800 * 104);

        $token = [
            'iat' => time(),
            'exp' => $expireTimestamp, // 604800 - 1 semana
            'user' => $user->toArray()
        ];
        
        $token = JWT::encode($token, env('JWT_SECRET'));
        // coloca na black list todos tokens antigos daquele usuario, lembrando que nao podemos deletar os tokens na blacklist, caso contrario 
        $user->jwtTokens()->update(['blacklist' => 1]);

        // Cria um novo token no banco
        $user->jwtTokens()->create([
            'token' => $token,
            'blacklist' => 0, // sem estar na black list
            'expire_at' => date('Y-m-d H:i:s', $expireTimestamp)
        ]);
    
        // deleta os tokens que ja estao    expirados
        JWTTokens::expired()->delete();

        return $token;
    }
}
