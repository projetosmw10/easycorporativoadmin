webpackJsonp([0],{

/***/ 11:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jwt_decode__ = __webpack_require__(481);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jwt_decode___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_jwt_decode__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthProvider = /** @class */ (function () {
    function AuthProvider(http, app, global) {
        this.http = http;
        this.app = app;
        this.global = global;
        this.knownRoles = new Array();
        this.knownPermissions = new Array();
        this.player_id = '';
        this.previousUrl = '';
    }
    AuthProvider.prototype.isPremium = function () {
        // console.log(this.user);
        if (!this.isLoggedIn()) {
            return false;
        }
        if (!this.user.user.pessoa) {
            return false;
        }
        if (!this.user.user.pessoa.code) {
            return false;
        }
        if (!__WEBPACK_IMPORTED_MODULE_5_moment__(this.user.user.pessoa.code.expire_at, 'DD/MM/YYYY').isAfter(__WEBPACK_IMPORTED_MODULE_5_moment__())) {
            return false;
        }
        return true;
    };
    AuthProvider.prototype.isPremiumAsync = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.get(_this.global.apiUrl + '/users/' + _this.user.user.id + '/premium', {
                headers: _this.getAuthorizationHeader()
            }).subscribe(function (r) {
                if (r.premium) {
                    resolve(r);
                }
                else {
                    reject(r);
                }
            }, function (e) { return reject(e); });
        });
    };
    AuthProvider.prototype.login = function (user) {
        return this.http.post(this.global.apiUrl + '/login', user);
    };
    AuthProvider.prototype.logout = function () {
        window.localStorage.removeItem('token');
        this.token = undefined;
        this.user = undefined;
        this.knownPermissions = new Array();
        this.knownRoles = new Array();
        if (this.player_id.length) {
            this.http.delete(this.global.apiUrl + '/users/onesignal/' + this.player_id).subscribe();
        }
    };
    AuthProvider.prototype.setToken = function (t) {
        if (!t) {
            return;
        }
        var tks = t.split('.');
        if (tks.length !== 3) {
            console.error('The specified token does not contain the 3 parts.');
            window.localStorage.removeItem('token');
            return;
        }
        this.token = t;
        this.user = __WEBPACK_IMPORTED_MODULE_2_jwt_decode___default()(this.token);
        if (Array.isArray(this.user.user.permissions)) {
            this.user.user.permissions = {};
        }
        window.localStorage.setItem('token', JSON.stringify(this.token));
    };
    /**
     * Verificar se determinado usuário está no role ou não, caso não informado um user será utilizado o que atualmente está logado
     * @param role slug do role
     * @param user (opcional) Objeto de usuário contendo os seus roles
     */
    AuthProvider.prototype.inRole = function (role, user) {
        var _this = this;
        if (!this.user) {
            return false;
        }
        role = role.trim();
        var currentUser;
        var addToKnownRoles = false;
        if (user) {
            currentUser = user;
        }
        else {
            currentUser = this.user.user;
            addToKnownRoles = true;
            if (this.knownRoles.indexOf(role) !== -1) {
                return true;
            }
        }
        if (!currentUser.roles) {
            console.error('No roles found in the current user');
            return false;
        }
        // !! faz com que o valor venha em boolean
        return !!currentUser.roles.find(function (r) {
            if (r.slug === role) {
                if (addToKnownRoles) {
                    _this.knownRoles.push(role);
                }
                return true;
            }
        });
    };
    AuthProvider.prototype.inAnyRole = function (roles) {
        if (!this.user) {
            return false;
        }
        for (var i = 0; i < roles.length; i++) {
            if (this.inRole(roles[i])) {
                return true;
            }
        }
    };
    AuthProvider.prototype.hasAccess = function (check) {
        var _this = this;
        if (!this.user) {
            return false;
        }
        if (!this.user.user.roles) {
            console.error('No roles found in the current user');
            return false;
        }
        if (!this.user.user.permissions) {
            console.error('No permissions found in the current user');
            return false;
        }
        var checkCount = check.length;
        var hasAccessCount = 0;
        for (var i = 0; i < check.length; i++) {
            if (this.knownPermissions.indexOf(check[i]) !== -1) {
                hasAccessCount++;
            }
        }
        // check the permissions on the user
        check.forEach(function (c) {
            if (hasAccessCount >= checkCount) {
                return;
            }
            if (_this.user.user.permissions[c] !== undefined &&
                (_this.user.user.permissions[c] === true || _this.user.user.permissions[c] === 'true')) {
                _this.knownPermissions.push(c);
                hasAccessCount++;
            }
        });
        if (hasAccessCount >= checkCount) {
            return true;
        }
        // check the permissions on the roles that the user has
        this.user.user.roles.forEach(function (r) {
            if (hasAccessCount >= checkCount) {
                return;
            }
            check.forEach(function (c) {
                if (hasAccessCount >= checkCount) {
                    return;
                }
                if (r.permissions[c] !== undefined && (r.permissions[c] === true || r.permissions[c] === 'true')) {
                    _this.knownPermissions.push(c);
                    hasAccessCount++;
                }
            });
        });
        return hasAccessCount >= checkCount;
    };
    AuthProvider.prototype.hasAnyAccess = function (check) {
        var _this = this;
        var hasAnyAccess = false;
        check.forEach(function (c) {
            if (hasAnyAccess) {
                return true;
            }
            if (_this.hasAccess([c])) {
                hasAnyAccess = true;
            }
        });
        return hasAnyAccess;
    };
    AuthProvider.prototype.getAuthorizationHeader = function () {
        return new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().append('Authorization', 'Bearer ' + this.token);
    };
    AuthProvider.prototype.isLoggedIn = function () {
        var now = new Date();
        if (!this.token || this.user.user.exp < now.getTime() / 1000) {
            // this.logout();
            return false;
        }
        return true;
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_4__global_global__["a" /* GlobalProvider */]])
    ], AuthProvider);
    return AuthProvider;
}());

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 12:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AplicativosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the AplicativosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AplicativosProvider = /** @class */ (function () {
    function AplicativosProvider(api, splashScreen, statusBar, app) {
        this.api = api;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.app = app;
        this.item = {};
        this.list = [];
        this.api.saveOneSignal();
        this.splashScreen.hide();
        console.log('Hello AplicativosProvider Provider');
    }
    AplicativosProvider.prototype.getAll = function () {
        var _this = this;
        return new Promise(function (resolve, reject) { return _this.api.get('/aplicativos2').subscribe(function (r) { return resolve(_this.list = r); }, function (e) { return reject(e); }); });
    };
    AplicativosProvider.prototype.setItem = function (item) {
        this.item = item;
        this.setTheme(item);
    };
    AplicativosProvider.prototype.setTheme = function (item) {
        this.statusBar.backgroundColorByHexString(item.cor);
        document.body.setAttribute('class', '');
        if (item.id == 1) {
            document.body.classList.add('theme-easymind');
        }
        else if (item.id == 2) {
            document.body.classList.add('theme-easyfit-sports');
        }
        else if (item.id == 3) {
            document.body.classList.add('theme-easyfit-nutri');
        }
        else if (item.id == 4) {
            document.body.classList.add('theme-easylife');
        }
        else if (item.id == 5) {
            document.body.classList.add('theme-easylife');
            document.body.classList.add('theme-easyfit-gestao');
        }
        else if (item.id == 6) {
            document.body.classList.add('theme-easyfit-nutri');
            document.body.classList.add('theme-easyfit-central');
        }
        else if (item.id == 7) {
            console.log('setou 7');
            document.body.classList.add('theme-easylife-gastronomia');
        }
    };
    AplicativosProvider.prototype.clearItem = function () {
        this.item = {};
    };
    AplicativosProvider.prototype.getEasyfitNutri = function () {
        return this.list.find(function (obj) { return obj.id == 3; });
    };
    AplicativosProvider.prototype.getEasyfitLife = function () {
        return this.list.find(function (obj) { return obj.id == 4; });
    };
    AplicativosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["b" /* App */]])
    ], AplicativosProvider);
    return AplicativosProvider;
}());

//# sourceMappingURL=aplicativos.js.map

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacaPartePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__esqueci_senha_esqueci_senha__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__ = __webpack_require__(55);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the FacaPartePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FacaPartePage = /** @class */ (function () {
    function FacaPartePage(navCtrl, navParams, toastCtrl, helpers, _loading, global, http, auth, keyboard, alert, iab, plt) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.helpers = helpers;
        this._loading = _loading;
        this.global = global;
        this.http = http;
        this.auth = auth;
        this.keyboard = keyboard;
        this.alert = alert;
        this.iab = iab;
        this.plt = plt;
        // @ViewChild('video') video: ElementRef;
        this.user = {
            first_name: '',
            telefone: '',
            email: '',
            password: '',
            confirmPassword: ''
        };
        this.showVideo = false;
        this.aceito = false;
    }
    FacaPartePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // this.video.nativeElement.muted = true;
        // this.video.nativeElement.play();
        setTimeout(function () {
            _this.showVideo = true;
        }, 2000);
    };
    FacaPartePage.prototype.termosDeUso = function () {
        if (!this.global.informacoes.termos_de_uso) {
            return;
        }
        if (this.plt.is('ios')) {
            this.iab.create(this.global.informacoes.termos_de_uso, 'system').show();
        }
        else {
            this.iab.create(this.global.informacoes.termos_de_uso, 'blank').show();
        }
    };
    FacaPartePage.prototype.onSubmit = function (form) {
        var _this = this;
        this.helpers.formMarkAllTouched(form);
        if (!this.aceito) {
            this.alert.create({ title: 'Termos de Uso', message: 'Você deve aceitar os termos de uso e condição para poder se cadastrar' }).present();
            return;
        }
        if (!form.valid) {
            this.toastCtrl.create({ message: 'Verifique se todos os campos estão preenchidos corretamente', duration: 4000 }).present();
            return;
        }
        if (!this.helpers.isEmail(this.user.email)) {
            form.controls.email.setErrors({ invalid: true });
            this.toastCtrl.create({ message: 'Informe um e-mail válido', duration: 4000 }).present();
            return;
        }
        if (this.user.password.length < 6) {
            this.toastCtrl.create({ message: 'Informe uma senha com pelo menos 6 caracteres', duration: 4000 }).present();
            return;
        }
        if (this.user.password != this.user.confirmPassword) {
            this.toastCtrl.create({ message: 'As senhas não conferem', duration: 4000 }).present();
            return;
        }
        console.log(this.user);
        this._loading.present();
        this.http.post(this.global.apiUrl + '/register', this.user).subscribe(function (r) {
            _this._loading.dismiss();
            _this.toastCtrl.create({ message: _this.user.first_name + ', sua conta foi criada!', duration: 4000 }).present();
            // Object.assign(this.loginPage.credentials, this.user);
            // setTimeout(() => this.loginPage.onSubmit(), 300);
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__login_login__["a" /* LoginPage */], { user: _this.user });
        }, function (e) {
            _this._loading.dismiss();
            if (e.status == 400) {
                _this.toastCtrl.create({ message: e.error.message, duration: 4000 }).present();
            }
            else {
                _this.toastCtrl.create({ message: 'Por favor, tente novamente', duration: 4000 }).present();
            }
        });
    };
    FacaPartePage.prototype.esqueciSenhaPage = function () {
        this.navCtrl.pop();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */]);
    };
    FacaPartePage.prototype.backspaceTelefone = function () {
        if (this.user.telefone.length <= 3) {
            this.user.telefone = '';
        }
    };
    FacaPartePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-faca-parte',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/faca-parte/faca-parte.html"*/'<!--\n  Generated template for the FacaPartePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content class="auth-pages">\n  <button navPop class="duo-navpop">\n    <ion-icon name="ios-arrow-back" color="primary"></ion-icon>\n  </button>\n  <!-- <div class="video" style="transition: all .3s ease">\n    <video #video [hidden]="!global.informacoes.thumbnail_principal" [src]="global.informacoes.thumbnail_principal"\n      autoplay="autoplay" loop="loop" muted muted="muted"></video>\n  </div> -->\n  <div class="logo">\n    <div class="logo-img"></div>\n    <div class="bem-vindo">B E M - V I N D O</div>\n  </div>\n  <form (ngSubmit)="onSubmit(form)" #form="ngForm" class="acesso">\n\n    <input required type="text" name="nome" [(ngModel)]="user.first_name" placeholder="NOME">\n    <input type="text" name="cpf" mask="000.000.000-00" [(ngModel)]="user.cpf" placeholder="CPF">\n    <!-- <ion-input [brmasker]="{phone: true}" style="margin:0 auto;" type="text" name="telefone" [(ngModel)]="user.telefone" placeholder="TELEFONE" details-none></ion-input> -->\n    <input type="tel" name="telefone" [(ngModel)]="user.telefone" placeholder="TELEFONE" mask="00 00000-0000" (keydown.backspace)="backspaceTelefone()">\n    <input required type="email" name="email" [(ngModel)]="user.email" placeholder="E-MAIL">\n    <input required type="password" minlength="6" name="password" [(ngModel)]="user.password" placeholder="SENHA">\n    <input required type="password" minlength="6" name="confirmPassword" [(ngModel)]="user.confirmPassword" placeholder="CONFIRMAR SENHA">\n    <div>\n      <label class="aceitar">\n        <ion-checkbox [(ngModel)]="aceito" name="aceito"></ion-checkbox> Eu aceito os\n      </label>\n      <b cursor-pointer (click)="termosDeUso()">termos de uso</b>\n    </div>\n    <button ion-button block margin-top margin-bottom>FAZER PARTE</button>\n    <div class="faca-parte">\n      <span navPop>Entrar</span>\n    </div>\n    <div class="continue-visitante">\n      <span cursor-pointer (click)="esqueciSenhaPage()"> Esqueci minha senha</span>\n    </div>\n    <a class="site" style="display: block" padding-bottom href="{{ global.informacoes.site }}" target="blank">\n      {{ helpers.removeHttp(global.informacoes.site) }}\n    </a>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/faca-parte/faca-parte.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */]])
    ], FacaPartePage);
    return FacaPartePage;
}());

//# sourceMappingURL=faca-parte.js.map

/***/ }),

/***/ 137:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EsqueciSenhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__faca_parte_faca_parte__ = __webpack_require__(136);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the EsqueciSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EsqueciSenhaPage = /** @class */ (function () {
    function EsqueciSenhaPage(navCtrl, navParams, toastCtrl, helpers, _loading, global, http, keyboard) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.helpers = helpers;
        this._loading = _loading;
        this.global = global;
        this.http = http;
        this.keyboard = keyboard;
        // @ViewChild('video') video: ElementRef;
        this.email = '';
    }
    EsqueciSenhaPage.prototype.ionViewDidLoad = function () {
        // this.video.nativeElement.muted = true;
        console.log('ionViewDidLoad EsqueciSenhaPage');
    };
    EsqueciSenhaPage.prototype.facaPartePage = function () {
        this.navCtrl.pop();
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__faca_parte_faca_parte__["a" /* FacaPartePage */]);
    };
    EsqueciSenhaPage.prototype.onSubmit = function () {
        var _this = this;
        if (!this.helpers.isEmail(this.email)) {
            this.toastCtrl.create({ message: 'Informe um e-mail válido', duration: 4000 }).present();
            return;
        }
        this._loading.present();
        this.http.post(this.global.apiUrl + '/forgot-password', { email: this.email }).subscribe(function (r) {
            _this._loading.dismiss();
            _this.toastCtrl.create({ message: 'Um e-mail foi enviado para você!', duration: 4000 }).present();
            _this.navCtrl.pop();
        }, function (e) {
            _this._loading.dismiss();
            if (e.status == 404) {
                _this.toastCtrl.create({ message: 'Verifique se seu e-mail foi digitado corretamente', duration: 4000 }).present();
            }
            else {
                _this.toastCtrl.create({ message: 'Por favor, tente novamente', duration: 4000 }).present();
            }
        });
    };
    EsqueciSenhaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-esqueci-senha',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/esqueci-senha/esqueci-senha.html"*/'<ion-content class="auth-pages">\n  <button navPop class="duo-navpop">\n    <ion-icon name="ios-arrow-back" color="primary"></ion-icon>\n  </button>\n  <!-- <div class="video">\n    <video #video [hidden]="!global.informacoes.thumbnail_principal" [src]="global.informacoes.thumbnail_principal" autoplay="autoplay" loop="loop" muted muted="muted"></video>\n  </div> -->\n  <div class="logo" [ngStyle]="{keyboard:  keyboard.isOpen() ? 0 : 1}">\n    <div class="logo-img"></div>\n  </div>\n  <form (ngSubmit)="onSubmit(form)" #form="ngForm" class="acesso">\n    <div class="acesse">VOCÊ RECEBERÁ UM E-MAIL COM MAIS INFORMAÇÕES</div>\n    <input placeholder="E-MAIL" type="email" name="email" [(ngModel)]="email">\n\n    <button ion-button block>RECUPERAR</button>\n    <div class="faca-parte" cursor-pointer (click)="facaPartePage()">\n      Faça parte\n    </div>\n    <div class="continue-visitante" navPop>\n      Entrar\n    </div>\n\n    <!-- <div class="continue-visitante">\n      Continue como visitante\n    </div> -->\n    <a class="site" href="{{ global.informacoes.site }}" target="blank">\n      {{ helpers.removeHttp(global.informacoes.site) }}\n    </a>\n  </form>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/esqueci-senha/esqueci-senha.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Keyboard */]])
    ], EsqueciSenhaPage);
    return EsqueciSenhaPage;
}());

//# sourceMappingURL=esqueci-senha.js.map

/***/ }),

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CodigoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_categorias_categorias__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the CodigoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CodigoPage = /** @class */ (function () {
    function CodigoPage(navCtrl, navParams, loading, toastCtrl, api, auth, alertCtrl, helpers, global, _categorias) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loading = loading;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.helpers = helpers;
        this.global = global;
        this._categorias = _categorias;
        this.code = '';
    }
    CodigoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CodigoPage');
    };
    CodigoPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.api.post('/users/' + this.auth.user.user.id, {}, false).subscribe(function (r) { return _this.auth.setToken(r.token); });
    };
    CodigoPage.prototype.onSubmit = function (form) {
        var _this = this;
        if (this.code.length == 0) {
            this.toastCtrl.create({ message: 'Você deve informar um código', duration: 4000 }).present();
            return;
        }
        this.loading.present();
        this.api.post('/users/' + this.auth.user.user.id + '/premium', { code: this.code }, false).subscribe(function (r) {
            _this.loading.dismiss();
            _this.code = '';
            console.log(_this.auth.user.user);
            _this.auth.setToken(r.token);
            console.log(_this.auth.user.user);
            _this.alertCtrl.create({
                title: 'Sucesso!', message: 'Seu código foi ativado com sucesso, ele irá expirar em ' + _this.helpers.moment(_this.auth.user.user.premium_expire_at, 'YYYY-MM-DD HH:mm:ss').format('LLL'), buttons: [
                    { text: 'Ok' }
                ]
            }).present();
            if (_this.navCtrl.canGoBack()) {
                _this.navCtrl.pop();
            }
            _this._categorias.getAll();
        }, function (e) {
            _this.loading.dismiss();
            _this.code = '';
            console.log(e);
            if (e.error.message) {
                _this.toastCtrl.create({ message: e.error.message, duration: 4000 }).present();
            }
            else {
                _this.toastCtrl.create({ message: 'Um erro desconhecido aconteceu, por favor tente novamente', duration: 4000 }).present();
            }
        });
    };
    CodigoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-codigo',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/codigo/codigo.html"*/'<!--\n  Generated template for the CodigoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Código"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <div padding>\n    <div class="logo">\n      <div class="logo-img"></div>\n    </div>\n    <div class="certeza">{{ global.informacoes.codigo_titulo }}</div>\n    <div class="explicacao" style="white-space: pre-wrap">{{ global.informacoes.codigo_descricao }}</div>\n    <div class="insira">\n      <span *ngIf="(auth.isPremium())">\n        O seu código irá expirar em\n        <br>  \n       <b style="text-transform: uppercase"> {{ helpers.moment(auth.user.user.premium_expire_at, \'YYYY-MM-DD HH:mm:ss\').format(\'LL\') }}</b>\n      </span>\n      <span *ngIf="(!auth.isPremium()) && auth.user.user.premium_expire_at">\n        O seu código expirou em\n        <br>\n       <b style="text-transform: uppercase;" style="text-transform: uppercase; color: #f53d3d;"> {{ helpers.moment(auth.user.user.premium_expire_at, \'YYYY-MM-DD HH:mm:ss\').format(\'LL\') }}</b>\n      </span>\n    \n      <b>INSIRA O CÓDIGO</b>\n      <form ion-list (ngSubmit)="onSubmit(form)" #form="ngForm">\n        <input type="text" name="code" [(ngModel)]="code" required placeholder="CÓDIGO">\n\n        <button ion-button block margin-top margin-bottom>RESGATAR</button>\n      </form>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/codigo/codigo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_categorias_categorias__["a" /* CategoriasProvider */]])
    ], CodigoPage);
    return CodigoPage;
}());

//# sourceMappingURL=codigo.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConhecaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_global__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ConhecaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ConhecaPage = /** @class */ (function () {
    function ConhecaPage(navCtrl, navParams, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.global = global;
    }
    ConhecaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ConhecaPage');
    };
    ConhecaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-conheca',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/conheca/conheca.html"*/'<!--\n  Generated template for the ConhecaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header [title]="global.conheca.titulo" [hideButtons]="true"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <div padding class="max-width-600">\n    <img [src]="global.conheca.thumbnail_principal" style="max-width: 100%;display: block;height: auto; margin-top:25px;margin-bottom: 50px;" >\n    <div [innerHtml]="global.conheca.descricao | linkify"></div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/conheca/conheca.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_global__["a" /* GlobalProvider */]])
    ], ConhecaPage);
    return ConhecaPage;
}());

//# sourceMappingURL=conheca.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PdfsEDicasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_pdfs_pdfs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the PdfsEDicasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PdfsEDicasPage = /** @class */ (function () {
    function PdfsEDicasPage(navCtrl, toastCtrl, navParams, api, _loading, iap, alertCtrl, _pdfs, _aplicativos) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.api = api;
        this._loading = _loading;
        this.iap = iap;
        this.alertCtrl = alertCtrl;
        this._pdfs = _pdfs;
        this._aplicativos = _aplicativos;
        this.active = this._pdfs.list.length ? 'pdf' : 'dicas';
        this.dicas = [];
        this.pdfs = [];
        this.loading = false;
        this.requests = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.requestsCompleted = 0;
        this.requests.subscribe(function () {
            if (_this.requestsCompleted == 2) {
                _this.refresher.complete();
                _this._loading.dismiss();
                _this.loading = false;
                _this.requestsCompleted = 0;
            }
        });
        this.onRefresher();
    }
    PdfsEDicasPage.prototype.onRefresher = function () {
        var _this = this;
        if (!(this.dicas.length) || !(this.pdfs.length)) {
            this._loading.present();
        }
        this.loading = true;
        this.api.get('/dicas?aplicativo_id=' + this._aplicativos.item.id).subscribe(function (r) {
            _this.requests.next(_this.requestsCompleted++);
            _this.dicas = r;
        }, function (e) { return _this.requests.next(_this.requestsCompleted++); });
        this._pdfs.getAll().subscribe(function (r) { return _this.requests.next(_this.requestsCompleted++); }, function (e) { return _this.requests.next(_this.requestsCompleted++); });
    };
    PdfsEDicasPage.prototype.openTab = function (tab) {
        this.active = tab;
    };
    PdfsEDicasPage.prototype.toggleDica = function (i) {
        this.dicas[i].active = !this.dicas[i].active;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], PdfsEDicasPage.prototype, "refresher", void 0);
    PdfsEDicasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-pdfs-e-dicas',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/pdfs-e-dicas/pdfs-e-dicas.html"*/'<!--\n  Generated template for the CodigoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header [title]="_pdfs.list.length ? \'CONTEÚDOS E DICAS\' : \'DICAS\'"></header>\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n\n  <div class="tabs-header"  *ngIf="_pdfs.list.length" >\n    <div class="header" cursor-pointer (click)="openTab(\'pdf\')" [ngClass]="{active: active == \'pdf\'}">CONTEÚDOS</div>\n    <div class="header" cursor-pointer (click)="openTab(\'dicas\')" [ngClass]="{active: active == \'dicas\'}">DICAS</div>\n  </div>\n  <div padding>\n\n    <div class="lista-pdf" *ngIf="active == \'pdf\'">\n\n      <div *ngFor="let item of _pdfs.list">\n        <div class="titulo" cursor-pointer (click)="_pdfs.open(item)">{{ item.titulo }}</div>\n        <div class="icones">\n          <!-- <button cursor-pointer (click)="_pdfs.favoritar(item)">\n            <ion-icon name="duo-star" [color]="item.favoritado ? \'yellow\' : \'\'"></ion-icon>\n          </button> -->\n          <button cursor-pointer (click)="_pdfs.open(item)">PDF</button>\n        </div>\n      </div>\n\n      <div *ngIf="!_pdfs.list.length && !loading" padding text-center>\n        <!-- <div style="line-height: 20px">Nenhum documento encontrado</div> -->\n      </div>\n\n    </div>\n\n\n\n    <div class="dicas" *ngIf="active == \'dicas\'">\n\n      <div class="item" *ngFor="let dica of dicas; let i = index">\n        <img [src]="dica.thumbnail_principal" alt="" cursor-pointer (click)="toggleDica(i)">\n        <div class="titulo" cursor-pointer (click)="toggleDica(i)">{{ dica.titulo }}</div>\n        <div class="descricao" [hidden]="!dica.active" [innerHtml]="dica.descricao | linkify"></div>\n      </div>\n\n      <div *ngIf="!dicas.length && !loading" padding text-center>\n        Nenhuma dica encontrada\n      </div>\n\n    </div>\n\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/pdfs-e-dicas/pdfs-e-dicas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_pdfs_pdfs__["a" /* PdfsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], PdfsEDicasPage);
    return PdfsEDicasPage;
}());

//# sourceMappingURL=pdfs-e-dicas.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroUsuarioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__cadastro_usuario_second_cadastro_usuario_second__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_cadastro_cadastro__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__easyfit_sports_tabs_easyfit_sports_tabs__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_loading_loading__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the CadastroUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CadastroUsuarioPage = /** @class */ (function () {
    function CadastroUsuarioPage(navCtrl, navParams, _cadastro, helpers, toastCtrl, aplicativos, app, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._cadastro = _cadastro;
        this.helpers = helpers;
        this.toastCtrl = toastCtrl;
        this.aplicativos = aplicativos;
        this.app = app;
        this.loading = loading;
        this.heightFruits = 0;
    }
    CadastroUsuarioPage.prototype.ionViewDidLoad = function () {
        this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
        if (this.heightFruits < 150) {
            this.heightFruits = 150;
        }
    };
    CadastroUsuarioPage.prototype.dataNascimentoPicker = function () {
        this.dataNascimento.nativeElement.click();
    };
    CadastroUsuarioPage.prototype.cadastroUsuarioSecondPage = function () {
        if (!this._cadastro.dados.genero) {
            this.toastCtrl.create({ message: 'Qual o seu gênero?', duration: 3000 }).present();
            return;
        }
        if (!this._cadastro.dados.data_nascimento) {
            this.toastCtrl.create({ message: 'Qual a sua data de nascimento?', duration: 3000 }).present();
            return;
        }
        if (!this._cadastro.dados.altura || !(this._cadastro.dados.altura < 300 && this._cadastro.dados.altura > 0)) {
            this.toastCtrl.create({ message: 'Qual a sua altura?', duration: 3000 }).present();
            return;
        }
        if (!this._cadastro.dados.inicio_kg || !(this._cadastro.dados.inicio_kg < 300 && this._cadastro.dados.inicio_kg > 0)) {
            this.toastCtrl.create({ message: 'Qual o seu peso?', duration: 3000 }).present();
            return;
        }
        this._cadastro.dados.atual_kg = this._cadastro.dados.inicio_kg;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__cadastro_usuario_second_cadastro_usuario_second__["a" /* CadastroUsuarioSecondPage */], {});
    };
    CadastroUsuarioPage.prototype.easylifePage = function () {
        var _this = this;
        this.loading.present();
        this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_6__easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */], { animate: true, animation: 'ios-transition', duration: 1000 });
        setTimeout(function () {
            _this.aplicativos.setItem(_this.aplicativos.getEasyfitLife());
            _this.loading.dismiss();
        }, 50);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])('dataNascimento'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ElementRef */])
    ], CadastroUsuarioPage.prototype, "dataNascimento", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], CadastroUsuarioPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])('layout'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ElementRef */])
    ], CadastroUsuarioPage.prototype, "layout", void 0);
    CadastroUsuarioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-cadastro-usuario',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/cadastro-usuario/cadastro-usuario.html"*/'<ion-header>\n  <ion-navbar>\n    <button start (click)="easylifePage()" style="font-size: 30px;\n    padding-left: 20px;background:transparent;border:none">\n      <ion-icon name="ios-arrow-back"></ion-icon>\n    </button>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding32>\n  <div class="max-width-600" #layout>\n    <div class="info" text-uppercase>\n      Preencha as seguintes perguntas<br />\n      para personalizar seu perfil.\n    </div>\n\n    <div class="radio-group-default">\n      <div class="radio-group-title blue-title" style="flex: 0 0 100%;">\n        QUAL O SEU GÊNERO?\n      </div>\n\n      <div class="radio-group-item half">\n        <input type="radio" id="masculino" name="genero" [(ngModel)]="_cadastro.dados.genero" value="masculino">\n        <label for="masculino">MASCULINO</label>\n      </div>\n      <div class="radio-group-item half">\n        <input type="radio" id="feminino" name="genero" [(ngModel)]="_cadastro.dados.genero" value="feminino">\n        <label for="feminino">FEMININO</label>\n      </div>\n    </div>\n\n    <form class="forms">\n      <div class="blue-title">\n        QUAL A SUA?\n      </div>\n\n      <ion-item class="details-none" details-none no-lines>\n        <ion-datetime doneText="Ok" cancelText="Fechar" displayFormat="DD/MM/YYYY"\n          [(ngModel)]="_cadastro.dados.data_nascimento" placeholder="DATA DE NASCIMENTO" name="data"></ion-datetime>\n\n      </ion-item>\n      <!-- <input mask="000" type="text" placeholder="ALTURA (cm)" [(ngModel)]="_cadastro.dados.altura" name="altura"\n        margin-top> -->\n        <ion-input type="tel"  placeholder="ALTURA (CM)" details-none\n        [(ngModel)]="_cadastro.dados.altura" name="altura" [brmasker]="{mask:\'000\', len:3, type: \'num\'}"></ion-input>\n      <!-- <input mask="000" minlength="2" type="text" placeholder="PESO (kg)" [(ngModel)]="_cadastro.dados.inicio_kg" name="peso"\n        margin-top> -->\n\n        <ion-input type="tel"  placeholder="PESO (kg)" details-none\n        [(ngModel)]="_cadastro.dados.inicio_kg" name="inicio_kg" [brmasker]="{mask:\'000\', len:3, type: \'num\'}"></ion-input>\n\n      <button cursor-pointer (click)="cadastroUsuarioSecondPage()" type="button" class="btn-arrow" ion-button margin-top\n        block color="light">\n        AVANÇAR <ion-icon name="ios-arrow-forward"></ion-icon>\n      </button>\n    </form>\n\n  </div>\n  <div class="max-width-600">\n    <img src="assets/imgs/fruits.png" [style.height.px]="heightFruits" alt="">\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/cadastro-usuario/cadastro-usuario.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_cadastro_cadastro__["a" /* CadastroProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_7__providers_loading_loading__["a" /* LoadingProvider */]])
    ], CadastroUsuarioPage);
    return CadastroUsuarioPage;
}());

//# sourceMappingURL=cadastro-usuario.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the CadastroProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CadastroProvider = /** @class */ (function () {
    function CadastroProvider(http) {
        this.http = http;
        this.dados = {
            genero: '',
            data_nascimento: '',
            altura: '',
            inicio_kg: '',
            objetivo_descricao: '',
            objetivo_kg: '',
            objetivo_kcal: '',
            first_name: '',
            telefone: '',
            email: '',
            password: '',
            confirmPassword: ''
        };
        console.log('Hello CadastroProvider Provider');
    }
    CadastroProvider.prototype.clear = function () {
        this.dados = {
            genero: '',
            data_nascimento: '',
            altura: '',
            inicio_kg: '',
            objetivo_descricao: '',
            objetivo_kg: '',
            objetivo_kcal: '',
            first_name: '',
            telefone: '',
            email: '',
            password: '',
            confirmPassword: ''
        };
    };
    CadastroProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], CadastroProvider);
    return CadastroProvider;
}());

//# sourceMappingURL=cadastro.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriRefeicoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RefeicoesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitNutriRefeicoesPage = /** @class */ (function () {
    function EasyfitNutriRefeicoesPage(navCtrl, navParams, api, _alimentos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this._alimentos = _alimentos;
        this.refeicoes = new Array(6);
        this.tipo = 0;
        this.options = {
            favoritado: 0,
            search: '',
            page: 1
        };
        this.loading = false;
        this._alimentos.getDestaques().subscribe();
        this.doSearch();
    }
    EasyfitNutriRefeicoesPage.prototype.ionViewWillEnter = function () {
        this.tipo = this._alimentos.tipo;
        this.tipo = this.tipo ? this.tipo : 1;
    };
    EasyfitNutriRefeicoesPage.prototype.refeicaoInternaPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__["a" /* EasyfitNutriRefeicoesInternaPage */], { tipo: this.tipo, item: item });
    };
    EasyfitNutriRefeicoesPage.prototype.doSearch = function () {
        var _this = this;
        this.loading = true;
        this.options.page = 1;
        this._alimentos._getPaginate = this._alimentos.getPaginate(this.options).subscribe(function (r) {
            _this.loading = false;
        }, function (e) { return _this.loading = false; });
    };
    EasyfitNutriRefeicoesPage.prototype.doInfinite = function () {
        var _this = this;
        this.options.page++;
        this._alimentos._getPaginate = this._alimentos.getPaginate(this.options).subscribe(function (r) {
            _this.infiniteScroll.complete();
            if (r.data.length === 0) {
                _this.options.page--;
            }
        }, function (e) { return _this.infiniteScroll.complete(); });
    };
    EasyfitNutriRefeicoesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RefeicoesPage');
    };
    EasyfitNutriRefeicoesPage.prototype.temDestaqueFavoritado = function () {
        return this._alimentos.destaques.find(function (obj) { return obj.favoritado; });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* InfiniteScroll */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* InfiniteScroll */])
    ], EasyfitNutriRefeicoesPage.prototype, "infiniteScroll", void 0);
    EasyfitNutriRefeicoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-refeicoes',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-refeicoes/easyfit-nutri-refeicoes.html"*/'<ion-header>\n  <header [title]="_alimentos.tipos[tipo]"></header>\n</ion-header>\n\n<ion-content padding>\n  <div class="max-width-600">\n    <form class="search-box">\n    <input type="search" placeholder="BUSCA DE ALIMENTOS" [(ngModel)]="options.search" (input)="doSearch()" name="search">\n  </form>\n\n  <div class="radio-group-default" margin-top>\n    <div class="radio-group-item half">\n      <input type="radio" [(ngModel)]="options.favoritado" (change)="doSearch()" [value]="0" id="lista" name="favoritado"\n        checked>\n      <label for="lista">LISTA</label>\n    </div>\n    <div class="radio-group-item half">\n      <input type="radio" [(ngModel)]="options.favoritado" (change)="doSearch()" [value]="1" id="favoritos" name="favoritado"\n        checked>\n      <label for="favoritos">FAVORITOS</label>\n    </div>\n  </div>\n\n  <div class="refeicoes-listagem" margin-top>\n\n    <div text-center>\n      <ion-spinner *ngIf="loading"></ion-spinner>\n    </div>\n\n    <div class="refeicoes-item" *ngFor="let item of _alimentos.destaques; let i = index" color="white" [hidden]="options.favoritado == 1 && item.favoritado == 0"\n      no-margin>\n      <div cursor-pointer (click)="refeicaoInternaPage(_alimentos.destaques[i])" class="refeicoes-item-esquerda">\n        <div class="refeicoes-item-image">\n          <img default="assets/imgs/symbol.png" [src]="item.thumbnail_principal" alt="">\n        </div>\n        <span text-left>{{ item.titulo }}</span>\n      </div>\n      <div class="refeicoes-item-direita">\n        <ion-icon name="star" [color]="item.favoritado == 1 ? \'gold\' : \'gray\'" cursor-pointer (click)="_alimentos.favoritar(_alimentos.destaques[i])"></ion-icon>\n        <!-- \n            ngIf para favoritos \n            <ion-icon name="star" color="gold"></ion-icon>\n          -->\n        <small cursor-pointer (click)="refeicaoInternaPage(_alimentos.destaques[i])">{{ item.energia }}</small>\n        <ion-icon cursor-pointer (click)="refeicaoInternaPage(_alimentos.destaques[i])" name="ios-arrow-forward" color="primary"></ion-icon>\n      </div>\n    </div>\n    <div class="refeicoes-item" *ngFor="let item of _alimentos.listPaginate; let i = index" color="white" no-margin>\n      <div cursor-pointer (click)="refeicaoInternaPage(_alimentos.listPaginate[i])" class="refeicoes-item-esquerda">\n        <div class="refeicoes-item-image">\n          <img default="assets/imgs/symbol.png" [src]="item.thumbnail_principal" alt="">\n        </div>\n        <span text-left>{{ item.titulo }}</span>\n      </div>\n      <div class="refeicoes-item-direita">\n        <ion-icon name="star" [color]="item.favoritado == 1 ? \'gold\' : \'gray\'" cursor-pointer (click)="_alimentos.favoritar(_alimentos.listPaginate[i])"></ion-icon>\n        <!-- \n            ngIf para favoritos \n            <ion-icon name="star" color="gold"></ion-icon>\n          -->\n        <small cursor-pointer (click)="refeicaoInternaPage(_alimentos.listPaginate[i])">{{ item.energia }}</small>\n        <ion-icon cursor-pointer (click)="refeicaoInternaPage(_alimentos.listPaginate[i])" name="ios-arrow-forward" color="primary"></ion-icon>\n      </div>\n    </div>\n  </div>\n  <div text-center *ngIf="!loading && (_alimentos.listPaginate.length == 0 && !temDestaqueFavoritado()) && infiniteScroll.state == \'enabled\'">\n    Nenhum alimento encontrado\n  </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite()">\n    <br><br>\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-refeicoes/easyfit-nutri-refeicoes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */]])
    ], EasyfitNutriRefeicoesPage);
    return EasyfitNutriRefeicoesPage;
}());

//# sourceMappingURL=easyfit-nutri-refeicoes.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriRefeicoesInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the RefeicaoInternaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitNutriRefeicoesInternaPage = /** @class */ (function () {
    function EasyfitNutriRefeicoesInternaPage(navCtrl, navParams, api, auth, helpers, _alimentos, toastCtrl, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.auth = auth;
        this.helpers = helpers;
        this._alimentos = _alimentos;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.item = {};
        this.tipo = 1;
        this.heightFruits = 0;
        this.item = this.navParams.get('item');
        this.atualizar = this.navParams.get('atualizar');
        this.quantidade = this.navParams.get('quantidade');
        this.gordura = this.item.gordura;
        this.energia = this.item.energia;
        this.carboidratos = this.item.carboidratos;
        this.proteina = this.item.proteina;
        if (this.atualizar) {
            this.calcular();
        }
    }
    EasyfitNutriRefeicoesInternaPage.prototype.ionViewWillEnter = function () {
        this.tipo = this._alimentos.tipo;
    };
    EasyfitNutriRefeicoesInternaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RefeicaoInternaPage');
        this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
        if (this.heightFruits < 150) {
            this.heightFruits = 150;
        }
    };
    EasyfitNutriRefeicoesInternaPage.prototype.salvar = function () {
        var _this = this;
        if (!this.quantidade || this.quantidade < 0) {
            this.toastCtrl.create({ message: 'Por favor, informe a quantidade desejada', duration: 4000 }).present();
            return;
        }
        this.loading.present();
        this.request().subscribe(function (r) {
            _this.toastCtrl.create({ message: 'Alimento salvo para o dia ' + _this.helpers.moment(_this._alimentos.selectedDate, 'YYYY-MM-DD').format('LL') + ' para ' + _this._alimentos.tipos[_this.tipo], duration: 4000 }).present();
            _this._alimentos.getDay().then(function () {
                _this.navCtrl.pop();
                _this.loading.dismiss();
            });
        });
    };
    EasyfitNutriRefeicoesInternaPage.prototype.favoritar = function () {
        var _this = this;
        this._alimentos.favoritar(this.item).then(function (alimento) { return _this.item = alimento; });
    };
    EasyfitNutriRefeicoesInternaPage.prototype.calcular = function () {
        this.energia = Math.ceil((this.item.energia * this.quantidade) / 100);
        this.gordura = ((this.item.gordura * this.quantidade) / 100).toFixed(2);
        this.carboidratos = ((this.item.carboidratos * this.quantidade) / 100).toFixed(2);
        this.proteina = ((this.item.proteina * this.quantidade) / 100).toFixed(2);
    };
    EasyfitNutriRefeicoesInternaPage.prototype.request = function () {
        if (this.atualizar) {
            return this.api.patch('/users/' + this.auth.user.user.id + '/alimentos', {
                alimento_id: this.item.id,
                data: this._alimentos.selectedDate,
                quantidade: this.quantidade,
                tipo: this.tipo
            });
        }
        else {
            return this.api.post('/users/' + this.auth.user.user.id + '/alimentos', {
                alimento_id: this.item.id,
                data: this._alimentos.selectedDate,
                quantidade: this.quantidade,
                tipo: this.tipo
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], EasyfitNutriRefeicoesInternaPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('layout'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], EasyfitNutriRefeicoesInternaPage.prototype, "layout", void 0);
    EasyfitNutriRefeicoesInternaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-refeicoes-interna',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-refeicoes-interna/easyfit-nutri-refeicoes-interna.html"*/'<ion-header>\n  <header [title]="_alimentos.tipos[tipo]"></header>\n</ion-header>\n\n<ion-content>\n  <div class="max-width-600" #layout>\n    <div class="cover-image" *ngIf="item.thumbnail_principal">\n    <img [src]="item.thumbnail_principal" alt="cover-banana">\n  </div>\n\n  <div class="refeicao-interna">\n    <div class="refeicao-interna-title">\n      <span>{{ item.titulo }}</span>\n      <ion-icon name="star" [color]="item.favoritado == 1 ? \'gold\' : \'gray\'" cursor-pointer (click)="favoritar()" name="star" color="gold"></ion-icon>\n    </div>\n\n    <div class="refeicao-interna-info">\n      <div class="info-item">\n        <small>{{ energia | toFixed }}kcal</small>\n        <span>ENERGIA</span>\n      </div>\n\n      <div class="info-item">\n        <small>{{ proteina | toFixed }}g</small>\n        <span>PROTEÍNA</span>\n      </div>\n\n      <div class="info-item">\n        <small>{{ carboidratos | toFixed }}g</small>\n        <span>CARBOIDRATOS</span>\n      </div>\n\n      <div class="info-item">\n        <small>{{ gordura | toFixed }}g</small>\n        <span>GORDURA</span>\n      </div>\n    </div>\n\n    <form class="forms inline-form" (ngSubmit)="salvar()" margin-top>\n      <input mask="000" type="tel" [(ngModel)]="quantidade" (input)="calcular()" name="numero" placeholder="Qtd (g ou ml)">\n      <button class="btn btn-green" no-margin margin-left>SALVAR</button>\n    </form>\n  </div>\n  </div>\n  <div class="max-width-600">\n    <img src="assets/imgs/fruits.png" [style.height.px]="heightFruits" alt="">\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-refeicoes-interna/easyfit-nutri-refeicoes-interna.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */]])
    ], EasyfitNutriRefeicoesInternaPage);
    return EasyfitNutriRefeicoesInternaPage;
}());

//# sourceMappingURL=easyfit-nutri-refeicoes-interna.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriControleAguaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ControleAguaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitNutriControleAguaPage = /** @class */ (function () {
    function EasyfitNutriControleAguaPage(navCtrl, navParams, auth, api, _alimentos, _agua, helpers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.api = api;
        this._alimentos = _alimentos;
        this._agua = _agua;
        this.helpers = helpers;
        this.qtdAgua = 0;
    }
    EasyfitNutriControleAguaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ControleAguaPage');
    };
    EasyfitNutriControleAguaPage.prototype.ionViewWillEnter = function () {
        this._agua._get = this._agua.get().subscribe();
    };
    EasyfitNutriControleAguaPage.prototype.save = function () {
        var _this = this;
        if (parseInt(this._agua.item.quantidade) < this._agua.item.consumido) {
            this._agua.item.consumido = this._agua.item.quantidade;
            console.log('blalblblba');
        }
        this._agua._save = this._agua.save().subscribe(function (r) { return _this._agua.updateCoposView(); });
    };
    EasyfitNutriControleAguaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-controle-agua',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-controle-agua/easyfit-nutri-controle-agua.html"*/'<ion-header>\n  <header title="CONTROLE DE ÁGUA"></header>\n</ion-header>\n\n\n<ion-content padding32>\n  <div class="max-width-600">\n  \n  <div margin-bottom padding-horizontal padding-bottom text-center>\n    {{ helpers.moment(_alimentos.selectedDate).format(\'LL\') }}\n  </div>\n  <div class="water-control">\n    <div class="water-cup" text-center>\n      <span>{{ _agua.item.quantidade / 1000 }} LITROS</span>\n      <img src="assets/imgs/cup-of-water.png" alt="water-cup">\n      <div class="helper-cup" [style.height.px]="90 * (_agua.item.consumido / _agua.item.quantidade)" style="transition: all .3s ease"></div>\n      <span style="display: block;margin-top:10px">{{ _agua.item.consumido }}ml</span>\n    </div>\n\n    <ion-item no-lines>\n      <ion-range style="opacity: 1" step="250" pin="true" [(ngModel)]="_agua.item.quantidade" (ionChange)="save()"\n        [max]="5000"></ion-range>\n    </ion-item>\n  </div>\n\n  <div class="info" text-uppercase margin-top>\n    ESTABELEÇA UM OBJETIVO DE CONSUMO DE ÁGUA. SE PRATICAR ESPORTES VOCÊ DEVE AUMENTAR A INGESTÃO DE ÁGUA\n  </div>\n\n  <hr>\n\n  <!-- <div class="section-interna">\n    <h4>TAMANHO DA BEBIDA</h4>\n\n    <div class="info" text-uppercase margin-top>Selecione o tamanho da bebida</div>\n\n    <form class="forms">\n      <div class="item-agua">\n        <label for="opcao1">COPO 200 mL</label>\n        <input mask="000" name="bebida_200" [(ngModel)]="_agua.item.bebida_200" (input)="save()" id="opcao1">\n      </div>\n\n      <div class="item-agua">\n        <label for="opcao2">COPO 250 mL</label>\n        <input mask="000" name="bebida_250" [(ngModel)]="_agua.item.bebida_250" (input)="save()" id="opcao2">\n      </div>\n\n      <div class="item-agua">\n        <label for="opcao3">GARRAFA 500 mL</label>\n        <input mask="000" name="bebida_500" [(ngModel)]="_agua.item.bebida_500" (input)="save()" id="opcao3">\n      </div>\n\n      <div class="item-agua">\n        <label for="opcao4">GARRAFA 700mL</label>\n        <input mask="000" name="bebida_700" [(ngModel)]="_agua.item.bebida_700" (input)="save()" id="opcao4">\n      </div>\n    </form>\n  </div> -->\n\n  <!-- <hr margin> -->\n  <consumo-agua margin-top></consumo-agua>\n\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-controle-agua/easyfit-nutri-controle-agua.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], EasyfitNutriControleAguaPage);
    return EasyfitNutriControleAguaPage;
}());

//# sourceMappingURL=easyfit-nutri-controle-agua.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsultasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConsultasProvider = /** @class */ (function () {
    function ConsultasProvider(api, toastCtrl, aplicativos, auth) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.aplicativos = aplicativos;
        this.auth = auth;
        this.list = [];
        this.watchToggle = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.current = {};
        console.log('Hello ConsultasProvider Provider');
    }
    ConsultasProvider.prototype.delete = function (id) {
        return this.api.delete('/users/' + this.auth.user.user.id + '/consultas/' + id);
    };
    ConsultasProvider.prototype.get = function (url) {
        if (!url) {
            url = '/users/' + this.auth.user.user.id + '/consultas';
        }
        return this.api.get(url);
    };
    ConsultasProvider.prototype.getAll = function (area) {
        var _this = this;
        if (area === void 0) { area = null; }
        var opened = this.list.find(function (consultas) { return consultas.open; });
        if (area && area > 0) {
            area = '?area_id=' + area;
        }
        else {
            area = '';
        }
        return new Promise(function (resolve, reject) {
            if (_this._getAll && !_this._getAll.closed) {
                _this._getAll.unsubscribe();
            }
            _this._getAll = _this.get('/users/' + _this.auth.user.user.id + '/consultas' + area).map(function (r) {
                _this.list = r;
                return _this.list;
            }).subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    ConsultasProvider.prototype.getImagens = function (area) {
        var _this = this;
        if (area === void 0) { area = null; }
        var opened = this.list.find(function (consultas) { return consultas.open; });
        if (area) {
            area = '?area_id=' + area;
        }
        else {
            area = '';
        }
        return new Promise(function (resolve, reject) {
            if (_this._getAll && !_this._getAll.closed) {
                _this._getAll.unsubscribe();
            }
            _this._getAll = _this.get('/users/' + _this.auth.user.user.id + '/consultasImagens' + area).map(function (r) {
                _this.list = r;
                return _this.list;
            }).subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    ConsultasProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__auth_auth__["a" /* AuthProvider */]])
    ], ConsultasProvider);
    return ConsultasProvider;
}());

//# sourceMappingURL=consultas.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoImagensPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_consultas_consultas__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__ = __webpack_require__(390);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GestaoImagensPage = /** @class */ (function () {
    function GestaoImagensPage(navCtrl, navParams, _area, api, loading, aplicativos, _consultas, helpers, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._area = _area;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this._consultas = _consultas;
        this.helpers = helpers;
        this.socialSharing = socialSharing;
        this.area_id = null;
        this.onRefresher();
    }
    GestaoImagensPage.prototype.onRefresher = function () {
        var _this = this;
        this._consultas.getImagens().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
            console.log(_this._consultas.list);
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoImagensPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._area.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoImagensPage.prototype.doSearch = function (e) {
        var _this = this;
        this._consultas.getImagens(this.area_id).then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoImagensPage.prototype.shareImg = function (item) {
        this.socialSharing.share(null, null, item.thumbnail_principal, null)
            .then(function () {
        }, function () {
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], GestaoImagensPage.prototype, "refresher", void 0);
    GestaoImagensPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-gestao-imagens',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-imagens/gestao-imagens.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Minhas Imagens" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="listagem">\n    <div class="filtro">\n      <ion-select class="input" [(ngModel)]="area_id" cancelText="Cancelar" placeholder="Área" id="area" name="area" (ngModelChange)="doSearch($event)">\n          <ion-option [value]="0"> Todas </ion-option>\n          <ion-option *ngFor="let item of _area.list" [value]="item.id" >{{item.descricao}}</ion-option>\n      </ion-select>\n    </div>\n      <div class="item" *ngFor="let item of _consultas.list">\n          <img [src]="item.thumbnail_principal" (press)="shareImg(item)"/>\n      </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-imagens/gestao-imagens.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_area_area__["a" /* AreaProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_consultas_consultas__["a" /* ConsultasProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_social_sharing__["a" /* SocialSharing */]])
    ], GestaoImagensPage);
    return GestaoImagensPage;
}());

//# sourceMappingURL=gestao-imagens.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CentralNotificacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_notificacoes_notificacoes__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CentralNotificacoesPage = /** @class */ (function () {
    function CentralNotificacoesPage(navCtrl, navParams, api, loading, aplicativos, _notificacoes, helpers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this._notificacoes = _notificacoes;
        this.helpers = helpers;
        this.area_id = null;
        if (!this._notificacoes.list.length) {
            this.loading.present();
        }
        this.onRefresher();
    }
    CentralNotificacoesPage.prototype.onRefresher = function () {
        var _this = this;
        this._notificacoes.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CentralNotificacoesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._notificacoes.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CentralNotificacoesPage.prototype, "refresher", void 0);
    CentralNotificacoesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-central-notificacoes',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-notificacoes/central-notificacoes.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Notificações" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="listagem">\n      \n      <div class="item" *ngFor="let item of _notificacoes.list">\n            <div class="row">\n                 {{item.titulo}} - {{helpers.moment(item.created_at).format(\'DD/MM/YYYY\')}}\n              </div> \n              <div class="row">\n                  <small>{{item.descricao}}</small>\n              </div>\n              \n      </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-notificacoes/central-notificacoes.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_notificacoes_notificacoes__["a" /* NotificacoesProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], CentralNotificacoesPage);
    return CentralNotificacoesPage;
}());

//# sourceMappingURL=central-notificacoes.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CentralNoticiasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_noticias_noticias__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__central_noticias_interna_central_noticias_interna__ = __webpack_require__(395);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CentralNoticiasPage = /** @class */ (function () {
    function CentralNoticiasPage(navCtrl, navParams, api, loading, aplicativos, _noticias, helpers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this._noticias = _noticias;
        this.helpers = helpers;
        this.area_id = null;
        if (!this._noticias.list.length) {
            this.loading.present();
        }
        this.onRefresher();
    }
    CentralNoticiasPage.prototype.onRefresher = function () {
        var _this = this;
        this._noticias.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CentralNoticiasPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._noticias.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CentralNoticiasPage.prototype.interna = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__central_noticias_interna_central_noticias_interna__["a" /* CentralNoticiasInternaPage */], { item: item, parentPage: this });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CentralNoticiasPage.prototype, "refresher", void 0);
    CentralNoticiasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-central-noticias',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-noticias/central-noticias.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Notícias" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="listagem">\n      \n      <div class="item" *ngFor="let item of _noticias.list" (click)="interna(item)">\n          <img src="{{item.thumbnail_principal}}">\n            <div class="row">\n                 {{item.titulo}} - {{helpers.moment(item.data).format(\'DD/MM/YYYY\')}}\n              </div>\n              \n      </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-noticias/central-noticias.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_noticias_noticias__["a" /* NoticiasProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], CentralNoticiasPage);
    return CentralNoticiasPage;
}());

//# sourceMappingURL=central-noticias.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_codigo_codigo__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_player_player__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











/*
  Generated class for the PlayerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PlayerProvider = /** @class */ (function () {
    function PlayerProvider(http, app, alert, auth, global, iap, plt, _categorias, aplicativos, helpers) {
        this.http = http;
        this.app = app;
        this.alert = alert;
        this.auth = auth;
        this.global = global;
        this.iap = iap;
        this.plt = plt;
        this._categorias = _categorias;
        this.aplicativos = aplicativos;
        this.helpers = helpers;
        console.log('Hello PlayerProvider Provider');
    }
    PlayerProvider.prototype.getNavCtrl = function () {
        return this.app.getActiveNavs()[0];
    };
    PlayerProvider.prototype.playerPage = function (item) {
        var _this = this;
        if (this.auth.isPremium()) {
            this.openPlayerPage(item);
        }
        else {
            if (!(item.gratuito) && !this.helpers.isAppleReview()) {
                this.alert.create({
                    title: 'Oops',
                    message: this.global.informacoes.codigo_necessario,
                    buttons: [
                        {
                            role: 'cancel',
                            text: 'Cancelar'
                        },
                        {
                            text: 'Ok',
                            handler: function () {
                                _this.getNavCtrl().push(__WEBPACK_IMPORTED_MODULE_2__pages_codigo_codigo__["a" /* CodigoPage */]);
                            }
                        }
                    ]
                }).present();
            }
            else {
                this.openPlayerPage(item);
            }
        }
    };
    PlayerProvider.prototype.openPlayerPage = function (item) {
        if (item.tipo == 'pdf') {
            if (!item.thumbnail_principal) {
                this.alert.create({ title: 'Oops', message: 'Não foi possível carregar, por favor tente novamente mais tarde' }).present();
                return;
            }
            var options = {
                hideurlbar: 'yes',
                hidenavigationbuttons: 'yes',
                fullscreen: 'yes'
            };
            if (this.plt.is('ios')) {
                if (this.aplicativos.item.id == 1) {
                    this.iap.create(item.thumbnail_principal, '_blank', options).show();
                }
                else {
                    window.open(item.thumbnail_principal, '_system');
                }
            }
            else {
                this.iap.create(item.thumbnail_principal, '_blank', options).show();
                // window.open(item.thumbnail_principal, '_system');
            }
            this._categorias.visualizar(item.id).subscribe(function (e) {
                item.assistido = 1;
            });
        }
        else {
            this.getNavCtrl().push(__WEBPACK_IMPORTED_MODULE_4__pages_player_player__["a" /* PlayerPage */], { item: item });
        }
    };
    PlayerProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_7__global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_8__categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_9__aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_10__helpers_helpers__["a" /* HelpersProvider */]])
    ], PlayerProvider);
    return PlayerProvider;
}());

//# sourceMappingURL=player.js.map

/***/ }),

/***/ 162:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 162;

/***/ }),

/***/ 17:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the GlobalProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GlobalProvider = /** @class */ (function () {
    function GlobalProvider(http) {
        var _this = this;
        this.http = http;
        this.loadingApp = true;
        this.apiUrl = 'https://corporativo.grupoeasylife.com.br/api';
        //apiUrl = 'http://easycorp.mw10.com.br/api';
        // apiUrl = 'http://localhost:8080/easycorporativoadmin/public/api';
        // apiUrl = 'http://localhost:8000/api';
        this.informacoes = {};
        this.conheca = {};
        this.informacoes = JSON.parse(window.localStorage.getItem('informacoes'));
        this.informacoes = this.informacoes ? this.informacoes : {};
        this.http.get(this.apiUrl + '/informacoes').subscribe(function (r) {
            _this.informacoes = r;
            window.localStorage.setItem('informacoes', JSON.stringify(_this.informacoes));
        });
        this.conheca = JSON.parse(window.localStorage.getItem('conheca'));
        this.conheca = this.conheca ? this.conheca : {};
        this.http.get(this.apiUrl + '/conheca').subscribe(function (r) {
            _this.conheca = r;
            window.localStorage.setItem('conheca', JSON.stringify(_this.conheca));
        });
    }
    GlobalProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], GlobalProvider);
    return GlobalProvider;
}());

//# sourceMappingURL=global.js.map

/***/ }),

/***/ 18:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var CategoriasProvider = /** @class */ (function () {
    function CategoriasProvider(api, toastCtrl, aplicativos) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.aplicativos = aplicativos;
        this.list = [];
        this.watchToggle = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.current = {};
        console.log('Hello CategoriasProvider Provider');
    }
    CategoriasProvider.prototype.get = function (url) {
        if (!url) {
            url = '/categorias?aplicativo_id=' + this.aplicativos.item.id;
        }
        return this.api.get(url);
    };
    CategoriasProvider.prototype.getAll = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        options.completo = options.completo !== undefined ? options.completo : true;
        if (options.completo) {
            options.completo = '&completo=true';
        }
        else {
            options.completo = '';
        }
        var opened = this.list.find(function (categoria) { return categoria.open; });
        return new Promise(function (resolve, reject) {
            if (_this._getAll && !_this._getAll.closed) {
                _this._getAll.unsubscribe();
            }
            _this._getAll = _this.get('/categorias?aplicativo_id=' + _this.aplicativos.item.id + options.completo).map(function (r) {
                _this.list = r;
                _this.list = _this.list.map(function (categoria) { return _this.parseCategoria(categoria, opened); });
                return _this.list;
            }).subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    CategoriasProvider.prototype.getTopicos = function (categoria_id) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.get('/categorias/' + categoria_id + '/topicos').subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    CategoriasProvider.prototype.parseCategoria = function (categoria, opened) {
        categoria.open = opened && opened.id == categoria.id ? true : false;
        categoria.topicos = categoria.topicos.map(function (topico) {
            topico.topicos = topico.topicos.map(function (topico2) {
                topico2.temAudio = topico2.conteudos.find(function (conteudo) { return conteudo.tipo == 'audio'; });
                topico2.temVideo = topico2.conteudos.find(function (conteudo) { return conteudo.tipo == 'video'; });
                return topico2;
            });
            topico.temAudio = topico.conteudos.find(function (conteudo) { return conteudo.tipo == 'audio'; }) || topico.topicos.find(function (topico2) { return topico2.temAudio; });
            topico.temVideo = topico.conteudos.find(function (conteudo) { return conteudo.tipo == 'video'; }) || topico.topicos.find(function (topico2) { return topico2.temVideo; });
            return topico;
        });
        categoria.temAudio = categoria.topicos.find(function (topico) { return topico.temAudio; });
        categoria.temVideo = categoria.topicos.find(function (topico) { return topico.temVideo; });
        return categoria;
    };
    CategoriasProvider.prototype.visualizar = function (conteudo_id) {
        this.list = this.list.map(function (categoria) {
            if (Array.isArray(categoria.topicos)) {
                categoria.topicos = categoria.topicos.map(function (topico) {
                    if (Array.isArray(topico.conteudos)) {
                        topico.conteudos = topico.conteudos.map(function (conteudo) {
                            if (conteudo.id == conteudo_id) {
                                if (!conteudo.assistido) {
                                    // assistiu conteudo adiciona um count nos assistidos
                                    categoria.conteudos_assistido_count++;
                                }
                                conteudo.assistido = 1;
                            }
                            return conteudo;
                        });
                    }
                    if (Array.isArray(topico.topicos)) {
                        topico.topicos = topico.topicos.map(function (topico2) {
                            if (Array.isArray(topico2.conteudos)) {
                                topico2.conteudos = topico2.conteudos.map(function (conteudo2) {
                                    if (conteudo2.id == conteudo_id) {
                                        if (!conteudo2.assistido) {
                                            // assistiu conteudo adiciona um count nos assistidos, somente se o conteudo ainda nao foi assistido ele aumenta o count
                                            categoria.conteudos_assistido_count++;
                                        }
                                        conteudo2.assistido = 1;
                                    }
                                    return conteudo2;
                                });
                            }
                            return topico2;
                        });
                    }
                    return topico;
                });
            }
            return categoria;
        });
        return this.api.patch('/conteudos/' + conteudo_id + '/visualizar');
    };
    CategoriasProvider.prototype.favoritar = function (conteudo) {
        var _this = this;
        console.log("favoritar", conteudo);
        return new Promise(function (resolve) {
            if (conteudo.favoritado != 1) {
                _this.patchFavoritar(conteudo.id).subscribe(function () {
                    _this.toastCtrl.create({ message: conteudo.titulo + ' adicionado aos seus favoritos ', duration: 3000 }).present();
                    resolve();
                });
            }
            else {
                _this.deleteFavoritar(conteudo.id).subscribe(function () {
                    _this.toastCtrl.create({ message: conteudo.titulo + ' removido de seus favoritos ', duration: 3000 }).present();
                    resolve();
                });
            }
        });
    };
    CategoriasProvider.prototype.patchFavoritar = function (conteudo_id) {
        this.list = this.list.map(function (categoria) {
            if (Array.isArray(categoria.topicos)) {
                categoria.topicos = categoria.topicos.map(function (topico) {
                    if (Array.isArray(topico.conteudos)) {
                        topico.conteudos = topico.conteudos.map(function (conteudo) {
                            if (conteudo.id == conteudo_id) {
                                conteudo.favoritado = 1;
                            }
                            return conteudo;
                        });
                    }
                    if (Array.isArray(topico.topicos)) {
                        topico.topicos = topico.topicos.map(function (topico2) {
                            if (Array.isArray(topico2.conteudos)) {
                                topico2.conteudos = topico2.conteudos.map(function (conteudo2) {
                                    if (conteudo2.id == conteudo_id) {
                                        conteudo2.favoritado = 1;
                                    }
                                    return conteudo2;
                                });
                            }
                            return topico2;
                        });
                    }
                    return topico;
                });
            }
            return categoria;
        });
        return this.api.patch('/conteudos/' + conteudo_id + '/favoritar');
    };
    CategoriasProvider.prototype.deleteFavoritar = function (conteudo_id) {
        this.list = this.list.map(function (categoria) {
            if (Array.isArray(categoria.topicos)) {
                categoria.topicos = categoria.topicos.map(function (topico) {
                    if (Array.isArray(topico.conteudos)) {
                        topico.conteudos = topico.conteudos.map(function (conteudo) {
                            if (conteudo.id == conteudo_id) {
                                conteudo.favoritado = 0;
                            }
                            return conteudo;
                        });
                    }
                    if (Array.isArray(topico.topicos)) {
                        topico.topicos = topico.topicos.map(function (topico2) {
                            if (Array.isArray(topico2.conteudos)) {
                                topico2.conteudos = topico2.conteudos.map(function (conteudo2) {
                                    if (conteudo2.id == conteudo_id) {
                                        conteudo2.favoritado = 0;
                                    }
                                    return conteudo2;
                                });
                            }
                            return topico2;
                        });
                    }
                    return topico;
                });
            }
            return categoria;
        });
        return this.api.delete('/conteudos/' + conteudo_id + '/favoritar');
    };
    CategoriasProvider.prototype.toggleItem = function (i) {
        this.list = this.list.map(function (obj, index) {
            //abre aquele especifico
            if (index == i) {
                obj.open = !obj.open;
            }
            else {
                obj.open = false;
            }
            return obj;
        });
    };
    CategoriasProvider.prototype.openItem = function (i) {
        this.list = this.list.map(function (obj, index) {
            //abre aquele especifico
            if (index == i) {
                obj.open = true;
            }
            else {
                obj.open = false;
            }
            return obj;
        });
    };
    CategoriasProvider.prototype.closeAll = function () {
        this.list = this.list.map(function (obj) {
            obj.open = false;
            return obj;
        });
    };
    CategoriasProvider.prototype.find = function (categoria_id) {
        return this.list.find(function (categoria) { return categoria.id == categoria_id; });
    };
    CategoriasProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], CategoriasProvider);
    return CategoriasProvider;
}());

//# sourceMappingURL=categorias.js.map

/***/ }),

/***/ 203:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 203;

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriAlimentosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_alimento__ = __webpack_require__(491);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the EasyfitNutriAlimentosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var EasyfitNutriAlimentosProvider = /** @class */ (function () {
    function EasyfitNutriAlimentosProvider(api, toastCtrl, helpers, auth, alertCtrl) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.helpers = helpers;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.selectedDate = this.helpers.moment().format('YYYY-MM-DD');
        this.weekDays = new Array();
        this.weekDaysFormated = new Array();
        this.tipos = [
            '',
            'CAFÉ DA MANHÃ',
            'ALMOÇO',
            'JANTA',
            'LANCHES'
        ];
        // tipo selecionado
        this.tipo = 1;
        this.loadingPaginate = false;
        this.listPaginate = new Array();
        this.loadingDestaques = false;
        this.destaques = new Array();
        if (!this.day) {
            this.day = {};
        }
        this.clearDay();
        console.log('Hello GlobalProvider Provider');
        var startOfWeek = this.helpers.moment().startOf('isoWeek');
        var endOfWeek = this.helpers.moment().endOf('isoWeek');
        var day = startOfWeek;
        while (day <= endOfWeek) {
            this.weekDays.push(day);
            this.weekDaysFormated.push(day.format('YYYY-MM-DD'));
            day = day.clone().add(1, 'd');
        }
        console.log(this.weekDays);
    }
    EasyfitNutriAlimentosProvider.prototype.getDestaques = function () {
        var _this = this;
        if (this._getDestaqeus && !this._getDestaqeus.closed) {
            this._getDestaqeus.unsubscribe();
        }
        this.loadingDestaques = true;
        return this.api.get('/alimentos?destaque=1').map(function (r) {
            _this.destaques = r.map(function (obj) { return new __WEBPACK_IMPORTED_MODULE_2__models_alimento__["a" /* Alimento */](obj); });
            _this.loadingDestaques = false;
            return _this.destaques;
        });
    };
    EasyfitNutriAlimentosProvider.prototype.getPaginate = function (options, load) {
        var _this = this;
        if (load === void 0) { load = false; }
        if (this._getPaginate && !this._getPaginate.closed) {
            this._getPaginate.unsubscribe();
        }
        this.loadingPaginate = load;
        return this.api.get('/alimentos?page=' + options.page + '&search=' + options.search + '&favoritado=' + options.favoritado).map(function (r) {
            _this.loadingPaginate = false;
            var array = r.data.map(function (obj) { return new __WEBPACK_IMPORTED_MODULE_2__models_alimento__["a" /* Alimento */](obj); });
            _this.listPaginate = options.page === 1 ? array : _this.listPaginate.concat(array);
            return r;
        });
    };
    EasyfitNutriAlimentosProvider.prototype.getDay = function (clear) {
        var _this = this;
        if (clear === void 0) { clear = true; }
        return new Promise(function (resolve) {
            if (_this.day._get && !_this.day._get.closed) {
                _this.day._get.unsubscribe();
            }
            if (clear) {
                _this.clearDay();
            }
            _this.day.loading = true;
            _this.day._get = _this.api.get('/users/' + _this.auth.user.user.id + '/alimentos?data=' + _this.selectedDate).subscribe(function (r) {
                _this.clearDay();
                _this.day.loading = false;
                r = r.map(function (obj) { return new __WEBPACK_IMPORTED_MODULE_2__models_alimento__["a" /* Alimento */](obj); });
                _this.day.consumidoKcal = 0;
                // Objetivo separar o array list pelos tipos 
                _this.tipos.forEach(function (tipo, i) {
                    r.forEach(function (obj) {
                        if (i == obj.pivot.tipo) {
                            _this.day.list[i].push(obj);
                        }
                    });
                });
                console.log(_this.day.list);
                r.forEach(function (obj) {
                    console.log(obj);
                    _this.day.consumidoKcal += obj.pivot.energia;
                    _this.day.consumidoProteina += obj.pivot.proteina;
                    _this.day.consumidoCarboidratos += obj.pivot.carboidratos;
                    _this.day.consumidoGordura += obj.pivot.gordura;
                });
                _this.day.restanteKcal = _this.auth.user.user.objetivo_kcal - _this.day.consumidoKcal;
                // nao  mostra numero negativo
                _this.day.restanteKcal = _this.day.restanteKcal < 0 ? 0 : _this.day.restanteKcal;
                resolve();
            }, function (e) {
                _this.day.loading = false;
                resolve();
            });
        });
    };
    EasyfitNutriAlimentosProvider.prototype.clearDay = function () {
        var _this = this;
        if (!this.day) {
            this.day = {};
        }
        this.day.restanteKcal = this.auth.user.user.objetivo_kcal;
        this.day.consumidoKcal = 0;
        this.day.consumidoProteina = 0;
        this.day.consumidoCarboidratos = 0;
        this.day.consumidoGordura = 0;
        this.day.list = [];
        // Inicializa a array list para cada tipo, para q quando facamos o request nao de undefined index no momento do .push
        this.tipos.forEach(function (tipo, i) { return _this.day.list[i] = new Array(); });
    };
    EasyfitNutriAlimentosProvider.prototype.favoritar = function (alimento) {
        var _this = this;
        console.log("favoritar", alimento);
        return new Promise(function (resolve) {
            if (alimento.favoritado != 1) {
                _this.patchFavoritar(alimento.id).subscribe(function () {
                    _this.toastCtrl.create({ message: alimento.titulo + ' adicionado aos seus favoritos ', duration: 3000 }).present();
                    alimento.favoritado = 1;
                    resolve(alimento);
                });
            }
            else {
                _this.deleteFavoritar(alimento.id).subscribe(function () {
                    _this.toastCtrl.create({ message: alimento.titulo + ' removido de seus favoritos ', duration: 3000 }).present();
                    alimento.favoritado = 0;
                    resolve(alimento);
                });
            }
        });
    };
    EasyfitNutriAlimentosProvider.prototype.patchFavoritar = function (alimento_id) {
        this.listPaginate = this.listPaginate.map(function (alimento) {
            if (alimento.id == alimento_id) {
                alimento.favoritado = 1;
            }
            return alimento;
        });
        return this.api.patch('/alimentos/' + alimento_id + '/favoritar');
    };
    EasyfitNutriAlimentosProvider.prototype.deleteFavoritar = function (alimento_id) {
        this.listPaginate = this.listPaginate.map(function (alimento) {
            if (alimento.id == alimento_id) {
                alimento.favoritado = 0;
            }
            return alimento;
        });
        return this.api.delete('/alimentos/' + alimento_id + '/favoritar');
    };
    EasyfitNutriAlimentosProvider.prototype.delete = function (item, tipo) {
        var _this = this;
        this.alertCtrl.create({
            message: 'Você tem certeza que deseja deletar esse alimento?',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel'
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.day.list[tipo] = _this.day.list[tipo].filter(function (obj) { return obj.id != item.id; });
                        _this.api.delete('/users/' + _this.auth.user.user.id + '/alimentos/' + item.id, { data: item.pivot.data, tipo: item.pivot.tipo }).subscribe();
                        _this.getDay(false);
                    }
                }
            ]
        }).present();
    };
    EasyfitNutriAlimentosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_5__auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */]])
    ], EasyfitNutriAlimentosProvider);
    return EasyfitNutriAlimentosProvider;
}());

//# sourceMappingURL=easyfit-nutri-alimentos.js.map

/***/ }),

/***/ 373:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_tabs_tabs__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CategoriasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CategoriasPage = /** @class */ (function () {
    function CategoriasPage(navCtrl, navParams, tabs, _categorias, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tabs = tabs;
        this._categorias = _categorias;
        this.loading = loading;
        if (this._categorias.list.length == 0) {
            this.loading.present();
            this._categorias.getAll();
        }
    }
    CategoriasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoriasPage');
    };
    CategoriasPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._categorias.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CategoriasPage.prototype.onRefresher = function () {
        this.ionViewDidEnter();
    };
    CategoriasPage.prototype.openRoteiro = function (index) {
        var _this = this;
        this._categorias.closeAll();
        this.tabs.selectTab(0);
        setTimeout(function () { return _this._categorias.watchToggle.next(index); }, 400);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CategoriasPage.prototype, "refresher", void 0);
    CategoriasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-categorias',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/categorias/categorias.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Home"></header>\n</ion-header>\n\n\n<ion-content #content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="categorias">\n    <div class="item item-100">\n      <img [src]="_categorias.list[1]?.thumbnail_principal" default="assets/imgs/categorias-orange.png" alt=""  >\n      <div class="top" cursor-pointer (click)="openRoteiro(0)">\n        <div class="play"></div> {{ _categorias.list[0]?.titulo }}\n      </div>\n      <button cursor-pointer (click)="openRoteiro(1)" style="width: 100%;height:100%;position:absolute;opacity: 0;"></button>\n      <div class="action" cursor-pointer (click)="openRoteiro(1)">\n        <div class="play" *ngIf="_categorias.list[1]"></div>\n        <div class="title">{{ _categorias.list[1]?.titulo }}</div>\n      </div>\n    </div>\n    <button ion-item details-none no-lines class="item" tappable cursor-pointer (click)="openRoteiro(2)">\n      <img [src]="_categorias.list[2]?.thumbnail_principal" default="assets/imgs/categorias-green.png" alt="">\n      <div class="action">\n        <div class="play" *ngIf="_categorias.list[2]"></div>\n        <div class="title">{{ _categorias.list[2]?.titulo }}</div>\n      </div>\n    </button>\n    <button ion-item details-none no-lines class="item" tappable cursor-pointer (click)="openRoteiro(3)">\n      <img [src]="_categorias.list[3]?.thumbnail_principal" default="assets/imgs/categorias-blue.png" alt="">\n      <div class="action">\n        <div class="play" *ngIf="_categorias.list[3]"></div>\n        <div class="title">{{ _categorias.list[3]?.titulo }}</div>\n      </div>\n    </button>\n    <button ion-item details-none no-lines class="item" tappable *ngFor="let categoria of _categorias.list; let key = index" cursor-pointer (click)="openRoteiro(key)" [hidden]="key < 4">\n      <img [src]="categoria.thumbnail_principal" default="assets/imgs/categorias-blue.png" alt="">\n      <div class="action">\n        <div class="play" *ngIf="categoria"></div>\n        <div class="title">{{ categoria.titulo }}</div>\n      </div>\n    </button>\n\n    <div class="isotipo"></div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/categorias/categorias.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_tabs_tabs__["a" /* TabsProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */]])
    ], CategoriasPage);
    return CategoriasPage;
}());

//# sourceMappingURL=categorias.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FraseDoDiaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the FraseDoDiaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FraseDoDiaPage = /** @class */ (function () {
    function FraseDoDiaPage(navCtrl, navParams, api) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.frase = {};
        this.api.get('/frases').subscribe(function (r) {
            _this.frase = r;
        });
    }
    FraseDoDiaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FraseDoDiaPage');
    };
    FraseDoDiaPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.api.get('/frases').subscribe(function (r) {
            _this.frase = r;
        });
    };
    FraseDoDiaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-frase-do-dia',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/frase-do-dia/frase-do-dia.html"*/'<!--\n  Generated template for the FraseDoDiaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Motive-se"></header>\n</ion-header>\n\n\n<ion-content>\n  <div class="frase-do-dia" [ngStyle]="{\'background-image\': \'url(\'+ (frase.thumbnail_principal ? frase.thumbnail_principal : \'../assets/imgs/frase-do-dia.png\') +\')\', color: frase.cor }">\n    <div class="icone">\n      <ion-icon name="duo-quotes"></ion-icon>\n    </div>\n    <div class="frase" [ngStyle]="{\'font-size\': frase.titulo?.length > 180 ? \'24px\' : \'\'}">\n      {{ frase.titulo }}\n      <span *ngIf="!frase.titulo">&ensp;</span>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/frase-do-dia/frase-do-dia.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */]])
    ], FraseDoDiaPage);
    return FraseDoDiaPage;
}());

//# sourceMappingURL=frase-do-dia.js.map

/***/ }),

/***/ 376:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AudiosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AudiosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AudiosPage = /** @class */ (function () {
    function AudiosPage(navCtrl, navParams, _categorias) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._categorias = _categorias;
        this.conteudos = [];
        this.audios = [];
        this.onlyAudios();
    }
    AudiosPage.prototype.onRefresher = function () {
        var _this = this;
        this._categorias.getAll().then(function () {
            _this.refresher.complete();
            _this.onlyAudios();
        }, function () { return _this.refresher.complete(); });
    };
    AudiosPage.prototype.onlyAudios = function () {
        this.audios = this._categorias.list.map(function (categoria) {
            categoria.topicos = categoria.topicos.map(function (topico) {
                topico.conteudos = topico.conteudos.filter(function (conteudo) { return conteudo.tipo == 'audio'; });
                topico.topicos = topico.topicos.map(function (topico2) {
                    topico2.conteudos = topico2.conteudos.filter(function (conteudo) { return conteudo.tipo == 'audio'; });
                    return topico2;
                });
                return topico;
            });
            return categoria;
        });
    };
    AudiosPage.prototype.toggleItem = function (i) {
        this.audios = this.audios.map(function (obj, index) {
            //abre aquele especifico
            if (index == i) {
                obj.open = !obj.open;
            }
            else {
                obj.open = false;
            }
            return obj;
        });
        this.conteudos = this.audios[i].topicos;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], AudiosPage.prototype, "refresher", void 0);
    AudiosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-audios',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/audios/audios.html"*/'<!--\n  Generated template for the AudiosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Áudios"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="easymind-list" padding-horizontal>\n\n    <div class="list-item" *ngFor="let item of audios; let i = index;" [hidden]="!item.temAudio"\n      [ngClass]="{\'open\': item.open}">\n      <div class="list-title" cursor-pointer (click)="toggleItem(i)">\n        {{ item.titulo }}\n        <ion-icon name="ios-arrow-forward"></ion-icon>\n      </div>\n      <div class="sublist">\n        <ng-container *ngFor="let topico of item.topicos">\n          <ng-container *ngIf="topico.temAudio">\n            <!--mostra somente qndo a lista de topicos > 1 e o titulo for direito do do roteiro-->\n            <div class="sublist-item sublist-item-100" *ngIf="item.topicos.length > 1 && topico.titulo != item.titulo">\n              <div class="sublist-title sublist-title-upper"><b>{{ topico.titulo }}</b></div>\n            </div>\n            <sublist-item *ngFor="let subitem of topico.conteudos" [item]="subitem"></sublist-item>\n\n\n            <ng-container *ngFor="let topico2 of topico.topicos">\n              <ng-container *ngIf="topico2.temAudio">\n                <!--mostra somente qndo a lista de topicos > 1 e o titulo for direito do do roteiro-->\n                <div class="sublist-item sublist-item-100">\n                  <div class="sublist-title"><b>{{ topico2.titulo }}</b></div>\n                </div>\n                <sublist-item *ngFor="let subitem2 of topico2.conteudos" [item]="subitem2"></sublist-item>\n              </ng-container>\n            </ng-container>\n\n          </ng-container>\n        </ng-container>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/audios/audios.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__["a" /* CategoriasProvider */]])
    ], AudiosPage);
    return AudiosPage;
}());

//# sourceMappingURL=audios.js.map

/***/ }),

/***/ 377:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UploadsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_api__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the UploadsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UploadsProvider = /** @class */ (function () {
    function UploadsProvider(loadingCtrl, alertCtrl, toastCtrl, api, plt) {
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.plt = plt;
        this.multiInfo = {
            files: new Array(),
            toSend: 0,
            sent: 0,
            failed: new Array(),
            loadingStr: '',
            url: '',
            loading: this.loadingCtrl.create({ content: '' }),
            response: []
        };
        console.log('Hello UploadsProvider Provider');
    }
    UploadsProvider.prototype.multipleUpload = function (file, options) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.multiInfo.loading.present();
            _this.multiInfo.loadingStr = 'Enviando ' + (_this.multiInfo.sent + 1) + '/' + _this.multiInfo.toSend + ' arquivos';
            _this.api.uploadProgress.subscribe(function (r) {
                var porcentagem = ' ' + r + '%';
                _this.multiInfo.loading.setContent(_this.multiInfo.loadingStr);
                /**
                 * @todo arrumar porcentagem, nao está calculando corretamente eu acredito
                 */
                // this.multiInfo.loading.setContent(this.multiInfo.loadingStr + ' - ' + porcentagem);
            });
            _this.api.uploadFiles(file, _this.multiInfo.url).then(function (r) {
                _this.multiInfo.response.push(r);
                console.log('multipleUpload => r', r);
                _this.multiInfo.sent++;
                if (_this.multiInfo.sent < _this.multiInfo.toSend) {
                    _this.multipleUpload(_this.multiInfo.files[_this.multiInfo.sent], options);
                }
                else {
                    resolve(_this.multiInfo.response);
                }
            }).catch(function (e) {
                console.error('uploadMultiple => catch ', e);
                _this.multiInfo.failed.push(_this.multiInfo.files[_this.multiInfo.sent]);
                _this.multiInfo.sent++;
                if (_this.multiInfo.sent < _this.multiInfo.toSend) {
                    _this.multipleUpload(_this.multiInfo.files[_this.multiInfo.sent], options);
                }
                else {
                    resolve(_this.multiInfo.response);
                }
            });
        });
    };
    UploadsProvider.prototype.startMultipleUpload = function (array, url, options) {
        var _this = this;
        if (!Array.isArray(array)) {
            array = [array];
        }
        return new Promise(function (resolve, reject) {
            _this.multiInfo.url = url;
            _this.multiInfo.files = array.filter(function (obj) { return obj; });
            _this.multiInfo.toSend = array.length;
            if (_this.multiInfo.files[0]) {
                _this.multipleUpload(_this.multiInfo.files[0], options).then(function (r) {
                    resolve(_this.multiInfo.response);
                    _this.onEndMultipleUpload();
                });
            }
            else {
                reject('Nenhum arquivo foi informado');
            }
        });
    };
    UploadsProvider.prototype.onEndMultipleUpload = function () {
        if (this.multiInfo.failed.length) {
            var strFailed = this.multiInfo.failed.map(function (v) { return v.split('/').pop() + "\n"; });
            var alert_1 = this.alertCtrl.create({
                title: 'Erro ao enviar arquivos',
                message: 'Não foi possível enviar ' + this.multiInfo.failed.length + ' arquivos: \n' + strFailed
            });
            alert_1.present();
        }
        if (this.multiInfo.failed.length !== this.multiInfo.files.length) {
            var toast = this.toastCtrl.create({ message: (this.multiInfo.files.length - this.multiInfo.failed.length) + ' arquivos enviados', duration: 3000 });
            toast.present();
        }
        this.resetMultiInfo();
    };
    UploadsProvider.prototype.resetMultiInfo = function () {
        this.multiInfo.loading.dismiss();
        this.multiInfo = {
            files: new Array(),
            toSend: 0,
            sent: 0,
            failed: new Array(),
            loadingStr: '',
            loading: this.loadingCtrl.create({ content: '' }),
            url: '',
            response: []
        };
    };
    UploadsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */]])
    ], UploadsProvider);
    return UploadsProvider;
}());

//# sourceMappingURL=uploads.js.map

/***/ }),

/***/ 378:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriasListaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__player_player__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_categorias_categorias__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CategoriasListaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CategoriasListaPage = /** @class */ (function () {
    function CategoriasListaPage(navCtrl, navParams, _categorias, loading) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._categorias = _categorias;
        this.loading = loading;
        this.conteudos = [];
        if (this._categorias.list.length == 0) {
            this.loading.present();
            this.onRefresher();
        }
        this._categorias.watchToggle.subscribe(function (index) { return _this.toggleItem(index); });
    }
    CategoriasListaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CategoriasListaPage');
    };
    CategoriasListaPage.prototype.onRefresher = function () {
        var _this = this;
        this._categorias.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CategoriasListaPage.prototype.playerPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__player_player__["a" /* PlayerPage */], { item: item });
    };
    CategoriasListaPage.prototype.toggleItem = function (i) {
        this._categorias.toggleItem(i);
        this.conteudos = this._categorias.list[i].topicos;
        this.content.scrollToTop(this.itemsRef.toArray()[i].nativeElement.offsetTop);
        this.content.resize();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CategoriasListaPage.prototype, "refresher", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], CategoriasListaPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_10" /* ViewChildren */])('itemsRef'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["U" /* QueryList */])
    ], CategoriasListaPage.prototype, "itemsRef", void 0);
    CategoriasListaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-categorias-lista',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/categorias-lista/categorias-lista.html"*/'<!--\n  Generated template for the RoteirosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Roteiros"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="easymind-list" padding-horizontal>\n\n    <div #itemsRef class="list-item" *ngFor="let item of _categorias.list; let i = index;" [ngClass]="{\'open\': item.open}">\n      <!-- <div style="        position: absolute;\n      top: 3px;\n      left: 0;\n      font-size: 12px;">{{ item.conteudos_assistido_count +\'/\'+ item.conteudos_count }}</div> -->\n      <div class="list-title" cursor-pointer (click)="toggleItem(i)">\n        {{ item.titulo }} \n        <ion-icon name="ios-arrow-forward"></ion-icon>\n      </div>\n\n      <div class="sublist">\n\n\n\n        <ng-container *ngFor="let topico of item.topicos">\n          <!--mostra somente qndo a lista de topicos > 1 e o titulo for direito do do roteiro-->\n          <div class="sublist-item sublist-item-100" *ngIf="item.topicos.length > 1 && topico.titulo != item.titulo">\n            <div class="sublist-title sublist-title-upper"><b>{{ topico.titulo }}</b></div>\n          </div>\n          <sublist-item *ngFor="let subitem of topico.conteudos" [item]="subitem"></sublist-item>\n\n          <ng-container *ngFor="let topico2 of topico.topicos">\n            <!--mostra somente qndo a lista de topicos > 1 e o titulo for direito do do roteiro-->\n            <div class="sublist-item sublist-item-100">\n              <div class="sublist-title"><b>{{ topico2.titulo }}</b></div>\n            </div>\n            <sublist-item *ngFor="let subitem2 of topico2.conteudos" [item]="subitem2"></sublist-item>\n\n\n          </ng-container>\n\n        </ng-container>\n\n\n\n\n\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/categorias-lista/categorias-lista.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */]])
    ], CategoriasListaPage);
    return CategoriasListaPage;
}());

//# sourceMappingURL=categorias-lista.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CertificadosPagarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_global_global__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the CertificadosPagarPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CertificadosPagarPage = /** @class */ (function () {
    function CertificadosPagarPage(navCtrl, navParams, helpers, auth, api, loading, alertCtrl, categorias, global, plt) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.helpers = helpers;
        this.auth = auth;
        this.api = api;
        this.loading = loading;
        this.alertCtrl = alertCtrl;
        this.categorias = categorias;
        this.global = global;
        this.plt = plt;
        this.cc = {
            name: '',
            number: '',
            month: '',
            year: '',
            cvv: ''
        };
        this.emailsArray = [this.auth.user.user.email];
        this.nome = '';
        this.certificado = {};
        this.init();
    }
    CertificadosPagarPage.prototype.init = function () {
        var _this = this;
        this.getCertificados();
        this.categoria = this.navParams.get('categoria');
        if (!this.categoria) {
            this.navCtrl.pop();
            return;
        }
        this.categorias.get('/categorias/' + this.categoria.id).subscribe(function (r) {
            if (r.conteudos_assistido_count != r.conteudos_assistido_count) {
                _this.navCtrl.pop();
                _this.helpers.toast('Você precisa ver todos os conteúdos antes de poder fazer o download do certificado');
            }
        }, function (e) {
            _this.navCtrl.pop();
        });
    };
    CertificadosPagarPage.prototype.getCertificados = function () {
        var _this = this;
        this.loading.present();
        this.api.get('/certificados').subscribe(function (r) {
            console.log(r);
            _this.certificado = r[0];
            _this.loading.dismiss();
        }, function (e) {
            _this.loading.dismiss();
            _this.alertCtrl.create({
                title: 'Ops...',
                message: 'Não foi possível conectar-se aos servidor da EasyLife, deseja tentar novamente?',
                buttons: [
                    {
                        role: 'cancel',
                        text: 'Não',
                        handler: function () {
                            _this.navCtrl.pop();
                        }
                    },
                    {
                        text: 'Sim',
                        handler: function () {
                            _this.getCertificados();
                        }
                    }
                ]
            }).present();
        });
    };
    CertificadosPagarPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CertificadosPagarPage');
    };
    CertificadosPagarPage.prototype.onSubmit = function (form) {
        var _this = this;
        this.helpers.formMarkAllTouched(form);
        if (!this.nome.trim().length) {
            return this.helpers.toast('Preencha o nome que será colocado no certificado');
        }
        if (!this.emailsArray[0].length) {
            return this.helpers.toast('Coloque pelo menos 1 e-mail que será enviado o certificado');
        }
        if (this.cc.name.split(' ').length < 2) {
            return this.helpers.toast('Preencha o nome completo que está no cartão de crédito');
        }
        if (!this.cc.month.trim().length) {
            return this.helpers.toast('Preencha o mês de validade do cartão de crédito');
        }
        if (this.cc.year.length != 2) {
            return this.helpers.toast('Preencha o ano de validade do cartão de crédito');
        }
        if (!this.cc.cvv.length) {
            return this.helpers.toast('Preencha o código de segurança do cartão de crédito');
        }
        if (!form.valid) {
            return this.helpers.toast('Verifique os campos em vermelho');
        }
        this.loading.present();
        this.api.post('/certificados/pagar', {
            nome: this.nome,
            cc: this.cc,
            emails: this.emailsArray.join(','),
            categoria_id: this.categoria.id,
            certificado_id: this.certificado.id
        }).subscribe(function (r) {
            _this.loading.dismiss();
            _this.alertCtrl.create({
                message: r.message,
                buttons: [
                    {
                        text: 'Ver certificado',
                        handler: function () {
                            if (_this.plt.is('ios')) {
                                window.open(r.link, '_system');
                            }
                            else {
                                window.open(r.link, '_blank');
                            }
                        }
                    }
                ]
            }).present();
            _this.navCtrl.pop();
        }, function (e) {
            _this.loading.dismiss();
        });
    };
    CertificadosPagarPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-certificados-pagar',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/certificados-pagar/certificados-pagar.html"*/'<ion-header>\n  <header title="Certificado" [hideButtons]="hideButtons"></header>\n</ion-header>\n\n\n<ion-content>\n  <form #form="ngForm" class="max-width-600">\n    <div class="card">\n      <div class="card__line"></div>\n      <div class="card__line card__line--2"></div>\n      <div class="card__number" *ngIf="cc.number">{{cc.number | mask: \'0000 0000 0000 0000\'}}</div>\n      <div class="card__number" *ngIf="!cc.number">0000 0000 0000 0000</div>\n      <div class="card__name" *ngIf="cc.name">{{cc.name}}</div>\n      <div class="card__name" *ngIf="!cc.name">XXXXX XXXXXXX</div>\n      <div class="card__cvv" *ngIf="cc.cvv">{{cc.cvv}}</div>\n      <div class="card__cvv" *ngIf="!cc.cvv">000</div>\n      <div class="card__date" *ngIf="cc.month && cc.year">{{cc.month}}/{{cc.year}}</div>\n      <div class="card__date" *ngIf="!(cc.month && cc.year)">00/00</div>\n\n    </div>\n    <div padding style="white-space: pre-wrap;" [innerHtml]="global?.informacoes?.certificado?.concluir_pagamento | linkify"></div>\n    <div padding-top padding-horizontal><strong><small>{{ global?.informacoes?.certificado?.titulo_dados_do_certificado }}</small></strong></div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <label>{{ global?.informacoes?.certificado?.label_nome_no_certificado }}</label>\n          <input required type="text" [(ngModel)]="nome" name="nome" required [placeholder]="global?.informacoes?.certificado?.placeholder_nome_no_certificado">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <label>{{ global?.informacoes?.certificado?.campo_enviar_certificado_para }}</label>\n          <!-- <div *ngFor="let email of emailsArray;let i = index" style="position: relative"> -->\n            <input required type="email" [name]="\'email\'+i" placeholder="E-mail "  [(ngModel)]="emailsArray[0]" margin-bottom style="padding-right: 46px;">\n            <!-- <button *ngIf="i" style="position: absolute;right:5px;top:0;width: 36px;min-height:36px;height: 36px;background-color: none;" ion-button icon-only (click)="emailsArray.splice(i, 1)"><ion-icon name="remove"></ion-icon></button> -->\n          <!-- </div> -->\n          <!-- <div text-center><button style="width: 36px;min-height:36px;height: 36px;" ion-button icon-only (click)="emailsArray[emailsArray.length - 1].length && emailsArray.push(\'\')"><ion-icon name="add"></ion-icon></button></div> -->\n          <!-- <small>Você pode enviar o certificado para mais de um e-mail, digite acima os e-mails</small> -->\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    <div padding-top padding-horizontal><strong><small>{{ global?.informacoes?.certificado?.titulo_dados_do_cartao }}</small></strong></div>\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <label>Nome no cartão</label>\n          <input required type="text" [(ngModel)]="cc.name" name="ccname" required placeholder="Digite nome do proprietário">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <label>Número do cartão</label>\n          <input required type="tel" [(ngModel)]="cc.number" name="ccnumber" mask="0000 0000 0000 0000" required\n            placeholder="0000 0000 0000 0000">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <label for="">Validade</label>\n          <ion-grid no-padding>\n            <ion-row no-padding>\n              <ion-col no-padding style="padding-right:5px">\n                <input required type="tel" mask="00" [(ngModel)]="cc.month" name="ccmonth" required placeholder="MÊS" maxlength="2">\n              </ion-col>\n              <ion-col no-padding>\n                <input required type="tel" mask="00" [(ngModel)]="cc.year" name="ccyear" required placeholder="ANO" maxlength="2">\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </ion-col>\n        <ion-col>\n          <label for="">CVV</label>\n          <input required type="tel" placeholder="000" name="cccvv" [(ngModel)]="cc.cvv" maxlength="4">\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <strong>Resumo:</strong><br>\n          {{ categoria.titulo }}<br><br>\n\n          <strong>Total</strong>: {{ certificado.preco | currency:\'BRL\' }}\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <button (click)="onSubmit(form)" ion-button block>Finalizar</button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </form>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/certificados-pagar/certificados-pagar.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */]])
    ], CertificadosPagarPage);
    return CertificadosPagarPage;
}());

//# sourceMappingURL=certificados-pagar.js.map

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitSportsHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cadastro_usuario_cadastro_usuario__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__easyfit_nutri_tabs_easyfit_nutri_tabs__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitSportsHomePage = /** @class */ (function () {
    function EasyfitSportsHomePage(navCtrl, navParams, _categorias, api, loading, aplicativos, auth, app) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._categorias = _categorias;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this.auth = auth;
        this.app = app;
        this.easyfitNutri = this.aplicativos.getEasyfitNutri();
        if (!this._categorias.list.length) {
            this.loading.present();
        }
        this.onRefresher();
    }
    EasyfitSportsHomePage.prototype.onRefresher = function () {
        this.ionViewDidEnter();
    };
    EasyfitSportsHomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        setTimeout(function () {
            _this._categorias.getAll({ completo: false }).then(function (r) {
                _this.loading.dismiss();
                _this.refresher.complete();
            }, function (e) {
                _this.loading.dismiss();
                _this.refresher.complete();
            });
        }, 50);
    };
    EasyfitSportsHomePage.prototype.categoriasConteudosPage = function (item) {
        this._categorias.current = item;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */]);
    };
    EasyfitSportsHomePage.prototype.easyfitNutriPage = function () {
        var _this = this;
        if (!this.auth.user.user.inicio_kg) {
            this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_8__cadastro_usuario_cadastro_usuario__["a" /* CadastroUsuarioPage */], { animate: true, animation: 'ios-transition', duration: 1000 });
        }
        else {
            this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_9__easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */], { animate: true, animation: 'ios-transition', duration: 1000 });
        }
        this.loading.present();
        setTimeout(function () {
            _this.aplicativos.setItem(_this.easyfitNutri);
            _this.loading.dismiss();
        }, 50);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], EasyfitSportsHomePage.prototype, "refresher", void 0);
    EasyfitSportsHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-sports-home',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-sports-home/easyfit-sports-home.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Home"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="categorias"\n    [style.background-image]="aplicativos.item.id == 4 ? \'url(assets/imgs/easylife-fundo.jpg)\' : \'\'">\n    <div class="top" cursor-pointer (click)="categoriasConteudosPage(_categorias.list[0])" *ngIf="aplicativos.item.id != 7">\n      <ion-icon name="duo-play" style="font-size: .7em;transform: translateY(-1px)"></ion-icon>\n      &ensp;{{ _categorias.list[0]?.titulo }}\n    </div>\n    <!-- <div class="item item-100" [ngStyle]="{\'background-image\': \'url(\'+ (_categorias.list[1] ? _categorias.list[1].thumbnail_principal : \'assets/imgs/categorias-orange.png\') +\')\'}" cursor-pointer (click)="moduloPage(_categorias.list[1])">\n        <div class="title">{{ _categorias.list[1]?.titulo }}</div>\n      </div> -->\n\n    <button ion-item detail-none no-lines *ngFor="let item of _categorias.list;let i = index" [hidden]="aplicativos.item.id != 7 && i == 0" class="item"\n      cursor-pointer (click)="categoriasConteudosPage(item)"\n      [ngStyle]="{\'background-image\': \'url(\'+ item.thumbnail_principal +\')\'}">\n      <ion-label class="title">\n        <ion-icon name="duo-play" style="font-size: .7em;"></ion-icon>&ensp;{{ item.titulo }}\n      </ion-label>\n    </button>\n    \n    <button ion-item detail-none no-lines class="item"\n      cursor-pointer (click)="easyfitNutriPage()" *ngIf="aplicativos.item.id == 4"\n      [ngStyle]="{\'background-image\': \'url(\'+ easyfitNutri.fundo +\')\'}">\n      <ion-label class="title">\n        <img src="{{ (easyfitNutri.thumbnail_principal) }}" style="filter: brightness(0) invert(1);\n        width: 40%;\n        min-width: 180px;\n        transform: translate(-50%, -50%);\n        position: absolute;\n        left: 50%;\n        top: 50%;\n        transition: all 0.3s ease;">\n      </ion-label>\n    </button>\n\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-sports-home/easyfit-sports-home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], EasyfitSportsHomePage);
    return EasyfitSportsHomePage;
}());

//# sourceMappingURL=easyfit-sports-home.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastroUsuarioSecondPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_cadastro_cadastro__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__easyfit_nutri_tabs_easyfit_nutri_tabs__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { CadastroUsuarioThirdPage } from './../cadastro-usuario-third/cadastro-usuario-third';






/**
 * Generated class for the CadastroUsuarioSecondPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CadastroUsuarioSecondPage = /** @class */ (function () {
    function CadastroUsuarioSecondPage(navCtrl, navParams, _cadastro, helpers, toastCtrl, api, auth, loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._cadastro = _cadastro;
        this.helpers = helpers;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.auth = auth;
        this.loading = loading;
        this.heightFruits = 0;
    }
    CadastroUsuarioSecondPage.prototype.ionViewDidLoad = function () {
        this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
        if (this.heightFruits < 150) {
            this.heightFruits = 150;
        }
    };
    CadastroUsuarioSecondPage.prototype.cadastroUsuarioThirdPage = function () {
        var _this = this;
        /** @todo descomentar validacao */
        if (!this._cadastro.dados.objetivo_descricao) {
            this.toastCtrl.create({ message: 'Qual o seu objetivo?', duration: 3000 }).present();
            return;
        }
        if (!this._cadastro.dados.objetivo_kg || !(this._cadastro.dados.objetivo_kg < 300 && this._cadastro.dados.objetivo_kg > 0)) {
            this.toastCtrl.create({ message: 'Qual o seu objetivo (kg)?', duration: 3000 }).present();
            return;
        }
        if (!this._cadastro.dados.objetivo_kcal || !(this._cadastro.dados.objetivo_kcal < 10000 && this._cadastro.dados.objetivo_kcal > 0)) {
            this.toastCtrl.create({ message: 'Qual o seu objetivo (kcal/dia)?', duration: 3000 }).present();
            return;
        }
        delete this._cadastro.dados.password;
        delete this._cadastro.dados.first_name;
        this.api.post('/users/' + this.auth.user.user.id, this._cadastro.dados).subscribe(function (r) {
            _this.toastCtrl.create({ message: 'Dados salvos com sucesso!', duration: 4000 }).present();
            _this.loading.dismiss();
            _this.auth.setToken(r.token);
            _this._cadastro.dados = _this.auth.user.user;
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */]);
        }, function (e) { return _this.loading.dismiss(); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], CadastroUsuarioSecondPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('layout'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], CadastroUsuarioSecondPage.prototype, "layout", void 0);
    CadastroUsuarioSecondPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-cadastro-usuario-second',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/cadastro-usuario-second/cadastro-usuario-second.html"*/'<!--\n  Generated template for the CadastroUsuarioSecondPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header [title]="\'\'"></header>\n</ion-header>\n\n\n<ion-content padding32>\n  <div class="max-width-600" #layout>\n    <div class="radio-group-default">\n      <div class="radio-group-title blue-title">\n        QUAL O SEU OBJETIVO?\n      </div>\n      <div class="radio-group-item">\n        <input [(ngModel)]="_cadastro.dados.objetivo_descricao" type="radio" id="opcao1" value="Manter Peso"\n          name="objetivo" checked>\n        <label for="opcao1">MANTER PESO</label>\n      </div>\n      <div class="radio-group-item" margin-top>\n        <input [(ngModel)]="_cadastro.dados.objetivo_descricao" type="radio" id="opcao2" value="Perder Peso"\n          name="objetivo">\n        <label for="opcao2">PERDER PESO</label>\n      </div>\n      <div class="radio-group-item" margin-top>\n        <input [(ngModel)]="_cadastro.dados.objetivo_descricao" type="radio" id="opcao3" value="Ganhar Peso"\n          name="objetivo">\n        <label for="opcao3">GANHAR PESO</label>\n      </div>\n      <form class="forms" style="width:100%">\n        <!-- <input mask="000" pattern="\d*" type="text" placeholder="PESO (kg)" [(ngModel)]="_cadastro.dados.objetivo_kg" name="peso"\n          margin-top> -->\n          <ion-input type="tel"  placeholder="PESO (kg)" details-none\n          [(ngModel)]="_cadastro.dados.objetivo_kg" name="objetivo_kg" [brmasker]="{mask:\'000\', len:3, type: \'num\'}"></ion-input>\n  \n        <!-- <input mask="00000" pattern="\d*"  type="text" placeholder="OBJETIVO (kcal/dia)" [(ngModel)]="_cadastro.dados.objetivo_kcal"\n          name="objetivo_kcal" margin-top> -->\n          <ion-input type="tel"  placeholder="OBJETIVO (kcal/dia)" details-none\n        [(ngModel)]="_cadastro.dados.objetivo_kcal" name="objetivo_kcal" [brmasker]="{mask:\'00000\', len:5, type: \'num\'}"></ion-input>\n      </form>\n    </div>\n\n    <button class="btn-arrow" ion-button margin-top block color="light" cursor-pointer\n      (click)="cadastroUsuarioThirdPage()">\n      AVANÇAR <ion-icon name="ios-arrow-forward"></ion-icon>\n    </button>\n  </div>\n  <div class="max-width-600">\n    <img src="assets/imgs/fruits.png" [style.height.px]="heightFruits" alt="">\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/cadastro-usuario-second/cadastro-usuario-second.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_cadastro_cadastro__["a" /* CadastroProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */]])
    ], CadastroUsuarioSecondPage);
    return CadastroUsuarioSecondPage;
}());

//# sourceMappingURL=cadastro-usuario-second.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Contact\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-list-header>Follow us on Twitter</ion-list-header>\n    <ion-item>\n      <ion-icon name="ionic" item-start></ion-icon>\n      @ionicframework\n    </ion-item>\n  </ion-list>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      About\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 384:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__ = __webpack_require__(145);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var EasyfitNutriHomePage = /** @class */ (function () {
    function EasyfitNutriHomePage(navCtrl, api, global, helpers, auth, _alimentos, _agua, _exercicios) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.global = global;
        this.helpers = helpers;
        this.auth = auth;
        this._alimentos = _alimentos;
        this._agua = _agua;
        this._exercicios = _exercicios;
        this.alimentos = new Array();
        this._exercicios.getDay();
    }
    EasyfitNutriHomePage.prototype.refeicoesPage = function (tipo) {
        this._alimentos.tipo = tipo;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__["a" /* EasyfitNutriRefeicoesPage */]);
    };
    EasyfitNutriHomePage.prototype.refeicoesInternaPage = function (item, tipo) {
        this._alimentos.tipo = tipo;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__["a" /* EasyfitNutriRefeicoesInternaPage */], { item: item, atualizar: true, quantidade: item.pivot.quantidade });
    };
    EasyfitNutriHomePage.prototype.controleAguaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_11__easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__["a" /* EasyfitNutriControleAguaPage */]);
    };
    EasyfitNutriHomePage.prototype.ionViewWillEnter = function () {
        this._alimentos.getDay(false);
        this._agua.get().subscribe();
    };
    EasyfitNutriHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-home',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-home/easyfit-nutri-home.html"*/'<ion-header>\n  <ion-navbar>\n    <button class="menu-toggle" ion-button menuToggle icon-only start>\n      <img src="assets/icon/menu.png" width="30">\n    </button>\n    <home-tabbar></home-tabbar>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding32>\n<div class="max-width-600">\n  <radio-week></radio-week>\n\n  <div class="contador" padding-vertcal>\n    <div class="contador-objetivo">\n      {{ auth.user.user.objetivo_kcal }}\n      <span>OBJETIVO</span>\n    </div>\n    /\n    <div class="contador-restante">\n      {{ _alimentos.day.restanteKcal }}\n      <span>RESTANTE</span>\n    </div>\n  </div>\n\n  <hr>\n\n  <div class="home-listagem">\n    <div text-center margin class="my-spinner" [ngClass]="{active: _alimentos.day.loading}">\n      <ion-spinner></ion-spinner>\n    </div>\n    <div class="home-item" *ngFor="let tipo of _alimentos.tipos; let i = index" [hidden]="!tipo">\n      <div class="top">\n        <span> {{ tipo }} </span>\n        <button ion-button class="btn btn-green" cursor-pointer (click)="refeicoesPage(i)">\n          <ion-icon name="add"></ion-icon>\n        </button>\n      </div>\n      <div class="home-item-food-list">\n\n        <div *ngFor="let item of _alimentos.day.list[i]; let j = index" class="home-item-food-item" ion-item\n          color="white" no-lines no-padding>\n          <ion-avatar item-start cursor-pointer (click)="refeicoesInternaPage(_alimentos.day.list[i][j], i)">\n            <img default="assets/imgs/symbol.png" [src]="item.thumbnail_principal" [alt]="item.titulo">\n          </ion-avatar>\n          <ion-label text-uppercase cursor-pointer (click)="refeicoesInternaPage(_alimentos.day.list[i][j], i)"><b>{{\n                item.pivot?.quantidade }} </b> {{ item.titulo }}</ion-label>\n          <ion-icon name="md-trash" item-end cursor-pointer (click)="_alimentos.delete(item, i)"></ion-icon>\n        </div>\n\n\n\n        <div *ngIf="_alimentos.day.list[i].length == 0" cursor-pointer (click)="refeicoesPage(i)">\n          Nenhum alimento encontrado, clique no botão ao lado para começar!\n        </div>\n\n      </div>\n    </div>\n\n    <!-- <div class="home-item">\n        <div class="top">\n          <span>ALMOÇO</span>\n          <button ion-button class="btn btn-green">\n            <ion-icon name="add"></ion-icon>\n          </button>\n        </div>\n        <div class="home-item-food-list">\n          <button class="home-item-food-item" ion-item color="white" no-lines no-padding>\n            <ion-avatar item-start>\n              <img src="assets/imgs/banana.jpg" alt="food">\n            </ion-avatar>\n            <ion-label text-uppercase>1 banana</ion-label>\n          </button>\n  \n          <button class="home-item-food-item" ion-item color="white" no-lines no-padding>\n            <ion-avatar item-start>\n              <img src="assets/imgs/banana.jpg" alt="food">\n            </ion-avatar>\n            <ion-label text-uppercase>1 banana</ion-label>\n          </button>\n        </div>\n      </div>\n   -->\n    <div class="home-item">\n      <div class="top">\n        <span>ÁGUA</span>\n        <button ion-button class="btn btn-green" cursor-pointer (click)="controleAguaPage()">\n          <ion-icon name="add"></ion-icon>\n        </button>\n      </div>\n\n      <consumo-agua></consumo-agua>\n\n\n    </div>\n  </div>\n\n  <div class="home-banner" *ngIf="global.informacoes.banner">\n    <!-- <h2>BANNER</h2> -->\n    <img cursor-pointer (click)="helpers.openLink(global.informacoes.banner_link, \'_blank\')" [src]="global.informacoes.banner"\n      alt="banner">\n  </div>\n\n</div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-home/easyfit-nutri-home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */],
            __WEBPACK_IMPORTED_MODULE_10__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */]])
    ], EasyfitNutriHomePage);
    return EasyfitNutriHomePage;
}());

//# sourceMappingURL=easyfit-nutri-home.js.map

/***/ }),

/***/ 385:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriCalculoCaloricoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CalculoCaloricoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitNutriCalculoCaloricoPage = /** @class */ (function () {
    function EasyfitNutriCalculoCaloricoPage(navCtrl, navParams, _alimentos, helpers, _exercicios, _agua) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._alimentos = _alimentos;
        this.helpers = helpers;
        this._exercicios = _exercicios;
        this._agua = _agua;
        this.heightFruits = 0;
    }
    EasyfitNutriCalculoCaloricoPage.prototype.ionViewDidLoad = function () {
        this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
        if (this.heightFruits < 150) {
            this.heightFruits = 150;
        }
    };
    EasyfitNutriCalculoCaloricoPage.prototype.getDay = function () {
        this._agua.get().subscribe();
        this._alimentos.getDay();
        this._exercicios.getDay();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], EasyfitNutriCalculoCaloricoPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('layout'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], EasyfitNutriCalculoCaloricoPage.prototype, "layout", void 0);
    EasyfitNutriCalculoCaloricoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-calculo-calorico',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-calculo-calorico/easyfit-nutri-calculo-calorico.html"*/'<ion-header>\n  <header title="CÁLCULO DE CALORIAS"></header>\n</ion-header>\n\n<ion-content padding32>\n  <div class="max-width-600" #layout>\n    <div class="info" text-uppercase>\n    <ion-item no-lines details-none>\n      <ion-label color="secondary" text-wrap>\n        MONITORE o quanto de calorias você ingere ou gasta durante o dia {{\n        helpers.moment(_alimentos.selectedDate).format(\'LL\') }}, Para demais datas\n        <ion-icon name="md-calendar"></ion-icon>\n      </ion-label>\n      <ion-datetime cancelText="Fechar" doneText="Ok" displayFormat="DD/MM/YYYY" min="2018" [(ngModel)]="_alimentos.selectedDate"\n        (ngModelChange)="getDay()">\n      </ion-datetime>\n    </ion-item>\n  </div>\n\n  <h2>TOTAL INGERIDO: {{ _alimentos.day.consumidoKcal | toFixed }} KCAL</h2>\n  <h2 padding-vertical>TOTAL GASTO: {{ _exercicios.totalDay | toFixed }} KCAL</h2>\n  <h2 padding-vertical *ngIf="(_alimentos.day.consumidoKcal - _exercicios.totalDay) > 0">TOTAL: {{\n    _alimentos.day.consumidoKcal - _exercicios.totalDay | toFixed }} KCAL</h2>\n\n  <ul no-padding>\n\n    <li>\n      <span>PROTEÍNA</span>\n      <span>{{ _alimentos.day.consumidoProteina | toFixed }}g</span>\n    </li>\n\n    <li>\n      <span>CARBOIDRATOS</span>\n      <span>{{ _alimentos.day.consumidoCarboidratos | toFixed }}g</span>\n    </li>\n\n    <li>\n      <span>GORDURA</span>\n      <span>{{ _alimentos.day.consumidoGordura | toFixed }}g</span>\n    </li>\n  </ul>\n  </div>\n  <div class="max-width-600">\n    <img src="assets/imgs/fruits.png" [style.height.px]="heightFruits" alt="">\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-calculo-calorico/easyfit-nutri-calculo-calorico.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */]])
    ], EasyfitNutriCalculoCaloricoPage);
    return EasyfitNutriCalculoCaloricoPage;
}());

//# sourceMappingURL=easyfit-nutri-calculo-calorico.js.map

/***/ }),

/***/ 386:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriExerciciosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__easyfit_nutri_exercicio_interna_easyfit_nutri_exercicio_interna__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ExerciciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitNutriExerciciosPage = /** @class */ (function () {
    function EasyfitNutriExerciciosPage(navCtrl, navParams, api, _alimentos, _exercicios, helpers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this._alimentos = _alimentos;
        this._exercicios = _exercicios;
        this.helpers = helpers;
        this.options = {
            search: '',
            page: 1
        };
        this.loading = false;
        this._exercicios.getDay();
        this.doSearch();
    }
    EasyfitNutriExerciciosPage.prototype.ionViewWillEnter = function () {
    };
    EasyfitNutriExerciciosPage.prototype.exercicioInternaPage = function (item, atualiza) {
        var quantidade = item.pivot ? item.pivot.quantidade : null;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__easyfit_nutri_exercicio_interna_easyfit_nutri_exercicio_interna__["a" /* EasyfitNutriExercicioInternaPage */], { item: item, atualiza: atualiza, quantidade: quantidade });
    };
    EasyfitNutriExerciciosPage.prototype.doSearch = function () {
        var _this = this;
        this.loading = true;
        this.options.page = 1;
        this._exercicios._getPaginate = this._exercicios.getPaginate(this.options).subscribe(function (r) {
            _this.loading = false;
        }, function (e) { return _this.loading = false; });
    };
    EasyfitNutriExerciciosPage.prototype.doInfinite = function () {
        var _this = this;
        this.options.page++;
        this._exercicios._getPaginate = this._exercicios.getPaginate(this.options).subscribe(function (r) {
            _this.infiniteScroll.complete();
            if (r.data.length === 0) {
                _this.options.page--;
            }
        }, function (e) { return _this.infiniteScroll.complete(); });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* InfiniteScroll */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* InfiniteScroll */])
    ], EasyfitNutriExerciciosPage.prototype, "infiniteScroll", void 0);
    EasyfitNutriExerciciosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-exercicios',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-exercicios/easyfit-nutri-exercicios.html"*/'<!--\n  Generated template for the ExerciciosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <button class="menu-toggle" ion-button menuToggle icon-only start>\n      <img src="assets/icon/menu.png" width="30">\n    </button>\n    <home-tabbar></home-tabbar>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding32>\n  <div class="max-width-600">\n    <radio-week></radio-week>\n\n  <div class="refeicoes-listagem" margin-top>\n\n    <div text-center margin class="my-spinner" [ngClass]="{active: _exercicios.loadingDay}">\n      <ion-spinner></ion-spinner>\n    </div>\n    <div>\n      <strong>ATIVIDADES FEITAS NO DIA</strong>\n    </div>\n    <div class="refeicoes-item" *ngFor="let item of _exercicios.day; let i = index" color="white" no-margin>\n      <div cursor-pointer (click)="exercicioInternaPage(_exercicios.day[i])" class="refeicoes-item-esquerda" text-wrap>\n        <!-- <div class="refeicoes-item-image">\n          <img default="assets/imgs/symbol.png" [src]="item.thumbnail_principal" alt="">\n        </div> -->\n        <span text-left style="text-transform: uppercase" text-wrap>{{ item.titulo }} / <div class="more-info" color="secondary">{{\n            item.pivot?.energia }} kcal / {{ item.pivot?.quantidade }} MINUTOS</div></span>\n      </div>\n\n      <ion-icon name="md-trash" item-end cursor-pointer (click)="_exercicios.delete(item)"></ion-icon>\n\n    </div>\n    <div text-center padding *ngIf="!_exercicios.loadingDay && _exercicios.day.length == 0">\n      Nenhuma atividade encontrada para o dia {{ helpers.moment(_alimentos.selectedDate).format(\'LL\')}}\n    </div>\n    <hr *ngIf="_exercicios.day.length != 0">\n    <span style="color: #008437" *ngIf="_exercicios.day.length != 0">SOMA TOTAL: {{ _exercicios.totalDay }} KCAL / {{\n      _exercicios.totalMinutesDay }} MINUTOS</span>\n    <hr>\n\n    <form class="search-box">\n      <input type="search" placeholder="BUSCA DE ATIVIDADES" [(ngModel)]="options.search" (input)="doSearch()" name="search">\n    </form>\n\n\n\n    <div class="refeicoes-item" *ngFor="let item of _exercicios.listPaginate; let i = index" color="white" no-margin\n      cursor-pointer (click)="exercicioInternaPage(_exercicios.listPaginate[i])">\n      <div class="refeicoes-item-esquerda">\n        <div class="refeicoes-item-image">\n          <img default="assets/imgs/symbol.png" [src]="item.thumbnail_principal" alt="">\n        </div>\n        <span text-left>{{ item.titulo }}</span>\n      </div>\n      <div class="refeicoes-item-direita">\n        <small>{{ item.energia }}/h</small>\n        <ion-icon name="ios-arrow-forward" color="primary"></ion-icon>\n      </div>\n    </div>\n  </div>\n  <div text-center *ngIf="!loading && _exercicios.listPaginate.length == 0 && infiniteScroll.state == \'enabled\'">\n    Nenhuma atividade encontrado\n  </div>\n  <ion-infinite-scroll (ionInfinite)="doInfinite()">\n    <br><br>\n    <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-exercicios/easyfit-nutri-exercicios.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], EasyfitNutriExerciciosPage);
    return EasyfitNutriExerciciosPage;
}());

//# sourceMappingURL=easyfit-nutri-exercicios.js.map

/***/ }),

/***/ 387:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriExercicioInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_global_global__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the ExercicioInternaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EasyfitNutriExercicioInternaPage = /** @class */ (function () {
    function EasyfitNutriExercicioInternaPage(navCtrl, navParams, api, auth, helpers, _exercicios, _alimentos, toastCtrl, loading, global) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.auth = auth;
        this.helpers = helpers;
        this._exercicios = _exercicios;
        this._alimentos = _alimentos;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.global = global;
        this.heightFruits = 0;
        this.item = {};
        this.item = this.navParams.get('item');
        this.atualizar = this.navParams.get('atualizar');
        this.quantidade = this.navParams.get('quantidade');
        this.energia = this.item.energia;
    }
    EasyfitNutriExercicioInternaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RefeicaoInternaPage');
        this.heightFruits = this.content.getNativeElement().clientHeight - this.layout.nativeElement.clientHeight - 60;
        if (this.heightFruits < 150) {
            this.heightFruits = 150;
        }
    };
    EasyfitNutriExercicioInternaPage.prototype.salvar = function () {
        var _this = this;
        if (!this.quantidade || this.quantidade < 0) {
            this.toastCtrl.create({ message: 'Por favor, informe a quantidade desejada', duration: 4000 }).present();
            return;
        }
        this.loading.present();
        this.request().subscribe(function (r) {
            _this.toastCtrl.create({ message: 'Atividade salva para o dia ' + _this.helpers.moment(_this._alimentos.selectedDate, 'YYYY-MM-DD').format('LL'), duration: 4000 }).present();
            _this._exercicios.getDay().then(function () {
                _this.navCtrl.pop();
                _this.loading.dismiss();
            });
            ;
        });
    };
    EasyfitNutriExercicioInternaPage.prototype.calcular = function () {
        this.energia = Math.ceil((this.item.energia * this.quantidade) / 60);
    };
    EasyfitNutriExercicioInternaPage.prototype.request = function () {
        if (this.atualizar) {
            return this.api.patch('/users/' + this.auth.user.user.id + '/exercicios', {
                exercicio_id: this.item.id,
                data: this._alimentos.selectedDate,
                quantidade: this.quantidade,
            });
        }
        else {
            return this.api.post('/users/' + this.auth.user.user.id + '/exercicios', {
                exercicio_id: this.item.id,
                data: this._alimentos.selectedDate,
                quantidade: this.quantidade,
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], EasyfitNutriExercicioInternaPage.prototype, "content", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('layout'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], EasyfitNutriExercicioInternaPage.prototype, "layout", void 0);
    EasyfitNutriExercicioInternaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-easyfit-nutri-exercicio-interna',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-exercicio-interna/easyfit-nutri-exercicio-interna.html"*/'<ion-header>\n  <header [title]="item.titulo"></header>\n</ion-header>\n\n<ion-content>\n  <div class="max-width-600">\n  <div class="cover-image" *ngIf="item.thumbnail_principal">\n    <img [src]="item.thumbnail_principal" alt="cover-banana">\n  </div>\n\n  <div class="refeicao-interna">\n    <div style="text-transform: uppercase;white-space: pre-wrap">{{ global.informacoes.exercicios_explicacao }}</div>\n    <div class="refeicao-interna-info">\n      <div class="info-item">\n        <span>ENERGIA\n          <small>{{ energia }}kcal</small>\n        </span>\n      </div>\n\n    </div>\n\n    <form class="forms inline-form" (ngSubmit)="salvar()" margin-top>\n      <input mask="000" type="tel" [(ngModel)]="quantidade" (input)="calcular()" name="numero" placeholder="TEMPO (m)">\n      <button class="btn btn-green" no-margin margin-left>SALVAR</button>\n    </form>\n  </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-exercicio-interna/easyfit-nutri-exercicio-interna.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_7__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_global_global__["a" /* GlobalProvider */]])
    ], EasyfitNutriExercicioInternaPage);
    return EasyfitNutriExercicioInternaPage;
}());

//# sourceMappingURL=easyfit-nutri-exercicio-interna.js.map

/***/ }),

/***/ 388:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__contato_contato__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__gestao_cadastro_gestao_cadastro__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_pdfs_pdfs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__gestao_home_gestao_home__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__gestao_listagem_gestao_listagem__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__gestao_imagens_gestao_imagens__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var GestaoTabsPage = /** @class */ (function () {
    function GestaoTabsPage(navParams, nativePageTransitions, _pdfs, aplicativos) {
        this.navParams = navParams;
        this.nativePageTransitions = nativePageTransitions;
        this._pdfs = _pdfs;
        this.aplicativos = aplicativos;
        this.tab0Root = __WEBPACK_IMPORTED_MODULE_4__gestao_cadastro_gestao_cadastro__["a" /* GestaoCadastroPage */];
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_8__gestao_imagens_gestao_imagens__["a" /* GestaoImagensPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_6__gestao_home_gestao_home__["a" /* GestaoHomePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_7__gestao_listagem_gestao_listagem__["a" /* GestaoListagemPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_3__contato_contato__["a" /* ContatoPage */];
        this.loaded = false;
        this.tabIndex = 0;
        this.select = this.navParams.get('select');
        this._pdfs.getAllIfNecessary();
        console.log(aplicativos);
        console.log('item', aplicativos.item);
    }
    GestaoTabsPage.prototype.ionViewDidLoad = function () {
        if (this.select !== undefined) {
            this.tabRef.select(this.select);
        }
    };
    GestaoTabsPage.prototype.getAnimationDirection = function (index) {
        var currentIndex = this.tabIndex;
        this.tabIndex = index;
        switch (true) {
            case (currentIndex < index):
                return ('left');
            case (currentIndex > index):
                return ('right');
        }
    };
    GestaoTabsPage.prototype.transition = function (e) {
        var options = {
            direction: this.getAnimationDirection(e.index),
            duration: 300,
            slowdownfactor: -.5,
            slidePixels: 0,
            iosdelay: 10,
            androiddelay: 10,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 0,
        };
        if (!this.loaded) {
            this.loaded = true;
            return;
        }
        this.nativePageTransitions.slide(options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('tabRef'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* Tabs */])
    ], GestaoTabsPage.prototype, "tabRef", void 0);
    GestaoTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-tabs/gestao-tabs.html"*/'<ion-tabs #tabRef selectedIndex="2" (ionChange)="transition($event)">\n    <ion-tab tabsHideOnSubPages="false" [root]="tab0Root" tabIcon="tab-duo-add"></ion-tab>\n    <ion-tab tabsHideOnSubPages="false" [root]="tab1Root" tabIcon="tab-duo-images"></ion-tab>\n    <ion-tab tabsHideOnSubPages="false" [root]="tab2Root" tabIcon="easymind-isotipo"></ion-tab>\n    <ion-tab tabsHideOnSubPages="false" [root]="tab3Root" tabIcon="tab-duo-book"></ion-tab>\n    <ion-tab tabsHideOnSubPages="false" [root]="tab4Root" tabIcon="tab-duo-phone"></ion-tab>\n  </ion-tabs>\n  '/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-tabs/gestao-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_5__providers_pdfs_pdfs__["a" /* PdfsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], GestaoTabsPage);
    return GestaoTabsPage;
}());

//# sourceMappingURL=gestao-tabs.js.map

/***/ }),

/***/ 389:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__gestao_cadastro_gestao_cadastro__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__gestao_listagem_gestao_listagem__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__gestao_imagens_gestao_imagens__ = __webpack_require__(147);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GestaoHomePage = /** @class */ (function () {
    function GestaoHomePage(navCtrl, navParams, _area, api, loading, aplicativos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._area = _area;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        if (!this._area.list.length) {
            this.loading.present();
        }
        this.onRefresher();
    }
    GestaoHomePage.prototype.onRefresher = function () {
        var _this = this;
        console.log(this._area.getAll());
        this._area.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoHomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._area.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoHomePage.prototype.categoriasConteudosPage = function (item) {
        this._area.current = item;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */]);
    };
    GestaoHomePage.prototype.cadastro = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__gestao_cadastro_gestao_cadastro__["a" /* GestaoCadastroPage */]);
    };
    GestaoHomePage.prototype.listagem = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__gestao_listagem_gestao_listagem__["a" /* GestaoListagemPage */]);
    };
    GestaoHomePage.prototype.imagens = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__gestao_imagens_gestao_imagens__["a" /* GestaoImagensPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], GestaoHomePage.prototype, "refresher", void 0);
    GestaoHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-gestao-home',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-home/gestao-home.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Gestão Consultas" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="item">\n    <button class="botao" (click)="cadastro()">NOVA CONSULTA</button> \n  </div>\n  <div class="item">\n    <button class="botao" (click)="imagens()">MINHAS IMAGENS</button> \n  </div>\n  <div class="item">\n    <button class="botao" (click)="listagem()">LISTAGEM CONSULTAS</button> \n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-home/gestao-home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_8__providers_area_area__["a" /* AreaProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], GestaoHomePage);
    return GestaoHomePage;
}());

//# sourceMappingURL=gestao-home.js.map

/***/ }),

/***/ 391:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CentralTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__central_home_central_home__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__central_notificacoes_central_notificacoes__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__central_noticias_central_noticias__ = __webpack_require__(149);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CentralTabsPage = /** @class */ (function () {
    function CentralTabsPage(nativePageTransitions, navParams, splashScreen) {
        this.nativePageTransitions = nativePageTransitions;
        this.navParams = navParams;
        this.splashScreen = splashScreen;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_5__central_notificacoes_central_notificacoes__["a" /* CentralNotificacoesPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__central_home_central_home__["a" /* CentralHomePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_6__central_noticias_central_noticias__["a" /* CentralNoticiasPage */];
        // tab8Root = DicasPage;
        this.tabIndex = 0;
        this.select = this.navParams.get('select');
    }
    CentralTabsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () { return _this.splashScreen.hide(); }, 600);
        if (this.select !== undefined) {
            this.tabRef.select(this.select);
        }
    };
    CentralTabsPage.prototype.getAnimationDirection = function (index) {
        var currentIndex = this.tabIndex;
        this.tabIndex = index;
        switch (true) {
            case (currentIndex < index):
                return ('left');
            case (currentIndex > index):
                return ('right');
        }
    };
    CentralTabsPage.prototype.transition = function (e) {
        var options = {
            direction: this.getAnimationDirection(e.index),
            duration: 300,
            slowdownfactor: -.5,
            slidePixels: 0,
            iosdelay: 10,
            androiddelay: 10,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 0,
        };
        // if (!this.loaded) {
        //   this.loaded = true;
        //   return;
        // }
        this.nativePageTransitions.slide(options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('tabRef'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* Tabs */])
    ], CentralTabsPage.prototype, "tabRef", void 0);
    CentralTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-tabs/central-tabs.html"*/'<ion-tabs #tabRef selectedIndex="1" (ionChange)="transition($event)">\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab1Root" tabIcon="duo-bell" color="gray"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab2Root" tabIcon="duo-easylife-large"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab3Root" tabIcon="duo-list" color="gray"></ion-tab>\n  </ion-tabs>\n  '/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-tabs/central-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], CentralTabsPage);
    return CentralTabsPage;
}());

//# sourceMappingURL=central-tabs.js.map

/***/ }),

/***/ 392:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CentralHomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__central_notificacoes_central_notificacoes__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__central_noticias_central_noticias__ = __webpack_require__(149);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CentralHomePage = /** @class */ (function () {
    function CentralHomePage(navCtrl, navParams, _area, api, loading, aplicativos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._area = _area;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        if (!this._area.list.length) {
            this.loading.present();
        }
        this.onRefresher();
    }
    CentralHomePage.prototype.onRefresher = function () {
        var _this = this;
        console.log(this._area.getAll());
        this._area.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CentralHomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._area.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    CentralHomePage.prototype.categoriasConteudosPage = function (item) {
        this._area.current = item;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */]);
    };
    CentralHomePage.prototype.noticias = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__central_noticias_central_noticias__["a" /* CentralNoticiasPage */]);
    };
    CentralHomePage.prototype.notificacoes = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__central_notificacoes_central_notificacoes__["a" /* CentralNotificacoesPage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CentralHomePage.prototype, "refresher", void 0);
    CentralHomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-central-home',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-home/central-home.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Central de Relacionamento" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="item">\n    <button class="botao" (click)="notificacoes()">NOTIFICAÇÕES</button> \n  </div>\n  <div class="item">\n    <button class="botao" (click)="noticias()">NOTÍCIAS</button> \n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-home/central-home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_6__providers_area_area__["a" /* AreaProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], CentralHomePage);
    return CentralHomePage;
}());

//# sourceMappingURL=central-home.js.map

/***/ }),

/***/ 393:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var NotificacoesProvider = /** @class */ (function () {
    function NotificacoesProvider(api, toastCtrl, aplicativos, auth) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.aplicativos = aplicativos;
        this.auth = auth;
        this.list = [];
        this.watchToggle = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.current = {};
        console.log('Hello NotificacoesProvider Provider');
    }
    NotificacoesProvider.prototype.get = function (url) {
        if (!url) {
            url = '/users/' + this.auth.user.user.id + '/notifications';
        }
        return this.api.get(url);
    };
    NotificacoesProvider.prototype.getAll = function (area) {
        var _this = this;
        if (area === void 0) { area = null; }
        var opened = this.list.find(function (consultas) { return consultas.open; });
        return new Promise(function (resolve, reject) {
            if (_this._getAll && !_this._getAll.closed) {
                _this._getAll.unsubscribe();
            }
            _this._getAll = _this.get('/users/' + _this.auth.user.user.id + '/notifications').map(function (r) {
                _this.list = r;
                return _this.list;
            }).subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    NotificacoesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__auth_auth__["a" /* AuthProvider */]])
    ], NotificacoesProvider);
    return NotificacoesProvider;
}());

//# sourceMappingURL=notificacoes.js.map

/***/ }),

/***/ 394:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NoticiasProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var NoticiasProvider = /** @class */ (function () {
    function NoticiasProvider(api, toastCtrl, aplicativos) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.aplicativos = aplicativos;
        this.list = [];
        this.watchToggle = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.current = {};
        console.log('Hello NoticiasProvider Provider');
    }
    NoticiasProvider.prototype.get = function (url) {
        if (!url) {
            url = '/noticias';
        }
        return this.api.get(url);
    };
    NoticiasProvider.prototype.getAll = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var opened = this.list.find(function (noticias) { return noticias.open; });
        return new Promise(function (resolve, reject) {
            if (_this._getAll && !_this._getAll.closed) {
                _this._getAll.unsubscribe();
            }
            _this._getAll = _this.get('/noticias').map(function (r) {
                _this.list = r;
                return _this.list;
            }).subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    NoticiasProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], NoticiasProvider);
    return NoticiasProvider;
}());

//# sourceMappingURL=noticias.js.map

/***/ }),

/***/ 395:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CentralNoticiasInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CentralNoticiasInternaPage = /** @class */ (function () {
    function CentralNoticiasInternaPage(navCtrl, navParams, _areas, api, loading, aplicativos, toastCtrl, auth, helpers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._areas = _areas;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.helpers = helpers;
        this.subareas = [];
        this.noticia = {
            titulo: '',
            data: '',
            texto: '',
            thumbnail_principal: ''
        };
        if (!this._areas.list.length) {
            this.loading.present();
        }
        this.onRefresher();
        if (this.navParams.get('item')) {
            this.noticia = this.navParams.get('item');
        }
        this.parentPage = this.navParams.get('parentPage');
    }
    CentralNoticiasInternaPage.prototype.onRefresher = function () {
        this.ionViewDidEnter();
    };
    CentralNoticiasInternaPage.prototype.ionViewDidEnter = function () {
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CentralNoticiasInternaPage.prototype, "refresher", void 0);
    CentralNoticiasInternaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-central-noticias-interna',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-noticias-interna/central-noticias-interna.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="{{noticia.titulo}}" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="cadastro">\n      <img src="{{noticia.thumbnail_principal}}">\n      <div class="titulo">\n          {{noticia.titulo}}\n      </div>\n      <div class="body" [innerHTML]="noticia.texto" >\n          \n      </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/central-noticias-interna/central-noticias-interna.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_area_area__["a" /* AreaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], CentralNoticiasInternaPage);
    return CentralNoticiasInternaPage;
}());

//# sourceMappingURL=central-noticias-interna.js.map

/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__player_player__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SearchPage = /** @class */ (function () {
    function SearchPage(navCtrl, navParams, _categorias, api, _loading) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._categorias = _categorias;
        this.api = api;
        this._loading = _loading;
        this.list = [];
        this.search = '';
        this.loading = false;
        this.search = this.navParams.get('search');
        this.onSearch(this.search.toLocaleLowerCase());
    }
    SearchPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SearchPage');
    };
    SearchPage.prototype.onSearch = function (search) {
        var _this = this;
        this.search = search.toLocaleLowerCase();
        this._loading.present();
        this.loading = true;
        this.api.get('/conteudos/search?search=' + search).subscribe(function (r) {
            _this._loading.dismiss();
            _this.loading = false;
            _this.list = r;
        }, function (e) {
            _this._loading.dismiss();
            _this.loading = false;
        });
    };
    SearchPage.prototype.playerPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__player_player__["a" /* PlayerPage */], { item: item });
    };
    SearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-search',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/search/search.html"*/'<ion-header>\n  <header [title]="search" [showMenu]="false" [searchPage]="true" (searchSubmit)="onSearch($event)"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <div class="easymind-list" padding-horizontal>\n\n    <div class="list-item open">\n      <div class="sublist">\n        <sublist-item *ngFor="let item of list" [forcarFavorito]="true" [item]="item"></sublist-item>\n\n      </div>\n    </div>\n  </div>\n\n  <div padding text-center *ngIf="!list.length && !loading">Nada foi encontrado com a pesquisa "{{ search }}"</div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/search/search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */]])
    ], SearchPage);
    return SearchPage;
}());

//# sourceMappingURL=search.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(422);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_page_transitions__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_onesignal__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_brmasker_ionic_3__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__app_component__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_locales_pt__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_about_about__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_contact_contact__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_document_viewer__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_header_header__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_categorias_categorias__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_frase_do_dia_frase_do_dia__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_favoritos_favoritos__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_audios_audios__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_perfil_perfil__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__directives_directives_module__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__providers_uploads_uploads__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_faca_parte_faca_parte__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_storage__ = __webpack_require__(497);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_player_player__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_codigo_codigo__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__ionic_native_file_chooser__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_search_search__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_esqueci_senha_esqueci_senha__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__components_sublist_item_sublist_item__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_conheca_conheca__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pipes_safe_safe__ = __webpack_require__(505);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pipes_linkify_linkify__ = __webpack_require__(506);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44_ngx_mask__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_login_codigo_login_codigo__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_aplicativos_aplicativos__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_easymind_tabs_easymind_tabs__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__providers_tabs_tabs__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__pages_categorias_lista_categorias_lista__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52__components_sublist_item_desktop_sublist_item_desktop__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53__providers_player_player__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__pages_contato_contato__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__pages_pdfs_e_dicas_pdfs_e_dicas__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__providers_pdfs_pdfs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57__providers_conteudos_conteudos__ = __webpack_require__(516);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__pages_easyfit_sports_home_easyfit_sports_home__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__pages_easyfit_sports_tabs_easyfit_sports_tabs__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60__components_tabs_tabs__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__pages_categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__pages_cadastro_usuario_cadastro_usuario__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__pages_cadastro_usuario_second_cadastro_usuario_second__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__providers_cadastro_cadastro__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68__pages_easyfit_nutri_tabs_easyfit_nutri_tabs__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69__pages_easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_70__pages_easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_71__pages_easyfit_nutri_exercicio_interna_easyfit_nutri_exercicio_interna__ = __webpack_require__(387);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_72__pages_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(386);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_73__pages_easyfit_nutri_home_easyfit_nutri_home__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_74__pages_easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_75__pages_easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_76__pipes_to_fixed_to_fixed__ = __webpack_require__(518);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_77__pipes_filter_filter__ = __webpack_require__(519);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_78__components_consumo_agua_consumo_agua__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_79__components_home_tabbar_home_tabbar__ = __webpack_require__(521);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_80__components_radio_week_radio_week__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_81__pages_gestao_home_gestao_home__ = __webpack_require__(389);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_82__pages_gestao_tabs_gestao_tabs__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_83__pages_gestao_cadastro_gestao_cadastro__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_84__pages_gestao_listagem_gestao_listagem__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_85__pages_gestao_imagens_gestao_imagens__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_86__pages_central_home_central_home__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_87__pages_central_tabs_central_tabs__ = __webpack_require__(391);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_88__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_89__providers_consultas_consultas__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_90__providers_notificacoes_notificacoes__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_91__pages_central_notificacoes_central_notificacoes__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_92__providers_noticias_noticias__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_93__pages_central_noticias_central_noticias__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_94__pages_central_noticias_interna_central_noticias_interna__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_95__ionic_native_social_sharing__ = __webpack_require__(390);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_96__pages_certificados_pagar_certificados_pagar__ = __webpack_require__(379);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













Object(__WEBPACK_IMPORTED_MODULE_10__angular_common__["i" /* registerLocaleData */])(__WEBPACK_IMPORTED_MODULE_11__angular_common_locales_pt__["a" /* default */]);





















































































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_18__components_header_header__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_39__components_sublist_item_sublist_item__["a" /* SublistItemComponent */],
                __WEBPACK_IMPORTED_MODULE_52__components_sublist_item_desktop_sublist_item_desktop__["a" /* SublistItemDesktopComponent */],
                __WEBPACK_IMPORTED_MODULE_78__components_consumo_agua_consumo_agua__["a" /* ConsumoAguaComponent */],
                __WEBPACK_IMPORTED_MODULE_79__components_home_tabbar_home_tabbar__["a" /* HomeTabbarComponent */],
                __WEBPACK_IMPORTED_MODULE_80__components_radio_week_radio_week__["a" /* RadioWeekComponent */],
                __WEBPACK_IMPORTED_MODULE_42__pipes_safe_safe__["a" /* SafePipe */],
                __WEBPACK_IMPORTED_MODULE_43__pipes_linkify_linkify__["a" /* LinkifyPipe */],
                __WEBPACK_IMPORTED_MODULE_76__pipes_to_fixed_to_fixed__["a" /* ToFixedPipe */],
                __WEBPACK_IMPORTED_MODULE_77__pipes_filter_filter__["a" /* FilterPipe */],
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_categorias_categorias__["a" /* CategoriasPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_easymind_tabs_easymind_tabs__["a" /* EasymindTabsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_categorias_lista_categorias_lista__["a" /* CategoriasListaPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_frase_do_dia_frase_do_dia__["a" /* FraseDoDiaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_favoritos_favoritos__["a" /* FavoritosPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_audios_audios__["a" /* AudiosPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_faca_parte_faca_parte__["a" /* FacaPartePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_player_player__["a" /* PlayerPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_codigo_codigo__["a" /* CodigoPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_conheca_conheca__["a" /* ConhecaPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_login_codigo_login_codigo__["a" /* LoginCodigoPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_aplicativos_aplicativos__["a" /* AplicativosPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_contato_contato__["a" /* ContatoPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_pdfs_e_dicas_pdfs_e_dicas__["a" /* PdfsEDicasPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_easyfit_sports_home_easyfit_sports_home__["a" /* EasyfitSportsHomePage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */],
                __WEBPACK_IMPORTED_MODULE_60__components_tabs_tabs__["a" /* TabsComponent */],
                __WEBPACK_IMPORTED_MODULE_61__pages_categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_cadastro_usuario_cadastro_usuario__["a" /* CadastroUsuarioPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_cadastro_usuario_second_cadastro_usuario_second__["a" /* CadastroUsuarioSecondPage */],
                __WEBPACK_IMPORTED_MODULE_68__pages_easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__["a" /* EasyfitNutriCalculoCaloricoPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__["a" /* EasyfitNutriCalculoCaloricoPage */],
                __WEBPACK_IMPORTED_MODULE_70__pages_easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__["a" /* EasyfitNutriControleAguaPage */],
                __WEBPACK_IMPORTED_MODULE_71__pages_easyfit_nutri_exercicio_interna_easyfit_nutri_exercicio_interna__["a" /* EasyfitNutriExercicioInternaPage */],
                __WEBPACK_IMPORTED_MODULE_72__pages_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosPage */],
                __WEBPACK_IMPORTED_MODULE_73__pages_easyfit_nutri_home_easyfit_nutri_home__["a" /* EasyfitNutriHomePage */],
                __WEBPACK_IMPORTED_MODULE_74__pages_easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__["a" /* EasyfitNutriRefeicoesInternaPage */],
                __WEBPACK_IMPORTED_MODULE_75__pages_easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__["a" /* EasyfitNutriRefeicoesPage */],
                __WEBPACK_IMPORTED_MODULE_81__pages_gestao_home_gestao_home__["a" /* GestaoHomePage */],
                __WEBPACK_IMPORTED_MODULE_82__pages_gestao_tabs_gestao_tabs__["a" /* GestaoTabsPage */],
                __WEBPACK_IMPORTED_MODULE_83__pages_gestao_cadastro_gestao_cadastro__["a" /* GestaoCadastroPage */],
                __WEBPACK_IMPORTED_MODULE_84__pages_gestao_listagem_gestao_listagem__["a" /* GestaoListagemPage */],
                __WEBPACK_IMPORTED_MODULE_85__pages_gestao_imagens_gestao_imagens__["a" /* GestaoImagensPage */],
                __WEBPACK_IMPORTED_MODULE_87__pages_central_tabs_central_tabs__["a" /* CentralTabsPage */],
                __WEBPACK_IMPORTED_MODULE_86__pages_central_home_central_home__["a" /* CentralHomePage */],
                __WEBPACK_IMPORTED_MODULE_91__pages_central_notificacoes_central_notificacoes__["a" /* CentralNotificacoesPage */],
                __WEBPACK_IMPORTED_MODULE_93__pages_central_noticias_central_noticias__["a" /* CentralNoticiasPage */],
                __WEBPACK_IMPORTED_MODULE_94__pages_central_noticias_interna_central_noticias_interna__["a" /* CentralNoticiasInternaPage */],
                __WEBPACK_IMPORTED_MODULE_96__pages_certificados_pagar_certificados_pagar__["a" /* CertificadosPagarPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_25__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_44_ngx_mask__["a" /* NgxMaskModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */], {
                    backButtonText: '',
                    backButtonIcon: 'ios-arrow-back-outline',
                    tabsPlacement: 'bottom',
                    mode: 'md',
                    pageTransition: 'ios-transition',
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_33__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_26__directives_directives_module__["a" /* DirectivesModule */],
                __WEBPACK_IMPORTED_MODULE_8_brmasker_ionic_3__["a" /* BrMaskerModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_9__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_12__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_categorias_categorias__["a" /* CategoriasPage */],
                __WEBPACK_IMPORTED_MODULE_48__pages_easymind_tabs_easymind_tabs__["a" /* EasymindTabsPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_50__pages_categorias_lista_categorias_lista__["a" /* CategoriasListaPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_frase_do_dia_frase_do_dia__["a" /* FraseDoDiaPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_favoritos_favoritos__["a" /* FavoritosPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_audios_audios__["a" /* AudiosPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_perfil_perfil__["a" /* PerfilPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_faca_parte_faca_parte__["a" /* FacaPartePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_player_player__["a" /* PlayerPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_codigo_codigo__["a" /* CodigoPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_search_search__["a" /* SearchPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_conheca_conheca__["a" /* ConhecaPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_login_codigo_login_codigo__["a" /* LoginCodigoPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_aplicativos_aplicativos__["a" /* AplicativosPage */],
                __WEBPACK_IMPORTED_MODULE_54__pages_contato_contato__["a" /* ContatoPage */],
                __WEBPACK_IMPORTED_MODULE_55__pages_pdfs_e_dicas_pdfs_e_dicas__["a" /* PdfsEDicasPage */],
                __WEBPACK_IMPORTED_MODULE_58__pages_easyfit_sports_home_easyfit_sports_home__["a" /* EasyfitSportsHomePage */],
                __WEBPACK_IMPORTED_MODULE_59__pages_easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */],
                __WEBPACK_IMPORTED_MODULE_60__components_tabs_tabs__["a" /* TabsComponent */],
                __WEBPACK_IMPORTED_MODULE_61__pages_categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */],
                __WEBPACK_IMPORTED_MODULE_62__pages_cadastro_usuario_cadastro_usuario__["a" /* CadastroUsuarioPage */],
                __WEBPACK_IMPORTED_MODULE_63__pages_cadastro_usuario_second_cadastro_usuario_second__["a" /* CadastroUsuarioSecondPage */],
                __WEBPACK_IMPORTED_MODULE_68__pages_easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__["a" /* EasyfitNutriCalculoCaloricoPage */],
                __WEBPACK_IMPORTED_MODULE_69__pages_easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__["a" /* EasyfitNutriCalculoCaloricoPage */],
                __WEBPACK_IMPORTED_MODULE_70__pages_easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__["a" /* EasyfitNutriControleAguaPage */],
                __WEBPACK_IMPORTED_MODULE_71__pages_easyfit_nutri_exercicio_interna_easyfit_nutri_exercicio_interna__["a" /* EasyfitNutriExercicioInternaPage */],
                __WEBPACK_IMPORTED_MODULE_72__pages_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosPage */],
                __WEBPACK_IMPORTED_MODULE_73__pages_easyfit_nutri_home_easyfit_nutri_home__["a" /* EasyfitNutriHomePage */],
                __WEBPACK_IMPORTED_MODULE_74__pages_easyfit_nutri_refeicoes_interna_easyfit_nutri_refeicoes_interna__["a" /* EasyfitNutriRefeicoesInternaPage */],
                __WEBPACK_IMPORTED_MODULE_75__pages_easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__["a" /* EasyfitNutriRefeicoesPage */],
                __WEBPACK_IMPORTED_MODULE_81__pages_gestao_home_gestao_home__["a" /* GestaoHomePage */],
                __WEBPACK_IMPORTED_MODULE_82__pages_gestao_tabs_gestao_tabs__["a" /* GestaoTabsPage */],
                __WEBPACK_IMPORTED_MODULE_83__pages_gestao_cadastro_gestao_cadastro__["a" /* GestaoCadastroPage */],
                __WEBPACK_IMPORTED_MODULE_84__pages_gestao_listagem_gestao_listagem__["a" /* GestaoListagemPage */],
                __WEBPACK_IMPORTED_MODULE_85__pages_gestao_imagens_gestao_imagens__["a" /* GestaoImagensPage */],
                __WEBPACK_IMPORTED_MODULE_87__pages_central_tabs_central_tabs__["a" /* CentralTabsPage */],
                __WEBPACK_IMPORTED_MODULE_86__pages_central_home_central_home__["a" /* CentralHomePage */],
                __WEBPACK_IMPORTED_MODULE_91__pages_central_notificacoes_central_notificacoes__["a" /* CentralNotificacoesPage */],
                __WEBPACK_IMPORTED_MODULE_93__pages_central_noticias_central_noticias__["a" /* CentralNoticiasPage */],
                __WEBPACK_IMPORTED_MODULE_94__pages_central_noticias_interna_central_noticias_interna__["a" /* CentralNoticiasInternaPage */],
                __WEBPACK_IMPORTED_MODULE_96__pages_certificados_pagar_certificados_pagar__["a" /* CertificadosPagarPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
                __WEBPACK_IMPORTED_MODULE_40__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_36__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_image_picker__["a" /* ImagePicker */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_onesignal__["a" /* OneSignal */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["H" /* LOCALE_ID */], useValue: 'pt' },
                __WEBPACK_IMPORTED_MODULE_24__providers_helpers_helpers__["a" /* HelpersProvider */],
                __WEBPACK_IMPORTED_MODULE_51__providers_categorias_categorias__["a" /* CategoriasProvider */],
                __WEBPACK_IMPORTED_MODULE_27__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_28__providers_global_global__["a" /* GlobalProvider */],
                __WEBPACK_IMPORTED_MODULE_29__providers_api_api__["a" /* ApiProvider */],
                __WEBPACK_IMPORTED_MODULE_30__providers_uploads_uploads__["a" /* UploadsProvider */],
                __WEBPACK_IMPORTED_MODULE_31__providers_loading_loading__["a" /* LoadingProvider */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_document_viewer__["a" /* DocumentViewer */],
                __WEBPACK_IMPORTED_MODULE_47__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
                __WEBPACK_IMPORTED_MODULE_49__providers_tabs_tabs__["a" /* TabsProvider */],
                __WEBPACK_IMPORTED_MODULE_53__providers_player_player__["a" /* PlayerProvider */],
                __WEBPACK_IMPORTED_MODULE_56__providers_pdfs_pdfs__["a" /* PdfsProvider */],
                __WEBPACK_IMPORTED_MODULE_57__providers_conteudos_conteudos__["a" /* ConteudosProvider */],
                __WEBPACK_IMPORTED_MODULE_49__providers_tabs_tabs__["a" /* TabsProvider */],
                __WEBPACK_IMPORTED_MODULE_64__providers_cadastro_cadastro__["a" /* CadastroProvider */],
                __WEBPACK_IMPORTED_MODULE_65__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
                __WEBPACK_IMPORTED_MODULE_66__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */],
                __WEBPACK_IMPORTED_MODULE_67__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */],
                __WEBPACK_IMPORTED_MODULE_88__providers_area_area__["a" /* AreaProvider */],
                __WEBPACK_IMPORTED_MODULE_89__providers_consultas_consultas__["a" /* ConsultasProvider */],
                __WEBPACK_IMPORTED_MODULE_90__providers_notificacoes_notificacoes__["a" /* NotificacoesProvider */],
                __WEBPACK_IMPORTED_MODULE_92__providers_noticias_noticias__["a" /* NoticiasProvider */],
                __WEBPACK_IMPORTED_MODULE_95__ionic_native_social_sharing__["a" /* SocialSharing */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoriasConteudosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__certificados_pagar_certificados_pagar__ = __webpack_require__(379);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ModuloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CategoriasConteudosPage = /** @class */ (function () {
    function CategoriasConteudosPage(navCtrl, alertCtrl, _categorias, loading, global) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this._categorias = _categorias;
        this.loading = loading;
        this.global = global;
    }
    CategoriasConteudosPage.prototype.ionViewDidLoad = function (e) {
        var _this = this;
        if (!this._categorias.current.id) {
            this._categorias.current = this._categorias.list[0];
        }
        this.loading.present();
        this._categorias.getTopicos(this._categorias.current.id).then(function (r) {
            _this._categorias.current.topicos = r;
            _this._categorias.current = _this._categorias.parseCategoria(_this._categorias.current);
            _this.refresher.complete();
            _this.loading.dismiss();
        }).catch(function (r) { return _this.loading.dismiss(); });
    };
    CategoriasConteudosPage.prototype.ionViewWillEnter = function () {
        console.log('will enter');
        if (this._categorias.current.topicos && this._categorias.current.topicos.length &&
            this._categorias.current.topicos.topicos) {
            this._categorias.current = this._categorias.parseCategoria(this._categorias.current);
        }
    };
    CategoriasConteudosPage.prototype.onRefresher = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this._categorias.getTopicos(_this._categorias.current.id).then(function (r) {
                _this._categorias.current.topicos = r;
                _this._categorias.current = _this._categorias.parseCategoria(_this._categorias.current);
                _this.refresher.complete();
                _this.loading.dismiss();
                resolve();
            }, function (e) {
                _this.refresher.complete();
                _this.loading.dismiss();
                reject();
            });
        });
    };
    CategoriasConteudosPage.prototype.certificadosPagarPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__certificados_pagar_certificados_pagar__["a" /* CertificadosPagarPage */], { categoria: Object.assign({}, this._categorias.current) });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], CategoriasConteudosPage.prototype, "refresher", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], CategoriasConteudosPage.prototype, "content", void 0);
    CategoriasConteudosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-categorias-conteudos',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/categorias-conteudos/categorias-conteudos.html"*/'<!--\n  Generated template for the ModuloPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <header [title]="_categorias.current?.titulo"></header>\n\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="onRefresher(false)">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="certificado-btn" *ngIf="(_categorias.current.conteudos_assistido_count == _categorias.current.conteudos_count) && _categorias.current.certificado" text-center style="font-weight: bold;">\n    <div [innerHtml]="global.informacoes?.certificado?.parabens | linkify" style="margin-bottom:10px;white-space: pre-wrap;"></div>\n    <button ion-button (click)="certificadosPagarPage()">{{global.informacoes?.certificado?.parabens_btn}} &nbsp; <ion-icon style="font-size:23px;" name="ios-document-outline" style="transform: rotate(90deg);"></ion-icon> </button>\n  </div>\n  <!-- <h3>{{ _categorias.current.conteudos_assistido_count }}</h3>\n  <h3>{{ _categorias.current.conteudos_count }}</h3>\n  <h3>{{ _categorias.current.certificado }}</h3> -->\n  <div class="easymind-list" padding-horizontal>\n\n    <div #itemsRef class="list-item open">\n      <div class="sublist">\n\n\n\n        <ng-container *ngFor="let topico of _categorias.current?.topicos">\n          <!--mostra somente qndo a lista de topicos > 1 e o titulo for direito do do roteiro-->\n          <div class="sublist-item sublist-item-100" *ngIf="_categorias.current?.topicos.length > 1 && topico.titulo != _categorias.current?.titulo">\n            <div class="sublist-title sublist-title-upper"><b>{{ topico.titulo }}</b></div>\n          </div>\n          <sublist-item *ngFor="let subitem of topico.conteudos" [item]="subitem"></sublist-item>\n\n          <ng-container *ngFor="let topico2 of topico.topicos">\n            <!--mostra somente qndo a lista de topicos > 1 e o titulo for direito do do roteiro-->\n            <div class="sublist-item sublist-item-100">\n              <div class="sublist-title"><b>{{ topico2.titulo }}</b></div>\n            </div>\n            <sublist-item *ngFor="let subitem2 of topico2.conteudos" [item]="subitem2"></sublist-item>\n\n\n          </ng-container>\n\n        </ng-container>\n\n\n\n\n\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/categorias-conteudos/categorias-conteudos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */]])
    ], CategoriasConteudosPage);
    return CategoriasConteudosPage;
}());

//# sourceMappingURL=categorias-conteudos.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriAguaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the AguaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var EasyfitNutriAguaProvider = /** @class */ (function () {
    function EasyfitNutriAguaProvider(auth, api, helpers, _alimentos) {
        this.auth = auth;
        this.api = api;
        this.helpers = helpers;
        this._alimentos = _alimentos;
        this.item = {
            quantidade: '0',
            consumido: 0,
            data: '',
            bebida_200: '0',
            bebida_250: '0',
            bebida_500: '0',
            bebida_700: '0',
            copos: []
        };
        console.log('Hello AguaProvider Provider');
    }
    EasyfitNutriAguaProvider.prototype.save = function () {
        var _this = this;
        if (this._save && !this._save.closed) {
            this._save.unsubscribe();
        }
        this.item.data = this._alimentos.selectedDate;
        this.updateCoposView();
        return this.api.post('/users/' + this.auth.user.user.id + '/agua', this.item).map(function (r) {
            return _this.item;
        });
    };
    EasyfitNutriAguaProvider.prototype.get = function () {
        var _this = this;
        if (this._get && !this._get.closed) {
            this._get.unsubscribe();
        }
        return this.api.get('/users/' + this.auth.user.user.id + '/agua?data=' + this._alimentos.selectedDate).map(function (r) {
            Object.assign(_this.item, r);
            _this.updateCoposView();
            return _this.item;
        });
    };
    EasyfitNutriAguaProvider.prototype.updateCoposView = function () {
        this.item.copos = [];
        var coposNecessarios = Math.round(parseInt(this.item.quantidade) / 250);
        var coposConsumidos = Math.round(parseInt(this.item.consumido.toString()) / 250);
        console.log(coposConsumidos);
        this.item.copos = new Array(coposNecessarios);
        console.log(this.item.copos);
        for (var i = 0; i < coposConsumidos; i++) {
            this.item.copos[i] = true;
        }
    };
    EasyfitNutriAguaProvider.prototype.bebeCopo = function (index) {
        if (this.item.copos[index]) {
            this.item.copos[index] = false;
            this.item.consumido = parseInt(this.item.consumido.toString()) - 250;
        }
        else {
            this.item.copos[index] = true;
            this.item.consumido = parseInt(this.item.consumido.toString()) + 250;
        }
        this._save = this.save().subscribe();
    };
    EasyfitNutriAguaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_4__easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */]])
    ], EasyfitNutriAguaProvider);
    return EasyfitNutriAguaProvider;
}());

//# sourceMappingURL=easyfit-nutri-agua.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriExerciciosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_exercicio__ = __webpack_require__(492);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the ExerciciosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var EasyfitNutriExerciciosProvider = /** @class */ (function () {
    function EasyfitNutriExerciciosProvider(api, toastCtrl, helpers, auth, _alimentos, alertCtrl) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.helpers = helpers;
        this.auth = auth;
        this._alimentos = _alimentos;
        this.alertCtrl = alertCtrl;
        this.loadingPaginate = false;
        this.listPaginate = new Array();
        this.loadingDay = false;
        this.day = new Array();
        this.totalDay = 0;
        this.totalMinutesDay = 0;
        this.clearDay();
    }
    EasyfitNutriExerciciosProvider.prototype.getPaginate = function (options, load) {
        var _this = this;
        if (load === void 0) { load = false; }
        if (this._getPaginate && !this._getPaginate.closed) {
            this._getPaginate.unsubscribe();
        }
        this.loadingPaginate = load;
        return this.api.get('/exercicios?page=' + options.page + '&search=' + options.search).map(function (r) {
            _this.loadingPaginate = false;
            var array = r.data.map(function (obj) { return new __WEBPACK_IMPORTED_MODULE_1__models_exercicio__["a" /* Exercicio */](obj); });
            _this.listPaginate = options.page === 1 ? array : _this.listPaginate.concat(array);
            return r;
        });
    };
    EasyfitNutriExerciciosProvider.prototype.getDay = function (clear) {
        var _this = this;
        if (clear === void 0) { clear = true; }
        return new Promise(function (resolve) {
            if (_this._getDay && !_this._getDay.closed) {
                _this._getDay.unsubscribe();
            }
            if (clear) {
                _this.clearDay();
            }
            _this.loadingDay = true;
            _this._getDay = _this.api.get('/users/' + _this.auth.user.user.id + '/exercicios?data=' + _this._alimentos.selectedDate).subscribe(function (r) {
                _this.clearDay();
                _this.loadingDay = false;
                _this.day = r.map(function (obj) { return new __WEBPACK_IMPORTED_MODULE_1__models_exercicio__["a" /* Exercicio */](obj); });
                _this.day.forEach(function (obj) {
                    _this.totalDay += obj.pivot.energia;
                    _this.totalMinutesDay += obj.pivot.quantidade;
                });
                resolve();
            }, function () {
                resolve();
                _this.loadingDay = false;
            });
        });
    };
    EasyfitNutriExerciciosProvider.prototype.clearDay = function () {
        this.day = [];
        this.totalDay = 0;
        this.totalMinutesDay = 0;
    };
    EasyfitNutriExerciciosProvider.prototype.delete = function (item) {
        var _this = this;
        this.alertCtrl.create({
            message: 'Você tem certeza que deseja deletar esse exercício?',
            buttons: [
                {
                    text: 'Não',
                    role: 'cancel'
                },
                {
                    text: 'Sim',
                    handler: function () {
                        _this.day = _this.day.filter(function (obj) { return obj.id != item.id; });
                        _this.api.delete('/users/' + _this.auth.user.user.id + '/exercicios/' + item.id, { data: item.pivot.data, }).subscribe();
                        _this.getDay();
                    }
                }
            ]
        }).present();
    };
    EasyfitNutriExerciciosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_5__auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_6__easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */]])
    ], EasyfitNutriExerciciosProvider);
    return EasyfitNutriExerciciosProvider;
}());

//# sourceMappingURL=easyfit-nutri-exercicios.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AreaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the CategoriasProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AreaProvider = /** @class */ (function () {
    function AreaProvider(api, toastCtrl, aplicativos) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.aplicativos = aplicativos;
        this.list = [];
        this.watchToggle = new __WEBPACK_IMPORTED_MODULE_3_rxjs_Subject__["Subject"]();
        this.current = {};
        console.log('Hello AreaProvider Provider');
    }
    AreaProvider.prototype.get = function (url) {
        if (!url) {
            url = '/area';
        }
        return this.api.get(url);
    };
    AreaProvider.prototype.getAll = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var opened = this.list.find(function (area) { return area.open; });
        return new Promise(function (resolve, reject) {
            if (_this._getAll && !_this._getAll.closed) {
                _this._getAll.unsubscribe();
            }
            _this._getAll = _this.get('/area').map(function (r) {
                _this.list = r;
                return _this.list;
            }).subscribe(function (r) { return resolve(r); }, function (e) { return reject(e); });
        });
    };
    AreaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], AreaProvider);
    return AreaProvider;
}());

//# sourceMappingURL=area.js.map

/***/ }),

/***/ 473:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_favoritos_favoritos__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_perfil_perfil__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_onesignal__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_aplicativos_aplicativos__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_codigo_login_codigo__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_tabs_tabs__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_contato_contato__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_conheca_conheca__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_pdfs_pdfs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_easyfit_sports_tabs_easyfit_sports_tabs__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_easyfit_nutri_tabs_easyfit_nutri_tabs__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_pdfs_e_dicas_pdfs_e_dicas__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

























// import { LoginPage } from '../pages/login-2/login-2';
var MyApp = /** @class */ (function () {
    function MyApp(global, plt, statusBar, splashScreen, tabs, auth, loading, oneSignal, api, alertCtrl, aplicativos, _pdfs, _categorias, app, helpers) {
        /***
         * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
         * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
         * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
         * ATENCAO SPLASHSCREEN HIDE NO TABSPAGE E LOGIN PAGE
         */
        this.global = global;
        this.plt = plt;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.tabs = tabs;
        this.auth = auth;
        this.loading = loading;
        this.oneSignal = oneSignal;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.aplicativos = aplicativos;
        this._pdfs = _pdfs;
        this._categorias = _categorias;
        this.app = app;
        this.helpers = helpers;
        this.counter = 0;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.pages = {
            perfil: __WEBPACK_IMPORTED_MODULE_6__pages_perfil_perfil__["a" /* PerfilPage */],
            login: __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */],
        };
        this.global.loadingApp = false;
        this.auth.setToken(JSON.parse(window.localStorage.getItem("token")));
        if (this.auth.isLoggedIn()) {
            if (this.auth.isPremium()) {
                this.rootPage = __WEBPACK_IMPORTED_MODULE_13__pages_aplicativos_aplicativos__["a" /* AplicativosPage */];
            }
            else {
                this.rootPage = __WEBPACK_IMPORTED_MODULE_14__pages_login_codigo_login_codigo__["a" /* LoginCodigoPage */];
            }
        }
        else {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        }
        this.initializeApp();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.plt.ready().then(function () {
            // Okay, so the plt is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            if (_this.plt.is('cordova')) {
                _this.oneSignal.startInit('2a60418c-204b-47b4-9a70-e0053a92ed16', '250982028144');
                _this.oneSignal.inFocusDisplaying(_this.oneSignal.OSInFocusDisplayOption.Notification);
                _this.oneSignal.handleNotificationOpened().subscribe(function (r) {
                    var data = r.notification.payload.additionalData;
                    // if (data.table == 'codes') {
                    //   this.openTabPage(6);
                    // }
                    // do something when a notification is opened
                    // console.log(r);
                    _this.alertCtrl.create({ title: r.notification.payload.title, message: r.notification.payload.body }).present();
                });
                _this.oneSignal.getIds().then(function (r) {
                    _this.auth.player_id = r.userId;
                    _this.api.saveOneSignal();
                });
                _this.oneSignal.endInit();
            }
            _this.statusBar.backgroundColorByHexString('#000000');
            if (_this.plt.is('android')) {
                _this.statusBar.styleLightContent();
            }
            if (_this.plt.is('ios')) {
                _this.statusBar.styleDefault();
            }
            _this.plt.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNav();
                if (nav.canGoBack()) {
                    nav.pop();
                }
                else if (_this.counter == 0) {
                    _this.counter++;
                    _this.helpers.toast('Pressione novamente para sair.');
                    setTimeout(function () { _this.counter = 0; }, 3000);
                }
                else {
                    _this.plt.exitApp();
                }
            }, 0);
        });
    };
    MyApp.prototype.openPage = function (page) {
        this.nav.setRoot(page, {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' });
    };
    MyApp.prototype.openTabPage = function (index) {
        this.tabs.selectTab(index);
    };
    MyApp.prototype.easyfitSportsTab = function (index) {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_21__pages_easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */], { select: index });
    };
    MyApp.prototype.easyfitSportsTabsPage = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_21__pages_easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */]);
    };
    MyApp.prototype.favoritosPage = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_5__pages_favoritos_favoritos__["a" /* FavoritosPage */]);
    };
    MyApp.prototype.categoriasCounteudosPage = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_24__pages_categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */]);
    };
    MyApp.prototype.pdfsEDicasPage = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_23__pages_pdfs_e_dicas_pdfs_e_dicas__["a" /* PdfsEDicasPage */]);
    };
    MyApp.prototype.easyfitNutriTab = function (index) {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_22__pages_easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */], { select: index });
    };
    MyApp.prototype.easyfitNutriTabsPage = function () {
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_22__pages_easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */]);
    };
    MyApp.prototype.aplicativosPage = function () {
        this.aplicativos.clearItem();
        this.openPage(__WEBPACK_IMPORTED_MODULE_13__pages_aplicativos_aplicativos__["a" /* AplicativosPage */]);
    };
    MyApp.prototype.contatoPage = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_17__pages_contato_contato__["a" /* ContatoPage */]);
    };
    MyApp.prototype.perfilPage = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_6__pages_perfil_perfil__["a" /* PerfilPage */], { hideButtons: true });
    };
    MyApp.prototype.conhecaPage = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_18__pages_conheca_conheca__["a" /* ConhecaPage */], { hideButtons: true });
    };
    MyApp.prototype.easylifePage = function () {
        var _this = this;
        this.loading.present();
        this.aplicativos.setItem(this.aplicativos.getEasyfitLife());
        setTimeout(function () {
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_21__pages_easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */]);
        }, 50);
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */], {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' }).then(function () {
            _this.aplicativos.clearItem();
            _this._categorias.list = [];
            _this._pdfs.list = [];
            _this.auth.logout();
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/app/app.html"*/'<ion-menu [content]="content" persistent="true" *ngIf="auth.isLoggedIn()">\n  <ion-content class="menu">\n\n    <ul class="menu-lista">\n\n      <ng-container *ngIf="aplicativos.item.id == 1">\n        <li menuClose tappable cursor-pointer (click)="openTabPage(2)">\n          <img src="assets/icon/home-white.png"> Home\n        </li>\n        <li menuClose tappable cursor-pointer (click)="openTabPage(0)">\n          <img src="assets/icon/play-white.svg" class="play"> Roteiros\n        </li>\n        <li menuClose tappable cursor-pointer (click)="openTabPage(1)">\n          <img src="assets/icon/quotes-white.svg" class="quote"> Motive-se\n        </li>\n        <li menuClose tappable cursor-pointer (click)="openTabPage(3)">\n          <img src="assets/icon/star-white.svg"> Favoritos\n        </li>\n        <li menuClose tappable cursor-pointer (click)="openTabPage(4)">\n          <img src="assets/icon/music-white.svg"> Áudios\n        </li>\n      </ng-container>\n\n      <ng-container *ngIf="aplicativos.item.id == 2 || aplicativos.item.id == 4 || aplicativos.item.id == 7">\n        <li menuClose tappable cursor-pointer (click)="easyfitSportsTabsPage()">\n          <img src="assets/icon/home-white.png"> Home\n        </li>\n\n\n        <li menuClose tappable cursor-pointer (click)="categoriasCounteudosPage()">\n          <ion-icon name="duo-hand" *ngIf="aplicativos.item.id == 2"></ion-icon>\n          <ion-icon name="duo-3books" *ngIf="aplicativos.item.id == 4"></ion-icon>\n          <ion-icon name="duo-utensils" *ngIf="aplicativos.item.id == 7"></ion-icon>\n          <!-- {{ _categorias.current.id ? _categorias.current.titulo : \'Categorias\'}} -->\n          Categoria\n        </li>\n\n        <li menuClose tappable cursor-pointer (click)="favoritosPage()">\n          <img src="assets/icon/star-white.svg"> Favoritos\n        </li>\n        <li menuClose tappable cursor-pointer (click)="pdfsEDicasPage()">\n          <ion-icon name="duo-book"></ion-icon>\n          <ng-container *ngIf="_pdfs.list.length">CONTEÚDOS E&ensp;</ng-container>DICAS\n        </li>\n      </ng-container>\n\n      <ng-container *ngIf="aplicativos.item.id == 3">\n        <li menuClose tappable cursor-pointer (click)="easyfitNutriTabsPage()">\n          <img src="assets/icon/home-white.png"> HOME\n        </li>\n        <li menuClose tappable cursor-pointer (click)="easyfitNutriTab(1)">\n          <ion-icon name="duo-restaurant"></ion-icon> MINHAS REFEIÇÕES\n        </li>\n        <li menuClose tappable cursor-pointer (click)="easyfitNutriTab(2)">\n          <ion-icon name="duo-water"></ion-icon> CONTROLE DE ÁGUA\n        </li>\n        <li menuClose tappable cursor-pointer (click)="easyfitNutriTab(4)">\n          <ion-icon name="duo-dumbbell"></ion-icon> ATIVIDADE FÍSICA\n        </li>\n        <li menuClose tappable cursor-pointer (click)="easyfitNutriTab(5)">\n          <ion-icon name="duo-kcal"></ion-icon> CÁLCULO DE CALORIAS\n        </li>\n      </ng-container>\n\n      <li menuClose tappable cursor-pointer (click)="perfilPage()">\n        <img src="assets/icon/profile-white.png"> Perfil\n      </li>\n      <!-- <li menuClose tappable cursor-pointer (click)="openTabPage(6)" *ngIf="global.informacoes.c">\n        <ion-icon name="barcode"></ion-icon> Código\n      </li> -->\n      <li menuClose tappable cursor-pointer (click)="conhecaPage()">\n        <img src="assets/icon/easymind-white.png"> {{ global.conheca.titulo }}\n      </li>\n      <li menuClose tappable cursor-pointer (click)="contatoPage()">\n        <img src="assets/icon/phone-white.svg"> Contato\n      </li>\n      <li menuClose tappable cursor-pointer (click)="aplicativosPage()">\n        <ion-icon name="phone-portrait"></ion-icon> Módulos\n      </li>\n      <ng-container *ngIf="aplicativos.item.id != 3">\n        <li menuClose tappable cursor-pointer *ngIf="!aplicativos.item.id" (click)="logout()">\n          <ion-icon name="md-log-out"></ion-icon> Sair\n        </li>\n        <li menuClose tappable cursor-pointer *ngIf="aplicativos.item.id" (click)="aplicativosPage()">\n          <ion-icon name="md-log-out"></ion-icon> Sair\n        </li>\n      </ng-container>\n      <ng-container *ngIf="aplicativos.item.id == 3">\n        <li menuClose tappable cursor-pointer *ngIf="aplicativos.item.id" (click)="easylifePage()">\n          <ion-icon name="md-log-out"></ion-icon> Sair\n        </li>\n      </ng-container>\n    </ul>\n    <div class="menu-rodape">\n      <div class="menu-redes">\n        <a *ngIf="global.informacoes.instagram" href="{{ global.informacoes.instagram }}" target="blank">\n          <img src="assets/icon/instagram-white.png">\n        </a>\n        <a *ngIf="global.informacoes.facebook" href="{{ global.informacoes.facebook }}" target="blank">\n          <img src="assets/icon/facebook-white.png">\n        </a>\n        <a *ngIf="global.informacoes.youtube" href="{{ global.informacoes.youtube }}" target="blank">\n          <img src="assets/icon/youtube-white.png">\n        </a>\n        <a *ngIf="global.informacoes.twitter" href="{{ global.informacoes.twitter }}" target="blank">\n          <img src="assets/icon/twitter-white.png">\n        </a>\n      </div>\n      <div class="menu-logo"></div>\n      <div class="menu-produto" margin-bottom>\n        <!-- UM PRODUTO EASYLIFE -->\n      </div>\n    </div>\n\n  </ion-content>\n\n</ion-menu>\n<ion-nav #content [root]="rootPage"></ion-nav>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_15__providers_tabs_tabs__["a" /* TabsProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_12__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_16__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_19__providers_pdfs_pdfs__["a" /* PdfsProvider */],
            __WEBPACK_IMPORTED_MODULE_20__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 475:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 250,
	"./af.js": 250,
	"./ar": 251,
	"./ar-dz": 252,
	"./ar-dz.js": 252,
	"./ar-kw": 253,
	"./ar-kw.js": 253,
	"./ar-ly": 254,
	"./ar-ly.js": 254,
	"./ar-ma": 255,
	"./ar-ma.js": 255,
	"./ar-sa": 256,
	"./ar-sa.js": 256,
	"./ar-tn": 257,
	"./ar-tn.js": 257,
	"./ar.js": 251,
	"./az": 258,
	"./az.js": 258,
	"./be": 259,
	"./be.js": 259,
	"./bg": 260,
	"./bg.js": 260,
	"./bm": 261,
	"./bm.js": 261,
	"./bn": 262,
	"./bn.js": 262,
	"./bo": 263,
	"./bo.js": 263,
	"./br": 264,
	"./br.js": 264,
	"./bs": 265,
	"./bs.js": 265,
	"./ca": 266,
	"./ca.js": 266,
	"./cs": 267,
	"./cs.js": 267,
	"./cv": 268,
	"./cv.js": 268,
	"./cy": 269,
	"./cy.js": 269,
	"./da": 270,
	"./da.js": 270,
	"./de": 271,
	"./de-at": 272,
	"./de-at.js": 272,
	"./de-ch": 273,
	"./de-ch.js": 273,
	"./de.js": 271,
	"./dv": 274,
	"./dv.js": 274,
	"./el": 275,
	"./el.js": 275,
	"./en-au": 276,
	"./en-au.js": 276,
	"./en-ca": 277,
	"./en-ca.js": 277,
	"./en-gb": 278,
	"./en-gb.js": 278,
	"./en-ie": 279,
	"./en-ie.js": 279,
	"./en-il": 280,
	"./en-il.js": 280,
	"./en-nz": 281,
	"./en-nz.js": 281,
	"./eo": 282,
	"./eo.js": 282,
	"./es": 283,
	"./es-do": 284,
	"./es-do.js": 284,
	"./es-us": 285,
	"./es-us.js": 285,
	"./es.js": 283,
	"./et": 286,
	"./et.js": 286,
	"./eu": 287,
	"./eu.js": 287,
	"./fa": 288,
	"./fa.js": 288,
	"./fi": 289,
	"./fi.js": 289,
	"./fo": 290,
	"./fo.js": 290,
	"./fr": 291,
	"./fr-ca": 292,
	"./fr-ca.js": 292,
	"./fr-ch": 293,
	"./fr-ch.js": 293,
	"./fr.js": 291,
	"./fy": 294,
	"./fy.js": 294,
	"./gd": 295,
	"./gd.js": 295,
	"./gl": 296,
	"./gl.js": 296,
	"./gom-latn": 297,
	"./gom-latn.js": 297,
	"./gu": 298,
	"./gu.js": 298,
	"./he": 299,
	"./he.js": 299,
	"./hi": 300,
	"./hi.js": 300,
	"./hr": 301,
	"./hr.js": 301,
	"./hu": 302,
	"./hu.js": 302,
	"./hy-am": 303,
	"./hy-am.js": 303,
	"./id": 304,
	"./id.js": 304,
	"./is": 305,
	"./is.js": 305,
	"./it": 306,
	"./it.js": 306,
	"./ja": 307,
	"./ja.js": 307,
	"./jv": 308,
	"./jv.js": 308,
	"./ka": 309,
	"./ka.js": 309,
	"./kk": 310,
	"./kk.js": 310,
	"./km": 311,
	"./km.js": 311,
	"./kn": 312,
	"./kn.js": 312,
	"./ko": 313,
	"./ko.js": 313,
	"./ky": 314,
	"./ky.js": 314,
	"./lb": 315,
	"./lb.js": 315,
	"./lo": 316,
	"./lo.js": 316,
	"./lt": 317,
	"./lt.js": 317,
	"./lv": 318,
	"./lv.js": 318,
	"./me": 319,
	"./me.js": 319,
	"./mi": 320,
	"./mi.js": 320,
	"./mk": 321,
	"./mk.js": 321,
	"./ml": 322,
	"./ml.js": 322,
	"./mn": 323,
	"./mn.js": 323,
	"./mr": 324,
	"./mr.js": 324,
	"./ms": 325,
	"./ms-my": 326,
	"./ms-my.js": 326,
	"./ms.js": 325,
	"./mt": 327,
	"./mt.js": 327,
	"./my": 328,
	"./my.js": 328,
	"./nb": 329,
	"./nb.js": 329,
	"./ne": 330,
	"./ne.js": 330,
	"./nl": 331,
	"./nl-be": 332,
	"./nl-be.js": 332,
	"./nl.js": 331,
	"./nn": 333,
	"./nn.js": 333,
	"./pa-in": 334,
	"./pa-in.js": 334,
	"./pl": 335,
	"./pl.js": 335,
	"./pt": 336,
	"./pt-br": 337,
	"./pt-br.js": 337,
	"./pt.js": 336,
	"./ro": 338,
	"./ro.js": 338,
	"./ru": 339,
	"./ru.js": 339,
	"./sd": 340,
	"./sd.js": 340,
	"./se": 341,
	"./se.js": 341,
	"./si": 342,
	"./si.js": 342,
	"./sk": 343,
	"./sk.js": 343,
	"./sl": 344,
	"./sl.js": 344,
	"./sq": 345,
	"./sq.js": 345,
	"./sr": 346,
	"./sr-cyrl": 347,
	"./sr-cyrl.js": 347,
	"./sr.js": 346,
	"./ss": 348,
	"./ss.js": 348,
	"./sv": 349,
	"./sv.js": 349,
	"./sw": 350,
	"./sw.js": 350,
	"./ta": 351,
	"./ta.js": 351,
	"./te": 352,
	"./te.js": 352,
	"./tet": 353,
	"./tet.js": 353,
	"./tg": 354,
	"./tg.js": 354,
	"./th": 355,
	"./th.js": 355,
	"./tl-ph": 356,
	"./tl-ph.js": 356,
	"./tlh": 357,
	"./tlh.js": 357,
	"./tr": 358,
	"./tr.js": 358,
	"./tzl": 359,
	"./tzl.js": 359,
	"./tzm": 360,
	"./tzm-latn": 361,
	"./tzm-latn.js": 361,
	"./tzm.js": 360,
	"./ug-cn": 362,
	"./ug-cn.js": 362,
	"./uk": 363,
	"./uk.js": 363,
	"./ur": 364,
	"./ur.js": 364,
	"./uz": 365,
	"./uz-latn": 366,
	"./uz-latn.js": 366,
	"./uz.js": 365,
	"./vi": 367,
	"./vi.js": 367,
	"./x-pseudo": 368,
	"./x-pseudo.js": 368,
	"./yo": 369,
	"./yo.js": 369,
	"./zh-cn": 370,
	"./zh-cn.js": 370,
	"./zh-hk": 371,
	"./zh-hk.js": 371,
	"./zh-tw": 372,
	"./zh-tw.js": 372
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 475;

/***/ }),

/***/ 491:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Alimento; });
var Alimento = /** @class */ (function () {
    function Alimento(obj) {
        Object.assign(this, obj);
        this.energia = this.energia ? parseInt(this.energia) : 0;
        this.gordura = this.gordura ? parseFloat(this.gordura) : 0;
        this.carboidratos = this.carboidratos ? parseFloat(this.carboidratos) : 0;
        this.proteina = this.proteina ? parseFloat(this.proteina) : 0;
        if (this.pivot) {
            this.pivot.energia = Math.ceil((this.energia * this.pivot.quantidade) / 100);
            this.pivot.gordura = parseFloat(((this.gordura * this.pivot.quantidade) / 100).toFixed(2));
            this.pivot.carboidratos = parseFloat(((this.carboidratos * this.pivot.quantidade) / 100).toFixed(2));
            this.pivot.proteina = parseFloat(((this.proteina * this.pivot.quantidade) / 100).toFixed(2));
        }
    }
    return Alimento;
}());

//# sourceMappingURL=alimento.js.map

/***/ }),

/***/ 492:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Exercicio; });
var Exercicio = /** @class */ (function () {
    function Exercicio(obj) {
        Object.assign(this, obj);
        this.energia = this.energia ? parseInt(this.energia) : 0;
        if (this.pivot) {
            this.pivot.energia = Math.ceil((this.energia * this.pivot.quantidade) / 60);
        }
    }
    return Exercicio;
}());

//# sourceMappingURL=exercicio.js.map

/***/ }),

/***/ 494:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_search_search__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_perfil_perfil__ = __webpack_require__(63);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.searchPage = false;
        this.showMenu = true;
        this.showPerfil = false;
        this.hideButtons = false;
        this.hideSearch = false;
        this.searchSubmit = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.search = '';
        this.searchOpen = false;
    }
    HeaderComponent.prototype.doSearch = function () {
        if (this.searchPage) {
            this.searchSubmit.next(this.search);
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__pages_search_search__["a" /* SearchPage */], { search: this.search });
        }
        this.closeSearch();
    };
    HeaderComponent.prototype.closeSearch = function () {
        var _this = this;
        this.searchOpen = false;
        setTimeout(function () { return _this.search = ''; }, 500);
    };
    HeaderComponent.prototype.openSearch = function () {
        this.input.nativeElement.focus();
        this.searchOpen = true;
    };
    HeaderComponent.prototype.toggleSearch = function () {
        this.searchOpen = !this.searchOpen;
    };
    HeaderComponent.prototype.perfilPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__pages_perfil_perfil__["a" /* PerfilPage */], { hideButtons: true });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "searchPage", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "showMenu", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "showPerfil", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "hideButtons", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "hideSearch", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])(),
        __metadata("design:type", Object)
    ], HeaderComponent.prototype, "searchSubmit", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('input', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], HeaderComponent.prototype, "input", void 0);
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'header',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/header/header.html"*/'<form class="search-input" (ngSubmit)="doSearch()" [ngClass]="{open: searchOpen}">\n  <input #input type="text" [(ngModel)]="search" name="search" placeholder="Buscar...">\n  <button details-none type="button">\n    <ion-icon name="ios-close" cursor-pointer (click)="closeSearch()"></ion-icon>\n  </button>\n</form>\n<ion-navbar>\n  <button class="menu-toggle" *ngIf="showMenu && !hideButtons" ion-button menuToggle icon-only start>\n    <img src="assets/icon/menu.png" width="30">\n  </button>\n  <ion-title>{{ title }}</ion-title>\n  <div *ngIf="!showPerfil && !hideButtons && !hideSearch" class="search" cursor-pointer (click)="openSearch()">\n    <img src="assets/icon/search.png" width="28">\n  </div>\n  <div *ngIf="showPerfil && !hideButtons && hideSearch" class="search" cursor-pointer (click)="perfilPage()">\n    <img src="assets/icon/profile-white.png" width="28" class="profile">\n  </div>\n</ion-navbar>\n '/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/header/header.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */]])
    ], HeaderComponent);
    return HeaderComponent;
}());

//# sourceMappingURL=header.js.map

/***/ }),

/***/ 495:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DirectivesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__image_preload_image_preload__ = __webpack_require__(496);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var DirectivesModule = /** @class */ (function () {
    function DirectivesModule() {
    }
    DirectivesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["J" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__image_preload_image_preload__["a" /* ImagePreloadDirective */],
                __WEBPACK_IMPORTED_MODULE_1__image_preload_image_preload__["a" /* ImagePreloadDirective */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__image_preload_image_preload__["a" /* ImagePreloadDirective */],
                __WEBPACK_IMPORTED_MODULE_1__image_preload_image_preload__["a" /* ImagePreloadDirective */]]
        })
    ], DirectivesModule);
    return DirectivesModule;
}());

//# sourceMappingURL=directives.module.js.map

/***/ }),

/***/ 496:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagePreloadDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ImagePreloadDirective = /** @class */ (function () {
    function ImagePreloadDirective() {
    }
    ImagePreloadDirective.prototype.updateUrl = function () {
        this.src = this.default;
    };
    ImagePreloadDirective.prototype.load = function () {
        this.className = 'image-loaded';
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], ImagePreloadDirective.prototype, "src", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", String)
    ], ImagePreloadDirective.prototype, "default", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["y" /* HostBinding */])('class'),
        __metadata("design:type", Object)
    ], ImagePreloadDirective.prototype, "className", void 0);
    ImagePreloadDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* Directive */])({
            selector: 'img[default]',
            host: {
                '(error)': 'updateUrl()',
                '(load)': 'load()',
                '[src]': 'src'
            }
        })
    ], ImagePreloadDirective);
    return ImagePreloadDirective;
}());

//# sourceMappingURL=image-preload.js.map

/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(484);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(485);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_observable_ErrorObservable__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_observable_ErrorObservable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_observable_ErrorObservable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_transfer__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_onesignal__ = __webpack_require__(135);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













// import { LoginPage } from '../../pages/login/login';
// import { LoginPage } from '../../pages/login/login';
/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ApiProvider = /** @class */ (function () {
    function ApiProvider(http, auth, global, alertCtrl, app, fileTransfer, plt, splashScreen, oneSignal) {
        this.http = http;
        this.auth = auth;
        this.global = global;
        this.alertCtrl = alertCtrl;
        this.app = app;
        this.fileTransfer = fileTransfer;
        this.plt = plt;
        this.splashScreen = splashScreen;
        this.oneSignal = oneSignal;
        this.uploadProgress = new __WEBPACK_IMPORTED_MODULE_9_rxjs_Subject__["Subject"]();
        console.log('API PROVIDER HAS STARTED');
    }
    ApiProvider.prototype.get = function (url) {
        var _this = this;
        return this.http.get(this.global.apiUrl + url, {
            headers: this.auth.getAuthorizationHeader()
        }).map(function (r) { return r; }).catch(function (err) { return _this.handleError(err); });
    };
    ApiProvider.prototype.postAsFormData = function (url, body, files, options) {
        var _this = this;
        if (options === void 0) { options = {
            method: 'POST',
            httpParams: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]()
        }; }
        var formData = new FormData();
        // Coloca todo body no form data.., aproveita e da trim nas strings
        // provavelmente da pra fazer uma função recursiva só pra tratar os dados que são passadas para FormData
        Object.keys(body).forEach(function (k) {
            if (typeof body[k] === 'string') {
                body[k] = body[k].trim();
            }
            if (Array.isArray(body[k])) {
                body[k].forEach(function (val) {
                    formData.append(k + '[]', val);
                });
            }
            else if (typeof body[k] === 'object' && body[k] !== null) {
                Object.keys(body[k]).forEach(function (l) {
                    formData.append(k + '[' + l + ']', body[k][l]);
                });
            }
            else if (body[k] === null) {
                formData.append(k, '');
            }
            else {
                formData.append(k, body[k]);
            }
        });
        // Coloca todos files no FormData
        files.forEach(function (obj) {
            obj.multiple = obj.multiple ? obj.multiple : false;
            if (!obj.fileList) {
                return;
            }
            formData.delete(obj.name);
            Array.from(obj.fileList).forEach(function (file, i) {
                var name = obj.name;
                if (obj.multiple) {
                    name = obj.name + '[' + i + ']';
                }
                formData.append(name, file);
            });
        });
        var req = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpRequest */](options.method, this.global.apiUrl + url, formData, {
            params: options.httpParams,
            reportProgress: true,
            headers: this.auth.getAuthorizationHeader()
        });
        return this.http.request(req).map(function (r) { return r; }).catch(function (err) { return _this.handleError(err); });
    };
    ApiProvider.prototype.post = function (url, body, handleError) {
        var _this = this;
        if (body === void 0) { body = {}; }
        if (handleError === void 0) { handleError = true; }
        if (handleError) {
            return this.http.post(this.global.apiUrl + url, body, {
                headers: this.auth.getAuthorizationHeader()
            }).map(function (r) { return r; }).catch(function (err) { return _this.handleError(err); });
        }
        else {
            return this.http.post(this.global.apiUrl + url, body, {
                headers: this.auth.getAuthorizationHeader()
            }).map(function (r) { return r; });
        }
    };
    ApiProvider.prototype.patch = function (url, body) {
        var _this = this;
        return this.http.patch(this.global.apiUrl + url, body, {
            headers: this.auth.getAuthorizationHeader()
        }).map(function (r) { return r; }).catch(function (err) { return _this.handleError(err); });
    };
    ApiProvider.prototype.put = function (url, body) {
        return this.patch(url, body);
    };
    ApiProvider.prototype.delete = function (url, params) {
        var _this = this;
        return this.http.delete(this.global.apiUrl + url, {
            headers: this.auth.getAuthorizationHeader(),
            params: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]({ fromObject: params })
        }).map(function (r) { return r; }).catch(function (err) { return _this.handleError(err); });
    };
    ApiProvider.prototype.handleError = function (error) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        }
        else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            if (error.status === 401) {
                this.expiredSession();
            }
            else if (error.status === 403) {
                var alert_1 = this.alertCtrl.create({
                    title: 'Permissão negada',
                    message: 'Infelizmente você não tem permissão para fazer isso',
                    cssClass: 'text-danger'
                });
                alert_1.present();
            }
            else if (error.status === 404) {
                var alert_2 = this.alertCtrl.create({
                    title: 'Não encontrado',
                    message: 'O que você estava procurando não foi encontrado',
                    cssClass: 'text-danger'
                });
                alert_2.present();
            }
            else if (error.status === 400) {
                var alert_3 = this.alertCtrl.create({
                    title: 'Algo errado aconteceu',
                    message: error.error.message,
                    cssClass: 'text-danger'
                });
                alert_3.present();
            }
            else if (error.status === 500) {
                // const alert = this.alertCtrl.create({
                //   title: 'Erro interno',
                //   message: 'Por favor tente novamente',
                //   cssClass: 'text-danger'
                // });
                // alert.present();
            }
            else {
                // const alert = this.alertCtrl.create({
                //   title: 'Erro desconhecido',
                //   message: 'Por favor tente novamente',
                //   cssClass: 'text-danger'
                // });
                // alert.present();
            }
        }
        // return an observable with a user-facing error message
        return new __WEBPACK_IMPORTED_MODULE_6_rxjs_observable_ErrorObservable__["ErrorObservable"](error);
    };
    ApiProvider.prototype.expiredSession = function () {
        var _this = this;
        if (this._expiredSession) {
            this._expiredSession.present();
            return;
        }
        this._expiredSession = this.alertCtrl.create({
            title: 'Sessão Expirada',
            message: 'Você precisa logar novamente para continuar usando o aplicativo',
            cssClass: 'text-danger',
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: 'OK',
                    handler: function () {
                        _this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_login_login__["a" /* LoginPage */]).then(function () { return _this.auth.logout(); });
                    },
                    role: 'cancel'
                }
            ]
        });
        this._expiredSession.present();
    };
    ApiProvider.prototype.saveOneSignal = function () {
        var _this = this;
        if (this.auth.player_id.length === 0) {
            console.error('this.auth.player_id está em branco');
            console.error('Trying to get onesignal now');
            this.oneSignal.getIds().then(function (r) {
                console.log('oneSingal getIds:', JSON.stringify(r));
                _this.auth.player_id = r.userId;
                _this.saveOneSignal();
            });
            return;
        }
        if (this.auth.isLoggedIn()) {
            this.post('/users/' + this.auth.user.user.id + '/onesignal', { player_id: this.auth.player_id }).subscribe();
        }
        else {
            console.error('User is not logged in, onesignal could not be saved, it was stored on this.auth.player_id');
        }
    };
    ApiProvider.prototype.uploadFiles = function (file, url, options) {
        var _this = this;
        file = this.plt.is('ios') ? file.replace(/^file:\/\//, '') : file;
        var defaultOptions = {
            fileKey: 'thumbnail_principal',
            fileName: file.split('/').pop(),
            chunkedMode: false,
            headers: { 'Authorization': this.auth.token }
        };
        Object.assign(defaultOptions, options);
        var fileTransfer = this.fileTransfer.create();
        fileTransfer.onProgress(function (progressEvent) {
            var progress = Math.round((progressEvent.loaded / progressEvent.total) * 100);
            if (isNaN(progress)) {
                progress = 100;
            }
            _this.uploadProgress.next(progress);
        });
        return fileTransfer.upload(file, this.global.apiUrl + url, defaultOptions);
    };
    ApiProvider.prototype.postFile = function (fileToUpload, url) {
        var _this = this;
        var formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        var options = {
            method: 'POST',
            httpParams: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["d" /* HttpParams */]()
        };
        var req = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["e" /* HttpRequest */](options.method, this.global.apiUrl + url, formData, {
            params: options.httpParams,
            reportProgress: true,
            headers: this.auth.getAuthorizationHeader()
        });
        return this.http
            .request(req)
            .map(function (r) { return r; })
            .catch(function (e) { return _this.handleError(e); });
    };
    ApiProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_7__global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_onesignal__["a" /* OneSignal */]])
    ], ApiProvider);
    return ApiProvider;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SublistItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_document_viewer__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_mime_types__ = __webpack_require__(501);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_mime_types___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_mime_types__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_player_player__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_loading_loading__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












/**
 * Generated class for the SublistItemComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var SublistItemComponent = /** @class */ (function () {
    function SublistItemComponent(_categorias, navCtrl, iap, auth, alert, global, helpers, plt, document, player, sanitizer, loading) {
        this._categorias = _categorias;
        this.navCtrl = navCtrl;
        this.iap = iap;
        this.auth = auth;
        this.alert = alert;
        this.global = global;
        this.helpers = helpers;
        this.plt = plt;
        this.document = document;
        this.player = player;
        this.sanitizer = sanitizer;
        this.loading = loading;
        this.favoritarEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.item = {};
        this.forcarFavorito = false;
    }
    SublistItemComponent.prototype.playerPage = function () {
        var _this = this;
        this.loading.present().then(function () { return setTimeout(function () { return _this.loading.dismiss(); }, 200); });
        this.player.playerPage(this.item);
    };
    SublistItemComponent.prototype.descricao = function () {
        this.alert.create({
            title: this.item.titulo, message: this.item.descricao.replace(/(?:\r\n|\r|\n)/g, '<br>'), cssClass: 'alert-lg', buttons: [{
                    role: 'cancel',
                    text: 'Ok'
                }]
        }).present();
    };
    SublistItemComponent.prototype.favoritar = function () {
        var _this = this;
        this._categorias.favoritar(this.item).then(function () {
            _this.favoritarEvent.next();
        });
        if (this.forcarFavorito) {
            this.item.favoritado = this.item.favoritado ? 0 : 1;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])('favoritar'),
        __metadata("design:type", Object)
    ], SublistItemComponent.prototype, "favoritarEvent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], SublistItemComponent.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], SublistItemComponent.prototype, "forcarFavorito", void 0);
    SublistItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'sublist-item',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/sublist-item/sublist-item.html"*/'<div class="sublist-img" cursor-pointer (click)="playerPage()" [style.opacity]="item.gratuito || auth.isPremium() ? 1 : .4" [style.background-image]="sanitizer.bypassSecurityTrustStyle(\'url(\'+item.imagem+\')\')"></div>\n<div class="sublist-title" cursor-pointer (click)="playerPage()">{{ item.titulo }}</div>\n<div class="sublist-icons">\n  <button cursor-pointer (click)="descricao()" *ngIf="item.descricao.length > 4">\n    <ion-icon name="md-information-circle" color="primary"></ion-icon>\n  </button>\n  <img *ngIf="item.dificuldade" [src]="\'assets/icon/level-\' + item.dificuldade + \'.png\'" class="level">\n  <button>\n    <ion-icon name="duo-checkmark" [color]="item.assistido ? \'green\' : \'\'"></ion-icon>\n  </button>\n  <button>\n    <ion-icon name="duo-star" cursor-pointer (click)="favoritar()" [color]="item.favoritado ? \'yellow\' : \'\'"></ion-icon>\n  </button>\n  <button cursor-pointer (click)="playerPage()" style="width: 36px">\n    <ion-icon [name]="item.tipo == \'pdf\' ? \'ios-eye\' : \'duo-play\'" color="primary"></ion-icon>\n  </button>\n</div>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/sublist-item/sublist-item.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_8__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_document_viewer__["a" /* DocumentViewer */],
            __WEBPACK_IMPORTED_MODULE_9__providers_player_player__["a" /* PlayerProvider */],
            __WEBPACK_IMPORTED_MODULE_10__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_11__providers_loading_loading__["a" /* LoadingProvider */]])
    ], SublistItemComponent);
    return SublistItemComponent;
}());

//# sourceMappingURL=sublist-item.js.map

/***/ }),

/***/ 505:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SafePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SafePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var SafePipe = /** @class */ (function () {
    function SafePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    SafePipe.prototype.transform = function (url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    };
    SafePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'safe',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], SafePipe);
    return SafePipe;
}());

//# sourceMappingURL=safe.js.map

/***/ }),

/***/ 506:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LinkifyPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_linkifyjs_string__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_linkifyjs_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_linkifyjs_string__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


/**
 * Generated class for the LinkifyPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var LinkifyPipe = /** @class */ (function () {
    function LinkifyPipe() {
    }
    LinkifyPipe.prototype.transform = function (str) {
        return str ? __WEBPACK_IMPORTED_MODULE_1_linkifyjs_string___default()(str, { target: '_system' }) : str;
    };
    LinkifyPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'linkify',
        })
    ], LinkifyPipe);
    return LinkifyPipe;
}());

//# sourceMappingURL=linkify.js.map

/***/ }),

/***/ 515:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SublistItemDesktopComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_player_player__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_categorias_categorias__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SublistItemDesktopComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var SublistItemDesktopComponent = /** @class */ (function () {
    function SublistItemDesktopComponent(alertCtrl, player, _categorias) {
        this.alertCtrl = alertCtrl;
        this.player = player;
        this._categorias = _categorias;
        this.favoritarEvent = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["w" /* EventEmitter */]();
        this.item = {};
        this.forcarFavorito = false;
        console.log('Hello SublistItemDesktopComponent Component');
        this.text = 'Hello World';
    }
    SublistItemDesktopComponent.prototype.playerPage = function () {
        this.player.playerPage(this.item);
    };
    SublistItemDesktopComponent.prototype.descricao = function () {
        this.alertCtrl.create({
            title: this.item.titulo, message: this.item.descricao.replace(/(?:\r\n|\r|\n)/g, '<br>'), cssClass: 'alert-lg', buttons: [{
                    role: 'cancel',
                    text: 'Ok'
                }]
        }).present();
    };
    SublistItemDesktopComponent.prototype.favoritar = function () {
        var _this = this;
        this._categorias.favoritar(this.item).then(function () {
            _this.favoritarEvent.next(_this.item);
        });
        if (this.forcarFavorito) {
            this.item.favoritado = this.item.favoritado ? 0 : 1;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["P" /* Output */])('favoritar'),
        __metadata("design:type", Object)
    ], SublistItemDesktopComponent.prototype, "favoritarEvent", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], SublistItemDesktopComponent.prototype, "item", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], SublistItemDesktopComponent.prototype, "forcarFavorito", void 0);
    SublistItemDesktopComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'sublist-item-desktop',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/sublist-item-desktop/sublist-item-desktop.html"*/'<div class="thumbnail" tappable cursor-pointer (click)="playerPage()">\n  <img *ngIf="item.vimeo_thumbnail && item.tipo != \'pdf\'" [src]="item.vimeo_thumbnail">\n  <img *ngIf="item.tipo == \'pdf\'" src="assets/icon/pdf-icon.png" class="pdf">\n</div>\n<div class="info">\n  <div class="title" cursor-pointer (click)="playerPage()">{{ item.titulo }}</div>\n  <div class="icons">\n    <button cursor-pointer (click)="descricao()" *ngIf="item.descricao.length > 4">\n      <ion-icon name="md-information-circle" color="primary"></ion-icon>\n    </button>\n    <img *ngIf="item.dificuldade" [src]="\'assets/icon/level-\' + item.dificuldade + \'.png\'" class="level">\n    <button>\n      <ion-icon name="duo-checkmark" [color]="item.assistido ? \'green\' : \'\'"></ion-icon>\n    </button>\n    <button>\n      <ion-icon name="duo-star" cursor-pointer (click)="favoritar()" [color]="item.favoritado ? \'yellow\' : \'\'"></ion-icon>\n    </button>\n  </div>\n\n</div>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/sublist-item-desktop/sublist-item-desktop.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_player_player__["a" /* PlayerProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_categorias_categorias__["a" /* CategoriasProvider */]])
    ], SublistItemDesktopComponent);
    return SublistItemDesktopComponent;
}());

//# sourceMappingURL=sublist-item-desktop.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConteudosProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/*
  Generated class for the ConteudosProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ConteudosProvider = /** @class */ (function () {
    function ConteudosProvider(api, toastCtrl, alertCtrl) {
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.list = [];
    }
    ConteudosProvider.prototype.get = function (url) {
        if (!url) {
            url = '/conteudos';
        }
        return this.api.get(url);
    };
    ConteudosProvider.prototype.getAll = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var params = new URLSearchParams();
        for (var key in options) {
            params.set(key, options[key]);
        }
        var queryStr = params.toString() ? '?' + params.toString() : '';
        return this.get('/conteudos' + queryStr).map(function (r) { return _this.list = r; });
    };
    ConteudosProvider.prototype.descricao = function (item) {
        this.alertCtrl.create({
            title: item.titulo, message: item.descricao.replace(/(?:\r\n|\r|\n)/g, '<br>'), cssClass: 'alert-lg', buttons: [{
                    role: 'cancel',
                    text: 'Ok'
                }]
        }).present();
    };
    ConteudosProvider.prototype.visualizar = function (conteudo_id) {
        this.list = this.list.map(function (conteudo) {
            if (conteudo.id == conteudo_id) {
                conteudo.assistido = 1;
            }
            return conteudo;
        });
        return this.api.patch('/conteudos/' + conteudo_id + '/visualizar');
    };
    ConteudosProvider.prototype.favoritar = function (conteudo) {
        var _this = this;
        console.log("favoritar", conteudo);
        return new Promise(function (resolve) {
            if (conteudo.favoritado != 1) {
                _this.patchFavoritar(conteudo.id).subscribe(function () {
                    _this.toastCtrl.create({ message: conteudo.titulo + ' adicionado aos seus favoritos ', duration: 3000 }).present();
                    resolve();
                });
            }
            else {
                _this.deleteFavoritar(conteudo.id).subscribe(function () {
                    _this.toastCtrl.create({ message: conteudo.titulo + ' removido de seus favoritos ', duration: 3000 }).present();
                    resolve();
                });
            }
        });
    };
    ConteudosProvider.prototype.patchFavoritar = function (conteudo_id) {
        this.list = this.list.map(function (conteudo) {
            if (conteudo.id == conteudo_id) {
                conteudo.favoritado = 1;
            }
            return conteudo;
        });
        return this.api.patch('/conteudos/' + conteudo_id + '/favoritar');
    };
    ConteudosProvider.prototype.deleteFavoritar = function (conteudo_id) {
        this.list = this.list.map(function (conteudo) {
            if (conteudo.id == conteudo_id) {
                conteudo.favoritado = 0;
            }
            return conteudo;
        });
        return this.api.delete('/conteudos/' + conteudo_id + '/favoritar');
    };
    ConteudosProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */]])
    ], ConteudosProvider);
    return ConteudosProvider;
}());

//# sourceMappingURL=conteudos.js.map

/***/ }),

/***/ 517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the TabsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var TabsComponent = /** @class */ (function () {
    function TabsComponent() {
        this.tabs = [];
        console.log('Hello TabsComponent Component');
        this.text = 'Hello World';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["E" /* Input */])(),
        __metadata("design:type", Object)
    ], TabsComponent.prototype, "tabs", void 0);
    TabsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'tabs',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/tabs/tabs.html"*/'<ion-footer>\n<div class="tabs">\n  <div class="tab-item" *ngFor="let tab of tabs" cursor-pointer (click)="switchTab">\n    <ion-icon [name]="tab.icon"></ion-icon>\n    <div class="tab-label">{{ tab.label }}</div>\n  </div>\n</div>\n</ion-footer>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsComponent);
    return TabsComponent;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ToFixedPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the ToFixedPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var ToFixedPipe = /** @class */ (function () {
    function ToFixedPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    ToFixedPipe.prototype.transform = function (value) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        args[0] = args[0] ? args[0] : 1;
        value = parseFloat(value);
        if (!value) {
            return 0;
        }
        if (Number.isInteger(value)) {
            return value;
        }
        return value.toFixed(args[0]);
    };
    ToFixedPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'toFixed',
        })
    ], ToFixedPipe);
    return ToFixedPipe;
}());

//# sourceMappingURL=to-fixed.js.map

/***/ }),

/***/ 519:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

/**
 * Generated class for the FilterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    /**
     * Takes a value and makes it lowercase.
     */
    FilterPipe.prototype.transform = function (value, key, filter, operator) {
        operator = operator ? operator : '==';
        var nice = 'obj[key] ' + operator + ' filter';
        return value.filter(function (obj) { return eval(nice); });
    };
    FilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* Pipe */])({
            name: 'filter',
        })
    ], FilterPipe);
    return FilterPipe;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 520:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConsumoAguaComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ConsumoAguaComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ConsumoAguaComponent = /** @class */ (function () {
    function ConsumoAguaComponent(_agua) {
        this._agua = _agua;
        console.log('Hello ConsumoAguaComponent Component');
        this.text = 'Hello World';
    }
    ConsumoAguaComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'consumo-agua',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/consumo-agua/consumo-agua.html"*/'<div class="home-item-water">\n  <div class="home-item-water-cups">\n    <ion-icon *ngFor="let copo of _agua.item.copos;let i = index" cursor-pointer (click)="_agua.bebeCopo(i)" name="duo-water" [color]="copo ? \'blue\' : \'gray\'"></ion-icon>\n  </div>\n  <div class="home-item-water-quantity" text-center>\n    <span >\n        <!-- {{ _agua.item.consumido / 1000 | toFixed:2 }} /  -->\n      {{ _agua.item.quantidade / 1000 | toFixed:2 }}</span> LITROS\n  </div>\n\n</div>\n\n<div style="margin-top:10px;">A cada copo marcado representa 250ml de água ingerida</div>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/consumo-agua/consumo-agua.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */]])
    ], ConsumoAguaComponent);
    return ConsumoAguaComponent;
}());

//# sourceMappingURL=consumo-agua.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomeTabbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the HomeTabbarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var HomeTabbarComponent = /** @class */ (function () {
    function HomeTabbarComponent(helpers, auth, _agua, _alimentos, _exercicios) {
        this.helpers = helpers;
        this.auth = auth;
        this._agua = _agua;
        this._alimentos = _alimentos;
        this._exercicios = _exercicios;
        console.log('Hello HomeTabbarComponent Component');
        this.text = 'Hello World';
    }
    HomeTabbarComponent.prototype.titulo = function () {
        var semanas = this.helpers.moment().diff(this.helpers.moment(this.auth.user.user.created_at, 'YYYY-MM-DD HH:mm:ss'), 'weeks');
        return (semanas + 1) + 'ª semana - ' + this.helpers.moment().format('[hoje, ] DD [de] MMMM');
    };
    HomeTabbarComponent.prototype.getDay = function () {
        this._agua.get().subscribe();
        this._alimentos.getDay();
        this._exercicios.getDay();
    };
    HomeTabbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'home-tabbar',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/home-tabbar/home-tabbar.html"*/'<div class="max-width-600">\n  <div class="home-tabbar">\n  <div class="home-bar"></div>\n  <div class="home-bar active"></div>\n</div>\n\n<div class="home-bar-description" text-center text-uppercase>\n  {{ titulo() }}\n</div>\n\n\n<ion-item no-lines>\n  <ion-label color="primary">\n    <ion-icon style="font-size: 1.7em;" name="md-calendar"></ion-icon>\n  </ion-label>\n  <ion-datetime cancelText="Fechar" doneText="Ok" displayFormat="DD/MM/YYYY" min="2018" [(ngModel)]="_alimentos.selectedDate "\n    (ngModelChange)="getDay()">\n  </ion-datetime>\n</ion-item>\n\n</div>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/home-tabbar/home-tabbar.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */]])
    ], HomeTabbarComponent);
    return HomeTabbarComponent;
}());

//# sourceMappingURL=home-tabbar.js.map

/***/ }),

/***/ 522:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RadioWeekComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_easyfit_nutri_agua_easyfit_nutri_agua__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RadioWeekComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var RadioWeekComponent = /** @class */ (function () {
    function RadioWeekComponent(_alimentos, _agua, _exercicios, helpers) {
        this._alimentos = _alimentos;
        this._agua = _agua;
        this._exercicios = _exercicios;
        this.helpers = helpers;
    }
    RadioWeekComponent.prototype.getDay = function () {
        this._alimentos.getDay();
        this._agua.get().subscribe();
        this._exercicios.getDay();
    };
    RadioWeekComponent.prototype.change = function (day) {
        this._alimentos.selectedDate = day;
        console.log('clicou');
        this.getDay();
    };
    RadioWeekComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'radio-week',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/radio-week/radio-week.html"*/'<div class="radio-week">\n\n  <div class="radio-day" *ngFor="let day of _alimentos.weekDays;" cursor-pointer (click)="change(day.format(\'YYYY-MM-DD\'))">\n    <input [checked]="day.format(\'YYYY-MM-DD\') == _alimentos.selectedDate" type="radio">\n    <label text-center>\n      {{ day.format(\'D\') }}<br>\n      {{ day.format(\'ddd\') | slice:0:1 }}\n    </label>\n  </div>\n\n</div>\n\n<div padding text-center *ngIf="!_alimentos.weekDaysFormated.includes(_alimentos.selectedDate)">\n  {{ helpers.moment(_alimentos.selectedDate).format(\'LL\') }}\n</div>\n<hr>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/components/radio-week/radio-week.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_easyfit_nutri_alimentos_easyfit_nutri_alimentos__["a" /* EasyfitNutriAlimentosProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_easyfit_nutri_agua_easyfit_nutri_agua__["a" /* EasyfitNutriAguaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], RadioWeekComponent);
    return RadioWeekComponent;
}());

//# sourceMappingURL=radio-week.js.map

/***/ }),

/***/ 54:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__faca_parte_faca_parte__ = __webpack_require__(136);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__esqueci_senha_esqueci_senha__ = __webpack_require__(137);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__login_codigo_login_codigo__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__easymind_tabs_easymind_tabs__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__aplicativos_aplicativos__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};















/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(global, helpers, navCtrl, navParams, auth, loading, toastCtrl, keyboard, splashScreen, http, aplicativos, statusBar) {
        this.global = global;
        this.helpers = helpers;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.loading = loading;
        this.toastCtrl = toastCtrl;
        this.keyboard = keyboard;
        this.splashScreen = splashScreen;
        this.http = http;
        this.aplicativos = aplicativos;
        this.statusBar = statusBar;
        // @ViewChild('video') video: ElementRef;
        this.user = {
            email: '',
            password: ''
        };
        this.showVideo = false;
        if (this.navParams.get('user')) {
            this.user = this.navParams.get('user');
            this.onSubmit();
        }
        this.statusBar.backgroundColorByHexString('#000000');
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.aplicativos.clearItem();
        // this.video.nativeElement.muted = true;
        // this.video.nativeElement.play();
        setTimeout(function () {
            _this.showVideo = true;
            setTimeout(function () { return _this.splashScreen.hide(); }, 600);
        }, 3000);
    };
    LoginPage.prototype.onSubmit = function (form) {
        var _this = this;
        if (form) {
            this.helpers.formMarkAllTouched(form);
        }
        if (!(this.user.email || this.user.password)) {
            this.toastCtrl.create({ message: 'Por favor, informe seu e-mail e sua senha', duration: 3000 }).present();
            return;
        }
        this.loading.present();
        this.auth.login(this.user).subscribe(function (res) {
            _this.auth.setToken(res.token);
            _this.loading.dismiss();
            if (!_this.navParams.get('user')) {
                _this.toastCtrl.create({ message: 'Olá, ' + _this.auth.user.user.first_name + ', seja bem vindo novamente!', duration: 3000 }).present();
            }
            else {
                setTimeout(function () {
                    _this.toastCtrl.create({ message: 'Olá, ' + _this.auth.user.user.first_name + ', seja bem vindo!', duration: 3000 }).present();
                }, 3000);
            }
            if (_this.auth.isPremium()) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__aplicativos_aplicativos__["a" /* AplicativosPage */], {}, { animate: true, animation: 'md-transition' });
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__login_codigo_login_codigo__["a" /* LoginCodigoPage */], {}, { animate: true, animation: 'md-transition' });
            }
        }, function () {
            _this.toastCtrl.create({ message: 'Credenciais inválidas, por favor tente novamente', duration: 3000 }).present();
            _this.loading.dismiss();
        });
    };
    LoginPage.prototype.tabsPage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__easymind_tabs_easymind_tabs__["a" /* EasymindTabsPage */], {}, { animate: true, animation: 'md-transition' });
    };
    LoginPage.prototype.facaPartePage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__faca_parte_faca_parte__["a" /* FacaPartePage */]);
    };
    LoginPage.prototype.esqueciSenhaPage = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__esqueci_senha_esqueci_senha__["a" /* EsqueciSenhaPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/login/login.html"*/'<ion-content class="auth-pages">\n  <!-- <div class="video" style="transition: all .3s ease" [ngStyle]="{opacity: showVideo ? \'1\' : \'0\'}">\n    <video #video [hidden]="!global.informacoes.thumbnail_principal" [src]="global.informacoes.thumbnail_principal"\n      autoplay="autoplay" loop="loop" muted muted="muted"></video>\n  </div> -->\n  <div class="logo">\n    <div class="logo-img"></div>\n    <div class="bem-vindo">B E M - V I N D O</div>\n  </div>\n  <form (ngSubmit)="onSubmit(form)" #form="ngForm" class="acesso">\n    <!-- <div class="acesse">ACESSE COM SEU CÓDIGO</div> -->\n    <input placeholder="E-MAIL" required type="email" name="email" [(ngModel)]="user.email">\n    <input placeholder="SENHA" required type="password" name="password" [(ngModel)]="user.password">\n    <button ion-button block>ENTRAR</button>\n    <div class="faca-parte">\n      <span cursor-pointer (click)="facaPartePage()">Faça parte</span>\n    </div>\n    <div class="continue-visitante">\n      <span cursor-pointer (click)="esqueciSenhaPage()">Esqueci minha senha</span>\n    </div>\n\n    <!-- <div class="continue-visitante">\n      Continue como visitante\n    </div> -->\n    <a class="site" href="{{ global.informacoes.site }}" target="blank">\n      {{ helpers.removeHttp(global.informacoes.site) }}\n    </a>\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_9__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_13__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__["a" /* StatusBar */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__vimeo_player__ = __webpack_require__(488);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common_http__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the PlayerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PlayerPage = /** @class */ (function () {
    function PlayerPage(navCtrl, navParams, toastCtrl, _categorias, loading, alert, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this._categorias = _categorias;
        this.loading = loading;
        this.alert = alert;
        this.http = http;
        this.item = this.navParams.get('item');
        if (!this.item) {
            this.item = {};
            this.toastCtrl.create({ message: 'Não foi possível reproduzir o conteúdo escolhido', duration: 3000 }).present();
            this.navCtrl.pop();
            return;
        }
        this.id = this.item.vimeo.split('/');
        this.id = this.id.filter(function (piece) { return piece; });
        console.log(this.id);
        // tudo isso para caso tenha uma url do tipo http://vimeo.com/312321/?playback=true ai vai pegar realmente so o this.id do vthis.ideo
        this.id = this.id[this.id.length - 1].split('/')[0].split('?')[0];
        console.log(this.id);
    }
    PlayerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PlayerPage');
        this.init();
    };
    PlayerPage.prototype.navPop = function () {
        this.navCtrl.pop();
    };
    PlayerPage.prototype.oniFrameLoad = function (event) {
        console.log(event);
        // event.path
    };
    PlayerPage.prototype.init = function () {
        var _this = this;
        try {
            if (!this.item.vimeo) {
                throw "Nao tem link";
            }
            var player_1 = new __WEBPACK_IMPORTED_MODULE_2__vimeo_player__["a" /* default */](this.video.nativeElement);
            player_1.ready().catch(function (e) { return _this.error('ready catch', e); });
            player_1.setColor('#f15b08').then(function (color) { return console.log(color); }).catch(function (error) { return console.error(error); });
            player_1.on('play', function (e) {
                console.log('play', e);
                _this._categorias.visualizar(_this.item.id).subscribe(function (r) { }, function (e) { return player_1.pause(); });
            });
            player_1.on('loaded', function (e) {
                console.log('loaded', e);
            });
            player_1.on('error', function (e) { return _this.error('error', e); });
        }
        catch (e) {
            this.error('try catch', e);
        }
    };
    PlayerPage.prototype.error = function (log, log_error) {
        if (log === void 0) { log = ''; }
        if (log_error === void 0) { log_error = {}; }
        console.error(log, log_error);
        this.alert.create({ title: 'Oops', message: 'Não foi possível carregar, por favor tente novamente mais tarde' }).present();
        this.navCtrl.pop();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('video', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], PlayerPage.prototype, "video", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('iframe', { read: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] }),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], PlayerPage.prototype, "iframe", void 0);
    PlayerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-player',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/player/player.html"*/'<!--\n  Generated template for the PlayerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-content>\n  <div #video>\n    <iframe #iframe [src]="(\'https://player.vimeo.com/video/\' + id) | safe" *ngIf="id" width="640" height="360" frameborder="0"\n      webkitallowfullscreen mozallowfullscreen allowfullscreen (load)="oniFrameLoad($event)"></iframe>\n  </div>\n  <button navPop class="duo-navpop">\n    <ion-icon name="ios-arrow-back"></ion-icon>\n  </button>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/player/player.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common_http__["a" /* HttpClient */]])
    ], PlayerPage);
    return PlayerPage;
}());

//# sourceMappingURL=player.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PerfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(247);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_image_picker__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_uploads_uploads__ = __webpack_require__(377);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the PerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PerfilPage = /** @class */ (function () {
    function PerfilPage(navCtrl, navParams, alertCtrl, camera, imagePicker, uploads, auth, loading, api, toastCtrl, plt, aplicativos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.imagePicker = imagePicker;
        this.uploads = uploads;
        this.auth = auth;
        this.loading = loading;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.aplicativos = aplicativos;
        this.hideButtons = false;
        this.barraObjetivo = 0;
        this.hideButtons = this.navParams.get('hideButtons');
        this.dados = Object.assign({}, this.auth.user.user);
        this.barraObjetivoChange(this.dados.objetivo_kg);
        console.log(this.aplicativos.item);
    }
    PerfilPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PerfilPage');
    };
    PerfilPage.prototype.barraObjetivoChange = function (objetivo_kg) {
        this.dados.objetivo_kg = objetivo_kg;
        if (!this.dados.objetivo_kg) {
            this.dados.objetivo_kg = 1;
        }
        this.barraObjetivo = (this.dados.atual_kg / this.dados.objetivo_kg) * 100;
        if (this.dados.atual_kg > this.dados.objetivo_kg) {
            this.barraObjetivo = this.barraObjetivo - 100;
        }
    };
    PerfilPage.prototype.onSubmit = function () {
        var _this = this;
        if (this.dados.inicio_kg) {
            if (!this.dados.objetivo_kcal) {
                this.toastCtrl.create({ message: 'Informe o seu objetivo (kcal)', duration: 4000 }).present();
                return;
            }
            if (this.dados.objetivo_kg < 10 || !this.dados.objetivo_kg) {
                this.toastCtrl.create({ message: 'Informe o seu objetivo (kg)', duration: 4000 }).present();
                return;
            }
            if (!this.dados.atual_kg || !(this.dados.atual_kg < 300 && this.dados.atual_kg > 0)) {
                this.toastCtrl.create({ message: 'Qual o seu peso?', duration: 3000 }).present();
                return;
            }
            if (!this.dados.first_name) {
                this.toastCtrl.create({ message: 'Informe o seu nome', duration: 4000 }).present();
                return;
            }
            if (!this.dados.telefone) {
                this.toastCtrl.create({ message: 'Informe o seu telefone', duration: 4000 }).present();
                return;
            }
            if (!this.dados.telefone) {
                this.toastCtrl.create({ message: 'Informe o seu telefone', duration: 4000 }).present();
                return;
            }
        }
        if (this.dados.password) {
            if (this.dados.password.length < 6) {
                this.toastCtrl.create({ message: 'Informe uma senha com pelo menos 6 caracteres', duration: 4000 }).present();
                return;
            }
            if (this.dados.password != this.dados.confirmPassword) {
                this.toastCtrl.create({ message: 'As senhas não conferem, por favor tente novamente', duration: 400 }).present();
                return;
            }
        }
        else {
            delete this.dados.password;
        }
        this.loading.present();
        this.api.post('/users/' + this.auth.user.user.id, this.dados).subscribe(function (r) {
            _this.toastCtrl.create({ message: 'Dados alterados com sucesso!', duration: 4000 }).present();
            _this.loading.dismiss();
            _this.auth.setToken(r.token);
            _this.dados = _this.auth.user.user;
        }, function (e) { return _this.loading.dismiss(); });
    };
    PerfilPage.prototype.enviarFoto = function () {
        var _this = this;
        if (!this.plt.is('cordova')) {
            return;
        }
        var alert = this.alertCtrl.create({
            title: 'Alterar foto',
            buttons: [
                {
                    text: 'Galeria',
                    handler: function () { return _this.openGallery(); }
                },
                {
                    text: 'Camera',
                    handler: function () { return _this.openCamera(); }
                }
            ]
        });
        alert.present();
    };
    PerfilPage.prototype.openGallery = function () {
        var _this = this;
        this.imagePicker.getPictures({
            quality: 80,
            width: 1280,
            maximumImagesCount: 1
        }).then(function (r) {
            _this.uploads.startMultipleUpload(r, '/users/' + _this.auth.user.user.id + '/avatar').then(function (r) {
                _this.auth.setToken(JSON.parse(r[0].response).token);
                _this.dados = _this.auth.user.user;
            }).catch(function (e) { return console.error('multiupload error', e); });
        }).catch(function (e) { return console.error('imagePicker error', e); });
    };
    PerfilPage.prototype.openCamera = function () {
        var _this = this;
        var options = {
            quality: 80,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
        };
        this.camera.getPicture(options)
            .then(function (r) {
            _this.uploads.startMultipleUpload(r, '/users/' + _this.auth.user.user.id + '/avatar').then(function (r) {
                _this.auth.setToken(JSON.parse(r[0].response).token);
                _this.dados = _this.auth.user.user;
            });
        })
            .catch(function (e) { return console.error(' openCamera => e', e); });
    };
    PerfilPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-perfil',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/perfil/perfil.html"*/'<!--\n  Generated template for the PerfilPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Perfil" [hideButtons]="hideButtons"></header>\n</ion-header>\n\n\n<ion-content>\n<div class="max-width-600">\n\n  <div class="profile" cursor-pointer (click)="enviarFoto()">\n    <img default="assets/imgs/profile.png" [src]="dados.avatar" alt="">\n    <div class="alterar" *ngIf="plt.is(\'cordova\')">ALTERAR FOTO DO PERFIL</div>\n  </div>\n\n  <div *ngIf="dados.inicio_kg && aplicativos?.item?.id == 3" class="perfil-objetivo">\n    <div class="perfil-objetivo-meta">\n      <span>INÍCIO: {{ dados.inicio_kg }} KG</span>\n      <span>ATUAL: {{ dados.atual_kg }} KG</span>\n      <span cursor-pointer (click)="objetivoKg()">OBJETIVO: {{ dados.objetivo_kg }} KG</span>\n    </div>\n    <div class="perfil-objetivo-tabbar">\n      <div class="perfil-objetivo-bar"></div>\n      <div class="perfil-objetivo-bar active" [style.width.%]="barraObjetivo"></div>\n    </div>\n    <!-- <div class="perfil-objetivo-time" text-center text-uppercase>\n      20 semanas para você atingir seu objetivo\n    </div> -->\n  </div>\n\n  <form ion-list (ngSubmit)="onSubmit()">\n    <ion-item>\n      <ion-label>Nome</ion-label>\n      <ion-input name="first_name" [(ngModel)]="dados.first_name"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>CPF</ion-label>\n      <ion-input name="cpf" [(ngModel)]="dados.cpf" [brmasker]="{mask: \'000.000.000-00\'}"></ion-input>\n    </ion-item>\n\n    <ion-item *ngIf="dados.inicio_kg && aplicativos?.item?.id == 3" no-padding class="label-duas-linhas">\n      <ion-label>OBJETIVO</ion-label>\n      <ion-select style="max-width: 100%;" [(ngModel)]="dados.objetivo_descricao" name="objetivo_descricao"\n        details-none>\n        <ion-option value="Manter Peso">Manter Peso</ion-option>\n        <ion-option value="Perder Peso">Perder Peso</ion-option>\n        <ion-option value="Ganhar Peso">Ganhar Peso</ion-option>\n      </ion-select>\n    </ion-item>\n\n    <ion-item *ngIf="dados.inicio_kg && aplicativos?.item?.id == 3" no-padding class="label-duas-linhas">\n      <ion-label>OBJETIVO<br>kcal</ion-label>\n      <ion-input required type="tel" step="50" min="0" placeholder="" [(ngModel)]="dados.objetivo_kcal" name="objetivo_kcal"\n        [brmasker]="{mask:\'0000\', len:4}"></ion-input>\n    </ion-item>\n\n    <ion-item *ngIf="dados.inicio_kg && aplicativos?.item?.id == 3" no-padding class="label-duas-linhas">\n      <ion-label>OBJETIVO<br>kg</ion-label>\n      <ion-input required type="tel" (ngModelChange)="barraObjetivoChange($event)" step="50" min="0" placeholder=""\n        [ngModel]="dados.objetivo_kg" name="objetivo_kg" [brmasker]="{mask:\'000\', len:3, type: \'num\'}"></ion-input>\n    </ion-item>\n\n    <ion-item *ngIf="dados.inicio_kg && aplicativos?.item?.id == 3" no-padding class="label-duas-linhas">\n      <ion-label>ATUAL<br>kg</ion-label>\n      <ion-input required type="tel" step="50" min="0" placeholder="" [(ngModel)]="dados.atual_kg" name="atual_kg"\n        (ngModelChange)="barraObjetivoChange(dados.objetivo_kg)" [brmasker]="{mask:\'000\', len:3, type: \'num\'}"></ion-input>\n    </ion-item>\n\n\n    <ion-item>\n      <ion-label>Telefone</ion-label>\n      <!-- <ion-input [brmasker]="{phone: true}" name="telefone" [(ngModel)]="dados.telefone"></ion-input> -->\n      <ion-input [brmasker]="{mask:\'00 000000000\', len:12}" required type="tel" name="telefone" [(ngModel)]="dados.telefone" maxlength="12"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Email</ion-label>\n      <ion-input disabled [value]="dados.email"></ion-input>\n    </ion-item>\n    <ion-item>\n      <ion-label>Senha</ion-label>\n      <ion-input name="password" type="password" [(ngModel)]="dados.password"></ion-input>\n    </ion-item>\n    <ion-item class="confirmar-senha">\n      <ion-label>Confirmar<br>Senha</ion-label>\n      <ion-input name="confirmPassword" type="password" [(ngModel)]="dados.confirmPassword"></ion-input>\n    </ion-item>\n    <button ion-button block margin-top margin-bottom>ALTERAR DADOS</button>\n  </form>\n  <br>\n  <br>\n</div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/perfil/perfil.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_image_picker__["a" /* ImagePicker */],
            __WEBPACK_IMPORTED_MODULE_5__providers_uploads_uploads__["a" /* UploadsProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_8__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], PerfilPage);
    return PerfilPage;
}());

//# sourceMappingURL=perfil.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PdfsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the PdfsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var PdfsProvider = /** @class */ (function () {
    function PdfsProvider(api, iap, plt, alertCtrl, toastCtrl, _aplicativos) {
        this.api = api;
        this.iap = iap;
        this.plt = plt;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this._aplicativos = _aplicativos;
        this.list = [];
        console.log('Hello PdfsProvider Provider');
    }
    PdfsProvider.prototype.getAll = function () {
        var _this = this;
        return this.api.get('/pdfs?aplicativo_id=' + this._aplicativos.item.id).map(function (r) { return _this.list = r; });
    };
    PdfsProvider.prototype.getAllIfNecessary = function () {
        // if(this.list.length == 0) {
        this.getAll().subscribe();
        // }
    };
    PdfsProvider.prototype.open = function (pdf) {
        if (!pdf.thumbnail_principal) {
            this.alertCtrl.create({ title: 'Oops', message: 'Não foi possível carregar, por favor tente novamente mais tarde' }).present();
            return;
        }
        var options = {
            hideurlbar: 'yes',
            hidenavigationbuttons: 'yes',
            fullscreen: 'yes'
        };
        if (this.plt.is('ios')) {
            this.iap.create(pdf.thumbnail_principal, '_blank', options).show();
        }
        else {
            // this.iap.create(pdf.thumbnail_principal, '_system', options).show();
            window.open(pdf.thumbnail_principal, '_system');
        }
        this.ler(pdf.id).subscribe();
    };
    PdfsProvider.prototype.ler = function (pdf_id) {
        this.list = this.list.map(function (pdf) {
            if (pdf.id == pdf_id) {
                pdf.lido = 1;
            }
            return pdf;
        });
        return this.api.patch('/pdfs/' + pdf_id + '/ler');
    };
    PdfsProvider.prototype.favoritar = function (pdf) {
        var _this = this;
        console.log("favoritar", pdf);
        return new Promise(function (resolve) {
            if (pdf.favoritado != 1) {
                _this.patchFavoritar(pdf.id).subscribe(function () {
                    _this.toastCtrl.create({ message: pdf.titulo + ' adicionado aos seus favoritos ', duration: 3000 }).present();
                    resolve();
                }, function (e) { return resolve(); });
            }
            else {
                _this.deleteFavoritar(pdf.id).subscribe(function () {
                    _this.toastCtrl.create({ message: pdf.titulo + ' removido de seus favoritos ', duration: 3000 }).present();
                    resolve();
                }, function (e) { return resolve(); });
            }
        });
    };
    PdfsProvider.prototype.patchFavoritar = function (pdf_id) {
        this.list = this.list.map(function (pdf) {
            if (pdf.id == pdf_id) {
                pdf.favoritado = 1;
            }
            return pdf;
        });
        return this.api.patch('/pdfs/' + pdf_id + '/favoritar');
    };
    PdfsProvider.prototype.deleteFavoritar = function (pdf_id) {
        this.list = this.list.map(function (conteudo) {
            if (conteudo.id == pdf_id) {
                conteudo.favoritado = 0;
            }
            return conteudo;
        });
        return this.api.delete('/pdfs/' + pdf_id + '/favoritar');
    };
    PdfsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_in_app_browser__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["m" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], PdfsProvider);
    return PdfsProvider;
}());

//# sourceMappingURL=pdfs.js.map

/***/ }),

/***/ 65:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitNutriTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__contact_contact__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__perfil_perfil__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_native_page_transitions__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__easyfit_nutri_home_easyfit_nutri_home__ = __webpack_require__(384);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__ = __webpack_require__(385);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__easyfit_nutri_exercicios_easyfit_nutri_exercicios__ = __webpack_require__(386);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








// import { EasyfitNutriDicasPage } from './../dicas/dicas';




var EasyfitNutriTabsPage = /** @class */ (function () {
    function EasyfitNutriTabsPage(nativePageTransitions, navParams, splashScreen) {
        this.nativePageTransitions = nativePageTransitions;
        this.navParams = navParams;
        this.splashScreen = splashScreen;
        this.tab0Root = __WEBPACK_IMPORTED_MODULE_2__perfil_perfil__["a" /* PerfilPage */];
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_9__easyfit_nutri_refeicoes_easyfit_nutri_refeicoes__["a" /* EasyfitNutriRefeicoesPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_7__easyfit_nutri_controle_agua_easyfit_nutri_controle_agua__["a" /* EasyfitNutriControleAguaPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_6__easyfit_nutri_home_easyfit_nutri_home__["a" /* EasyfitNutriHomePage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_11__easyfit_nutri_exercicios_easyfit_nutri_exercicios__["a" /* EasyfitNutriExerciciosPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_8__easyfit_nutri_calculo_calorico_easyfit_nutri_calculo_calorico__["a" /* EasyfitNutriCalculoCaloricoPage */];
        this.tab6Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab7Root = __WEBPACK_IMPORTED_MODULE_0__contact_contact__["a" /* ContactPage */];
        // tab8Root = DicasPage;
        this.tabIndex = 0;
        this.select = this.navParams.get('select');
    }
    EasyfitNutriTabsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () { return _this.splashScreen.hide(); }, 600);
        if (this.select !== undefined) {
            this.tabRef.select(this.select);
        }
    };
    EasyfitNutriTabsPage.prototype.getAnimationDirection = function (index) {
        var currentIndex = this.tabIndex;
        this.tabIndex = index;
        switch (true) {
            case (currentIndex < index):
                return ('left');
            case (currentIndex > index):
                return ('right');
        }
    };
    EasyfitNutriTabsPage.prototype.transition = function (e) {
        var options = {
            direction: this.getAnimationDirection(e.index),
            duration: 300,
            slowdownfactor: -.5,
            slidePixels: 0,
            iosdelay: 10,
            androiddelay: 10,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 0,
        };
        // if (!this.loaded) {
        //   this.loaded = true;
        //   return;
        // }
        this.nativePageTransitions.slide(options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["_9" /* ViewChild */])('tabRef'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["o" /* Tabs */])
    ], EasyfitNutriTabsPage.prototype, "tabRef", void 0);
    EasyfitNutriTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_3__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-tabs/easyfit-nutri-tabs.html"*/'<ion-tabs #tabRef selectedIndex="3" (ionChange)="transition($event)">\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab0Root" [show]="false"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab1Root" tabIcon="duo-restaurant" color="gray"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab2Root" tabIcon="duo-water" color="gray"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab3Root" tabIcon="duo-easylife-large"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab4Root" tabIcon="duo-dumbbell" color="gray"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab5Root" tabIcon="duo-kcal" color="gray"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab6Root" [show]="false"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab7Root" [show]="false"></ion-tab>\n    <ion-tab [tabsHideOnSubPages]="false" [root]="tab8Root" [show]="false"></ion-tab>\n  </ion-tabs>\n  '/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-nutri-tabs/easyfit-nutri-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], EasyfitNutriTabsPage);
    return EasyfitNutriTabsPage;
}());

//# sourceMappingURL=easyfit-nutri-tabs.js.map

/***/ }),

/***/ 7:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadingProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var LoadingProvider = /** @class */ (function () {
    function LoadingProvider(loadingCtrl) {
        this.loadingCtrl = loadingCtrl;
        console.log('Hello LoadingProvider Provider');
    }
    LoadingProvider.prototype.present = function () {
        // this.loading = this.loadingCtrl.create({
        //   spinner: 'hide',
        //   content: `
        //   <div class="custom-spinner-container">
        //     <img class="loading" width="80" height="80" src="assets/loading.gif" />
        //   </div>`
        // });
        this.loading = this.loadingCtrl.create();
        return this.loading.present();
    };
    LoadingProvider.prototype.dismiss = function () {
        var _this = this;
        return new Promise(function (resolve) {
            if (_this.loading) {
                return _this.loading.dismiss(resolve(true)).catch(function (error) {
                    console.log('loading error: ', error);
                });
            }
            else {
                resolve(true);
            }
        });
    };
    LoadingProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* LoadingController */]])
    ], LoadingProvider);
    return LoadingProvider;
}());

//# sourceMappingURL=loading.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginCodigoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_global__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__aplicativos_aplicativos__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__ = __webpack_require__(53);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the LoginCodigoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginCodigoPage = /** @class */ (function () {
    function LoginCodigoPage(navCtrl, navParams, auth, global, keyboard, toastCtrl, loading, http, helpers, alertCtrl, statusBar) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.global = global;
        this.keyboard = keyboard;
        this.toastCtrl = toastCtrl;
        this.loading = loading;
        this.http = http;
        this.helpers = helpers;
        this.alertCtrl = alertCtrl;
        this.statusBar = statusBar;
        this.obj = {
            identificador: '',
            code: ''
        };
        this.statusBar.backgroundColorByHexString('#000000');
        if (this.helpers.isAppleReview()) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__aplicativos_aplicativos__["a" /* AplicativosPage */]);
        }
    }
    LoginCodigoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginCodigoPage');
        var obj = window.localStorage.getItem('login-codigo');
        if (obj) {
            this.obj = JSON.parse(obj);
        }
    };
    LoginCodigoPage.prototype.onSubmit = function (form) {
        var _this = this;
        if (form.invalid) {
            this.toastCtrl.create({ message: 'Preencha todos os campos', duration: 2000 }).present();
            return;
        }
        this.loading.present();
        window.localStorage.setItem('login-codigo', JSON.stringify(this.obj));
        this.http.post(this.global.apiUrl + '/users/' + this.auth.user.user.id + '/premium', this.obj, {
            headers: this.auth.getAuthorizationHeader()
        }).subscribe(function (r) {
            _this.auth.setToken(r.token);
            _this.loading.dismiss();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__aplicativos_aplicativos__["a" /* AplicativosPage */], {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' });
        }, function (e) {
            if (e.status == 400) {
                _this.alertCtrl.create({ title: 'Oops...', message: e.error.message }).present();
            }
            if (e.status == 401) {
                _this.alertCtrl.create({
                    title: 'Sessão Expirada',
                    message: 'Você precisa logar novamente para continuar usando o aplicativo',
                    cssClass: 'text-danger',
                    enableBackdropDismiss: false,
                    buttons: [
                        {
                            text: 'OK',
                            handler: function () {
                                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]).then(function () { return _this.auth.logout(); });
                            },
                            role: 'cancel'
                        }
                    ]
                }).present();
            }
            _this.loading.dismiss();
        });
    };
    LoginCodigoPage.prototype.logout = function () {
        var _this = this;
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */], {}, { animate: true, duration: 1000, animation: 'wp-transition', direction: 'forward' }).then(function () { return _this.auth.logout(); });
    };
    LoginCodigoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-login-codigo',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/login-codigo/login-codigo.html"*/'<ion-content class="auth-pages" [hidden]="helpers.isAppleReview()">\n  <div class="logo" [ngStyle]="{keyboard:  keyboard.isOpen() ? 0 : 1}">\n    <div class="logo-img"></div>\n    <div class="bem-vindo">ACESSE COM SEU CÓDIGO</div>\n  </div>\n  <form (ngSubmit)="onSubmit(form)" #form="ngForm" class="acesso">\n    <!-- <div class="acesse">ACESSE COM SEU CÓDIGO</div> -->\n    <div class="acesse">Olá, {{ auth.user.user.first_name }}</div>\n    <input placeholder="Código" type="text" name="code" required [(ngModel)]="obj.code">\n    <input placeholder="Identificação" type="text" name="identificador" required [(ngModel)]="obj.identificador">\n\n    <button ion-button block>ENTRAR</button>\n    <div class="faca-parte" cursor-pointer (click)="logout()">\n      Voltar\n    </div>\n\n    <!-- <div class="continue-visitante">\n      Continue como visitante\n    </div> -->\n    <a class="site" href="{{ global.informacoes.site }}" target="blank">\n      {{ helpers.removeHttp(global.informacoes.site) }}\n    </a>\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/login-codigo/login-codigo.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_global__["a" /* GlobalProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Keyboard */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_status_bar__["a" /* StatusBar */]])
    ], LoginCodigoPage);
    return LoginCodigoPage;
}());

//# sourceMappingURL=login-codigo.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AplicativosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__easymind_tabs_easymind_tabs__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__easyfit_sports_tabs_easyfit_sports_tabs__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cadastro_usuario_cadastro_usuario__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__easyfit_nutri_tabs_easyfit_nutri_tabs__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__login_codigo_login_codigo__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__login_login__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__gestao_tabs_gestao_tabs__ = __webpack_require__(388);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__central_tabs_central_tabs__ = __webpack_require__(391);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var AplicativosPage = /** @class */ (function () {
    function AplicativosPage(navCtrl, loading, aplicativos, helpers, auth, statusBar, alertCtrl, _categorias) {
        this.navCtrl = navCtrl;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this.helpers = helpers;
        this.auth = auth;
        this.statusBar = statusBar;
        this.alertCtrl = alertCtrl;
        this._categorias = _categorias;
    }
    AplicativosPage.prototype.ionViewWillEnter = function () {
        this.aplicativos.clearItem();
    };
    AplicativosPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loading.present();
        this.auth.isPremiumAsync().catch(function (r) {
            if (!_this.helpers.isAppleReview()) {
                if (r.status == 401) {
                    _this.alertCtrl.create({
                        title: 'Sessão Expirada',
                        message: 'Você precisa logar novamente para continuar usando o aplicativo',
                    }).present().then(function (r) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_13__login_login__["a" /* LoginPage */]).then(function (r) { return _this.auth.logout(); });
                    });
                }
                else {
                    _this.alertCtrl.create({ title: 'Código expirado', message: 'Seu código expirou' }).present().then(function (r) {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__login_codigo_login_codigo__["a" /* LoginCodigoPage */]);
                    });
                }
            }
        });
        this.aplicativos.getAll().then(function () { return _this.loading.dismiss(); }).catch(function () { return _this.loading.dismiss(); });
    };
    AplicativosPage.prototype.setItem = function (item) {
        var _this = this;
        this.aplicativos.setItem(item);
        setTimeout(function () {
            if (item.id == 1) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__easymind_tabs_easymind_tabs__["a" /* EasymindTabsPage */]);
            }
            else if (item.id == 2) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */]);
            }
            else if (item.id == 3) {
                if (!_this.auth.user.user.inicio_kg) {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__cadastro_usuario_cadastro_usuario__["a" /* CadastroUsuarioPage */]);
                }
                else {
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__easyfit_nutri_tabs_easyfit_nutri_tabs__["a" /* EasyfitNutriTabsPage */]);
                }
            }
            else if (item.id == 5) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__gestao_tabs_gestao_tabs__["a" /* GestaoTabsPage */]);
            }
            else if (item.id == 6) {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__central_tabs_central_tabs__["a" /* CentralTabsPage */]);
            }
            else {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__easyfit_sports_tabs_easyfit_sports_tabs__["a" /* EasyfitSportsTabsPage */]);
            }
        }, 50);
        // limpa alguns dados que estavam salvos anteriormente
        this._categorias.current = {};
        this._categorias.list = [];
    };
    AplicativosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-aplicativos',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/aplicativos/aplicativos.html"*/'<!--\n  Generated template for the AplicativosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Módulos" [showPerfil]="true"></header>\n</ion-header>\n<ion-content>\n  <div class="apps">\n    <a *ngFor="let app of aplicativos.list;let key = index" [hidden]="app.id == 3" cursor-pointer (click)="setItem(app)"\n      [style.background-image]="helpers.sanitizeStyle(\'url(\' + app.fundo +  \')\')" [style.background-color]="app.cor"\n      [class.linear]="!app.fundo">\n      <img src="{{ (app.thumbnail_principal) }}">\n      <div class="entrar">Entrar</div>\n    </a>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/aplicativos/aplicativos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_12__providers_categorias_categorias__["a" /* CategoriasProvider */]])
    ], AplicativosPage);
    return AplicativosPage;
}());

//# sourceMappingURL=aplicativos.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasymindTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__categorias_categorias__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__frase_do_dia_frase_do_dia__ = __webpack_require__(374);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__favoritos_favoritos__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__audios_audios__ = __webpack_require__(376);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__perfil_perfil__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__codigo_codigo__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__conheca_conheca__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_tabs_tabs__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__categorias_lista_categorias_lista__ = __webpack_require__(378);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_categorias_categorias__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var EasymindTabsPage = /** @class */ (function () {
    function EasymindTabsPage(navParams, nativePageTransitions, splashScreen, api, auth, _categorias, loading, tabs) {
        this.navParams = navParams;
        this.nativePageTransitions = nativePageTransitions;
        this.splashScreen = splashScreen;
        this.api = api;
        this.auth = auth;
        this._categorias = _categorias;
        this.loading = loading;
        this.tabs = tabs;
        this.tab0Root = __WEBPACK_IMPORTED_MODULE_15__categorias_lista_categorias_lista__["a" /* CategoriasListaPage */];
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__frase_do_dia_frase_do_dia__["a" /* FraseDoDiaPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__categorias_categorias__["a" /* CategoriasPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_4__favoritos_favoritos__["a" /* FavoritosPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_5__audios_audios__["a" /* AudiosPage */];
        this.tab5Root = __WEBPACK_IMPORTED_MODULE_7__perfil_perfil__["a" /* PerfilPage */];
        this.tab6Root = __WEBPACK_IMPORTED_MODULE_8__codigo_codigo__["a" /* CodigoPage */];
        this.tab7Root = __WEBPACK_IMPORTED_MODULE_10__conheca_conheca__["a" /* ConhecaPage */];
        this.loaded = false;
        this.tabIndex = 0;
        this.select = this.navParams.get('select');
    }
    EasymindTabsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        // salva onesignal
        this.api.saveOneSignal();
        // encontra todas conteudos
        this._categorias.getAll().then(function () { return _this.loading.dismiss(); }, function () { return _this.loading.dismiss(); });
        //atualiza token do usuario
        setTimeout(function () { return _this.splashScreen.hide(); }, 600);
        this.tabs._selectTab.subscribe(function (select) {
            try {
                _this.tabRef.select(select);
            }
            catch (e) {
                console.error('BUGOU MAS LOGO DESBUGAMOS');
                try {
                    _this.tabRef.select(select);
                }
                catch (e) {
                    // deu erro mas deu certo vai entender
                    console.log(' and the gambiarra has won once again');
                }
            }
        });
        if (this.select !== undefined) {
            this.tabRef.select(this.select);
        }
    };
    EasymindTabsPage.prototype.getAnimationDirection = function (index) {
        var currentIndex = this.tabIndex;
        this.tabIndex = index;
        switch (true) {
            case (currentIndex < index):
                return ('left');
            case (currentIndex > index):
                return ('right');
        }
    };
    EasymindTabsPage.prototype.transition = function (e) {
        var options = {
            direction: this.getAnimationDirection(e.index),
            duration: 300,
            slowdownfactor: -.5,
            slidePixels: 0,
            iosdelay: 10,
            androiddelay: 10,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 0,
        };
        if (!this.loaded) {
            this.loaded = true;
            return;
        }
        this.nativePageTransitions.slide(options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('tabRef'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["o" /* Tabs */])
    ], EasymindTabsPage.prototype, "tabRef", void 0);
    EasymindTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easymind-tabs/easymind-tabs.html"*/'<ion-tabs #tabRef selectedIndex="2" (ionChange)="transition($event)">\n  <ion-tab tabsHideOnSubPages="true" [root]="tab0Root" tabIcon="tab-duo-play"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab1Root" tabIcon="tab-duo-quotes"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab2Root" tabIcon="easymind-isotipo"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab3Root" tabIcon="tab-duo-star"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab4Root" tabIcon="tab-duo-music"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab5Root" tabIcon="duo-profile" [show]="false"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab6Root" tabIcon="duo-profile" [show]="false"></ion-tab>\n  <ion-tab tabsHideOnSubPages="true" [root]="tab7Root" tabIcon="duo-profile" [show]="false"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easymind-tabs/easymind-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_11__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_12__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_16__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_13__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_14__providers_tabs_tabs__["a" /* TabsProvider */]])
    ], EasymindTabsPage);
    return EasymindTabsPage;
}());

//# sourceMappingURL=easymind-tabs.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_easymind_tabs_easymind_tabs__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the TabsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var TabsProvider = /** @class */ (function () {
    function TabsProvider(app, aplicativos) {
        this.app = app;
        this.aplicativos = aplicativos;
        this._selectTab = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        console.log('Hello TabsProvider Provider');
    }
    TabsProvider.prototype.selectTab = function (index) {
        console.log('open tab 3');
        if (this.aplicativos.item.id == 1) {
            var tabPage = this.app.getRootNav().getViews().find(function (v) { return v.component.name == 'EasymindTabsPage'; });
            if (tabPage) {
                // console.log(tabPage, 'FOUND');
                this._selectTab.next(index);
            }
            else {
                this.app.getRootNav().setRoot(__WEBPACK_IMPORTED_MODULE_3__pages_easymind_tabs_easymind_tabs__["a" /* EasymindTabsPage */], { select: index }, { animate: true, animation: 'md-transition', duration: 1000, direction: 'forward' });
            }
        }
        // this.app.getRootNav().setRoot(TabsPage, { select: 3 }, { animate: true, animation: 'wp-transition' });
    };
    TabsProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_4__aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], TabsProvider);
    return TabsProvider;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_categorias_categorias__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__player_player__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the FavoritosPage page.
 *
 * See https://ionicframework.com/docsodel/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FavoritosPage = /** @class */ (function () {
    function FavoritosPage(navCtrl, navParams, api, auth, toastCtrl, _categorias, _loading, aplicativos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.api = api;
        this.auth = auth;
        this.toastCtrl = toastCtrl;
        this._categorias = _categorias;
        this._loading = _loading;
        this.aplicativos = aplicativos;
        this.videos_open = true;
        this.audios_open = false;
        this.pdfs_open = false;
        this.videos = [];
        this.audios = [];
        this.pdfs = [];
        this.conteudos = [];
        this.loading = false;
    }
    // cada vez q a tab é aberta ele busca na api os favoritos do usuario
    FavoritosPage.prototype.ionViewWillEnter = function () {
        this.onRefresher();
        console.log('ionViewDidLoad AudiosPage');
    };
    FavoritosPage.prototype.onRefresher = function () {
        var _this = this;
        if (!(this.videos.length || this.audios.length)) {
            this._loading.present();
        }
        this.loading = true;
        this.api.get('/users/' + this.auth.user.user.id + '/favoritos?aplicativo_id=' + this.aplicativos.item.id).subscribe(function (r) {
            _this.refresher.complete();
            _this._loading.dismiss();
            _this.loading = false;
            _this.videos = r.filter(function (obj) { return obj.tipo == 'video'; });
            _this.conteudos = _this.videos;
            _this.audios = r.filter(function (obj) { return obj.tipo == 'audio'; });
            _this.pdfs = r.filter(function (obj) { return obj.tipo == 'pdf'; });
        }, function (e) { return _this._loading.dismiss(); });
    };
    FavoritosPage.prototype.playerPage = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__player_player__["a" /* PlayerPage */], { item: item });
    };
    FavoritosPage.prototype.favoritar = function (item) {
        this.videos = this.videos.filter(function (obj) { return obj.id != item.id; });
        this.audios = this.audios.filter(function (obj) { return obj.id != item.id; });
        this.pdfs = this.pdfs.filter(function (obj) { return obj.id != item.id; });
        this.conteudos = this.conteudos.filter(function (obj) { return obj.id != item.id; });
        // this._categorias.favoritar(item);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], FavoritosPage.prototype, "refresher", void 0);
    FavoritosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-favoritos',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/favoritos/favoritos.html"*/'<!--\n  Generated template for the FavoritosPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Favoritos"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="easymind-list" padding-horizontal>\n\n    <div class="list-item" *ngIf="videos.length" [ngClass]="{\'open\': videos_open}">\n      <div class="list-title" cursor-pointer (click)="videos_open = !videos_open; audios_open = false; pdfs_open = false">\n        Vídeos\n        <ion-icon name="ios-arrow-forward"></ion-icon>\n      </div>\n      <div class="sublist">\n        <sublist-item *ngFor="let video of videos" [item]="video" (favoritar)="favoritar(video)"></sublist-item>\n\n      </div>\n    </div>\n\n    <div class="list-item" *ngIf="audios.length" [ngClass]="{\'open\': audios_open}">\n      <div class="list-title" cursor-pointer (click)="audios_open = !audios_open; videos_open = false; pdfs_open = false">\n        Áudios\n        <ion-icon name="ios-arrow-forward"></ion-icon>\n      </div>\n      <div class="sublist">\n        <sublist-item *ngFor="let audio of audios" [item]="audio" (favoritar)="favoritar(audio)"></sublist-item>\n      </div>\n    </div>\n\n    <div class="list-item" *ngIf="pdfs.length" [ngClass]="{\'open\': pdfs_open}">\n      <div class="list-title" cursor-pointer (click)="pdfs_open = !pdfs_open; videos_open = false; audios_open = false">\n        Documentos\n        <ion-icon name="ios-arrow-forward"></ion-icon>\n      </div>\n      <div class="sublist">\n        <sublist-item *ngFor="let pdf of pdfs" [item]="pdf" (favoritar)="favoritar(pdf)"></sublist-item>\n      </div>\n    </div>\n\n  </div>\n\n  <div *ngIf="!(videos.length || audios.length || pdfs.length) && !loading" padding text-center>\n    Você ainda não adicionou nada aos seus favoritos\n  </div>\n\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/favoritos/favoritos.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_categorias_categorias__["a" /* CategoriasProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], FavoritosPage);
    return FavoritosPage;
}());

//# sourceMappingURL=favoritos.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EasyfitSportsTabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__ = __webpack_require__(52);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__favoritos_favoritos__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__contato_contato__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pdfs_e_dicas_pdfs_e_dicas__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_pdfs_pdfs__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__easyfit_sports_home_easyfit_sports_home__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var EasyfitSportsTabsPage = /** @class */ (function () {
    function EasyfitSportsTabsPage(navParams, nativePageTransitions, _pdfs, aplicativos) {
        this.navParams = navParams;
        this.nativePageTransitions = nativePageTransitions;
        this._pdfs = _pdfs;
        this.aplicativos = aplicativos;
        this.tab0Root = __WEBPACK_IMPORTED_MODULE_5__categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */];
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_2__favoritos_favoritos__["a" /* FavoritosPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_8__easyfit_sports_home_easyfit_sports_home__["a" /* EasyfitSportsHomePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_6__pdfs_e_dicas_pdfs_e_dicas__["a" /* PdfsEDicasPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__contato_contato__["a" /* ContatoPage */];
        this.loaded = false;
        this.tabIndex = 0;
        this.select = this.navParams.get('select');
        this._pdfs.getAllIfNecessary();
        console.log(aplicativos);
        console.log('item', aplicativos.item);
    }
    EasyfitSportsTabsPage.prototype.ionViewDidLoad = function () {
        if (this.select !== undefined) {
            this.tabRef.select(this.select);
        }
    };
    EasyfitSportsTabsPage.prototype.getAnimationDirection = function (index) {
        var currentIndex = this.tabIndex;
        this.tabIndex = index;
        switch (true) {
            case (currentIndex < index):
                return ('left');
            case (currentIndex > index):
                return ('right');
        }
    };
    EasyfitSportsTabsPage.prototype.transition = function (e) {
        var options = {
            direction: this.getAnimationDirection(e.index),
            duration: 300,
            slowdownfactor: -.5,
            slidePixels: 0,
            iosdelay: 10,
            androiddelay: 10,
            fixedPixelsTop: 0,
            fixedPixelsBottom: 0,
        };
        if (!this.loaded) {
            this.loaded = true;
            return;
        }
        this.nativePageTransitions.slide(options);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('tabRef'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* Tabs */])
    ], EasyfitSportsTabsPage.prototype, "tabRef", void 0);
    EasyfitSportsTabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-sports-tabs/easyfit-sports-tabs.html"*/'<ion-tabs #tabRef selectedIndex="2" (ionChange)="transition($event)">\n    <ion-tab tabsHideOnSubPages="true" [root]="tab0Root" [tabIcon]="aplicativos?.item?.id == 4 ? \'tab-duo-3books\' : (aplicativos?.item?.id == 7 ? \'tab-duo-utensils\' : \'tab-duo-hand\')"></ion-tab>\n    <ion-tab tabsHideOnSubPages="true" [root]="tab1Root" tabIcon="tab-duo-star"></ion-tab>\n    <ion-tab tabsHideOnSubPages="true" [root]="tab2Root" tabIcon="easymind-isotipo"></ion-tab>\n    <ion-tab tabsHideOnSubPages="true" [root]="tab3Root" tabIcon="tab-duo-book"></ion-tab>\n    <ion-tab tabsHideOnSubPages="true" [root]="tab4Root" tabIcon="tab-duo-phone"></ion-tab>\n  </ion-tabs>\n  '/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/easyfit-sports-tabs/easyfit-sports-tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_native_page_transitions__["a" /* NativePageTransitions */],
            __WEBPACK_IMPORTED_MODULE_7__providers_pdfs_pdfs__["a" /* PdfsProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], EasyfitSportsTabsPage);
    return EasyfitSportsTabsPage;
}());

//# sourceMappingURL=easyfit-sports-tabs.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContatoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_helpers_helpers__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_aplicativos_aplicativos__ = __webpack_require__(12);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the ContatoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ContatoPage = /** @class */ (function () {
    function ContatoPage(navCtrl, navParams, auth, helpers, toastCtrl, api, loading, aplicativos) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.helpers = helpers;
        this.toastCtrl = toastCtrl;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this.contato = {
            nome: this.auth.user.user.first_name,
            telefone: this.auth.user.user.telefone,
            email: this.auth.user.user.email,
            mensagem: ''
        };
    }
    ContatoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContatoPage');
    };
    ContatoPage.prototype.onSubmit = function (form) {
        var _this = this;
        if (this.aplicativos.item.id) {
            this.contato.aplicativo_id = this.aplicativos.item.id;
        }
        else {
            this.contato.aplicativo_id = null;
        }
        if (!this.helpers.isEmail(this.contato.email)) {
            this.toastCtrl.create({ message: 'Digite um e-mail válido', duration: 4000 }).present();
            return;
        }
        if (!form.valid) {
            this.toastCtrl.create({ message: 'Verifique se todos os campos estão preenchidos', duration: 4000 }).present();
            return;
        }
        this.loading.present();
        this.api.post('/contato', this.contato).subscribe(function (r) {
            _this.loading.dismiss();
            _this.toastCtrl.create({ message: 'Sua mensagem foi enviada, responderemos o mais breve possível!', duration: 4000 }).present();
            _this.contato = {
                nome: _this.auth.user.user.first_name,
                telefone: _this.auth.user.user.telefone,
                email: _this.auth.user.user.email,
                mensagem: ''
            };
        }, function (e) { return _this.loading.dismiss(); });
    };
    ContatoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-contato',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/contato/contato.html"*/'<!--\n  Generated template for the ContatoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Contato" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content padding>\n  <form class="max-width-600" ion-list padding (ngSubmit)="onSubmit(form)" #form="ngForm">\n    <p>\n      PARA QUALQUER INFORMAÇÃO ENTRE EM CONTATO\n    </p>\n    <input required type="text" name="nome" [(ngModel)]="contato.nome" required placeholder="NOME">\n    <input style="margin-top:16px;margin-bottom:16px;" required type="tel" name="telefone" [(ngModel)]="contato.telefone" placeholder="TELEFONE" mask="00 00000-0000">\n    <input required type="email" name="email" [(ngModel)]="contato.email" required placeholder="E-MAIL">\n    <textarea required name="mensagem" margin-top rows="4" [(ngModel)]="contato.mensagem"\n      placeholder="MENSAGEM"></textarea>\n\n    <button ion-button block margin-top margin-bottom>ENVIAR</button>\n  </form>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/contato/contato.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_helpers_helpers__["a" /* HelpersProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */]])
    ], ContatoPage);
    return ContatoPage;
}());

//# sourceMappingURL=contato.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoCadastroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__gestao_listagem_gestao_listagem__ = __webpack_require__(85);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GestaoCadastroPage = /** @class */ (function () {
    function GestaoCadastroPage(navCtrl, navParams, _areas, api, loading, aplicativos, toastCtrl, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._areas = _areas;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this.toastCtrl = toastCtrl;
        this.auth = auth;
        this.subareas = [];
        this.consulta = {
            area_id: '',
            subarea_id: '',
            nome: '',
            telefone: '',
            data_consulta: '',
            observacoes: '',
            tem_revisao: false,
            data_revisao: '',
            thumbnail_principal: ''
        };
        this.file = null;
        this.imgSrc = '';
        this.minDate = new Date().toISOString();
        if (!this._areas.list.length) {
            this.loading.present();
        }
        this.onRefresher();
        if (this.navParams.get('item')) {
            this.consulta = this.navParams.get('item');
            this.imgSrc = this.consulta.thumbnail_principal ? this.consulta.thumbnail_principal : '';
            console.log(this.imgSrc);
            this.doSearch(this.consulta.area_id);
        }
        this.parentPage = this.navParams.get('parentPage');
    }
    GestaoCadastroPage.prototype.onRefresher = function () {
        var _this = this;
        this._areas.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
        this.ionViewDidEnter();
    };
    GestaoCadastroPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._areas.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoCadastroPage.prototype.doSearch = function (e) {
        var area = this._areas.list.find(function (area) { return area.id == e; });
        this.subareas = area.subareas;
    };
    GestaoCadastroPage.prototype.onSubmit = function (form) {
        var _this = this;
        if (!form.valid) {
            this.toastCtrl.create({ message: 'Verifique se todos os campos estão preenchidos', duration: 4000 }).present();
            return;
        }
        this.loading.present();
        this.api.post('/users/' + this.auth.user.user.id + '/consultas', this.consulta).subscribe(function (r) {
            _this.loading.dismiss();
            _this.toastCtrl.create({ message: 'Sua consulta foi cadastrada com sucesso!', duration: 4000 }).present();
            _this.consulta = {
                area_id: '',
                subarea_id: '',
                nome: '',
                telefone: '',
                data_consulta: '',
                observacoes: '',
                tem_revisao: '',
                data_revisao: '',
                thumbnail_principal: ''
            };
            _this.imgSrc = '';
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__gestao_listagem_gestao_listagem__["a" /* GestaoListagemPage */]);
        }, function (e) { return _this.loading.dismiss(); });
    };
    GestaoCadastroPage.prototype.onChangeInputFiles = function (files) {
        var _this = this;
        this.file = files.item(0);
        this.submitImagens('/users/' + this.auth.user.user.id + '/imagemConsulta').then(function (e) {
            _this.consulta.thumbnail_principal = e.body.thumbnail_principal;
            _this.imgSrc = e.body.preview;
            ;
        });
    };
    GestaoCadastroPage.prototype.submitImagens = function (url) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.loading.present();
            _this.api.postFile(_this.file, url)
                .subscribe(function (event) {
                if (event.type === 4) {
                    _this.loading.dismiss();
                    resolve(event);
                }
            }, function (err) { return _this.loading.dismiss(); });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], GestaoCadastroPage.prototype, "refresher", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])('file'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */])
    ], GestaoCadastroPage.prototype, "inputFile", void 0);
    GestaoCadastroPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-gestao-cadastro',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-cadastro/gestao-cadastro.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Cadastro" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="cadastro">\n    <form (ngSubmit)="onSubmit(form)" #form="ngForm">\n      <div class="row">\n        <label for="area">ÁREA:</label> \n        <ion-select cancelText="Cancelar" [(ngModel)]="consulta.area_id" id="area_id" name="area_id" (ngModelChange)="doSearch($event)">\n          <ion-option *ngFor="let item of _areas.list" [value]="item.id" >{{item.descricao}}</ion-option>\n        </ion-select>\n      </div>  \n      <div class="row">\n        <label for="subarea_id">SUBÁREA:</label> \n        <ion-select cancelText="Cancelar" [(ngModel)]="consulta.subarea_id" id="subarea_id" name="subarea_id" >\n          <ion-option *ngFor="let s of subareas" [value]="s.id" >{{s.descricao}}</ion-option>\n        </ion-select>\n      </div> \n        <div class="row">\n            <label for="nome">NOME:</label> \n            <input class="input"  [(ngModel)]="consulta.nome" required id="nome" name="nome"/>\n        </div> \n        <div class="row">\n            <label for="telefone">TELEFONE:</label> \n            <input class="input" [(ngModel)]="consulta.telefone" id="telefone" name="telefone" mask="00 00000-0000"/>\n        </div> \n        <div class="row">\n          <label for="data" stacked>DATA DA CONSULTA:</label> \n            <ion-item>\n              <ion-datetime cancelText="Fechar" max="2030-10-31" doneText="Confirmar" [(ngModel)]="consulta.data_consulta" name="data_consulta" required class="input" displayFormat="DD/MM/YYYY" ></ion-datetime>\n            </ion-item>\n        </div>\n        <div class="row">\n            <label for="resumo">RESUMO DA CONSULTA:</label> \n            <textarea class="input" [(ngModel)]="consulta.observacoes" id="observacoes" name="observacoes" rows="4"> </textarea>\n        </div>\n        <div class="row">\n          <input type="file" #file name="file" id="file" (change)="onChangeInputFiles($event.target.files)"\n            style="position:absolute;left: 0;opacity: 0;" accept="image/*">\n          <label for="file" stacked>ANEXAR IMAGEM:<br><small>(Laudo, exame, receita)</small></label> \n          <label for="file"><ion-icon name="camera"></ion-icon></label>\n          <ion-img [src]="imgSrc"></ion-img>\n\n        </div>\n        <div class="row row-toggle">\n          <label for="data" stacked>REVISÃO:</label> \n          <ion-item>\n            <ion-toggle [(ngModel)]="consulta.tem_revisao" name="tem_revisao" color="secondary"></ion-toggle>\n          </ion-item>\n        </div> \n        <div class="row " [hidden]="!consulta.tem_revisao">\n          <label for="data" stacked>DATA REVISÃO:</label> \n            <ion-item>\n              <ion-datetime cancelText="Fechar" [min]="minDate" max="2030-10-31" doneText="Confirmar" [(ngModel)]="consulta.data_revisao" name="data_revisao" class="input" displayFormat="DD/MM/YYYY" ></ion-datetime>\n            </ion-item>\n        </div>\n        <div class="row">\n          <button class="botao">SALVAR</button>\n        </div>\n        \n      </form>  \n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-cadastro/gestao-cadastro.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_area_area__["a" /* AreaProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_auth_auth__["a" /* AuthProvider */]])
    ], GestaoCadastroPage);
    return GestaoCadastroPage;
}());

//# sourceMappingURL=gestao-cadastro.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestaoListagemPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_api__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__gestao_cadastro_gestao_cadastro__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__categorias_conteudos_categorias_conteudos__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_aplicativos_aplicativos__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_area_area__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_consultas_consultas__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_helpers_helpers__ = __webpack_require__(9);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










/**
 * Generated class for the EasyfitSportsHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GestaoListagemPage = /** @class */ (function () {
    function GestaoListagemPage(navCtrl, navParams, _area, api, loading, aplicativos, _consultas, helpers) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._area = _area;
        this.api = api;
        this.loading = loading;
        this.aplicativos = aplicativos;
        this._consultas = _consultas;
        this.helpers = helpers;
        this.area_id = null;
        if (!this._area.list.length) {
            this.loading.present();
        }
        this.onRefresher();
    }
    GestaoListagemPage.prototype.onRefresher = function () {
        var _this = this;
        this._area.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
        this._consultas.getAll().then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
            console.log(_this._consultas.list);
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
            console.log(_this._consultas.list);
        });
    };
    GestaoListagemPage.prototype.doSearch = function (e) {
        var _this = this;
        this._consultas.getAll(this.area_id).then(function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function () {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoListagemPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this._area.getAll().then(function (r) {
            _this.loading.dismiss();
            _this.refresher.complete();
        }, function (e) {
            _this.loading.dismiss();
            _this.refresher.complete();
        });
    };
    GestaoListagemPage.prototype.categoriasConteudosPage = function (item) {
        this._area.current = item;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__categorias_conteudos_categorias_conteudos__["a" /* CategoriasConteudosPage */]);
    };
    GestaoListagemPage.prototype.cadastro = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__gestao_cadastro_gestao_cadastro__["a" /* GestaoCadastroPage */], { item: item, parentPage: this });
    };
    GestaoListagemPage.prototype.excluir = function (id) {
        var _this = this;
        this._consultas.delete(id).subscribe(function () {
            _this._consultas.getAll(_this.area_id).then(function () {
                _this.loading.dismiss();
                _this.refresher.complete();
            }, function () {
                _this.loading.dismiss();
                _this.refresher.complete();
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_9" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Refresher */])
    ], GestaoListagemPage.prototype, "refresher", void 0);
    GestaoListagemPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'page-gestao-listagem',template:/*ion-inline-start:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-listagem/gestao-listagem.html"*/'<!--\n  Generated template for the CategoriasPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <header title="Listagem" [hideSearch]="true"></header>\n</ion-header>\n\n\n<ion-content>\n  <ion-refresher (ionRefresh)="onRefresher()">\n    <ion-refresher-content>\n    </ion-refresher-content>\n  </ion-refresher>\n  <div class="listagem">\n      <div class="row">\n          <ion-select class="input" [(ngModel)]="area_id" cancelText="Cancelar" placeholder="Área" id="area" name="area" (ngModelChange)="doSearch($event)">\n              <ion-option [value]="0"> Todas </ion-option>\n              <ion-option *ngFor="let item of _area.list" [value]="item.id" >{{item.descricao}}</ion-option>\n          </ion-select>\n        </div> \n      <div class="item" *ngFor="let item of _consultas.list" [className]="item.novo ? \'item active\' : \'item inactive\'">\n          <div class="row">\n              <label>Data:</label> {{helpers.moment(item.data_consulta).format(\'DD/MM/YYYY\')}}\n            </div>  \n            <div class="row">\n                <label>Profissional:</label> {{item.nome}}\n              </div> \n              <button class="botao" (click)="cadastro(item)">ACESSAR</button> \n\n            <button class="icone" (click)="excluir(item.id)" ion-button icon-only><ion-icon name="trash" color="danger"></ion-icon></button>\n      </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/bitbucket/jones/easycorporativoapp/src/pages/gestao-listagem/gestao-listagem.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__providers_area_area__["a" /* AreaProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_api__["a" /* ApiProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_loading_loading__["a" /* LoadingProvider */],
            __WEBPACK_IMPORTED_MODULE_6__providers_aplicativos_aplicativos__["a" /* AplicativosProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_consultas_consultas__["a" /* ConsultasProvider */],
            __WEBPACK_IMPORTED_MODULE_9__providers_helpers_helpers__["a" /* HelpersProvider */]])
    ], GestaoListagemPage);
    return GestaoListagemPage;
}());

//# sourceMappingURL=gestao-listagem.js.map

/***/ }),

/***/ 9:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HelpersProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global__ = __webpack_require__(17);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/*
  Generated class for the HelpersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HelpersProvider = /** @class */ (function () {
    function HelpersProvider(app, sanitizer, toastCtrl, global) {
        this.app = app;
        this.sanitizer = sanitizer;
        this.toastCtrl = toastCtrl;
        this.global = global;
        this.moment = __WEBPACK_IMPORTED_MODULE_2_moment__;
        this.extToMime = {
            "aac": "audio/aac",
            "abw": "application/x-abiword",
            "arc": "application/octet-stream",
            "avi": "video/x-msvideo",
            "azw": "application/vnd.amazon.ebook",
            "bin": "application/octet-stream",
            "bz": "application/x-bzip",
            "bz2": "application/x-bzip2",
            "csh": "application/x-csh",
            "css": "text/css",
            "csv": "text/csv",
            "doc": "application/msword",
            "eot": "application/vnd.ms-fontobject",
            "epub": "application/epub+zip",
            "gif": "image/gif",
            "htm": "text/html",
            "html": "text/html",
            "ico": "image/x-icon",
            "ics": "text/calendar",
            "jar": "application/java-archive",
            "jpeg": "image/jpeg",
            "jpg": "image/jpeg",
            "js": "application/javascript",
            "json": "application/json",
            "mid": "audio/midi",
            "midi": "audio/midi",
            "mpeg": "video/mpeg",
            "mpkg": "application/vnd.apple.installer+xml",
            "odp": "application/vnd.oasis.opendocument.presentation",
            "ods": "application/vnd.oasis.opendocument.spreadsheet",
            "odt": "application/vnd.oasis.opendocument.text",
            "oga": "audio/ogg",
            "ogv": "video/ogg",
            "ogx": "application/ogg",
            "otf": "font/otf",
            "png": "image/png",
            "pdf": "application/pdf",
            "ppt": "application/vnd.ms-powerpoint",
            "rar": "application/x-rar-compressed",
            "rtf": "application/rtf",
            "sh": "application/x-sh",
            "svg": "image/svg+xml",
            "swf": "application/x-shockwave-flash",
            "tar": "application/x-tar",
            "tif": "image/tiff",
            "tiff": "image/tiff",
            "ts": "application/typescript",
            "ttf": "font/ttf",
            "vsd": "application/vnd.visio",
            "wav": "audio/x-wav",
            "weba": "audio/webm",
            "webm": "video/webm",
            "webp": "image/webp",
            "woff": "font/woff",
            "woff2": "font/woff2",
            "xhtml": "application/xhtml+xml",
            "xls": "application/vnd.ms-excel",
            "xlsx": "application/vnd.ms-excel",
            "xml": "application/xml",
            "xul": "application/vnd.mozilla.xul+xml",
            "zip": "application/zip",
            "3gp": "video/3gpp",
            "3g2": "video/3gpp2",
            "7z": "application/x-7z-compressed"
        };
        this.moment.locale('pt-BR');
        console.log('Hello HelpersProvider Provider');
    }
    HelpersProvider.prototype.formMarkAllTouched = function (form) {
        var _this = this;
        Object.values(form.controls).forEach(function (control) {
            control.markAsTouched();
            if (control.controls) {
                Object.values(control.controls).forEach(function (c) { return _this.formMarkAllTouched(c); });
            }
        });
    };
    HelpersProvider.prototype.sanitizeStyle = function (style) {
        return this.sanitizer.bypassSecurityTrustStyle(style);
    };
    HelpersProvider.prototype.isEmail = function (str) {
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return pattern.test(String(str).toLowerCase());
    };
    HelpersProvider.prototype.removeHttp = function (str) {
        if (!str) {
            return '';
        }
        return str.replace('http://', '').replace('https://', '');
    };
    HelpersProvider.prototype.getMimeType = function (str) {
        var extension = str.split('.');
        extension = extension[extension.length - 1];
        return this.extToMime[extension];
    };
    HelpersProvider.prototype.toast = function (message) {
        this.toastCtrl.create({ message: message, duration: 4000 }).present();
    };
    HelpersProvider.prototype.isAppleReview = function () {
        // ele é review se informacoes.c1 está false
        return !this.global.informacoes.c2;
    };
    HelpersProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__global_global__["a" /* GlobalProvider */]])
    ], HelpersProvider);
    return HelpersProvider;
}());

//# sourceMappingURL=helpers.js.map

/***/ })

},[401]);
//# sourceMappingURL=main.js.map