
$(document).ready(function(){

if(window.isGaleria){
  var myDropzone = new Dropzone("form#galeria-dropzone", {
    dictDefaultMessage: "»Drop files to upload (or click)«",
    uploadMultiple: false,
    addRemoveLinks: false,
  });
}
  /* DataTable PARA AS LISTAGENS */
  $('#list-data-table, .list-data-table').DataTable({
      "paging": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "order": [[ 0, "desc" ]],
       "oLanguage": {
            "sProcessing":   "Processando...",
            "sLengthMenu":   "Mostrar _MENU_ registros",
            "sZeroRecords":  "Não foram encontrados resultados",
            "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "",
            "sInfoPostFix":  "",
            "sSearch":       "Buscar:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "Primeiro",
                "sPrevious": "Anterior",
                "sNext":     "Seguinte",
                "sLast":     "Último"
            }
        }
   });

  var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
    $('.telefone').mask(SPMaskBehavior, spOptions);


	/*
	Script para submeter o formulário caso não possua campos required em branco.
	Faz a validação dos campos required do #mainForm
	*/
  $('.box-footer [type="submit"]').click(function(e){
       e.preventDefault();
       var wrongValidation = 0;
       var inputsNumber = 0;
       $('#mainForm [required]').each(function(){
//console.log(tinyMCE.editors[$(this).attr('id')].getContent());
           if(!$(this).val() || ($(this).is('textarea') && $(this).hasClass('tinymce') && !tinyMCE.editors[$(this).attr('id')].getContent())){
              inputsNumber++;
              $(this).css('border-color', '#dd4b39');
              if($(this).is('textarea') && $(this).hasClass('tinymce')){
                if(!tinyMCE.editors[$(this).attr('id')].getContent()){
                  $(this).prev().css('border-color', '#dd4b39');
                }else{
                  wrongValidation++;
                }
              }
           }
       });
       if(inputsNumber > wrongValidation){
$('[href="#info-tab"]').closest('ul').find('li.active').removeClass('active');
           $('[href="#info-tab"]').closest('li').attr('class', 'active');
           $('.tab-content .tab-pane').removeClass('in active');
           $('#info-tab').addClass('in active');
           alertUtil.alertError('Verifique os erros do formulário.');
       }else{
           $('#mainForm').submit();
       }
   });

  $('#mainForm input').focus(function(){
    $(this).css('border-color', '#3c8dbc');
  });

  $('#mainForm input').blur(function(){
    $(this).css('border-color', '#d2d6de');
  });


  /*
  $('.session-return-wrapper .fa-times').click(function(){
  	$('.session-return-wrapper').fadeOut();
  });*/

  $('[href="#"]').click(function(e){
    e.preventDefault();
  });

  $('.fecha-alerta').click(function(){
    $(this).parents('.alerta').removeClass('active');
    $('.alerta').hide();
    $('.alerta').remove();
  });


  /* BUSCA DE ENDERECO POR CEP */
  $('[name="cep"]').blur(function() {
      var cep = $(this).val().replace('-', '').replace('.', '');
      var verify = $.trim(cep);
      $.ajax({
          url: "/admin/getcep",
          dataType: 'json',
          type: 'POST',
          data: {
              'cep': verify,
              '_token': $('[name="_token"]').val()
          },
          success: function(resultadoCEP) {
              if (resultadoCEP["resultado"] == "1" || resultadoCEP["resultado"] == "2") {
                  //$(div_endereco).find('[name="endereco-fieldset[cidade]"]').val(unescape(resultadoCEP["cidade"]));
						$('#bairro').val(unescape(resultadoCEP["bairro"]));
						$('#endereco').val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                  $('#enderecoGmaps').val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"]));
                  $('#enderecoGmaps').focus();
              }
          },
          error: function(xhr, ajaxOptions, thrownError) {
              alertUtil.alertError(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
          }
      });

  });


  tinymce.init({
    selector:'.tinymce',
    force_br_newlines : true,
    force_p_newlines : false,
    forced_root_block : '', // Needed for 3.x
    plugins: [
      "advlist autolink lists link image charmap print preview anchor",
      "searchreplace visualblocks code fullscreen",
      "insertdatetime media table contextmenu paste jbimages"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image jbimages",
    relative_urls: false
  });

  $('.deletar').click(function(e){
    if(!confirm('Você tem certeza? Isso não pode ser desfeito.')){
      e.preventDefault();
    }
  });

  $('[name^="titulo"], [name^="nome"]').slugify({ slug: '[name="slug"]', type: '-' });

  $('.delete-image').click(function(){
	  var id = $(this).attr('data');
	  var modulo = $(this).attr('data-modulo');
	  $.ajax({
			 url: '/admin/'+modulo+'/delete_imagem/'+id,
			 dataType: 'JSON',
			 data:{
				 _token : $('[name="_token"]').val()
			 },
			 type: "POST",
			 success: function(data) {
				 $( '.imagem-galeria-' + id).remove();
				 if(data.status){
					 alertUtil.alertSuccess(data.message);
				 }else{
					 alertUtil.alertError(data.message);
				 }

			  }

		 });

  });

  $('.select2').select2();

  $('.select-icone').change(function(){
	  $('.icone-viewer i').attr('class','');
	  $('.icone-viewer i').attr('class', 'fa fa-3x '+$(this).val());
  });if($('.select-icone') !== undefined) $('.select-icone').trigger('change');

  $('.busca-cupom-box form').submit(function(e){
	  e.preventDefault();
	  window.location.href = "/admin/cupom/perfil/"+$(this).find('[name="codigo"]').val();
  });

});
