var informacoes_tecnicas = [];
var tipos = [];
var cores = [];


$('.add-modelo').click(function(){

	if($('select[name="id_categoria"]').val() == ''){
		alertUtil.alertError('Você precisa definir a categoria.');
		return true;
	}

	$("#loading").show();
	$.ajax({
		url: "admin/produtos/informacoes",
		dataType: 'json',
		type: 'POST',
		data: {
		  '_token': $('[name="_token"]').val(),
		  'categoria_id': $('select[name="id_categoria"]').val()
		},
		success: function(response) {
			informacoes_tecnicas = response.informacoes_tecnicas;
			tipos = response.tipos;
			cores = response.cores;
			$("#loading").hide();
			montaModelo();
		}
	});

});

function montaModelo(){

	modelo++;

var _html = '';

_html += '<div class="col-md-6" id="modelo-'+modelo+'">';
	_html += '<div class="box box-primary">';
        _html += '<div class="box-header">';
          	_html += '<div class="form-group">';
				_html += '<label for="referencia" class="col-md-3 control-label">Referência *</label>';
				_html += '<div class="col-md-6">';
					_html += '<input id="referencia" required type="text" class="form-control" value="" name="modelos['+modelo+'][referencia]" />';
				_html += '</div>';
				_html += '<div class="col-md-1 btn btn-danger deletar-modelo" data-modelo="'+modelo+'"><i class="fa fa-trash"></i></div>';
			_html += '</div>';
        _html += '</div>';
        _html += '<div class="box-body">';

        	_html += '<div class="form-group">';
				_html += '<label for="imagem" class="col-md-3 control-label">Imagem</label>';
				_html += '<div class="col-md-7">';
					_html += '<input id="imagem" type="file" value="" name="modelos['+modelo+'][file]" />';
				_html += '</div>';
			_html += '</div>';


			_html += '<div class="form-group">';
				_html += '<label for="cor" class="col-md-3 control-label">Cor *</label>';
				_html += '<div class="col-md-7">';
					_html += '<select class="form-control" name="modelos['+modelo+'][cor]" id="cor">';
					_html += '<option value="">Selecione a cor</option>';
						$(cores).each(function(key,val) {
							_html += '<option value="'+val.id+'">'+val.nome+'</option>';
						});
					_html += '</select>';
				_html += '</div>';
			_html += '</div>';



        	//ADD AS INFORMACOES
			_html += '<div class="form-group">';
				_html += '<h3>Informações Técnicas *</h3>';
			_html += '</div>';

			//FORM DAS INFORMACOES
			_html += '<div class=" caracteristicas" id="informacoes-'+modelo+'">';
				$(informacoes_tecnicas).each(function(key,val) {
					_html += '<div class="form-group item-informacoes">';
						_html += '<label class="col-md-5 control-label">';
							_html += '<input type="hidden" name="modelos['+modelo+'][informacoes][tecnica]['+key+']" value="'+val.id+'">';
							_html += val.nome;
						_html += '</label>';
						_html += '<div class="col-md-5">';
							_html += '<input type="text" class="form-control" name="modelos['+modelo+'][informacoes][tecnica_valor]['+key+']">';
						_html += '</div>';

					_html += '</div>';
				});

			_html += '</div>';

			//ADD OS TIPOS
			_html += '<div class="form-group">';
				_html += '<h3>Equivalências *</h3>';
			_html += '</div>';

			//FORM DOS TIPOS
			_html += '<div class="tipos" id="tipos-'+modelo+'">';

				$(tipos).each(function(key,val) {
					_html += '<div class="form-group item-tipos">';
						_html += '<label class="col-md-5 control-label">';
							_html += '<input type="hidden" class="form-control" name="modelos['+modelo+'][informacoes][equivalencias]['+key+']" value="'+val.id+'">';
							_html += val.nome;
						_html += '</label>';
						_html += '<div class="col-md-5">';
							_html += '<input type="text" class="form-control" name="modelos['+modelo+'][informacoes][equivalencias_valor]['+key+']">';
						_html += '</div>';
					_html += '</div>';
				});

			_html += '</div>';

		_html += '</div>';
	_html += '</div>';
_html += '</div>';

$("#modelo-content").append(_html);



}

$(document).on('click', '.add-tipo', function(){
	/*

	var modelo_atual = $(this).data('modelo');

	var qtd_tipos = $("#tipos-"+modelo_atual+"  > .item-tipos").length;

	var _tipo = '<div class="form-group item-tipos">';
			_tipo += '<div class="col-md-5">';
				_tipo += '<select class="form-control" name="modelos['+modelo_atual+'][informacoes][tipo]['+qtd_tipos+']" id="">';
					$(tipos).each(function(key,val) {
						_tipo += '<option value="'+val.id+'">'+val.nome+'</option>';
					});
				_tipo += '</select>';
			_tipo += '</div>';
			_tipo += '<div class="col-md-5">';
				_tipo += '<input type="text" class="form-control" name="modelos['+modelo_atual+'][informacoes][tipo_valor]['+qtd_tipos+']">';
			_tipo += '</div>';
		_tipo += '</div>';

	$("#tipos-"+modelo_atual).append(_tipo);
	*/
});

/*
$(document).on('click', '.add-informacoes', function(){

	var modelo_atual = $(this).data('modelo');

	var qtd_informacoes = $("#informacoes-"+modelo_atual+"  > .item-informacoes").length;

	var _informacoes = '<div class="form-group item-informacoes">';
		_informacoes += '<div class="col-md-5">';
			_informacoes += '<select class="form-control" name="modelos['+modelo_atual+'][informacoes][tecnica]['+qtd_informacoes+']" id="">';
				//_informacoes += '<option value="1">Referência</option>';
				$(informacoes_tecnicas).each(function(key,val) {
					_informacoes += '<option value="'+val.id+'">'+val.nome+'</option>';
				});
			_informacoes += '</select>';
		_informacoes += '</div>';
		_informacoes += '<div class="col-md-5">';
			_informacoes += '<input type="text" class="form-control" name="modelos['+modelo_atual+'][informacoes][tecnica_valor]['+qtd_informacoes+']">';
		_informacoes += '</div>';
		_informacoes += '<div class="col-md-1">';
			_informacoes += '<div class="btn btn-danger deletar-informacoes"><i class="fa fa-trash"></i></div>';
		_informacoes += '</div>';
	_informacoes += '</div>';

	$("#informacoes-"+modelo_atual).append(_informacoes);
});


$(document).on('click', '.deletar-informacoes', function(){
	var r = confirm("A informação será deletada!");
	if (r == true) {
		$(this).parents('.item-informacoes').remove();
	}
});

$(document).on('click', '.deletar-tipos', function(){
	var r = confirm("O tipo será deletado!");
	if (r == true) {
		$(this).parents('.item-tipos').remove();
	}
});
*/

$(document).on('click', '.deletar-modelo', function(){
	var r = confirm("O modelo será deletado!");
	if (r == true) {
		var id = $(this).data('modelo');
		$('#modelo-'+id).remove();
	}
});

