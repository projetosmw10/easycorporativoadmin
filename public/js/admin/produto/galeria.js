$(document).ready(function(){

jQuery(function($) {
  $(function() {
    // retrieve the ids of root pages so we can POST them along
    function data_callback(even, ui) {
      var item_array = $(".ui-sortable-images").sortable("toArray");
      $.post("admin/institucional/galeria/order", {
        items: item_array,
        csrf_hash_name: $.cookie(pyro.csrf_cookie_name)
      });
    }
    $(".ui-sortable-images").sortable({
      opacity: 0.7,
      // placeholder: 'ui-state-highlight',
      forcePlaceholderSize: true,
      items: 'li',
      cursor: "move",
      scroll: false,
      update: function(event, ui) {
        data_callback();
      }
    }).disableSelection();
  });
});


jQuery(function($) {
	  $(function() {
	    // retrieve the ids of root pages so we can POST them along
	    function data_callback(even, ui) {
	      var item_array = $(".ui-sortable-images-obra").sortable("toArray");
	      $.post("admin/institucional/galeria/order", {
	        items: item_array,
	        csrf_hash_name: $.cookie(pyro.csrf_cookie_name)
	      });
	    }
	    $(".ui-sortable-images-obra").sortable({
	      opacity: 0.7,
	      // placeholder: 'ui-state-highlight',
	      forcePlaceholderSize: true,
	      items: 'li',
	      cursor: "move",
	      scroll: false,
	      update: function(event, ui) {
	        data_callback();
	      }
	    }).disableSelection();
	  });
	});



var settings = {
    url: 'admin/institucional/galeria/upload_galeria/',
    dragDrop:true,
    autoSubmit:true,
    fileName: "myfile",
    formData: {"csrf_hash_name":$('input[name="csrf_hash_name"]').val(),'pasta':$('input[name="pasta_imagens"]').val()},
    allowedTypes:"jpg,png,gif,doc,pdf,zip",	
    returnType:"json",
    showStatusAfterSuccess:false,
    afterUploadAll:function(files,data,xhr)
    {
    	var dados = { 'pasta' : $('input[name="pasta_imagens"]').val()};
    	$.ajax({  
			   url: 'admin/institucional/galeria/get_imagens', 
			   dataType: 'json',
			   data: dados,
			   type: "POST", 
			   success: function(obj) {
				   $("#galeria ul").empty();
				   jQuery.each(obj, function(i, item) {
						var img = '<img src="'+item.cache+'" alt="'+item.filename+'"/><br />';
						$("#galeria ul").append(
							'<li id="item_'+item.id+'" class="'+item.id+'" data-name="'+item.filename+'"><div class="excluir-imagem" id="'+item.id+'"></div>'
							+img+
							'</li>'
						).fadeIn(3000);
				    });
				   excluir_imagem();
		   		}
    	
		});
    },
    
}
$("#mulitplefileuploader").uploadFile(settings);


var settingsObra = {
	    url: 'admin/institucional/galeria/upload_galeria/',
	    dragDrop:true,
	    autoSubmit:true,
	    fileName: "myfile",
	    formData: {"csrf_hash_name":$('input[name="csrf_hash_name"]').val(),'pasta':$('input[name="pasta_imagens_obra"]').val()},
	    allowedTypes:"jpg,png,gif,doc,pdf,zip",	
	    returnType:"json",
	    showStatusAfterSuccess:false,
	    afterUploadAll:function(files,data,xhr)
	    {
	    	var dados = { 'pasta' : $('input[name="pasta_imagens_obra"]').val()};
	    	$.ajax({  
				   url: 'admin/institucional/galeria/get_imagens', 
				   dataType: 'json',
				   data: dados,
				   type: "POST", 
				   success: function(obj) {
					   $("#galeria-obra-imagens ul").empty();
					   jQuery.each(obj, function(i, item) {
							var img = '<img src="'+item.cache+'" alt="'+item.filename+'"/><br />';
							$("#galeria-obra-imagens ul").append(
								'<li id="item_'+item.id+'" class="'+item.id+'" data-name="'+item.filename+'"><div class="excluir-imagem" id="'+item.id+'"></div><div class="remove_image"></div>'
								
								+img+
								'</li>'
							).fadeIn(3000);
					    });
					   excluir_imagem();
			   		}
			});
	    },
	    
	}
	$("#galeria-obra").uploadFile(settingsObra);
		
	function excluir_imagem(){
		
		$('.excluir-imagem').click(function(){
			var id = $(this).attr('id');
			 var r = confirm("Você tem certeza que deseja remover a imagem?");
			    if (r == true) {
			    	$.ajax({  
						   url: 'admin/institucional/galeria/deletar', 
						   data: { 'id' : id},
						   type: "POST", 
						   success: function() {
							   $("#item_"+id).remove();
						   }
			    	});
			    }
					
		
		});
		
	}
	excluir_imagem();
});