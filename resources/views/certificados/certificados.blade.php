<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
@php

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

function properCase( $string ) {    
   $ignorar = array( 'do', 'dos', 'da', 'das', 'de' );
   $array = explode(' ', strtolower( $string ) );
   $out = '';
   foreach ($array as $ar) {
      $out .= ( in_array ( $ar, $ignorar ) ? $ar : ucfirst( $ar ) ).' ';
   }
   return trim( $out );
}

@endphp
<style>
    * {
        margin: 0;
        padding: 0;
    }
    html {
        width: 100%;
        height: 100%;
        position: relative;
        font-family: Arial, Helvetica, sans-serif;
    }
    div {
        position: absolute;
        z-index: 20;
        width: 100%;
        text-align: center;
        padding-left: 0;
    }
    #nome {
        margin-top: 680px;
        font-size: 56pt;
        font-family: 'Great Vibes', cursive;
        opacity: .65;

    }
    #curso {
        margin-top: 1165px;
        font-weight: 400;
        font-size: 22pt;
        opacity: .65;
    }
    #data {
        margin-top: 1900px;
        font-weight: 400;
        font-size: 20pt;
        opacity: .65;
        text-transform: lowercase
    }
</style>

<html>
    <head>
        <title>Certificado: {{ mb_strtoupper($nome) }} - {{ mb_strtoupper($categoria) }}</title>
    </head>
    <img style="position         : absolute;

    top              : 0px;
    left             : 0px;
    right            : 0px;
    bottom           : 0px;

    overflow         : hidden;
    margin           : 0;
    padding          : 0;object-fit:cover" src="{{ public_path($certificado) }}" alt="">
    <div id="nome">{{ properCase($nome) }}</div>
    <div id="curso">{{ mb_strtoupper($categoria) }}</div>
    <div id="data">{{ strftime('%d de %B de %Y', strtotime($data)) }}</div>
</html>