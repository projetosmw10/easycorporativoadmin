<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: sans-serif;font-size:18px;">
    Atenção o usuário <strong>{{ $user->first_name }}</strong><br>
    Já gerou {{ $countCertificados }} certificados para o curso: <strong>{{ $categoria->titulo }}</strong>!
    <br><br>
    
    Dados do usuário:<br>
    <strong>E-mail:</strong> {{ $user->email }}<br>
    <strong>Telefone:</strong> {{ $user->telefone }}<br>

</body>
</html>