<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: sans-serif;font-size:18px;">
    Olá, {{ $nome }}<br>
    Estamos felizes em saber que você concluio o curso <strong>{{ $categoria->titulo }}</strong>!
    <br><br>
    Você pode imprimir seu <strong><a href="{{ url('api/certificados/gerar/'.encrypt($categoria_certificado)) }}">certificado agora!</a></strong><br><br><br>
    Caso não consiga acessar o certificado copie e cole o link em seu navegador<br>
    <small><a href="{{ url('api/certificados/gerar/'.encrypt($categoria_certificado)) }}">{{ url('api/certificados/gerar/'.encrypt($categoria_certificado)) }}</a></small><br><br>
    Ficamos feliz em ter você conosco!<br><br>

    <strong>EasyLife</strong>
</body>
</html>