<?php

namespace App\Modules\<NOME_MODULO>\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class <NOME_MODULO> extends Model
{
    protected $table = '<NOME_TABELA>';
    protected $fillable = <COLUNAS_FILLABLE>;

	public static function getImagens($id){
		return DB::table('<NOME_TABELA>_imagens')->where('id_<ITEM_MODULO>', $id)->get();
    }
    public static function getImagem($id){
		return DB::table('<NOME_TABELA>_imagens')->find($id);
    }
    
	public static function criar_imagem($input){
		return DB::table('<NOME_TABELA>_imagens')->insert([
			[
				'id_<ITEM_MODULO>' => $input['id_<ITEM_MODULO>'],
				'thumbnail_principal' => $input['thumbnail_principal'],
			]
		]);
    }
    public static function deletar_imagem($id){
		return DB::table('<NOME_TABELA>_imagens')
				->where('id', $id)
				->delete();
	}

	public static function getNextAutoIncrement(){
		$lastId = DB::select("SELECT AUTO_INCREMENT FROM information_schema.tables WHERE TABLE_NAME = '<NOME_TABELA>' ORDER BY table_name;")[0]->AUTO_INCREMENT;
		return $lastId;
	}
}
