<?php

namespace App\Modules\<NOME_MODULO>\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class <NOME_MODULO> extends Model
{
    protected $table = '<NOME_TABELA>';
    protected $fillable = <COLUNAS_FILLABLE>;

	private $rules = array();

	private $messages = array();

	public function __construct(){
		$this->setMessages($this->messages);
		$this->setRules($this->rules);
	}

}
