<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Duo Studio</title>
    <style>
    * {
        margin: 0;
        padding: 0;
    }
    html {
        overflow: hidden;
    }
    </style>
</head>
<body>
    <a href="http://duostudio.com.br/">
        <img src="<?=url('/img/background-app.png')?>" style="width:100vw;height:100vh;object-fit:cover;cursor:pointer;" alt="">
    </a>
</body>
</html>