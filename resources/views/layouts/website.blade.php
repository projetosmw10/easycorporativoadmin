<!DOCTYPE html>

<html lang="en-US" prefix="og: http://ogp.me/ns#">

<head>

	<title>Easylife APPS | As melhores soluções em abraçadeiras para todo o Brasil</title>

	<meta http-equiv="content-type" content="text/html; charset=UTF-8">

	<meta charset="UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta name="HandheldFriendly" content="True">

	<meta name="apple-touch-fullscreen" content="yes" />

	<base href="{{ url('') }}"/>

	<meta name="MobileOptimized" content="320">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="keywords" content="Easylife APPS, metal, matrix, Abraçadeiras" />

	<meta name="description" content="Easylife APPS | As melhores soluções em abraçadeiras para todo o Brasil">

	<meta property="og:locale" content="pt_BR" />

	<meta property="og:type" content="website" />

	<meta property="og:title" content="Easylife APPS | As melhores soluções em abraçadeiras para todo o Brasil" />

	<meta property="og:url" content="http://www.Easylife APPS.com.br" />

	<meta property="og:site_name" content="Easylife APPS | As melhores soluções em abraçadeiras para todo o Brasil" />

	<meta property="og:image" content="http://www.Easylife APPS.com.br/img/imagem.jpg" />

	<meta property="og:description" content="As melhores soluções em abraçadeiras para todo o Brasil">

	<link href="https://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,500,700,900" rel="stylesheet">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">

	<link rel="stylesheet" href="{{ url('css/style.css') }}" media="screen and (min-width: 701px)">

	<link rel="stylesheet" href="{{ url('css/stylemob.css') }}" media="screen and (max-width: 700px)">

	<link rel="stylesheet" type="text/css" href="{{ url('css/carousel.css') }}">

	<link rel="stylesheet" type="text/css" href="{{ url('css/modal.css') }}">

	<link href="{{ url('img/favicon.png') }}" rel="shortcut icon" type="image/x-icon" />

	<script type="text/javascript" src="{{ url('js/jquery.min.js') }}"></script>

	<script type="text/javascript" src="{{ url('js/carousel.js') }}"></script>

	<script type="text/javascript" src="{{ url('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('plugins/jquery-validation/additional-methods.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('plugins/jquery-validation/jquery.validate.cnpj.js') }}"></script>

	<script src="{{ url('plugins/input-mask/jquery.inputmask.js') }}"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>

	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

	<script type="text/javascript" src="https://static.sketchfab.com/api/sketchfab-viewer-1.0.1.js"></script>




	<script type="text/javascript">

		function scrollMenu(menu) {
			$('body, html').animate({ scrollTop: $('#'+menu).offset().top }, 1500);
		}

		function fecharModal() {
            $('.modalEstado').fadeOut();
        }

	</script>


</head>

<body>
		@include('layouts.header')

		@yield('content')

		@include('layouts.footer')
</body>
</html>
