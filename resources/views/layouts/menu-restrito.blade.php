<aside>
	<ul class="treeview-menu">
		<?php if($current_role->hasAccess('consultorio.view')){ ?>
			<li ><a href="{{ url('admin/consultorios') }}">Consultórios</a></li>
		<?php } ?>
		<?php if($current_role->hasAccess('meus_alunos.view')){ ?>
			<li ><a href="{{ url('admin/meus-alunos') }}">Alunos</a></li>
		<?php } ?>
		<?php if($current_role->hasAccess('alunos.view')){ ?>
			<li ><a href="{{ url('admin/alunos') }}">Meus alunos</a></li>
		<?php } ?>
		<?php if($current_role->hasAccess('cliente.minha_assinatura')){ ?>
			<li ><a href="{{ url('admin/clientes/minha-assinatura') }}">Minha assinatura</a></li>
		<?php } ?>

		<?php if($current_role->hasAccess('cliente_dados_bancarios.view')){ ?>
			<li ><a href="{{ url('admin/minha-conta-bancaria') }}">Dados Bancários</a></li>
		<?php } ?>
		<?php if($current_role->hasAccess('video.view')){ ?>
			<li ><a href="{{ url('admin/videos') }}">Videos</a></li>
		<?php } ?>
		<li ><a href="{{ url('admin/logout') }}">Sair</a></li>
	</ul>
</aside>
