<div class="catalogos">

  <div class="container">

    <a class="item modal-trigger" data-modal="modal-name">

      <div class="imagem">

        <img src="{{ url('img/produto3.png') }}">

      </div>

      <div class="info">

        <h2><?php echo trans('default.baixe_nosso'); ?></h2>

        <h2 class="text-red"><?php echo trans('default.catal_prod'); ?></h2>

        <span><?php echo trans('default.baixe_agr'); ?></span>

      </div>

    </a>

    <a class="item" href="https://www.youtube.com/channel/UCzDjLpzTvz60bPK8etAcP8w" target="_blank">

      <div class="imagem">

        <img src="{{ url('img/produto4.png') }}">

      </div>

      <div class="info">

        <?php echo trans('default.youtube'); ?>

        <span><?php echo trans('default.acesse_agr'); ?></span>

      </div>

    </a>

  </div>

</div>

<div class="clear"></div>

<?php if($MapaFooter){?>
  <div class="mapaHome">

    <div class="info">

      <?php echo trans('default.represen_brasil'); ?>

    </div>

    <div id="map"></div>


  </div>


<script type="text/javascript">

  var map;

  var url = '<?php echo url('/'); ?>';
  var locations = [];

  $.getJSON(url+'/representantes/mapa/', function(j){
    initMap(j);
  });

  function initMap(locations) {

      var styles = {

          default: null,

          silver: [

            {

              elementType: 'geometry',

              stylers: [{color: '#f7f7f7'}]

            },

            {

              elementType: 'labels.icon',

              stylers: [{visibility: 'off'}]

            },

            {

              elementType: 'labels.text.fill',

              stylers: [{color: '#616161'}]

            },

            {

              elementType: 'labels.text.stroke',

              stylers: [{color: '#f5f5f5'}]

            },

            {

              featureType: 'administrative.land_parcel',

              elementType: 'labels.text.fill',

              stylers: [{color: '#bdbdbd'}]

            },

            {

              featureType: 'poi',

              elementType: 'geometry',

              stylers: [{color: '#eeeeee'}]

            },

            {

              featureType: 'poi',

              elementType: 'labels.text.fill',

              stylers: [{color: '#757575'}]

            },

            {

              featureType: 'poi.park',

              elementType: 'geometry',

              stylers: [{color: '#e5e5e5'}]

            },

            {

              featureType: 'poi.park',

              elementType: 'labels.text.fill',

              stylers: [{color: '#9e9e9e'}]

            },

            {

              featureType: 'road',

              elementType: 'geometry',

              stylers: [{color: '#ffffff'}]

            },

            {

              featureType: 'road.arterial',

              elementType: 'labels.text.fill',

              stylers: [{color: '#757575'}]

            },

            {

              featureType: 'road.highway',

              elementType: 'geometry',

              stylers: [{color: '#dadada'}]

            },

            {

              featureType: 'road.highway',

              elementType: 'labels.text.fill',

              stylers: [{color: '#616161'}]

            },

            {

              featureType: 'road.local',

              elementType: 'labels.text.fill',

              stylers: [{color: '#9e9e9e'}]

            },

            {

              featureType: 'transit.line',

              elementType: 'geometry',

              stylers: [{color: '#e5e5e5'}]

            },

            {

              featureType: 'transit.station',

              elementType: 'geometry',

              stylers: [{color: '#eeeeee'}]

            },

            {

              featureType: 'water',

              elementType: 'geometry',

              stylers: [{color: '#ededed'}]

            },

            {

              featureType: 'water',

              elementType: 'labels.text.fill',

              stylers: [{color: '#9f9f9f'}]

            }

          ]

      }



    map = new google.maps.Map(document.getElementById('map'), {

        zoom: 4,

        center: new google.maps.LatLng(-18.51, -44.55),

        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false

      });





        map.setOptions({styles: styles.silver});

      var infowindow = new google.maps.InfoWindow();



      var marker, i;



      for (i = 0; i < locations.length; i++) {

        marker = new google.maps.Marker({

          position: new google.maps.LatLng(locations[i][1], locations[i][2]),

          map: map,

          icon: 'img/marker.png'

        });



        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            
          return function() {
            
            // representante,estado
            if(locations[i][5] == 1){
                window.localStorage.setItem('representante', i+','+999);
                window.localStorage.setItem('representantes', i+','+999);
            }else{
                window.localStorage.setItem('representante', i+','+locations[i][4]);

            }
            window.location.href = '<?php echo url($local.'/representantes#estados') ?>';
            

            infowindow.setContent(locations[i][0]);

            infowindow.open(map, marker);

            }

        })(marker, i));

      }

  }



</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClSUUv-TwBNBWsf2ckjLPksBilKMAFlNs" async defer></script>
<?php } ?>

<div class="clear"></div>


<footer>

  <div class="container">

    <div class="linha1">

      <div class="menu">

        <ul>

          <li class=""><a href="{{ url($local.'/Easylife APPS') }}">a Easylife APPS</a></li>

          <li><a href="{{ url($local.'/produtos') }}"><?php echo trans('default.produtos'); ?></a></li>

          <li><a href="{{ url($local.'/representantes') }}"><?php echo trans('default.representantes'); ?></a></li>

          <li><a href="{{ url($local.'/noticias') }}"><?php echo trans('default.noticias'); ?></a></li>

          <li><a  href="{{ url($local.'/contato') }}"><?php echo trans('default.fale_conosco'); ?></a></li>

        </ul>

      </div>

      <div class="menu">

        <ul>

          <li class="paypal">paypal</li>

          <li class="cartoes">
            <span><?php echo trans('default.cart_cred'); ?></span>
          </li>

          <!--li class="boleto"></li-->

        </ul>

      </div>

    </div>

    <div class="clear"></div>

    <div class="linha2">

      <div class="logo">logo</div>



      <div class="redesSociais">

        <ul>

          <li><a href="https://www.facebook.com/Easylife APPS.abracadeiras/" target="_blank"><i class="fab fa-facebook-square fa-lg"></i></a></li>

          <li><a href="https://www.linkedin.com/company/Easylife APPS-industria-metal%C3%BArgica-ltda/" target="_blank"><i class="fab fa-linkedin fa-lg"></i></a></li>

          <li><a href="https://www.youtube.com/channel/UCzDjLpzTvz60bPK8etAcP8w" target="_blank"><i class="fab fa-youtube fa-lg"></i></a></li>

        </ul>

      </div>



      <div class="menu2">

        <ul>

          <?php foreach ($politicas as $key => $politica): ?>
            <li><a href="{{ url($local.'/politicas/'.$politica->translate($local)->slug) }}"><?php echo $politica->translate($local)->nome; ?></a></li>
          <?php endforeach ?>

          <li>

            <small>Copyright &copy; 2018 <?php echo trans('default.todos_direitos')?>. </small>

            <br>

            <small>Easylife APPS - CNPJ: 21.601.312/0002-01</small>

          </li>

          <li class="secure">secure</li>

          <li class="duo"><a href="https://www.duostudio.com.br/" target="_blank">duo</a></li>



        </ul>

      </div>

    </div>

    <div class="clear"></div>

  </div>

  <div class="clear"></div>

</footer>


<a class="lz_text_link" href="javascript:void(window.open('https://novo.Easylife APPS.com.br/chat/chat.php?ptl=pt-br&epc=I2VkMWMyNA__&esc=IzAwMDAwMA__','','width=400,height=600,left=0,top=0,resizable=yes,menubar=no,location=no,status=yes,scrollbars=yes'))" alt="LiveZilla Live Chat Software" data-text-online="Iniciar o chat" data-text-offline="Deixar mensagem" data-css-online="" data-css-offline="" data-online-only="0">
  <div class="backToTop">

    <i class="fas fa-comments"></i>

    <?php echo trans('default.fale_conosco_online'); ?>

  </div>
</a>



<script>

  $(document).ready(function() {

    $("#owl-demo").owlCarousel({

      navigation : false,

      slideSpeed : 300,

      paginationSpeed : 400,

      singleItem : true,

      lazyLoad: true,

      margin: 10

    });

  });

  </script>

  <!-- Modal -->

<div class="modal" id="modal-name">

  <div class="modal-sandbox"></div>

  <div class="modal-box">

    <div class="modal-header">

      <div class="close-modal">&#10006;</div>

      <h1>

        <?php echo trans('default.para_baixar'); ?>

      </h1>



      <div class="clear"></div>

    </div>

    <div class="modal-body">

      <form id="download-catalogo" method="POST">

        {{ csrf_field() }}
        <div class="campo">
          <input type="input" id="cpfcnpj" name="cpf_cnpj" placeholder="CPF ou CNPJ" />
        </div>
        <div class="campo">
          <input type="input" name="nome" placeholder="<?php echo trans('default.nome'); ?>" />
        </div>

        <div class="rest">
          <div class="campo">
            <input type="text" id="telefone-catalogo" name="telefone" placeholder="<?php echo trans('default.telefone'); ?>" />
          </div>

          <div class="campo">
            <input type="email" name="email" placeholder="<?php echo trans('default.email'); ?>" />
          </div>
        </div>

        <button type="submit"><?php echo trans('default.enviar_baixar'); ?></button>

      </form>

      <div class="clear"></div>

    </div>

  </div>

  <div class="clear"></div>

</div>

  <script type="text/javascript">



  $(".modal-trigger").click(function(e){

    e.preventDefault();

    dataModal = $(this).attr("data-modal");

    $("#" + dataModal).css({"display":"block"});



  });

  $(".close-modal, .modal-sandbox").click(function(){
    $(".modal").css({"display":"none"});
  });


  $('#download-catalogo').validate({
      rules: {
          cpf_cnpj: {required: true},
          email: {required: true, email: true},
          telefone: {required: true},
          nome: {required: true},
      },
      messages: {
        cpf_cnpj: {required: 'O CPF ou CNPJ é obrigatório'},
        nome: {required: 'Por favor informe o nome!'},
        email: {required: 'Por favor informe o E-mail!', email: 'E-mail inválido!'},
        telefone: {required: 'Por favor informe o telefone!'},
      },
      submitHandler: function( form ){
        var dados = $( form ).serialize();

          $.ajax({
            type: "POST",
            url: "<?php echo url($local.'/download-catalogo/save'); ?>",
            data: dados,
            success: function( data ){
              window.open(data.arquivo,'_blank');
              $(".modal").css({"display":"none"});
            }
          });

        return false;
      }
  })

  $('#telefone-catalogo').mask('(00) 0000-00000', {reverse: false});

  $('#telefone-catalogo').blur(function(event) {
     if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
        $('#telefone-catalogo').mask('(00) 00000-0009');
     } else {
        $('#telefone-catalogo').mask('(00) 0000-00009');
     }
  });

   $("#cpfcnpj").unmask();
    $("#cpfcnpj").focusout(function() {
      $("#cpfcnpj").unmask();
      var tamanho = $("#cpfcnpj").val().replace(/\D/g, '').length;
      if (tamanho == 11) {
        $("#cpfcnpj").mask("999.999.999-99");
      } else if (tamanho == 14) {
        $("#cpfcnpj").mask("99.999.999/9999-99");
      }
    });


</script>


