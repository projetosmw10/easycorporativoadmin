<label class="close-carrinho"></label>
<label class="modal-anuncio-close"></label>
<header>

  <div class="container">

    <a href="{{ url($local) }}"><h1 class="logo">Easylife APPS</h1></a>

    <input type="checkbox" id="control-nav" />

      <label for="control-nav" class="control-nav"></label>

      <label for="control-nav" class="control-nav-close"></label>

      <div class="contaDiv" align="right">
        <div class="conta">
          <?php if($isLogged){?>
              <a href="{{ url($local.'/minha-conta') }}"><i class="fa fa-user"></i><?php echo trans('default.minha_conta_ola'); ?>, <?=$userFirstName?></a>
          <?php } else { ?>
              <a href="{{ url($local.'/'.trans('url.conta')) }}"><i class="fa fa-user"></i><?php echo trans('default.sou_cliente'); ?></a>
           <?php } ?>

          <?php
          if(!$isLogged){
            ?> |
            <a href="{{ url($local.'/'.trans('url.nova_conta')) }}"><?php echo trans('default.nova_conta'); ?><i class="fa fa-plus-square"></i></a>
            <?php
          }
          ?>
        </div>
      </div>

      <div class="menu">

      <ul>

        <li><a href="{{ url($local.'/Easylife APPS') }}">Easylife APPS</a></li>

        <li class="produtos">

          <a style="cursor:auto;"><?php echo trans('default.produtos'); ?></a>

          <ul class="subMenu">

              <?php foreach ($categorias as $key => $categoria) { ?>
                  <li>
                    <a href="{{ url($local.'/produtos/categoria/'.$categoria->id.'/'.$categoria->translate($local)->slug) }}"><?php echo $categoria->translate($local)->nome; ?> <!--i class="fas fa-chevron-right"--></i></a>
                  </li>
              <?php } ?>

                <li>

                  <a><?php echo trans('default.pecas_repos'); ?> <i class="fas fa-chevron-right"></i></a>

                  <ul class="subMenu2">

                    <?php foreach ($marcas as $key => $marca): ?>
                      <li><a href="{{ url($local.'/produtos/marca/'.$marca->id.'/'.$marca->translate($local)->slug) }}"><?php echo $marca->translate($local)->nome; ?></a></li>
                    <?php endforeach ?>

                  </ul>

                </li>
            </ul>

        </li>


        <li><a href="{{ url($local.'/representantes') }}"><?php echo trans('default.representantes'); ?></a></li>

        <li><a href="{{ url($local.'/noticias') }}"><?php echo trans('default.noticias'); ?></a></li>

        <li><a href="{{ url($local.'/contato') }}"><?php echo trans('default.fale_conosco'); ?></a></li>

      </ul>



      <ul class="idiomas">

        <li class="<?php echo ($local == 'br' ? 'active' : ''); ?>"><a href="{{ url('br/'.Request::segment(2).'/'.Request::segment(3).'/'.Request::segment(4).'/'.Request::segment(5)) }}">BR</a></li>

        <li class="<?php echo ($local == 'en' ? 'active' : ''); ?>"><a href="{{ url('en/'.Request::segment(2).'/'.Request::segment(3).'/'.Request::segment(4).'/'.Request::segment(5)) }}">EN</a></li>

        <li class="<?php echo ($local == 'es' ? 'active' : ''); ?>"><a href="{{ url('es/'.Request::segment(2).'/'.Request::segment(3).'/'.Request::segment(4).'/'.Request::segment(5)) }}">ES</a></li>

      </ul>



      <div class="search">

        <a id="searchButton"><i class="fa fa-search"></i></a>

        <div class="search-box close">

          <form action="{{ url($local.'/produtos/buscar') }}" method="GET">

            <input type="search" name="termo" placeholder="<?php echo trans('default.pesqui_algo'); ?>">

          </form>



          <script type="text/javascript">

            $('#searchButton').click( function (event) {

              let searchBox = $('.search-box');

              event.preventDefault();

              if (searchBox.hasClass('close')) {

                searchBox.addClass('open').removeClass('close');

              } else if (searchBox.hasClass('open')) {

                searchBox.addClass('close').removeClass('open');

              }

            });

          </script>

        </div>

      </div>



      <div class="carrinho">

        <span id="qtd_cart" class="brand"><?php echo $qtd_carrinho; ?></span>

        <a href="{{ url($local.'/carrinho') }}" id="headerCarrinho"><i class="fas fa-shopping-cart"></i></a>

      </div>

    </div>

    <div class="clear"></div>

  </div>

</header>

<div class="modalCarrinho">

</div>




<script type="text/javascript">
  $(document).ready( function(){
    $('#headerCarrinho').click(function(ev){
      ev.preventDefault();

      $.ajax({
        type: "GET",
        url: "<?php echo url($local.'/carrinho/modal'); ?>",
        success: function( data ){
          $('.modalCarrinho').empty().append(data);
        }
      });

      $('.modalCarrinho').toggleClass('open');
      $('.close-carrinho').toggleClass('active');
    })
    $('.close-carrinho').click(function(ev){
      ev.preventDefault();
      $('.modalCarrinho').removeClass('open');
      $(this).removeClass('active');
    })
  })
</script>
