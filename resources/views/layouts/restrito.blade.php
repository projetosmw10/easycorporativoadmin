<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Painel Desafio Tudo ou Nada</title>

    <base href="{{ url('') }}/"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ url('fonts/admin/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/admin/bootstrap.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('fonts/admin/ionicons.css') }}">

    <!-- Styles -->


    <link href="{{ elixir('restrito/restrito.css') }}" rel="stylesheet">


    <!-- Taggles JS -->
    <script type="text/javascript" src="{{ url('js/admin/jquery-3.1.0.min.js') }}"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>

</head>

<body>

  <div class="conteudo">

    <header class="main-header">
        Desafio Tudo ou nada Bem vindo!
    </header>

    @include('layouts.menu-restrito')


    @yield('content')

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Version</b> 2.3.6
      </div>
      <strong>Copyright &copy; 2014-2016.</strong> All rights
      reserved.
    </footer>

  </div>

<!-- jQuery 2.2.3 -->
<script src="{{ url('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

<script src="{{ url('js/admin/jquery.maskedinput.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ url('plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script src="{{ url('js/admin/bootstrap.min.js') }}"></script>
<!-- alertUtil.js -->
<script src="{{ url('js/admin/alertUtil.js') }}"></script>
<!-- Slugify -->
<script src="{{ url('js/admin/jquery.slugify.js') }}"></script>

</body>
</html>
