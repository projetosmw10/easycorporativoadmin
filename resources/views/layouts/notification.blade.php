<!-- Logo -->
<a href="{{ url('admin') }}" class="logo">
  <!-- mini logo for sidebar mini 50x50 pixels -->
  <span class="logo-mini"><b>M</b>M</span>
  <!-- logo for regular state and mobile devices -->
  <span class="logo-lg" style="background-image:url(<?=asset($current_app->thumbnail_principal)?>)"><b>Easylife APPS</span>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
  <!-- Sidebar toggle button-->

  <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>

  </a>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">

        <!-- User Account: style can be found in dropdown.less -->
      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ 'img/user2-160x160.jpg' }}" class="user-image" alt="User Image">
          <span class="hidden-xs">{{ Sentinel::getUser()->first_name }}</span>
        </a>
        <ul class="dropdown-menu">
          <!-- User image -->
          <li class="user-header">
                <img src="{{ 'img/user2-160x160.jpg' }}" class="img-circle" alt="User Image">
            <p>
              {{ Sentinel::getUser()->first_name.' '.Sentinel::getUser()->last_name }}
              <small>Membro desde <?php echo date('M',strtotime(Sentinel::getUser()->created_at)); ?>. <?php echo date('Y',strtotime(Sentinel::getUser()->created_at)); ?></small>
            </p>
          </li>

          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-right">
              <a href="{{ url('admin/logout') }}" class="btn btn-default btn-flat">Sair</a>
            </div>
          </li>
        </ul>
      </li>


    </ul>
  </div>
</nav>
