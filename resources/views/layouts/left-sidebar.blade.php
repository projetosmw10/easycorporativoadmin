
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">

				<img src="{{ 'img/user2-160x160.jpg' }}" class="img-circle" alt="User Image">

			</div>
			<div class="pull-left info">
				<p>{{ Sentinel::getUser()->first_name }}</p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu">
					<?php 

					// role 3 é a empresa
					if($current_role->id != 3){
            ?>
            <?php
            // se easylife mostra o aplicativo do Nutri
            if($current_app->id == 4 || $current_app->id == 7) {
              ?>
              <li><a href="/admin/aplicativos/session/3"><i class="fa fa-leaf"></i> Easynutri</a></li>
            <?php 
            }
            ?>
            <?php
					foreach($data['modulos'] as $modulo){ 
						if($modulo->rota == 'tags') {
							continue;
						}
						if($modulo->rota == 'certificados') {
							continue;
						}
                        if(
							$modulo->rota == 'topicos' && $modulo->id == 23
							|| $modulo->rota == 'aplicativos'
							|| $modulo->rota == 'conteudos'
							|| $modulo->rota == 'pessoas'
							// id 1 = easymind
							|| ($modulo->rota == 'frases' && $current_app->id != 1)
							// id 2 = easyfit sports
							|| ($modulo->rota == 'dicas' && ($current_app->id != 2 && $current_app->id != 4 && $current_app->id != 7 ))
							|| ($modulo->rota == 'pdfs' && ($current_app->id != 2 && $current_app->id != 4))
							// id 3 = easyfit nutri
							|| ($modulo->rota == 'alimentos' && $current_app->id != 3)
							|| ($modulo->rota == 'exercicios' && $current_app->id != 3)
							|| ($modulo->rota == 'categorias' && $current_app->id == 3)
							|| ($modulo->rota == 'categorias' && $current_app->id == 5)
							|| ($modulo->rota == 'area' && $current_app->id != 5)
							|| ($modulo->rota == 'medicos' && $current_app->id != 5)
							|| ($modulo->rota == 'noticias' && $current_app->id != 6)
							|| ($modulo->rota == 'tags' && $current_app->id == 3)
							) continue;

							if($modulo->rota == 'codes') {
								$icone = 'fa-code';
							}else if ($modulo->rota == 'conteudos') {
								$icone = 'fa-square';
							}else if ($modulo->rota == 'frases') {
								$icone = 'fa-quote-right';
							}else if ($modulo->rota == 'informacoes') {
								$icone = 'fa-exclamation';
							}else if ($modulo->rota == 'notifications') {
								$icone = 'fa-bell';
							}else if ($modulo->rota == 'categorias') {
								$icone = 'fa-book';
							}else if ($modulo->rota == 'tags') {
								$icone = 'fa-tags';
							}else if ($modulo->rota == 'contato') {
								$icone = 'fa-envelope';
							}else if ($modulo->rota == 'dicas') {
								$icone = 'fa-thumb-tack';
							}else if ($modulo->rota == 'alimentos') {
								$icone = 'fa-cutlery';
							}else if ($modulo->rota == 'aplicativos') {
								$icone = 'fa-mobile';
							}else if ($modulo->rota == 'empresas') {
								$icone = 'fa-briefcase';
							}else if ($modulo->rota == 'pdfs') {
								$icone = 'fa-file';
							}else if ($modulo->rota == 'area') {
								$icone = 'fa-book';
							} else if ($modulo->rota == 'medicos') {
								$icone = 'fa-user';
							} else if ($modulo->rota == 'noticias') {
								$icone = 'fa-list';
							}
							 else {
								$icone = 'fa-circle-o';
							}
                        ?>
						<?php if($current_role->hasAccess($modulo->nome_tabela.'.view')){ ?>
							<li  class="treeview"><a class="except" href="{{ url('admin/'.$modulo->rota) }}"><i class="fa <?=$icone?>"></i><?php echo $modulo->label; ?></a></li>
						<?php } ?>
					<?php } ?>
					<?php if($current_user->inRole('admins') ){ ?>
					<li><a href="{{ url('admin/users') }}"><i class="fa fa-users"></i> Usuários</a></li>
					<?php
					}
				}
					?>

			</li>


			<?php if($current_user->id == 1){ ?>
				<li class="treeview">
					<a href="#">
						<i class="fa fa-cog"></i>
						<span>Configurações</span>
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<?php if($current_user->inRole('admins') ){ ?>
						<li><a href="{{ url('admin/informacoes-basicas') }}"><i class="fa fa-circle-o"></i> Informações Básicas Website</a></li>
							<li><a href="{{ url('admin/gerador') }}"><i class="fa fa-circle-o"></i> Gerador de Módulos</a></li>
							<li><a href="{{ url('admin/tipo-modulo') }}"><i class="fa fa-circle-o"></i> Tipos de Módulos</a></li>
							<li><a href="{{ url('admin/roles') }}"><i class="fa fa-circle-o"></i> Roles</a></li>
						<?php } ?>

					</ul>
				</li>
			<?php } ?>

		</ul>
	</section>
	<!-- /.sidebar -->
</aside>
