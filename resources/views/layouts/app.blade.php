<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Easylife Empresarial</title>

    <base href="{{ url('') }}/"/>

    <!-- Fonts -->
    <link rel="stylesheet" href="{{ url('fonts/admin/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('fonts/admin/font_lato.css') }}">

    <link rel="manifest" href="/manifest.json">

    <!-- Styles -->


    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    <!-- Taggle JS -->
    <link rel="stylesheet" href="{{ url('plugins/taggle/example/css/taggle.css') }}">
	 <!-- Select2 -->
    <link rel="stylesheet" href="{{ url('css/admin/select2.min.css') }}">
	 <!-- Dropzone Galeria -->
    <link rel="stylesheet" href="{{ url('css/admin/dropzone-galeria.css') }}">
    <link rel="stylesheet" href="{{ url('css/admin/cropper.min.css') }}">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{ url('css/admin/bootstrap.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ url('fonts/admin/ionicons.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('css/admin/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
    <style>
    :root {
      --duo-primary-color: {{ $current_app->cor }}
    }
    </style>
    <link rel="stylesheet" href="{{ url('css/admin/style.css') }}">
    <link rel="stylesheet" href="{{ url('css/admin/skin_duo.css') }}">

    <script type="text/javascript" src="{{ url('js/admin/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/admin/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ url('js/admin/jquery-3.1.0.min.js') }}"></script>
    <!-- Pickadate.JS -->
    <link rel="stylesheet" href="{{ url('plugins/jquery-ui-1.12.1.custom/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/pickadate/lib/themes/default.css') }}">
    <link rel="stylesheet" href="{{ url('plugins/pickadate/lib/themes/default.date.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css">
    <script src="{{ url('plugins/pickadate/lib/picker.js') }}"></script>
    <script src="{{ url('plugins/pickadate/lib/picker.date.js') }}"></script>

    <!-- Taggles JS -->
    <script src="{{ url('plugins/taggle/src/taggle.js') }}"></script>

    <!-- jQuery 2.2.3 -->
    <script src="{{ url('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
    <script src="{{ url('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <!-- alertUtil.js -->
    <script src="{{ url('js/admin/alertUtil.js') }}"></script>

    <!-- Input-masks -->
    <script src="{{ url('js/admin/jquery.maskedinput.js') }}"></script>
    <script src="{{ url('plugins/input-mask/jquery.inputmask.js') }}"></script>
    <script src="{{ url('plugins/input-mask/jquery.maskMoney.min.js') }}"></script>


  <!--script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async='async'></script-->
  <!--script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
      appId: "3dcd0bd8-7561-4662-a394-3897fa937377",
      autoRegister: true, /* Set to true to automatically prompt visitors */
      httpPermissionRequest: {
        enable: true
      },
      notifyButton: {
          enable: true /* Set to false to hide */
      },
      subdomainName: "adog-duo.os.tc"
    }]);
  </script-->

</head>

<body class="hold-transition skin-blue fixed sidebar-mini">
  <img src="img/25.gif" id="loading">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    @include('layouts.notification')
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  @include('layouts.left-sidebar')
  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  @if(\Session::has('type') && \Session::has('message'))
    <div class="alerta alerta-{{ \Session::get('type') }}" id="alerta">
      <div class="wrap-icone">
        <i class="icone fa fa-exclamation"></i>
      </div>
      <div class="text">
        <span class="titulo">{{ \Session::get('message') }}</span>
        <span></span>
      </div>
      <a href="#" class="fecha-alerta"><i class="fa fa-times"></i></a>
    </div>
  @endif

  @yield('content')

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.1.2
    </div>
    <strong>Copyright &copy;<?php echo date("Y"); ?>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  @include('layouts.control-sidebar')
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- Slugify -->
<script src="{{ url('js/admin/jquery.slugify.js') }}"></script>
<!-- TinyMCE -->
<script src="{{ url('plugins/tinymce/tinymce.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ url('js/admin/bootstrap.min.js') }}"></script>
<!-- DataTables -->
<!-- <script src="{{ url('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('plugins/datatables/buttons.html5.min.js') }}"></script>
<script src="{{ url('plugins/datatables/dataTables.buttons.min.js') }}"></script>
<script src="{{ url('plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
<script>
$(document).ready(function () {
  var table = $('#list-data-table-excel').DataTable({
        "paging": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "order": [[ 0, "desc" ]],
          "buttons": [ {
            "extend" : 'excel',
            "exportOptions": {
                "columns": 'th:not(:last-child)'
            }
          }],
 
         "oLanguage": {
              "sProcessing":   "Processando...",
              "sLengthMenu":   "Mostrar _MENU_ registros",
              "sZeroRecords":  "Não foram encontrados resultados",
              "sInfo":         "Mostrando de _START_ até _END_ de _TOTAL_ registros",
              "sInfoEmpty":    "Mostrando de 0 até 0 de 0 registros",
              "sInfoFiltered": "",
              "sInfoPostFix":  "",
              "sSearch":       "Buscar:",
              "sUrl":          "",
              "oPaginate": {
                  "sFirst":    "Primeiro",
                  "sPrevious": "Anterior",
                  "sNext":     "Seguinte",
                  "sLast":     "Último"
              }
          },
         
     });
   table.buttons().container()
        .appendTo( '#list-data-table-excel_wrapper .col-md-6:eq(0)' );
})
</script>
<style>
.btn.buttons-excel {
  background-color: green;
  color: white;
  font-weight:bold;
  min-width: 150px
}
</style>
<!-- SlimScroll -->
<script src="{{ url('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ url('plugins/fastclick/fastclick.js') }}"></script>
<!-- Select2 -->
<script src="{{ url('js/admin/select2.full.min.js') }}"></script>

<!-- AdminLTE App -->
<script src="{{ url('js/admin/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('js/admin/demo.js') }}"></script>
<!-- iCheck -->
<script src="{{ url('plugins/iCheck/icheck.min.js') }}"></script>
<!-- Custom scripts -->
<script src="{{ url('js/admin/scripts.js') }}"></script>
</body>
</html>
