<!DOCTYPE html>
<html lang="pt">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Easylife APPS</title>
  <style>
    *,
    *::before,
    *::after {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }
    body,html {
      width: 100%;
      height: 100%;
    }
    .apps {
      display: flex;
      flex-wrap: wrap;
      height: 100%;
    }

    .apps a {
      flex: 0 0 50%;
      width: 50%;
      height: 50%;
      position: relative;
      display: flex;
      align-items: center;
    }
    .apps a:hover {
      opacity: .8;
    }

    .apps a::after {
      content: '';
      display: block;
      height: 100%;
      width: 100%;
      position: absolute;
      left: 0;
      bottom: 0;
      transition: all .3s ease;
      background-image: linear-gradient(transparent, rgba(0,0,0,0.3))
    }
    .apps a:hover::after {
      /* opacity: 0.4; */
    }

    /* .apps a:nth-child(3n) {
      width: 100%;
      flex: 0 0 100%;
    } */

    .apps a img {
      filter: brightness(0) invert(1);
      width: 50%;
      max-width: 280px;
      margin: auto;
      transition: all .3s ease;
    }

    .apps a:hover img{
      /* transform: scale(1.1) */
    }
    .apps a .entrar {
        color: white;
        font-family: 'Roboto', sans-serif;
        text-decoration: none;
        border: 1px solid white;
        padding: 10px 20px;
        height: 40px;
        border-radius: 50px;
        position: absolute;
        right: 10px;
        bottom: 15px;
        transition: all .3s ease;
    }
    .apps a:hover .entrar {
        transform: scale(1.1)
    }


    @media(max-width: 768px) {
      .apps a,
      .apps a:nth-child(3n) {
        flex: 0 0 100%;
        width: 100%;
        height: 33vh;
      }
    }
  </style>
</head>

<body>

  <div class="apps">
    @foreach($aplicativos as $app)
    <a style="background-color: <?=$app->cor?>;background-image: url(<?=$app->fundo?>);background-size:cover;background-repeat:no-repeat;background-position:center center" href="/admin/aplicativos/session/{{ $app->id }}">
      <img src="{{ ($app->thumbnail_principal) }}" alt="">
      <div class="entrar">Entrar</div>
    </a>
    @endforeach
  </div>

</body>

</html>