@extends('layouts.app')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			<?php echo (isset($user)) ? 'Editar' : 'Criar'; ?>
			<small>Informações Usuário</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ url('/admin') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li><a href="{{ url('/admin/users') }}">Usuários</a></li>
			<li class="active">
				<?php echo (isset($user)) ? 'Editar' : 'Criar'; ?>
			</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<div class="box">
					<div class="box-header">

					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<ul class="nav nav-pills nav-justified">
							<li class="active"><a data-toggle="pill" href="#info-tab">Informações</a></li>
							<li class=""><a data-toggle="pill" href="#hist-tab">Histórico</a></li>
							<li class=""><a data-toggle="pill" href="#fav-tab">Favoritos</a></li>
						</ul>
						<div class="spacer"></div>
						<form id="mainForm" class="form-horizontal" role="form" method="POST" action="{{ url('/admin/users/save') }}">
							<div class="tab-content">

								<div id="info-tab" class="tab-pane fade in active">
									{{ csrf_field() }}

									<?php if (isset($user)) {
    ?>
									<input type="hidden" name="id" value="<?php echo $user->id; ?>" />
									<?php
} ?>

									<div class="form-group">
										<label  for="first_name" class="col-md-3 control-label">Nome *</label>

										<div class="col-md-7">
											<input required id="first_name" type="text" class="form-control" value="<?php echo (isset($user)) ? $user->first_name : ''; ?>"
											    name="first_name" />
										</div>
									</div>

									<div class="form-group">
										<label for="email" class="col-md-3 control-label">E-mail *</label>

										<div class="col-md-7">
											<input required id="email" type="text" class="form-control" value="<?php echo (isset($user)) ? $user->email : ''; ?>"
											    name="email" />
										</div>
									</div>

									<div class="form-group">
										<label for="password" class="col-md-3 control-label">Senha</label>

										<div class="col-md-7">
											<input id="password" type="password" class="form-control" value="" name="password" />
										</div>
									</div>
									<?php if (!Sentinel::getUser()->inRole('admins')) {
        ?>
									<input type="hidden" name="id_role" value="<?php echo (isset($user)) ? $user->roleUser->role->id : $userLogado->roleUser->role->id; ?>">
									<?php
    } else {
        ?>
									<div class="form-group">
										<label for="id_role" class="col-md-3 control-label">Role *</label>
										<div class="col-md-7">
											<select required id="id_role" class="form-control" name="id_role">
												<?php foreach ($roles as $role):
												// if empresa
													if($role->id == 3) {
														continue;
													}
													?>
												<?php if (isset($user) && isset($user->roleUser) && $user->roleUser->role->id == $role->id) {
													$selected = 'selected';
												} else {
													$selected = '';
												} ?>
												<option <?php echo $selected; ?> value="<?php echo $role->id; ?>">
													<?php echo $role->name; ?>
												</option>
												<?php endforeach ?>
											</select>
										</div>
									</div>
									<?php
	} ?>
	
	<div class="form-group">
										<label for="telefone" class="col-md-3 control-label">Telefone</label>

										<div class="col-md-7">
											<input id="telefone" type="text" class="form-control" value="<?php echo (isset($user)) ? $user->telefone : ''; ?>"
												name="telefone" />
												<script>
												$('#telefone').mask('00 000000000')
												</script>
										</div>
									</div>

									<div class="form-group">
										<label for="genero" class="col-md-3 control-label">Genero</label>

										<div class="col-md-7">

											<select name="genero" id="" class="form-control">
												<option <?php echo (isset($user)) && $user->genero == 'masculino' ? 'selected' : ''; ?> value="masculino">Masculino</option>
												<option <?php echo (isset($user)) && $user->genero == 'feminino' ? 'selected' : ''; ?> value="feminino">Feminino</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="data_nascimento" class="col-md-3 control-label">Data de Nascimento</label>

										<div class="col-md-7">
											<input id="data_nascimento" type="date" class="form-control" value="<?php echo (isset($user)) ? $user->data_nascimento : ''; ?>"
											    name="data_nascimento" />
										</div>
									</div>
									<div class="form-group">
										<label for="altura" class="col-md-3 control-label">Altura</label>

										<div class="col-md-7">
											<input id="altura" type="number" class="form-control" value="<?php echo (isset($user)) ? $user->altura : ''; ?>"
											    name="altura" />
										</div>
									</div>
									<div class="form-group">
										<label for="objetivo_descricao" class="col-md-3 control-label">Objetivo</label>

										<div class="col-md-7">
										<select name="objetivo_descricao" id="" class="form-control">
												<option <?php echo (isset($user)) && $user->objetivo_descricao == 'Manter Peso' ? 'selected' : ''; ?> value="Manter Peso">Manter Peso</option>
												<option <?php echo (isset($user)) && $user->objetivo_descricao == 'Perder Peso' ? 'selected' : ''; ?> value="Perder Peso">Perder Peso</option>
												<option <?php echo (isset($user)) && $user->objetivo_descricao == 'Ganhar Peso' ? 'selected' : ''; ?> value="Ganhar Peso">Ganhar Peso</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="inicio_kg" class="col-md-3 control-label">Peso Inicial</label>

										<div class="col-md-7">
											<input id="inicio_kg" type="number" class="form-control" value="<?php echo (isset($user)) ? $user->inicio_kg : ''; ?>"
											    name="inicio_kg" />
										</div>
									</div>
									<div class="form-group">
										<label for="atual_kg" class="col-md-3 control-label">Peso Atual</label>

										<div class="col-md-7">
											<input id="atual_kg" type="number" class="form-control" value="<?php echo (isset($user)) ? $user->atual_kg : ''; ?>"
											    name="atual_kg" />
										</div>
									</div>
									<div class="form-group">
										<label for="objetivo_kg" class="col-md-3 control-label">Objetivo de Peso</label>

										<div class="col-md-7">
											<input id="objetivo_kg" type="number" class="form-control" value="<?php echo (isset($user)) ? $user->objetivo_kg : ''; ?>"
											    name="objetivo_kg" />
										</div>
									</div>
									<div class="form-group">
										<label for="objetivo_kcal" class="col-md-3 control-label">Objetivo de consumo (kcal)</label>

										<div class="col-md-7">
											<input id="objetivo_kcal" type="number" class="form-control" value="<?php echo (isset($user)) ? $user->objetivo_kcal : ''; ?>"
											    name="objetivo_kcal" />
										</div>
									</div>

								</div>

								<div id="hist-tab" class="tab-pane">
									<ul class="list-group" >
									<?php
                                    if (isset($user)) {
                                        foreach ($user->conteudos as $key => $c) {
                                            ?>

												
											<a class="list-group-item" href="/admin/conteudos/edit/<?=$c->id?>?tipo=<?=$c->tipo?>&topico_id=<?=$c->topico_id?>">
											<?=(new DateTime($c->pivot->watched_at))->format('d/m/Y H:i:s')?>	- 
											<?=$c->titulo_completo?> (<?=$c->tipo?>)
												
											</a>
												<?php
										}
										if(!count($user->conteudos)){
											echo '<li class="list-group-item">Esse usuário não contém nenhum histórico</li>';
										}
                                    }
                                    ?>
									</ul>
								</div><!-- tab-->
								<div id="fav-tab" class="tab-pane">
								<ul class="list-group" >
									<?php
                                    if (isset($user)) {
										$count_fav = 0;
                                        foreach ($user->conteudos as $key => $c) {
											if($c->pivot->favorited_at === null) continue;
											$count_fav++;
                                            ?>	
											<a class="list-group-item" href="/admin/conteudos/edit/<?=$c->id?>?tipo=<?=$c->tipo?>&topico_id=<?=$c->topico_id?>">
											<?=(new DateTime($c->pivot->favorited_at))->format('d/m/Y H:i:s')?>	- 
											<?=$c->titulo_completo?> (<?=$c->tipo?>)
												
											</a>
											<?php
										}
										if(!($count_fav)){
											echo '<li class="list-group-item">Nenhum vídeo marcado como favorito</li>';
										}
                                    }
                                    ?>
									</ul>
								</div>
							</div>



						</form>

					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="text-center">
						<button type="submit" class="btn btn-primary">
							<i class="fa fa-btn fa-pencil"></i> Salvar
						</button>
					</div>
				</div>
			</div>
			<!-- /.box -->
		</div>
</div>
</section>
</div>

@endsection