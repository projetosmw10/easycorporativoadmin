@extends($current_template)


@section('content')

<section class="content-wrapper ">

  <br><br>
  <h3 class="text-center">Números de todos os aplicativos</h3>
  <div class="cards">
  <?php
  ;
  foreach (DB::table('sis_modulos')->orderBy('rota', 'asc')->get() as $key => $modulo) {
    if($modulo->rota == 'topicos'
      || $modulo->rota == 'conheca'
      || $modulo->rota == 'informacoes'
    ) {
      continue;
    }
    
    if($modulo->rota == 'codes') {
        $icone = 'fa-code';
    }else if ($modulo->rota == 'conteudos') {
        $icone = 'fa-square';
    }else if ($modulo->rota == 'frases') {
        $icone = 'fa-quote-right';
    }else if ($modulo->rota == 'informacoes') {
        $icone = 'fa-exclamation';
    }else if ($modulo->rota == 'notifications') {
        $icone = 'fa-bell';
    }else if ($modulo->rota == 'categorias') {
        $icone = 'fa-book';
    }else if ($modulo->rota == 'tags') {
        $icone = 'fa-tags';
    }else if ($modulo->rota == 'contato') {
        $icone = 'fa-envelope';
    }else if ($modulo->rota == 'dicas') {
        $icone = 'fa-thumb-tack';
    }else if ($modulo->rota == 'alimentos') {
        $icone = 'fa-cutlery';
    }else if ($modulo->rota == 'aplicativos') {
        $icone = 'fa-mobile';
    }else if ($modulo->rota == 'empresas') {
        $icone = 'fa-briefcase';
    } else {
        $icone = 'fa-circle-o';
    }
  ?>
    <div class="card">
      <div class="icon">
        <i class="fa <?=$icone?>"></i>
      </div>
      <div class="numero">
      <?php
      $var = '\App\Modules\\'.$modulo->nome.'\Models\\'.$modulo->nome;
      echo $var::count();
      ?>
      </div>
      <div class="oque">
        <?=$modulo->label?>
      </div>
    </div>
    <?php
  }
    ?>

  </div>
</section>


@endsection
