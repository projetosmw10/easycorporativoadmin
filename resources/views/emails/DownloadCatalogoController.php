<?php

namespace App\Modules\DownloadCatalogo\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\DownloadCatalogo\Models\DownloadCatalogo;
use App\Modules\Catalogos\Models\Catalogos;
use Redirect;

class DownloadCatalogoController extends Controller
{
	private $modulo;
	private $fields;

    public function __construct(){
		$this->modulo = \App\Gerador::find(18);
		$this->fields = \App\CampoModulo::where('id_modulo',18)->get();
	}

	public function index(){
		$data = array();
		return view('download-catalogo',$data);
	}


	public function save(Request $request){

		$post = $request->input();

		DownloadCatalogo::create($post);

		$catalogo = Catalogos::find(1);

		Redirect::away(url($catalogo->arquivo));

//		echo "<script>window.open('".url($catalogo->arquivo)."', '_blank')</script>";

		//return redirect()->back();

	}
}
