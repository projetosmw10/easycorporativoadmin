<?php
function Mask($mask,$val){
    $maskared = '';
      $k = 0;
      for($i = 0; $i<=strlen($mask)-1; $i++)
      {
      if($mask[$i] == '#')
      {
      if(isset($val[$k]))
      $maskared .= $val[$k++];
      }
      else
      {
      if(isset($mask[$i]))
      $maskared .= $mask[$i];
      }
      }
      return $maskared;
 }
?>

<!DOCTYPE html>
<html lang="pt-BR" prefix="og: http://ogp.me/ns#">
<head>
	<title>Easylife APPS</title>
</head>
<body style="width:100%; margin:0; padding:0; font-family:'Calibri', sans-serif; color: #666666; line-height: 30px; font-size: 14px; letter-spacing: -0.3px; text-align:center;">


	<div style="width: 100%; position:relative; display: block; padding:30px; background: #F1F1F1; box-sizing:border-box;">
		<div style="width: 100%; position:relative; display: block; padding:40px 60px 60px 60px; background: #FFFFFF; border:1px solid #E1E1E1; box-sizing:border-box; border-top:20px solid #ed1c24;">

                <h1 style="color:#000000; font-size:32px; font-style:italic; font-weight:lighter; letter-spacing:-1px; margin:0px;">Easylife APPS<br/><span style="font-weight:bold;"></span>
                </h1>

                <p style="line-height:26px; color:#1A1A1A; font-size:16px;">
                    <b><?php echo $user->first_name?> <?php echo $user->last_name?></b>! <br>
                    <?php echo trans('default.cadastro_realizado')?><br>
                    <?php echo url('/')?><br>
                    <b><?php echo trans('default.cnpj')?></b>: <?php echo Mask("##.###.###/####-##",$user->cnpj) ?><br>
                    <b><?php echo trans('default.inscricao_estadual')?></b>: <?php echo $user->inscricao_estadual ?><br>
                    <b><?php echo trans('default.telefone')?></b>: <?php echo Mask("(##) #####-####",$user->telefone); ?><br>
                    <b><?php echo trans('default.celular')?></b>: <?php echo Mask("(##) #####-####",$user->celular); ?>
                    
                </p>


		</div>
	</div>

</body>
</html>
