<!DOCTYPE html>
<html lang="en-US" prefix="og: http://ogp.me/ns#">
<head>
	<title>Easylife APPS</title>
</head>
<body style="width:100%; margin:0; padding:0; font-family:'Calibri', sans-serif; color: #666666; line-height: 30px; font-size: 14px; letter-spacing: -0.3px; text-align:center;">

	<div style="width: 100%; position:relative; display: block; padding:30px; background: #F1F1F1; box-sizing:border-box;">
		<div style="width: 100%; position:relative; display: block; padding:40px 60px 60px 60px; background: #FFFFFF; border:1px solid #E1E1E1; box-sizing:border-box; border-top:20px solid #ed1c24;">

            <img src="https://Easylife APPS.com.br/img/logo.png" style="height:60px;" />
            <h1 style="color:#000000; font-size:32px; font-style:italic; font-weight:lighter; letter-spacing:-1px; margin:0px;">Easylife APPS<br/><span style="font-weight:bold;">Cadastro</span></h1>

            <p style="line-height:26px; color:#1A1A1A; font-size:16px;">
                Recebemos um novo pedido de orçamento: <br /><br />

                Produto: <?php echo $orcamento['produto']; ?><br />
                De: <?php echo $orcamento['nome']; ?><br />
                E-mail: <?php echo $orcamento['email']; ?><br />
                Observação: <?php echo $orcamento['observacao'] ? $orcamento['observacao'] : '<em>Nenhuma observação foi feita pelo cliente</em>'; ?><br />
            </p>

		</div>
	</div>

</body>
</html>
