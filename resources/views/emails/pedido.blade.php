<!DOCTYPE html>
<html>
<head>
	<title>Easylife APPS pedido</title>

	<link href="https://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,500,700,900" rel="stylesheet">
</head>
<?php function Mask($mask,$val){
	   $maskared = '';
		 $k = 0;
		 for($i = 0; $i<=strlen($mask)-1; $i++)
		 {
		 if($mask[$i] == '#')
		 {
		 if(isset($val[$k]))
		 $maskared .= $val[$k++];
		 }
		 else
		 {
		 if(isset($mask[$i]))
		 $maskared .= $mask[$i];
		 }
		 }
		 return $maskared;
	}
?>
<body style="width:100%; margin:0; padding:0; font-family:'Calibri', sans-serif; color: #666666; line-height: 30px; font-size: 14px; letter-spacing: -0.3px; text-align:center;">


<div style="width: 100%; position:relative; display: block; padding:30px; background: #F1F1F1; box-sizing:border-box;">
    <div style="width: 100%; position:relative; display: block; padding:40px 60px 60px 60px; background: #FFFFFF; border:1px solid #E1E1E1; box-sizing:border-box; border-top:20px solid #ed1c24;">

            <h1 style="color:#000000; font-size:32px; font-style:italic; font-weight:lighter; letter-spacing:-1px; margin:0px;">Easylife APPS<br/><span style="font-weight:bold;"></span>
            </h1>

	<div style="display: block;width: 100%;">

		<div style="padding: 10px 0;border-bottom: 1px solid #d7d8da">
			<h2 style="display: block;width: 100%;text-align: right;padding: 0;margin: 0;color: #c6c6c6; font-weight: 400;">FATURA</h2>
		</div>

		<div>

			<div style="display: inline-block;float: left;width: 50%;padding: 20px 0;">
				<ul style="list-style: none;margin: 0;padding: 0;">
					<?php echo $pedidos['pedidos']->cliente->first_name; ?><br>
					<?php echo Mask("##.###.###/####-##",$pedidos['pedidos']->cliente->cnpj); ?><br>
					<?php echo Mask("(##) ####-####",$pedidos['pedidos']->cliente->telefone); ?><br>
					<?php echo $pedidos['pedidos']->cliente->email; ?><br>
				</ul>
			</div>

			<div style="display: inline-block;float: right;width: 50%;text-align: right;padding: 15px 0;">
				<ul style="list-style: none;margin: 0;padding: 0;">
					
						<strong>Cadastro: </strong> <?php echo date('d/m/Y H:i', strtotime($pedidos['pedidos']->created_at));?>
					<br>
					
						<strong>Pedido Nº: </strong> <?php echo $pedidos['pedidos']->id; ?>
					<br>
				</ul>
            </div>
            <div style="clear:both;"></div>

			<table style="border-collapse: collapse;width: 100%;border: 1px solid #d7d8da;margin-top: 20px;display: block;">
                <tbody>
                    <tr>
				<td style="text-align: left;">
						<strong>Para</strong><br>
						<?php echo Mask("##.###-###",$pedidos['pedidos']->cep); ?><br>
						<?php echo $pedidos['pedidos']->endereco; ?>, <?php echo $pedidos['pedidos']->numero; ?>, <?php echo $pedidos['pedidos']->complemento; ?><br>
						<?php echo $pedidos['pedidos']->cidade; ?><br>
						<?php echo $pedidos['pedidos']->estado; ?><br>
						<?php echo $pedidos['pedidos']->pais; ?><br>
                </td>

				<td style="text-align: left;">
						<strong>Transportadora</strong><br>
						<?php echo Mask("##.###.###/####-##",$pedidos['pedidos']->transportadora_cnpj); ?><br>
						<?php echo $pedidos['pedidos']->transportadora_razao_social; ?><br>
						<?php echo Mask("(##) ####-####",$pedidos['pedidos']->transportadora_telefone); ?><br>
						<?php echo $pedidos['pedidos']->transportadora_contato; ?><br>

                </td>
                <?php
                if($pedidos['pedidos']->redespacho){
                ?>
				<td style="text-align: left;">
						<strong>Transportadora redespacho</strong><br>
						<?php echo Mask("##.###.###/####-##",$pedidos['pedidos']->transportadora_redespacho_cnpj); ?><br>
						<?php echo $pedidos['pedidos']->transportadora_redespacho_razao_social; ?><br>
						<?php echo Mask("(##) ####-####",$pedidos['pedidos']->transportadora_redespacho_telefone); ?><br>
						<?php echo $pedidos['pedidos']->transportadora_redespacho_contato; ?><br>

                </td>
                <?php
                }
                ?>
            </td>
        </tr>
            </table>
            <div class="clear:both"></div>
			<table style="border-collapse: collapse;width: 100%;border: 1px solid #d7d8da;margin-top: 20px;display: block;">
				<tr>
					<th style="text-align: left;padding: 15px 10px; background: #e7f0ef;width: 140px;"><strong>Produto</strong></th>
					<th style="text-align: left;padding: 15px 10px; background: #e7f0ef;border-left: 1px solid #d7d8da;width: 420px;"><strong>Modelo</strong></th>
					<th style="text-align: right;padding: 15px 10px; background: #e7f0ef;border-left: 1px solid #d7d8da;width: 140px;"><strong>Quantidade</strong></th>
				</tr>


				<?php foreach ($pedidos['produtos'] as $key => $item){ ?>
			        <tr>
				      <td style="padding: 10px;border-bottom: 1px solid #e7f0ef;"><?php echo $item->produto->nome; ?></td>
				      <td style="padding: 10px;border-left: 1px solid #e7f0ef;border-bottom: 1px solid #e7f0ef;"><?php echo $item->modelo->descricao; ?></td>
				      <td style="padding: 10px;border-left: 1px solid #e7f0ef;text-align: right;border-bottom: 1px solid #e7f0ef;"><?php echo $item->quantidade; ?></td>
				    </tr>
			    <?php } ?>


				<tr>
					<td></td>
					<td style="text-align: right;padding: 10px;"><strong>Total:</strong></td>
					<td style="padding: 10px;border-left: 1px solid #e7f0ef;text-align: right;"><?php echo 'R$ '.number_format($pedidos['pedidos']->valor_total, 2, ',', '.'); ?></td>
				</tr>
			</table>

		</div>

	</div>
	</div>
	</div>
</body>
</html>
